package com.logic.foregroundcamera;

import android.app.Activity;
import android.os.Bundle;

import com.logic.photo.application.PhotoGrabApplication;
import com.logic.photo.filters.MultipleObjectFilter;
import com.logic.photo.models.InputFilterInfo;
import com.logic.photo.views.BaseObjectSelectionView;

/**
 * @author Dewesh Kumar
 */
public class ImageSelectionActivity extends Activity {
    private BaseObjectSelectionView mSelectionView = null;
    private MultipleObjectFilter mMultipleObjectFilter = null;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_selection);
        mSelectionView = (BaseObjectSelectionView)findViewById(R.id.viewObjectSelection);
        mSelectionView.hideAddObjectFilter();
        mSelectionView.hideObjectSelectionFilter();
        mSelectionView.setImagePreviewListener(new BaseObjectSelectionView.ImagePreviewListener() {

            @Override
            public void OnImagePreview() {
                // TODO Auto-generated method stub
                InputFilterInfo currentInputFilterInfo = PhotoGrabApplication.getInstance().getCurrentInputFilterInfo();
                currentInputFilterInfo.getMultipleObjectFilter().updateSelectedMaskMatWithLargestContour();
                //currentInputFilterInfo.setIntermediateMat(currentInputFilterInfo.getMultipleObjectFilter().getProcessedFGMat());
                //Add current selection in list
                PhotoGrabApplication.getInstance().getInputFilterInfoList().add(currentInputFilterInfo);
                setResult(RESULT_OK);
                finish();
            }
        });
    }
}
