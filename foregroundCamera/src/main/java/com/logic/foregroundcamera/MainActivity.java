package com.logic.foregroundcamera;


import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends BaseHomeActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.setGallerySelectionListener(new GallerySelectionListener() {

			@Override
			public void OnGallerySelction() {
				// TODO Auto-generated method stub
				Intent intent = new Intent(MainActivity.this,ForegroundCameraActivity.class);
				startActivity(intent);
			}
		});
        super.onCreate(savedInstanceState);
		//Toast.makeText(this, NDKMethods.getNativeString(), Toast.LENGTH_LONG).show();
       /*mPhotoGalleryView.setGallerySelectionListener(new GallerySelectionListener() {

            @Override
            public void OnGallerySelction() {
                // TODO Auto-generated method stub
                Intent intent = new Intent(MainActivity.this,ForegroundCameraActivity.class);
                startActivity(intent);
            }
        });*/

	}

	@Override
	public void onResume() {
		super.onResume();
		//Toast.makeText(this, NativeMethods.helloWorld(),Toast.LENGTH_SHORT).show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
