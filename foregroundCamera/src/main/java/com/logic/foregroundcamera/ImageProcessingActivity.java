package com.logic.foregroundcamera;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class ImageProcessingActivity extends BaseObjectSelectionActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setImagePreviewListener(new ImagePreviewListener() {
			
			@Override
			public void OnImagePreview() {
				// TODO Auto-generated method stub
				mMultiObjectFilter.updateSelectedMaskMatWithLargestContour();
				Intent intent = new Intent(ImageProcessingActivity.this,ImagePreviewActivityV2.class);
                //Intent intent = new Intent(ImageProcessingActivity.this,ImagePreviewActivity.class);
				startActivity(intent);
			}
		});
		hideAddObjectFilter();
		hideColorFilter();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.image_processing, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
