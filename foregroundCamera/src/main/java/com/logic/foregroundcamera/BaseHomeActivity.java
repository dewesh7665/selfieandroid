package com.logic.foregroundcamera;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.logic.photo.application.PhotoGrabApplication;
import com.logic.photo.models.InputFilterInfo;
import com.logic.ui.views.PhotoGalleryView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

public class BaseHomeActivity extends Activity {

	//private Button mCameraButton;
	//private Button mGalleryViewButton;
	private String mSelectedImagePath = "";
	private PhotoGrabApplication mApplication = null;
	
	private LinearLayout mParentGallery;
	protected PhotoGalleryView mPhotoGalleryView;
	//set interface 
	public static interface GallerySelectionListener{
		public void OnGallerySelction();
	}
	private GallerySelectionListener mGallerySelectionListener;
	
	public void setGallerySelectionListener(GallerySelectionListener listener){
		this.mGallerySelectionListener = listener;
	}
	
	
	private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    System.loadLibrary("stickerapp");
                	//Toast.makeText(MainActivity.this, "OpenCV loader", Toast.LENGTH_SHORT).show();
                    //Log.i(TAG, "OpenCV loaded successfully");
                    //mOpenCvCameraView.enableView();
                    //mOpenCvCameraView.setOnTouchListener(ColorBlobDetectionActivity.this);
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.logic.photolibrary.R.layout.activity_base_home);
        //mCameraButton = (Button)findViewById(R.id.btnCamera);
        //mGalleryViewButton = (Button)findViewById(R.id.btnGallery);
        mApplication = (PhotoGrabApplication)this.getApplicationContext();
        findViewById(com.logic.photolibrary.R.id.abh_cusBtn_camera).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Toast.makeText(MainActivity.this,"Open Camera Intent here", Toast.LENGTH_LONG).show();
			}
		});
        
        findViewById(com.logic.photolibrary.R.id.abh_cusBtn_gallary).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(Intent.ACTION_PICK,
		                   android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI);
		        final int ACTIVITY_SELECT_IMAGE = 1234;
		        startActivityForResult(i, ACTIVITY_SELECT_IMAGE); 
				//Toast.makeText(MainActivity.this,"Open Gallery View here", Toast.LENGTH_LONG).show();
			}
		});
        
        mPhotoGalleryView = new PhotoGalleryView(this);

        mPhotoGalleryView.setGallerySelectionListener(mGallerySelectionListener);
        mPhotoGalleryView.requestPhotoGalleryView();
        mParentGallery = (LinearLayout) findViewById(com.logic.photolibrary.R.id.llParentGallery);
        mParentGallery.addView(mPhotoGalleryView.getListView());
        
    }
    
    @Override
    public void onResume()
    {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) //Will be null if nothing selected and back pressed
            return;
        switch (requestCode) {
        case 1234:
        	Uri selectedImageUri = data.getData();
        	mSelectedImagePath = getPath(selectedImageUri);
        	//mApplication.setImagePath(mSelectedImagePath);

            InputFilterInfo inputFilterInfo = new InputFilterInfo(mSelectedImagePath);
            //mApplication.getInputFilterInfoList().add(inputFilterInfo);
            mApplication.setCurrentInputFilterInfo(inputFilterInfo);

        	if(this.mGallerySelectionListener != null)
        		mGallerySelectionListener.OnGallerySelction();
        	//Toast.makeText(this, mSelectedImagePath, Toast.LENGTH_SHORT).show();
        	//Intent intent = new Intent(MainActivity.this,ImageProcessingActivity.class);
			//startActivity(intent);
        	break;
        	
        default:
            break;
        }
    }

    private String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(com.logic.photolibrary.R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == com.logic.photolibrary.R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
