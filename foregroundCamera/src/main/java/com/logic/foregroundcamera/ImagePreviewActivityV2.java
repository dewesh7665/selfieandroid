package com.logic.foregroundcamera;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.logic.business.image.chooser.ActivityResultManager;
import com.logic.business.image.chooser.ImageChooser;
import com.logic.business.managers.BitmapManager;
import com.logic.photo.application.PhotoGrabApplication;
import com.logic.photo.filters.MultipleObjectFilter;
import com.logic.photo.gestures.MultiTouchListener;
import com.logic.photo.models.InputFilterInfo;
import com.logic.photo.ui.views.CustomButtonView;
import com.logic.ui.TextFilter.ZoomPanEditTextView;
import com.logic.ui.UIConstants;
import com.logic.ui.imagesticker.ImageStickerView;
import com.logic.ui.imagesticker.StickerToolView;
import com.logic.ui.toolbox.AddTextToolView;
import com.logic.ui.toolbox.BackgroundToolBox;
import com.logic.ui.toolbox.ForegroundToolBox;
import com.logic.ui.toolbox.GPUFilterToolView;
import com.logic.ui.utils.FontUtil;
import com.logic.ui.utils.UIUtil;
import com.logic.ui.views.InputStickerView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.core.Mat;

import java.io.File;

import jp.co.cyberagent.android.gpuimage.GPUImageView;

public class ImagePreviewActivityV2 extends Activity {
    public static enum EditType {IMAGE, BACKGROUND};
    public static final int ADD_PIC_REQUEST_CODE = 3001;
    private PhotoGrabApplication mApplication;
    private GPUImageView mGPUImageView;
    private CustomButtonView mBtnEditBG = null;
    private CustomButtonView mBtnEditFG = null;
    private BitmapManager mBitmapManager;

    private LinearLayout mLLSave;

    private ForegroundToolBox mForegroundToolBox;
    private BackgroundToolBox mBackgorundToolBox;

    private GPUFilterToolView mGPUFilterToolView;
    private AddTextToolView mAddTextToolView;
    private Boolean isGPUContainerToBeShown = false; // Will be true only if Filter selected from EditBackground

    private RelativeLayout rlPictureBoard;
    private LayoutInflater mInflater;
    private ImageChooser mImageChooser;
    private RelativeLayout rlContainerInputImage;

    //private InputImageView mActiveRenderView; // TO be used for switching filters
    private InputStickerView mActiveRenderView; // TO be used for switching filters

    //To be used
    private int mInputImageCount = 0;
    private EditType mEditType = EditType.IMAGE; //Default configuration
    private StickerToolView mStickerToolView;
    private ImageStickerView mActiveImageStickerView;
    private View vgStickerMenu;

    private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    //Toast.makeText(MainActivity.this, "OpenCV loader", Toast.LENGTH_SHORT).show();
                    //Log.i(TAG, "OpenCV loaded successfully");
                    //mOpenCvCameraView.enableView();
                    //mOpenCvCameraView.setOnTouchListener(ColorBlobDetectionActivity.this);
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview_v2);
        //Below 2 lines for using uinversal image downloader
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);

        mApplication = (PhotoGrabApplication)getApplicationContext();
        mImageChooser = new ImageChooser(this);

        rlPictureBoard = (RelativeLayout) findViewById(R.id.rlPictureBoard);
        rlContainerInputImage = (RelativeLayout) findViewById(R.id.rlContainerInputImage);
        mInflater = LayoutInflater.from(this);

        mBitmapManager = new BitmapManager(this);
        mForegroundToolBox = (ForegroundToolBox) findViewById(R.id.fgToolsView);
        mBackgorundToolBox = (BackgroundToolBox) findViewById(R.id.bgToolsView);

        mGPUImageView = (GPUImageView) findViewById(R.id.gpuImageView);

        mLLSave = (LinearLayout) findViewById(R.id.llSave);
        mBtnEditBG = (CustomButtonView)findViewById(R.id.btnAddBg);
        mBtnEditFG = (CustomButtonView)findViewById(R.id.btnEditFG);
        setClickListeners();


        //mBackgorundToolBox.setBGRenderingIds((ViewGroup)findViewById(R.id.rlContainerBG),
        //(ImageView)findViewById(R.id.ivBgImage));
        mBackgorundToolBox.setBGRenderingIds((ViewGroup)findViewById(R.id.rlContainerBG));
        mBackgorundToolBox.setToolsSelectionListener(onToolsSelectionListener);

        mGPUFilterToolView = (GPUFilterToolView) findViewById(R.id.gpuToolsView);
        mGPUFilterToolView.setGPUImageView((GPUImageView)findViewById(R.id.gpuImageView));

        mAddTextToolView = (AddTextToolView) findViewById(R.id.addTextToolView);

        mInputImageCount = mApplication.getInputFilterInfoList().size();
        InputStickerView renderView = addNewInputImage(mApplication.getInputFilterInfoList().get(0));
        renderView.setTag(0); //Index of list position
        activateInputImage(renderView);

        prepareAddTextOption();
        prepareFilterOption();
        prepareAddPictureOption();
        prepareImageStickerOption();
    }

    @Override
    protected void onResume() {
        super.onResume();
        int currentInputImageSize = mApplication.getInputFilterInfoList().size();
        if (currentInputImageSize > mInputImageCount) { //New image added
            InputStickerView renderView =  addNewInputImage(mApplication.getInputFilterInfoList().get(currentInputImageSize - 1));
            renderView.setTag(currentInputImageSize - 1); //Index of list position
            activateInputImage(renderView);
            mInputImageCount = currentInputImageSize;
        }
    }

    public void setClickListeners(){
        mBtnEditBG.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetPreviewOnEditType(EditType.BACKGROUND);
                /*mBtnEditBG.updateSelection(true);
                mBtnEditFG.updateSelection(false);
                onAddBackgroundSelection();*/
            }
        });

        mBtnEditFG.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                resetPreviewOnEditType(EditType.IMAGE);
            }
        });

        mLLSave.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mActiveRenderView.setActiveState(false);
                mBitmapManager.requestBitmapFromView(rlPictureBoard, new BitmapManager.OnBitmapSavedListener() {
                    @Override
                    public void onBitmapSaved(String path) {
                        galleryAddPic(path);
                        Toast.makeText(ImagePreviewActivityV2.this, UIConstants.SUCCESS_MSG_DEFAULT, Toast.LENGTH_SHORT).show();
                        goToHomeClearingAllBackStack();
                    }

                    @Override
                    public void onError(String msg) {
                        Toast.makeText(ImagePreviewActivityV2.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });
                /*String filePath =  mBitmapManager.saveBitmap(mProcessedBitmap);
                if (filePath != null) {
                    galleryAddPic(filePath);
                    Toast.makeText(ImagePreviewActivityV2.this, UIConstants.SUCCESS_MSG_DEFAULT, Toast.LENGTH_SHORT).show();
                    goToHomeClearingAllBackStack();
                }
                else {
                    Toast.makeText(ImagePreviewActivityV2.this, UIConstants.BMP_ERROR, Toast.LENGTH_SHORT).show();
                }*/
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.image_preview, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void goToHomeClearingAllBackStack() {
        PhotoGrabApplication.getInstance().resetAppState();
        Intent homeIntent = new Intent(this, MainActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(homeIntent);
    }

    public void galleryAddPic(String photoPath) {
        Intent mediaScanIntent = new Intent(
                "android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(photoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);
    }

    private ProgressDialog mProgressDialog;
    private void showProgressDialog(String text) {
        try {
            if(mProgressDialog == null){
                mProgressDialog = ProgressDialog.show(this, "Alert!",text +"\t\t\t\t\t", true, false);
                mProgressDialog.setCancelable(true);
            }
            else{
                if(!mProgressDialog.isShowing()){
                    mProgressDialog.show();
                }
            }
        } catch (Exception ex) {
            mProgressDialog = null;
        }
    }

    private void hideProgressDialog() {
        try{
            if(mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismiss();
        }
        catch (IllegalArgumentException e) {
            mProgressDialog = null;
            //Never mind
            //Will throw if progress dialog has not attached to windows.If not attached then no need to hide
        }
    }

    @Override
    protected void onActivityResult(int arg0, int arg1, Intent arg2) {
        super.onActivityResult(arg0, arg1, arg2);
        if (arg0 == ADD_PIC_REQUEST_CODE) {
            //Toast.makeText(this, "Image Added", Toast.LENGTH_SHORT).show();
            //Do nothing here. Logic are in onResume of activity
        }
        else {
            ActivityResultManager.getInstance().updateActivityResultListeners(
                    arg0, arg1, arg2);
        }
    }




    BackgroundToolBox.OnToolsSelectionListener onToolsSelectionListener = new BackgroundToolBox.OnToolsSelectionListener() {
        @Override
        public void onToolSelected(int toolIndex) {
            switch (toolIndex) {
                case 0: //Bg
                    isGPUContainerToBeShown = false;
                    updateContainerVisibility(isGPUContainerToBeShown);
                    mGPUFilterToolView.setVisibility(View.GONE);
                    break;
                case 1: //Filters
                    //isGPUContainerToBeShown = true;
                    //updateContainerVisibility(isGPUContainerToBeShown);
                    mGPUImageView.setVisibility(View.VISIBLE);
                    mGPUFilterToolView.initFilterOptions(mActiveRenderView.getInputFilterInfo().getMultipleObjectFilter().getProcessedFGMat(),
                            new GPUFilterToolView.OnFilterApplied() {
                                @Override
                                public void onFilterApplied(Mat mat, Bitmap bitmap) {
                                    mActiveRenderView.setBitmapFromMat(mat, true);
                                }
                            });
                    mGPUFilterToolView.setVisibility(View.VISIBLE);
                    break;
                case 2: //Text
                    isGPUContainerToBeShown = false;
                    updateContainerVisibility(isGPUContainerToBeShown);
                    mGPUFilterToolView.setVisibility(View.GONE);
                    mForegroundToolBox.resetVisibility();
                    mBackgorundToolBox.resetVisibility();
                    mAddTextToolView.setVisibility(View.VISIBLE);
                    addNewText();
                    break;
                case 3: //Background Filters
                    //isGPUContainerToBeShown = true;
                    //updateContainerVisibility(isGPUContainerToBeShown);
                    mGPUImageView.setVisibility(View.VISIBLE);
                    mGPUFilterToolView.initFilterOptions(mBackgorundToolBox.getBGImageView().getOriginalMat(),
                            new GPUFilterToolView.OnFilterApplied() {
                                @Override
                                public void onFilterApplied(Mat mat, Bitmap bitmap) {
                                    mBackgorundToolBox.getBGImageView().setResetBitmap(bitmap);
                                }
                            });
                    mGPUFilterToolView.setVisibility(View.VISIBLE);
                    break;
            }
        }
    };

    private void addNewText() {
        /*View view = mInflater.inflate(R.layout.container_zoom_pan_edit_text, rlPictureBoard, false);
        ZoomPanEditText zoomPanEditText = (ZoomPanEditText)view.findViewById(R.id.zoomPanEditText);
        zoomPanEditText.setOnSingleTapConfirmedListener(new ZoomPanEditText.OnSingleTapConfirmedListener() {
            @Override
            public void onSingleTapConfirmed(View view) {
                mAddTextToolView.setZoomPanEditText((ZoomPanEditText)view);
            }
        });
        mAddTextToolView.setZoomPanEditText(zoomPanEditText);
        rlPictureBoard.addView(view);*/


        ZoomPanEditTextView zoomPanEditTextView = new ZoomPanEditTextView(this);
        zoomPanEditTextView.setOnSingleTapConfirmedListener(new ZoomPanEditTextView.OnSingleTapConfirmedListener() {
            @Override
            public void onSingleTapConfirmed(View view) {
                mAddTextToolView.setZoomPanEditText((ZoomPanEditTextView)view);
            }
        });
        mAddTextToolView.setZoomPanEditText(zoomPanEditTextView);
        rlPictureBoard.addView(zoomPanEditTextView);

        //view.bringToFront();
    }

    private void prepareAddTextOption() {
        TextView tvAddText = (TextView)  findViewById(R.id.tvAddText);
        FontUtil.setFontToTextView(this, tvAddText, FontUtil.FontFamily.ROBOTO_SLAB_REGULAR);
        tvAddText.setOnTouchListener(new MultiTouchListener(new MultiTouchListener.OnSingleTapConfirmed() {
            @Override
            public void onSingleTapConfirmed(View view) {
                onToolsSelectionListener.onToolSelected(2);
            }
        }));
    }

    private void prepareAddPictureOption() {
        /*findViewById(R.id.imgBtnAddNewImage).setOnTouchListener(new MultiTouchListener(new MultiTouchListener.OnSingleTapConfirmed() {
            @Override
            public void onSingleTapConfirmed(View view) {
                showChooseOptionPopUp();
                //startActivityForResult(new Intent(ImagePreviewActivityV2.this, ImageSelectionActivity.class), ADD_PIC_REQUEST_CODE);
            }
        }));*/

        findViewById(R.id.llParentAddSticker).setOnTouchListener(new MultiTouchListener(new MultiTouchListener.OnSingleTapConfirmed() {
            @Override
            public void onSingleTapConfirmed(View view) {
                showChooseOptionPopUp();
                //startActivityForResult(new Intent(ImagePreviewActivityV2.this, ImageSelectionActivity.class), ADD_PIC_REQUEST_CODE);
            }
        }));
    }



    private void prepareFilterOption() {
        CustomButtonView btnFilter = (CustomButtonView) findViewById(R.id.btnFilter);
        btnFilter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onToolsSelectionListener.onToolSelected(1);
            }
        });
    }

    private void showChooseOptionPopUp() {
        CharSequence options[] = new CharSequence[] {"Camera", "Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Selection Mode");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // the user clicked on colors[which]
                switch (which) {
                    case 0: //Camera
                        mImageChooser.chooseFromCamera(onImageSelectionListener);
                        break;
                    case 1: //Gallery
                        mImageChooser.chooseFromGallery(onImageSelectionListener);
                        break;

                }
            }
        });
        builder.show();
    }

    ImageChooser.OnImageSelectionListener onImageSelectionListener = new ImageChooser.OnImageSelectionListener() {
        @Override
        public void onImageSelected(String filePath) {
            InputFilterInfo inputFilterInfo = new InputFilterInfo(filePath);
            PhotoGrabApplication.getInstance().setCurrentInputFilterInfo(inputFilterInfo);
            startActivityForResult(new Intent(ImagePreviewActivityV2.this, ImageSelectionActivity.class), ADD_PIC_REQUEST_CODE);
            //mApplication.setSelectedBGPath(filePath);
            //prepareImageWithSelectedBG(filePath);
        }
    };

    private InputStickerView addNewInputImage(InputFilterInfo inputFilterInfo) {

        InputStickerView renderView  = new InputStickerView(this);
        MultipleObjectFilter multipleObjectFilter = inputFilterInfo.getMultipleObjectFilter();
        Mat processedMat = multipleObjectFilter.getTrimmedFGMat();
        inputFilterInfo.setIntermediateMat(processedMat);
        renderView.setInputFilterInfo(inputFilterInfo);
        renderView.setOnSingleTapConfirmedListener(onSingleTapConfirmed);
        renderView.setOnCopyListener(onStickerCopyListener);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

        /*SingleFingerZoomRotateWidget singleFingerZoomRotateWidget  = new SingleFingerZoomRotateWidget(this);
        singleFingerZoomRotateWidget.setView(renderView);
        rlContainerInputImage.addView(singleFingerZoomRotateWidget, layoutParams);
        */
        rlContainerInputImage.addView(renderView, layoutParams);
        return renderView;
    }

    private void activateInputImage(InputStickerView renderView) {
        if (mActiveRenderView != null) {
            mActiveRenderView.setActiveState(false);
        }
        mActiveRenderView = renderView;
        mActiveRenderView.setActiveState(true);
        mForegroundToolBox.setImageRenderView(renderView);
        mForegroundToolBox.refreshViews();
        resetPreviewOnEditType(EditType.IMAGE);
        //mBackgorundToolBox.setImageRenderView(renderView);
    }

    MultiTouchListener.OnSingleTapConfirmed onSingleTapConfirmed = new MultiTouchListener.OnSingleTapConfirmed() {
        @Override
        public void onSingleTapConfirmed(View view) {
            view.bringToFront();
            activateInputImage((InputStickerView) view);
        }
    };

    InputStickerView.OnCopyListener onStickerCopyListener = new InputStickerView.OnCopyListener() {
        @Override
        public void onCopy(InputFilterInfo inputFilterInfo) {
            PhotoGrabApplication.getInstance().getInputFilterInfoList().add(inputFilterInfo);
            int totalStickerCount = mApplication.getInputFilterInfoList().size();
            InputStickerView renderView =  addNewInputImage(inputFilterInfo);
            renderView.setTag(totalStickerCount - 1); //Index of list position
            mInputImageCount = totalStickerCount;
            activateInputImage(renderView);
        }
    };

    private void resetPreviewOnEditType(EditType editType) {
        if (editType == EditType.IMAGE) {
            mBtnEditFG.updateSelection(true);
            mBtnEditBG.updateSelection(false);
            onForegroundSelection();
        }
        else if(editType == EditType.BACKGROUND){
            mBtnEditBG.updateSelection(true);
            mBtnEditFG.updateSelection(false);
            onAddBackgroundSelection();
        }
        else {
            //Intentionally left for more room for options
        }
    }

    private void onAddBackgroundSelection() {
        if (mForegroundToolBox.isImageDirty()) {
            mForegroundToolBox.setImageDirty(false);
        }
        mBackgorundToolBox.setVisibility(View.VISIBLE);
        mBackgorundToolBox.refreshViews();
        mForegroundToolBox.setVisibility(View.GONE);
        mAddTextToolView.setVisibility(View.GONE);
        updateContainerVisibility(isGPUContainerToBeShown);
    }

    private void onForegroundSelection() {
        mBackgorundToolBox.setVisibility(View.GONE);
        mAddTextToolView.setVisibility(View.GONE);
        mForegroundToolBox.setVisibility(View.VISIBLE);
        mForegroundToolBox.refreshViews();
        updateContainerVisibility(false);
    }

    private void updateContainerVisibility(Boolean isGPUContainerToBeShown) {
        if (isGPUContainerToBeShown) {
            rlContainerInputImage.setVisibility(View.GONE);
            mGPUImageView.setVisibility(View.VISIBLE);
        }
        else {
            rlContainerInputImage.setVisibility(View.VISIBLE);
            mGPUImageView.setVisibility(View.GONE);
        }
    }

    private void prepareImageStickerOption() {
        if (vgStickerMenu == null)
            vgStickerMenu = findViewById(R.id.rlBottomToolSubMenu);
        findViewById(R.id.btnSticker).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //vgStickerMenu.setVisibility(vgStickerMenu.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
                UIUtil.showHide(vgStickerMenu);
            }
        });
        mStickerToolView = (StickerToolView) findViewById(R.id.stickerToolView);
        mStickerToolView.setOnStickerSelectedListener(new StickerToolView.OnStickerSelectedListener() {
            @Override
            public void onStickerSelectedListener(View view, StickerToolView.Sticker sticker) {
                //if no sticker added, add one else change drawable
                if (mActiveImageStickerView == null) { //No sticker added till now
                    addNewImageSticker(sticker.getDrawableId());
                }
                else {
                    mActiveImageStickerView.setImageResource(sticker.getDrawableId());
                }

            }
        });
    }

    private void addNewImageSticker(int drawableId) {
        final ImageStickerView imageStickerView = new ImageStickerView(this);
        imageStickerView.setImageResource(drawableId);
        imageStickerView.setOnSingleTapConfirmedListener(new MultiTouchListener.OnSingleTapConfirmed() {
            @Override
            public void onSingleTapConfirmed(View view) {
                mActiveImageStickerView.setActiveState(false);
                mActiveImageStickerView = imageStickerView;
                mActiveImageStickerView.setActiveState(true);
            }
        });

        imageStickerView.setOnPlusOneListener(new ImageStickerView.OnPlusOneListener() {
            @Override
            public void onPlusOne(View view, int drawableId) {
                //Add new sticker with coming drawable id
                addNewImageSticker(drawableId);
            }
        });
        rlPictureBoard.addView(imageStickerView);
        if (mActiveImageStickerView != null)
            mActiveImageStickerView.setActiveState(false);
        mActiveImageStickerView = imageStickerView;
        mActiveImageStickerView.setActiveState(true);
    }

}