package com.logic.foregroundcamera;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PointF;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

import com.logic.photo.application.PhotoGrabApplication;
import com.logic.photo.filters.MultipleObjectFilter;
import com.logic.photo.filters.MultipleObjectFilter.MaskFilterCompleteListener;
import com.logic.photo.gestures.DrawableMultiTouchListener.TouchMode;
import com.logic.photo.views.DrawableImageRenderView.DrawMode;
import com.logic.photo.views.DrawableImageRenderView.OnPointDrawn;
import com.logic.photo.views.DrawableImageRenderView.onDrawContourComplete;
import com.logic.photo.views.MaskDrawableImageRenderView;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;

import java.util.ArrayList;

import afzkl.development.colorpickerview.view.ColorPickerView;
import afzkl.development.colorpickerview.view.ColorPickerView.OnColorChangedListener;

public class BaseObjectSelectionActivity extends Activity {

	private MaskDrawableImageRenderView mRenderView = null;
	
	//Undo,erase buttons
	private Button mBtnUndo = null;
	private Button mBtnErase = null;
	private Button mBtnPaint = null;
	private Button mBtnBg = null;
	private Button mBtnFg = null;
	private Button mBtnNewObject = null;
	private Button mBtnColor = null;
	private LinearLayout mColorViewLayout = null;
	private Button mBtnClose = null;
	private Button mBtnZoomTranslate = null;
	private Button mBtnSave = null;
	//private ObjectColorFilter mObjectFilter = null;
	protected MultipleObjectFilter mMultiObjectFilter = null;
	
	private enum FilterMaskMode{FOREGROUND_MODE,ERASE_MODE,PAINT_MODE,BACKGROUND_MODE,COLOR_MODE};
	private FilterMaskMode mFilterMaskMode;
	private int mPaintRadius = 20;
	private int mEraseRadius = 30;
	
	private PhotoGrabApplication mApplication;
	//Color picker
	private ColorPickerView mColorPickerView;
	
	//set interface 
	public interface ImagePreviewListener{
		public void OnImagePreview();
	}
	
	private ImagePreviewListener mPreviewListener;
	
	public void setImagePreviewListener(ImagePreviewListener listener){
		this.mPreviewListener = listener;
	}
		
	private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                	//Toast.makeText(MainActivity.this, "OpenCV loader", Toast.LENGTH_SHORT).show();
                    //Log.i(TAG, "OpenCV loaded successfully");
                    //mOpenCvCameraView.enableView();
                    //mOpenCvCameraView.setOnTouchListener(ColorBlobDetectionActivity.this);
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*setContentView(com.logic.photolibrary.R.layout.activity_base_image_processing);
		mRenderView =(MaskDrawableImageRenderView)findViewById(com.logic.photolibrary.R.id.imageView);
		mBtnUndo = (Button)findViewById(com.logic.photolibrary.R.id.btnUndo);
		mBtnErase = (Button)findViewById(com.logic.photolibrary.R.id.btnErase);
		mBtnPaint = (Button)findViewById(com.logic.photolibrary.R.id.btnPaint);
		mBtnBg = (Button)findViewById(com.logic.photolibrary.R.id.btnBg);
		mBtnFg = (Button)findViewById(com.logic.photolibrary.R.id.btnFg);
		mBtnNewObject = (Button)findViewById(com.logic.photolibrary.R.id.btnNewObject);
		mBtnColor = (Button)findViewById(com.logic.photolibrary.R.id.btnColor);
		mBtnClose = (Button)findViewById(com.logic.photolibrary.R.id.btnClose);
		mBtnZoomTranslate = (Button)findViewById(com.logic.photolibrary.R.id.btnZoom);
		mBtnSave = (Button)findViewById(com.logic.photolibrary.R.id.btnSave);
		
		mColorPickerView = (ColorPickerView) findViewById(com.logic.photolibrary.R.id.color_picker_view);
		mColorViewLayout = (LinearLayout)findViewById(com.logic.photolibrary.R.id.llColorView);
		
		mApplication = (PhotoGrabApplication)getApplicationContext();
		mRenderView.setBitmapFromPath(mApplication.getImagePath());	
		//First reset object filter.. Since it is in application state
		mApplication.resetObjectFilter();
		mMultiObjectFilter = mApplication.getObjectFilter();
		
		//mColorPickerView.setAlpha(0.7f);
		setFilter();
		setClickListeners();
		setRenderViewListeners();
		setColorChangeListener();
		mColorViewLayout.setVisibility(View.GONE);
		//set initial mode to hair mode
		mRenderView.setOnImageLoadListener(new onImageLoaded() {
			
			@Override
			public void imageLoaded(Bitmap bmp) {
				// TODO Auto-generated method stub
				mMultiObjectFilter.setImageMatFromBitmap(mRenderView.getResizedBitmap());
				mMultiObjectFilter.addNewObject();
			}
		});
		mFilterMaskMode = FilterMaskMode.FOREGROUND_MODE;
		*/
	}

	public void hideAddObjectFilter(){
		mBtnNewObject.setVisibility(View.GONE);
	}
	
	public void showAddObjectFilter(){
		mBtnNewObject.setVisibility(View.VISIBLE);
	}
	public void hideColorFilter(){
		mBtnColor.setVisibility(View.GONE);
	}
	public void showColorFilter(){
		mBtnColor.setVisibility(View.VISIBLE);
	}
	
	private void setFilter(){
		
		mMultiObjectFilter.setImageMaskFilterListener(new MaskFilterCompleteListener() {
			
			@Override
			public void OnMaskFilterComplete(Bitmap bmp) {
				// TODO Auto-generated method stub
				mRenderView.setForegroundMaskImage(bmp,150);
			}
		});
	}
	
	private void setClickListeners(){
		
		mBtnUndo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// TODO Auto-generated method stub
				mMultiObjectFilter.undoFilter();
				
			}
		});
		
		mBtnErase.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFilterMaskMode = FilterMaskMode.ERASE_MODE;
				mRenderView.setTouchMode(TouchMode.DRAW_MODE);
				mRenderView.setEraseAndPaintMode();
			}
		});
		
		mBtnPaint.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFilterMaskMode = FilterMaskMode.PAINT_MODE;
				mRenderView.setTouchMode(TouchMode.DRAW_MODE);
				mRenderView.setEraseAndPaintMode();
			}
		});
		
		
		mBtnBg.setOnClickListener(new OnClickListener() {		
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFilterMaskMode = FilterMaskMode.BACKGROUND_MODE;
				mRenderView.setTouchMode(TouchMode.DRAW_MODE);
				mRenderView.setBackgroundColorMode();
			}
		});
		
		mBtnFg.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFilterMaskMode = FilterMaskMode.FOREGROUND_MODE;
				mRenderView.setTouchMode(TouchMode.DRAW_MODE);
				mRenderView.setDrawCurveType(DrawMode.FREE_CURVE);
				mRenderView.setForegroundColorMode();
			}
		});
		
		mBtnNewObject.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFilterMaskMode = FilterMaskMode.FOREGROUND_MODE;
				mRenderView.setTouchMode(TouchMode.DRAW_MODE);
				mRenderView.setDrawCurveType(DrawMode.FREE_CURVE);
				//Also reset previously drawn contour path
				mRenderView.resetPath();
				
				mMultiObjectFilter.addNewObject();
			}
		});
		
		mBtnColor.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFilterMaskMode = FilterMaskMode.COLOR_MODE;
				mRenderView.setTouchMode(TouchMode.ZOOM_TRANSLATE_MODE);
				mRenderView.resetPath();
				mColorViewLayout.setVisibility(View.VISIBLE);
			}
		});
		
		mBtnClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mColorViewLayout.setVisibility(View.GONE);
			}
		});
		
		mBtnZoomTranslate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mRenderView.setTouchMode(TouchMode.ZOOM_TRANSLATE_MODE);
			}
		});
		
		mBtnSave.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//get both resized bitmap and foreground bitmap and merge them 
				//Intent intent = new Intent(ImageProcessingActivity.this,ImagePreviewActivity.class);
				//startActivity(intent);
				if(mPreviewListener != null){
					mPreviewListener.OnImagePreview();
				}
			}
		});
	}
	
	private void setRenderViewListeners(){
		mRenderView.setOnDrawListener(new OnPointDrawn() {
			
			@Override
			public void pointDrawn(float x, float y) {
				// TODO Auto-generated method stub
				switch (mFilterMaskMode) {
				case PAINT_MODE:
					mMultiObjectFilter.paintPointWithColor(new PointF(x, y), mPaintRadius, 0, 0, 0);
					break;
				
				case ERASE_MODE:
					mMultiObjectFilter.erasePointWithRadius(new PointF(x, y), mEraseRadius);
					break;
					
				default:
					break;
				}
				
			}
		});
		
		mRenderView.setContourDrawListener(new onDrawContourComplete() {
			
			@Override
			public void contourDrawn(ArrayList<PointF> contourPoints) {
				// TODO Auto-generated method stub
				switch (mFilterMaskMode) {
				case FOREGROUND_MODE:
					
					//mObjectFilter.setImageMatFromBitmap(mRenderView.getResizedBitmap(), ColorSpace.RGB_COLORSPACE);
					mMultiObjectFilter.setForegroundPoints(contourPoints);
					//mObjectFilter.applyFilter();
					break;
					
				case BACKGROUND_MODE:
					mMultiObjectFilter.setBackgroundPoints(contourPoints);
					//mObjectFilter.applyFilter();
					break;
					
				case PAINT_MODE:
					mMultiObjectFilter.setPaintPointContour(contourPoints, mPaintRadius);
					break;
					
				case ERASE_MODE:
					mMultiObjectFilter.setErasePointContour(contourPoints, mEraseRadius);
					break;
					
				default:
					break;
				}
				
				
			}
		});
	}
	
	private void setColorChangeListener(){
		mColorPickerView.setOnColorChangedListener(new OnColorChangedListener() {
			
			@Override
			public void onColorChanged(int newColor) {
				// TODO Auto-generated method stub
				mMultiObjectFilter.applyRGBColor(Color.red(newColor), Color.green(newColor), Color.blue(newColor));
			}
		});
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(com.logic.photolibrary.R.menu.base_image_processing, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == com.logic.photolibrary.R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
