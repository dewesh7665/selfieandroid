package com.logic.foregroundcamera;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.logic.photo.application.PhotoGrabApplication;
import com.logic.photo.filters.MultipleObjectFilter;
import com.logic.photo.models.InputFilterInfo;
import com.logic.photo.views.BaseObjectSelectionView;
import com.logic.photo.views.BaseObjectSelectionView.ImagePreviewListener;
import com.logic.ui.views.ZoomPanWidget;

public class ForegroundCameraActivity extends Activity {
	private BaseObjectSelectionView mSelectionView = null;
	private MultipleObjectFilter mMultipleObjectFilter = null;
	//mMultiObjectFilter = mApplication.getObjectFilter();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_foreground_camera);
		mSelectionView = (BaseObjectSelectionView)findViewById(R.id.viewObjectSelection);
		mSelectionView.hideAddObjectFilter();
		mSelectionView.hideObjectSelectionFilter();
		mSelectionView.setImagePreviewListener(new ImagePreviewListener() {

			@Override
			public void OnImagePreview() {
				// TODO Auto-generated method stub
				InputFilterInfo currentInputFilterInfo = PhotoGrabApplication.getInstance().getCurrentInputFilterInfo();
				currentInputFilterInfo.getMultipleObjectFilter().updateSelectedMaskMatWithLargestContour();
				currentInputFilterInfo.setIntermediateMat(currentInputFilterInfo.getMultipleObjectFilter().getProcessedFGMat());

				//Add current selection in list
				PhotoGrabApplication.getInstance().getInputFilterInfoList().add(currentInputFilterInfo);

				Intent intent = new Intent(ForegroundCameraActivity.this, ImagePreviewActivityV2.class);
				startActivity(intent);
			}
		});
		((ZoomPanWidget) findViewById(R.id.zoomPanWidget)).setView(mSelectionView.getRenderView());
		//findViewById(R.id.imgGestureHand1).setOnTouchListener(new SingleTouchPanListener(gestureHandTouchListener));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.foreground_camera, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
