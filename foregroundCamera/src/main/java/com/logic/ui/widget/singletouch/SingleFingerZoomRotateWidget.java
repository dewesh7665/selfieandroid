package com.logic.ui.widget.singletouch;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.logic.foregroundcamera.R;

/**
 * @author Dewesh Kumar
 */
public class SingleFingerZoomRotateWidget extends LinearLayout {
    private View mListenerView;
    private View mControllerView;
    private ViewGroup mListenerViewContainer;
    public SingleFingerZoomRotateWidget(Context context) {
        this(context, null, 0);
    }

    public SingleFingerZoomRotateWidget(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SingleFingerZoomRotateWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.widget_single_finger_zoom_rotate, this);
        mControllerView = findViewById(R.id.ivZoomRotate);
        mListenerViewContainer = (ViewGroup) findViewById(R.id.rlContainer);
    }

    public void setView(View view) {
        mListenerView = view;
        mListenerViewContainer.addView(mListenerView);
        mControllerView.setOnTouchListener(new PushBtnTouchListener(mListenerViewContainer));
        mListenerViewContainer.setOnTouchListener(new ViewOnTouchListener(mControllerView));
    }
}
