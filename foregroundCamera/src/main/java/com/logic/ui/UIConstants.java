package com.logic.ui;

/**
 * @author Dewesh Kumar
 */
public class UIConstants {
    public static final String FRAG_BG_FILTER_LANDING = "fragment_bg_filter_landing";
    public static final String FRAG_BG_FILTER_EFFECTS = "fragment_bg_filter_effects";
    public static final String FRAG_BG_FILTER_TEXT = "fragment_bg_filter_text";
    public static final String BMP_ERROR = "Oops ! Something went wrong. Please try again.";
    public static final String PROGRESS_MSG_DEFAULT = "Please wait while we process you selection.";
    public static final String SUCCESS_MSG_DEFAULT = "Your creation has been successfully saved in gallery.";
    public static final String FOLDER_NAME = "LB_FOREGROUND_CAMERA";
}
