package com.logic.ui.TextFilter;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logic.foregroundcamera.R;
import com.logic.ui.utils.FontUtil;

/**
 * @author Dewesh Kumar
 */
public class ColorOptionItemView extends LinearLayout {
    private Context mContext;
    private TextView mTvOption;
    public ColorOptionItemView(Context context) {
        this(context, null);
    }

    public ColorOptionItemView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ColorOptionItemView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        inflate(context, R.layout.view_item_option_color, this);
        mTvOption = (TextView) this.findViewById(R.id.tvOption);
        mContext = context;
        this.setFocusable(true);
        this.setClickable(true);
    }

    public void updateSelection(Boolean isSelected) {
        int visiblity = isSelected ? View.VISIBLE : View.GONE;
        this.findViewById(R.id.tvBorder).setVisibility(visiblity);
    }

    public void setColor(int colorId) {
        setBGColor(colorId);
    }

    public void setFont(FontUtil.FontFamily fontFamily) {

    }

    private void setBGColor(int colorId) {
        GradientDrawable drawable = new GradientDrawable();
        drawable.setColor(getResources().getColor(colorId));
        drawable.setShape(GradientDrawable.OVAL);
        mTvOption.setBackground(drawable);
        //drawable.setStroke((int)cET.dpToPx(2), Color.parseColor("#EEEEEE"));
        //drawable.setSize((int)cET.dpToPx(240), (int)cET.dpToPx(240));
    }

}

