package com.logic.ui.TextFilter;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.widget.EditText;

/**
 * @author Dewesh Kumar
 */
public class ZoomableEditText extends EditText {
    public static interface OnActionUpListener {
        public void onActionUp();
    }
    private OnActionUpListener onActionUpListener;
    public ZoomableEditText(Context context) {
        super(context);
    }

    public ZoomableEditText(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.editTextStyle);
    }

    public ZoomableEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setOnActionUpListener(OnActionUpListener onActionUpListener) {
        this.onActionUpListener = onActionUpListener;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
            if (this.onActionUpListener != null)
                this.onActionUpListener.onActionUp();
        }
        return super.dispatchKeyEvent(event);
    }

}
