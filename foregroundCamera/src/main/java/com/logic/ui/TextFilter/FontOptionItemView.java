package com.logic.ui.TextFilter;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logic.foregroundcamera.R;
import com.logic.ui.utils.FontUtil;

/**
 * @author Dewesh Kumar
 */
public class FontOptionItemView extends LinearLayout {
    private Context mContext;
    private TextView mTextView;
    public FontOptionItemView(Context context) {
        this(context,null);
    }

    public FontOptionItemView(Context context, AttributeSet attrs) {
        this(context,attrs,0);
    }

    public FontOptionItemView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        inflate(context, R.layout.view_item_option_font, this);
        mTextView = (TextView)this.findViewById(R.id.viof_tv);
        this.setFocusable(true);
        this.setClickable(true);
    }

    public void updateSelection(Boolean isSelected) {
        int visiblity = isSelected ? View.VISIBLE : View.GONE;
        this.findViewById(R.id.viof_border).setVisibility(visiblity);
    }

    public void setText(String text) {
        mTextView.setText(text);
    }

    public void setFont(FontUtil.FontFamily fontFamily) {
        FontUtil.setFontToTextView(mContext, mTextView, fontFamily);
    }

}


