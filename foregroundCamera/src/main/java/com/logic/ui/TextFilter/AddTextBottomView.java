package com.logic.ui.TextFilter;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.logic.business.FileIOUtil;
import com.logic.foregroundcamera.R;
import com.logic.photo.utils.BitmapUtils;
import com.logic.ui.utils.FontUtil;
import com.logic.ui.utils.UIUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Dewesh Kumar
 */
public class AddTextBottomView extends LinearLayout {
    public interface OnFontSelectionListener {
        void onFontSelected(FontUtil.FontFamily fontFamily);
    }
    public interface OnColorSelectionListener {
        void onColorSelected(int colorId);
    }
    public interface OnFilterSelectionListener {
        void onFilteringStart();
        void onFilteringEnd();
    }
    public interface OnOkCancelListener {
        void onOk();
        void onCancel();
    }
    public interface OnSpeechBubbleSelectionListener {
        void onSpeechBubbleSelection(View view,SpeechBubble speechBubble);
    }


    private int[] mActionableIds = {R.id.cancelLL, R.id.okLL, R.id.changeColorBtn, R.id.changeFontBtn, R.id.changeSizeBtn,
            R.id.hideFontHSV, R.id.textBubbleBtn};
    private LinearLayout llParentCollection;
    private LinearLayout llParentScrollView;
    private LayoutInflater mInflater;
    private Context mContext;
    private FontUtil.FontFamily[] fontFamilies = {FontUtil.FontFamily.ROBOTO_SLAB_REGULAR, FontUtil.FontFamily.PRECIUOS, FontUtil.FontFamily.TANGERINE_REGULAR, FontUtil.FontFamily.TANGERINE_BOLD};
    private int[] colorIds = {R.color.white, R.color.background_floating_material_dark, R.color.red, R.color.green, R.color.material_blue_grey_800, R.color.material_deep_teal_200};
    private OnFontSelectionListener onFontSelectionListener;
    private OnColorSelectionListener onColorSelectionListener;
    private OnFilterSelectionListener onFilterSelectionListener;
    private OnOkCancelListener onOkCancelListener;
    private OnSpeechBubbleSelectionListener onSpeechBubbleSelectionListener;
    private List<SpeechBubble> mStickerList = new ArrayList<SpeechBubble>();
    private int mFilterThumbSize;
    private int mStickerMargin;
    //private RelativeLayout rlParentTextFilter;


    public AddTextBottomView(Context context) {
        this(context, null);
    }

    public AddTextBottomView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AddTextBottomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        inflate(context, R.layout.view_bottom_add_text, this);
        llParentCollection = (LinearLayout) findViewById(R.id.llParentCollection);
        llParentScrollView = (LinearLayout) findViewById(R.id.fontLL);
        for (int i = 0; i < mActionableIds.length; i++) {
            findViewById(mActionableIds[i]).setOnClickListener(onClickListener);
        }
        mInflater = LayoutInflater.from(mContext);
        mFilterThumbSize = getResources().getDimensionPixelSize(R.dimen.sticker_thumb_size);
        mStickerMargin = getResources().getDimensionPixelSize(R.dimen.sticker_margin);
        initSpeechBubbleList();
        //rlParentTextFilter = (RelativeLayout) findViewById(R.id.rlParentTextFilter);
    }

    public void setOnFontSelectionListener(OnFontSelectionListener onFontSelectionListener) {
        this.onFontSelectionListener = onFontSelectionListener;
    }

    public void setOnColorSelectionListener(OnColorSelectionListener onColorSelectionListener) {
        this.onColorSelectionListener = onColorSelectionListener;
    }

    public void setOnFilterSelectionListener(OnFilterSelectionListener onFilterSelectionListener) {
        this.onFilterSelectionListener = onFilterSelectionListener;
    }

    public void setOnOkCancelListener(OnOkCancelListener onOkCancelListener) {
        this.onOkCancelListener = onOkCancelListener;
    }

    public void setOnSpeechBubbleSelectionListener(OnSpeechBubbleSelectionListener onSpeechBubbleSelectionListener) {

    }

    OnClickListener onClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.cancelLL:
                    if (onOkCancelListener != null)
                        onOkCancelListener.onCancel();
                    break;
                case R.id.okLL:
                    //Save
                    //galleryAddPic(saveImage(rlParentTextFilter));
                    if (onOkCancelListener != null)
                        onOkCancelListener.onOk();
                    break;
                case R.id.changeColorBtn:
                    if (onFilterSelectionListener != null)
                        onFilterSelectionListener.onFilteringStart();
                    createColorCollection();
                    showOptions();
                    break;
                case R.id.changeFontBtn:
                    if (onFilterSelectionListener != null)
                        onFilterSelectionListener.onFilteringStart();
                    createFontCollection();
                    showOptions();
                    break;
                case R.id.textBubbleBtn:
                    createSpeechBubbleCollection();
                    showOptions();
                    break;
                case R.id.changeSizeBtn:
                    showOptions();
                    break;
                case R.id.hideFontHSV:
                    if (onFilterSelectionListener != null)
                        onFilterSelectionListener.onFilteringEnd();
                    hideOptions();
                    break;

            }
        }
    };

    private void createFontCollection() {
        llParentScrollView.removeAllViews();
        for (int i = 0; i < fontFamilies.length; i++) {
            FontOptionItemView fontOptionItemView = new FontOptionItemView(mContext);
            fontOptionItemView.setText("Aa");
            fontOptionItemView.setFont(fontFamilies[i]);
            fontOptionItemView.setTag(fontFamilies[i]);
            fontOptionItemView.setId(i);
            fontOptionItemView.setOnClickListener(fontOptionClickListener);
            llParentScrollView.addView(fontOptionItemView);
        }
    }

    private void createSpeechBubbleCollection() {
        llParentScrollView.removeAllViews();
        for (int i = 0; i < mStickerList.size(); i++) {
            ImageView imageView = new ImageView(mContext);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, mFilterThumbSize);
            layoutParams.setMargins(mStickerMargin, mStickerMargin, 0, mStickerMargin);
            imageView.setLayoutParams(layoutParams);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setOnClickListener(stickerClickListener);
            imageView.setAdjustViewBounds(true);
            imageView.setImageResource(mStickerList.get(i).getDrawableId());
            //imageView.setPadding(mStickerMargin, mStickerMargin, mStickerMargin, mStickerMargin);
            //imageView.setImageDrawable(getResources().getDrawable(mStickerList.get(i).getDrawableId()));
            //imageView.setBackgroundResource(R.drawable.shape_round_rect_glass);
            imageView.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            imageView.setTag(mStickerList.get(i));
            llParentScrollView.addView(imageView);
        }

    }

    View.OnClickListener stickerClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SpeechBubble sticker = (SpeechBubble) v.getTag();
            if (onSpeechBubbleSelectionListener != null)
                onSpeechBubbleSelectionListener.onSpeechBubbleSelection(v, sticker);
            // if (onStickerSelectedListener != null)
            //onStickerSelectedListener.onStickerSelectedListener(v, sticker);
        }
    };


    OnClickListener fontOptionClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            FontUtil.FontFamily fontFamily = (FontUtil.FontFamily)v.getTag();
            updateFontSelection(v);
            if (onFontSelectionListener != null)
                onFontSelectionListener.onFontSelected(fontFamily);
        }
    };

    private int mCurrentFontSelectedId = -1;
    private void updateFontSelection(View view) {
        if (mCurrentFontSelectedId != -1)
            ((FontOptionItemView) llParentScrollView.findViewById(mCurrentFontSelectedId)).updateSelection(false);
        mCurrentFontSelectedId = view.getId();
        ((FontOptionItemView) llParentScrollView.findViewById(mCurrentFontSelectedId)).updateSelection(true);
    }

    private void createColorCollection() {
        llParentScrollView.removeAllViews();
        for (int i = 0; i < colorIds.length; i++) {
            ColorOptionItemView colorOptionItemView = new ColorOptionItemView(mContext);
            colorOptionItemView.setColor(colorIds[i]);
            colorOptionItemView.setTag(colorIds[i]);
            colorOptionItemView.setId(i);
            colorOptionItemView.setOnClickListener(colorOptionClickListener);
            llParentScrollView.addView(colorOptionItemView);
        }
    }

    OnClickListener colorOptionClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int colorId = (int) v.getTag();
            updateColorSelection(v);
            if (onColorSelectionListener != null)
                onColorSelectionListener.onColorSelected(colorId);
        }
    };

    private int mCurrentColorSelectedId = -1;
    private void updateColorSelection(View view) {
        if (mCurrentColorSelectedId != -1)
            ((ColorOptionItemView) llParentScrollView.findViewById(mCurrentColorSelectedId)).updateSelection(false);
        mCurrentColorSelectedId = view.getId();
        ((ColorOptionItemView) llParentScrollView.findViewById(mCurrentColorSelectedId)).updateSelection(true);
    }

    private void showOptions() {
        /*llParentCollection.setVisibility(VISIBLE);
        ObjectAnimator animator = ObjectAnimator.ofInt(llParentCollection, "top", 120, 0);
        animator.setDuration(500L);
        animator.setInterpolator( new LinearInterpolator() );
        animator.addListener( new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd( Animator animation ){

            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                llParentCollection.setVisibility(VISIBLE);

            }
        } );

        animator.start();*/
        llParentCollection.setVisibility(VISIBLE);
        UIUtil.showHide(llParentCollection, true);
    }

    private void hideOptions() {
        /*llParentCollection.setVisibility(VISIBLE);
        ObjectAnimator animator = ObjectAnimator.ofInt(llParentCollection, "top", 0, 120);
        animator.setDuration(500L);
        animator.setInterpolator( new LinearInterpolator() );
        animator.addListener( new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd( Animator animation ){

            }

            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                llParentCollection.setVisibility(VISIBLE);

            }
        } );

        animator.start();
        */
        llParentCollection.setVisibility(VISIBLE);
        UIUtil.showHide(llParentCollection, false);
    }

    private String saveImage(RelativeLayout relativeLayout) {
        File dataFile = FileIOUtil.getNewFileBasedOnTimeStamp();
        Bitmap map = BitmapUtils.createBitmap(relativeLayout);

        try {
            FileOutputStream out = new FileOutputStream(dataFile, false);
            map.compress(Bitmap.CompressFormat.PNG, 95, out);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // return map;
        return dataFile.getAbsolutePath();

    }

    private void initSpeechBubbleList() {
        mStickerList.add(new SpeechBubble(R.drawable.speech_bubble_1, false));
        mStickerList.add(new SpeechBubble(R.drawable.speech_bubble_2, false));
        mStickerList.add(new SpeechBubble(R.drawable.speech_bubble_3, false));
        //mStickerList.add(new SpeechBubble(R.drawable.speech_bubble4, false));
        //mStickerList.add(new Sticker(R.drawable.speech_bubble6, false));
        mStickerList.add(new SpeechBubble(R.drawable.speech_bulle1, false));
        mStickerList.add(new SpeechBubble(R.drawable.speech_bulle2, false));
        mStickerList.add(new SpeechBubble(R.drawable.speech_bulle3, false));
        mStickerList.add(new SpeechBubble(R.drawable.speech_bulle4, false));
        mStickerList.add(new SpeechBubble(R.drawable.thought_bubble, false));
    }

    public static class SpeechBubble {
        private int drawableId = -1;
        private Boolean isActive = false;
        public SpeechBubble(int drawableId, Boolean isActive) {
            this.drawableId = drawableId;
            this.isActive = isActive;
        }

        public int getDrawableId() {
            return drawableId;
        }

        public Boolean isActive() {
            return isActive;
        }

        public void setActive(Boolean isActive) {
            this.isActive = isActive;
        }
    }

}
