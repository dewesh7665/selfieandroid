package com.logic.ui.TextFilter;

import android.content.Context;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.logic.foregroundcamera.R;
import com.logic.photo.gestures.MultiTouchListener;
import com.logic.photo.gestures.SingleTouchListener;
import com.logic.photo.gestures.Vector2D;

/**
 * @author Dewesh Kumar
 */
public class ZoomPanEditTextView extends LinearLayout {
    public static interface OnSingleTapConfirmedListener {
        public void onSingleTapConfirmed(View view);
    }
    private Context mContext;
    private OnSingleTapConfirmedListener onSingleTapConfirmedListener;
    private EditText mEditText;
    private ImageView ivRemoveButton;
    private View mZoomRotateView;
    public ZoomPanEditTextView(Context context) {
        this(context, null);
    }

    public ZoomPanEditTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ZoomPanEditTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        //this.setLayoutParams(new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        LayoutInflater.from(context).inflate(R.layout.view_edit_text_zoom_pan, this);
        mContext = context;
        mEditText = (EditText) findViewById(R.id.zoomPanEditText);
        setTouchListener(true);
        ivRemoveButton = (ImageView) findViewById(R.id.ivRemove);
        ivRemoveButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ViewGroup) ZoomPanEditTextView.this.getParent()).removeView(ZoomPanEditTextView.this);
            }
        });

        mZoomRotateView = findViewById(R.id.ivZoomRotate);
        mZoomRotateView.setOnTouchListener(new SingleTouchListener(new SingleTouchListener.OnTouchChanged() {
            @Override
            public void onTouchChanged(SingleTouchListener.TouchTransformInfo touchTransInfo) {
                rotateZoom(touchTransInfo);

            }
        }));
    }

    public void setOnSingleTapConfirmedListener(OnSingleTapConfirmedListener onSingleTapConfirmedListener) {
        this.onSingleTapConfirmedListener = onSingleTapConfirmedListener;
    }

    private void setTouchListener(Boolean isTouchEnable) {
        if (isTouchEnable) {
            this.setOnTouchListener(new MultiTouchListener(new MultiTouchListener.OnSingleTapConfirmed() {
                @Override
                public void onSingleTapConfirmed(View view) {
                    if (onSingleTapConfirmedListener != null)
                        onSingleTapConfirmedListener.onSingleTapConfirmed(ZoomPanEditTextView.this);
                    setEditMode(true);
                    //Show keyboard
                    ZoomPanEditTextView.this.setOnTouchListener(null);
                    mEditText.requestFocus();
                    ((ZoomableEditText) mEditText).setOnActionUpListener(new ZoomableEditText.OnActionUpListener() {
                        @Override
                        public void onActionUp() {
                            setTouchListener(true);
                        }
                    });
                    mEditText.post(new Runnable() {
                        @Override
                        public void run() {
                            InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(mEditText, InputMethodManager.SHOW_IMPLICIT);
                        }
                    });

                   /*ZoomPanEditText.this.requestFocus();
                   ZoomPanEditText.this.post(new Runnable() {
                       @Override
                       public void run() {
                           InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                           imm.showSoftInput(ZoomPanEditText.this, InputMethodManager.SHOW_IMPLICIT);
                       }
                   });*/
                }
            }));
        }
        else {
            ZoomPanEditTextView.this.setOnTouchListener(null);
        }
    }

    /*@Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP) {
            setTouchListener(true);
        }
        return super.dispatchKeyEvent(event);
    }*/

    public void setEditMode(Boolean isEnabled) {
        if (isEnabled) {
            mEditText.setBackgroundResource(R.drawable.shape_round_rect_glass);
            ivRemoveButton.setVisibility(VISIBLE);
            //setTouchListener(true);
        }
        else {
            mEditText.setBackgroundResource(android.R.color.transparent);
            ivRemoveButton.setVisibility(GONE);
            //setTouchListener(false);
        }
    }

    private float minimumScale = 0.50f;
    private float maximumScale = 3.0f;
    private int yOffSet = 0;
    private void rotateZoom(SingleTouchListener.TouchTransformInfo touchTransformInfo) {
        PointF currentPoint = touchTransformInfo.currentPoint;
        PointF prevPoint = touchTransformInfo.prevPoint;
        android.graphics.Rect rect = new android.graphics.Rect();
        if (yOffSet == 0)
            yOffSet = getResources().getDimensionPixelSize(R.dimen.yOffSet);
        int[] location = new int[2];
        this.getLocationOnScreen(location);
        Vector2D c = new Vector2D(this.getX() + this.getWidth()/2, this.getY() + this.getHeight()/2 + yOffSet);

        Vector2D p2 = new Vector2D(currentPoint.x, currentPoint.y);
        Vector2D p1 = new Vector2D(prevPoint.x, prevPoint.y);
        float rotationAngle = Vector2D.getAngle(c, p1, p2);
        float angle = this.getRotation() + rotationAngle;
        this.setRotation(angle);

        float scale = this.getScaleX()  *  Vector2D.getScale(c, p1, p2);
        scale = Math.max(minimumScale, Math.min(maximumScale, scale));
        this.setScaleX(scale);
        this.setScaleY(scale);


    }

    private float adjustAngle(float degrees) {
        if (degrees > 180.0f) {
            degrees -= 360.0f;
        } else if (degrees < -180.0f) {
            degrees += 360.0f;
        }

        return degrees;
    }

    private void log(String msg) {
        Log.i("SingleTouchListener", msg);
    }

}
