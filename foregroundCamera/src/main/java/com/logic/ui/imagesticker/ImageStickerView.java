package com.logic.ui.imagesticker;

import android.content.Context;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.logic.foregroundcamera.R;
import com.logic.photo.gestures.MultiTouchListener;
import com.logic.photo.gestures.SingleTouchListener;
import com.logic.photo.gestures.Vector2D;

/**
 * @author Dewesh Kumar
 */
public class ImageStickerView extends LinearLayout {
    public interface OnPlusOneListener {
         void onPlusOne(View view, int drawableId);
    }
    private final int[] mActionableIds = {R.id.tvPlusOne, R.id.ivRemove};
    private MultiTouchListener mMultiTouchListener;
    private Context mContext;
    private ImageView mImageView;
    private Boolean isActive = false;
    private int mDrawableId = -1;
    private OnPlusOneListener onPlusOneListener;
    private MultiTouchListener.OnSingleTapConfirmed onSingleTapConfirmed;
    private View mZoomRotateView;
    public ImageStickerView(Context context) {
        this(context, null, 0);
    }

    public ImageStickerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ImageStickerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        inflate(mContext, R.layout.view_sticker, this);
        mImageView = (ImageView)findViewById(R.id.ivSticker);
        for (int i = 0; i < mActionableIds.length; i++) {
            findViewById(mActionableIds[i]).setOnClickListener(onClickListener);
        }
        mMultiTouchListener = new MultiTouchListener();
        this.setOnTouchListener(mMultiTouchListener);

        int padding = getResources().getDimensionPixelSize(R.dimen.input_image_view_padding);
        this.setPadding(padding, padding, padding, padding);
        mZoomRotateView = findViewById(R.id.ivZoomRotate);
        mZoomRotateView.setOnTouchListener(new SingleTouchListener(new SingleTouchListener.OnTouchChanged() {
            @Override
            public void onTouchChanged(SingleTouchListener.TouchTransformInfo touchTransInfo) {
                //Log.i("SingleTouch", "dx="+touchTransInfo.deltaX+" dy="+touchTransInfo.deltaY+" " +
                       // "Pnt="+touchTransInfo.currentPoint.x+","+touchTransInfo.currentPoint.y);
                scaleView(touchTransInfo);

            }
        }));
    }

    public void setOnPlusOneListener(OnPlusOneListener onPlusOneListener) {
        this.onPlusOneListener = onPlusOneListener;
    }

    public void setOnSingleTapConfirmedListener(MultiTouchListener.OnSingleTapConfirmed onSingleTapConfirmed) {
        this.onSingleTapConfirmed = onSingleTapConfirmed;
        mMultiTouchListener.setOnSingleTapConfirmedListener(this.onSingleTapConfirmed);
    }

    OnClickListener onClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tvPlusOne:
                    if (onPlusOneListener != null) {
                        onPlusOneListener.onPlusOne(ImageStickerView.this, mDrawableId);
                    }
                    break;
                case R.id.ivRemove:
                    ViewGroup container = ((ViewGroup) ImageStickerView.this.getParent());
                    if (container.getChildCount() == 1) {
                        Toast.makeText(mContext, "At least 1 sticker is required. Please add 1 if you want to delete this.", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        container.removeView(ImageStickerView.this);
                    }
                    break;
            }
        }
    };

    public void setImageResource(int drawableId) {
        this.mDrawableId = drawableId;
        mImageView.setImageResource(drawableId);
    }

    public void setActiveState(Boolean isActive) {
        this.isActive = isActive;
        if (isActive) {
            mImageView.setBackgroundResource(R.drawable.shape_input_img_active_bg);
            findViewById(R.id.ivRemove).setVisibility(View.VISIBLE);
            findViewById(R.id.tvPlusOne).setVisibility(View.VISIBLE);
        }
        else {
            mImageView.setBackgroundResource(android.R.color.transparent);
            findViewById(R.id.ivRemove).setVisibility(View.GONE);
            findViewById(R.id.tvPlusOne).setVisibility(View.GONE);
        }
    }

    private float minimumScale = 0.50f;
    private float maximumScale = 3.0f;
    private void scaleView(SingleTouchListener.TouchTransformInfo touchTransformInfo) {
        PointF currentPoint = touchTransformInfo.currentPoint;
        Vector2D c = new Vector2D(this.getX() + this.getWidth()/2, this.getY() + this.getHeight()/2);
        Vector2D p2 = new Vector2D(currentPoint.x, currentPoint.y);
        Vector2D p1 = new Vector2D(currentPoint.x - touchTransformInfo.deltaX, currentPoint.y-touchTransformInfo.deltaY);

        float angle = this.getRotation() + Vector2D.getAngle(c, p1, p2);
        angle = angle % 360;
        this.setRotation(angle);

        float scale = this.getScaleX()  *  Vector2D.getScale(c, p1, p2);
        scale = Math.max(minimumScale, Math.min(maximumScale, scale));
        this.setScaleX(scale);
        this.setScaleY(scale);


    }

    private float adjustAngle(float degrees) {
        if (degrees > 180.0f) {
            degrees -= 360.0f;
        } else if (degrees < -180.0f) {
            degrees += 360.0f;
        }

        return degrees;
    }


}
