package com.logic.ui.imagesticker;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.logic.foregroundcamera.R;
import com.logic.ui.toolbox.BaseToolBox;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dewesh Kumar
 */
public class StickerToolView extends BaseToolBox {
    public interface OnStickerSelectedListener {
        void onStickerSelectedListener(View view, Sticker sticker);
    }
    private LinearLayout llParentScroll;
    private int mFilterThumbSize;
    private int mStickerMargin;
    private List<Sticker> mStickerList = new ArrayList<Sticker>();
    private OnStickerSelectedListener onStickerSelectedListener;
    public StickerToolView(Context context) {
        this(context, null, 0);
    }

    public StickerToolView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public StickerToolView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mInflater.inflate(R.layout.tool_sticker, this);
        mFilterThumbSize = getResources().getDimensionPixelSize(R.dimen.sticker_thumb_size);
        mStickerMargin = getResources().getDimensionPixelSize(R.dimen.sticker_margin);
        initStickerList();
        prepareHorScrollView();
    }

    private void initStickerList() {
        mStickerList.add(new Sticker(R.drawable.sticker1, false));
        mStickerList.add(new Sticker(R.drawable.sticker2, false));
        mStickerList.add(new Sticker(R.drawable.sticker3, false));
        mStickerList.add(new Sticker(R.drawable.sticker4, false));
        mStickerList.add(new Sticker(R.drawable.sticker5, false));
        mStickerList.add(new Sticker(R.drawable.sticker6, false));
        mStickerList.add(new Sticker(R.drawable.sticker7, false));
        mStickerList.add(new Sticker(R.drawable.sticker8, false));
        mStickerList.add(new Sticker(R.drawable.sticker9, false));

        /*mStickerList.add(new Sticker(R.drawable.speech_bubble_1, false));
        mStickerList.add(new Sticker(R.drawable.speech_bubble_2, false));
        mStickerList.add(new Sticker(R.drawable.speech_bubble_3, false));
        //mStickerList.add(new Sticker(R.drawable.speech_bubble4, false));
        //mStickerList.add(new Sticker(R.drawable.speech_bubble6, false));
        mStickerList.add(new Sticker(R.drawable.speech_bulle1, false));
        mStickerList.add(new Sticker(R.drawable.speech_bulle2, false));
        mStickerList.add(new Sticker(R.drawable.speech_bulle3, false));
        mStickerList.add(new Sticker(R.drawable.speech_bulle4, false));
        mStickerList.add(new Sticker(R.drawable.thought_bubble, false));*/

    }

    private void prepareHorScrollView() {
        llParentScroll = (LinearLayout) findViewById(R.id.llScrollParent);
        llParentScroll.removeAllViews();
        for (int i = 0; i < mStickerList.size(); i++) {
            ImageView imageView = new ImageView(mContext);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, mFilterThumbSize);
            layoutParams.setMargins(mStickerMargin, mStickerMargin, 0, mStickerMargin);
            imageView.setLayoutParams(layoutParams);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
            imageView.setOnClickListener(stickerClickListener);
            imageView.setAdjustViewBounds(true);
            imageView.setImageResource(mStickerList.get(i).getDrawableId());
            //imageView.setPadding(mStickerMargin, mStickerMargin, mStickerMargin, mStickerMargin);
            //imageView.setImageDrawable(getResources().getDrawable(mStickerList.get(i).getDrawableId()));
            //imageView.setBackgroundResource(R.drawable.shape_round_rect_glass);
            imageView.setBackgroundColor(getResources().getColor(android.R.color.transparent));
            imageView.setTag(mStickerList.get(i));
            llParentScroll.addView(imageView);
        }
    }

    public void setOnStickerSelectedListener(OnStickerSelectedListener onStickerSelectedListener) {
        this.onStickerSelectedListener = onStickerSelectedListener;
    }

    View.OnClickListener stickerClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Sticker sticker = (Sticker) v.getTag();
            if (onStickerSelectedListener != null)
                onStickerSelectedListener.onStickerSelectedListener(v, sticker);
        }
    };

    public static class Sticker {
        private int drawableId = -1;
        private Boolean isActive = false;
        public Sticker(int drawableId, Boolean isActive) {
            this.drawableId = drawableId;
            this.isActive = isActive;
        }

        public int getDrawableId() {
            return drawableId;
        }

        public Boolean isActive() {
            return isActive;
        }

        public void setActive(Boolean isActive) {
            this.isActive = isActive;
        }
    }
}
