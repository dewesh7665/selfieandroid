package com.logic.ui.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.logic.photo.filters.ColorChangeFilter;
import com.logic.photo.filters.PaintFilter;
import com.logic.photo.interfaces.FilterInterfaces;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

/**
 * @author Dewesh Kumar
 */
public class BGImageView extends ImageView implements FilterInterfaces.ColorFilterListener, FilterInterfaces.PaintFilterListener{
    private Mat mOriginalMat;
    private Mat mOriginalMat3Channel;
    private Mat mMaskMat;
    private Mat mProcessedMat;
    public BGImageView(Context context) {
        this(context, null);
    }

    public BGImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BGImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {

    }

    private void resetView() {
        mOriginalMat = new Mat();
        mOriginalMat3Channel = new Mat();
        mMaskMat = new Mat();
        mColorChangeFilter = null;
        mPaintFilter = null;
    }

    /**
     * External call. Don't call setImageBitmap directly
     * @param bitmap
     */
    public void setResetBitmap(Bitmap bitmap) {
        setImageBitmap(bitmap);
        mFilterAppliedBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
        resetView();
        Utils.bitmapToMat(bitmap, mOriginalMat);
        Imgproc.cvtColor(mOriginalMat, mOriginalMat3Channel, Imgproc.COLOR_RGBA2RGB);
        mMaskMat.create(mOriginalMat3Channel.size(), CvType.CV_8UC1);
        //mMaskMat.setTo(new Scalar(0));
        mMaskMat.setTo(new Scalar(255));
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
    }

    public Mat getOriginalMat() {
        return mOriginalMat;
    }

    /************************************************************ Filters ********************************************/

    private Bitmap mFilterAppliedBitmap;
    private ColorChangeFilter mColorChangeFilter = null;
    @Override
    public void applyColorFilter(int newColor) {
        if(mColorChangeFilter== null){
            mColorChangeFilter = new ColorChangeFilter();
            mColorChangeFilter.setOriginalMat(mOriginalMat3Channel);
            mColorChangeFilter.setMaskMat(mMaskMat);
            //Scalar meanColor = mMultiObjectFilter.getCurrentMeanColor();
            Scalar meanColor = Core.mean(mOriginalMat3Channel);
            mColorChangeFilter.setMeanColor((int)meanColor.val[0], (int)meanColor.val[1], (int)meanColor.val[2]);
        }
        //Now just apply filter and reset bitmap
        mColorChangeFilter.applyColorOnForeground(Color.red(newColor), Color.green(newColor), Color.blue(newColor));
        trimAndSetBitmap(mColorChangeFilter.getProcessedMat());
    }

    private PaintFilter mPaintFilter = null;
    private int mPaintRadius = 40;
    @Override
    public void applyPaintFilter(int value) {
        if(mPaintFilter == null){
            mPaintFilter = new PaintFilter();
            //mPaintFilter.setOriginalMat(mMultiObjectFilter.getOriginalMat());
            mPaintFilter.setOriginalMat(mOriginalMat3Channel);
            mPaintFilter.setMaskMat(mMaskMat);
            mPaintFilter.setBlurRadius(mPaintRadius);
        }
        mPaintFilter.applyFilterOnForeground();
        trimAndSetBitmap(mPaintFilter.getProcessedMat());
    }

    private void trimAndSetBitmap(Mat inputMat) {
        Utils.matToBitmap(inputMat, mFilterAppliedBitmap, true);
        this.setImageBitmap(mFilterAppliedBitmap);
    }

}
