package com.logic.ui.views;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.logic.foregroundcamera.BaseHomeActivity;
import com.logic.photo.application.PhotoGrabApplication;
import com.logic.photo.models.InputFilterInfo;
import com.logic.photolibrary.R;

import static com.controls.library.interfaces.MultiListInterfaces.OnMultiListGetViewCalledListener;

public class PhotoGalleryItemView extends LinearLayout implements OnMultiListGetViewCalledListener{
	private LayoutInflater mInflater;
	private ContentResolver mContentResolver;
    private Context mContext;
    private BaseHomeActivity.GallerySelectionListener gallerySelectionListener;
	public PhotoGalleryItemView(Context context) {
		super(context);
		mInflater = LayoutInflater.from(context);
		mContentResolver = context.getContentResolver();
        mContext = context;
	}

	@Override
	public View onGetViewCalled(View view, ViewGroup parent, Object object,
			Boolean isScrolling) {
		if (view == null)
			view = mInflater.inflate(R.layout.view_gallery_item, parent, false);
        view.setClickable(true);
        view.setOnClickListener(onClickListener);
        CameraPhotoItem cameraPhotoItem = (CameraPhotoItem) object;
		ImageView ivThumb = (ImageView)view.findViewById(R.id.ivThumb);
		Bitmap bitmap = MediaStore.Images.Thumbnails.getThumbnail(mContentResolver, cameraPhotoItem.getSrcId(), MediaStore.Images.Thumbnails.MICRO_KIND, null );
		view.setTag(cameraPhotoItem);
		//BitmapUtils.bindBitmapToImageView(imageUri.getPath(), ivThumb);
		ivThumb.setImageBitmap(bitmap);
		return view;
	}
	
	OnClickListener onClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            CameraPhotoItem cameraPhotoItem = (CameraPhotoItem) view.getTag();
            InputFilterInfo inputFilterInfo = new InputFilterInfo(getRealPathFromURI(cameraPhotoItem.getImageUri()));
            PhotoGrabApplication.getInstance().setCurrentInputFilterInfo(inputFilterInfo);
            //((PhotoGrabApplication) mContext.getApplicationContext()).setImagePath(getRealPathFromURI(cameraPhotoItem.getImageUri()));
            if (gallerySelectionListener != null )
                gallerySelectionListener.OnGallerySelction();
        }
    };



    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = mContext.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public void setGallerySelectionListener(BaseHomeActivity.GallerySelectionListener gallerySelectionListener) {
        this.gallerySelectionListener = gallerySelectionListener;
    }

    public static class CameraPhotoItem {
        private long srcId;
        private Uri imageUri;

        public long getSrcId() {
            return srcId;
        }

        public void setSrcId(long srcId) {
            this.srcId = srcId;
        }

        public Uri getImageUri() {
            return imageUri;
        }

        public void setImageUri(Uri imageUri) {
            this.imageUri = imageUri;
        }
    }
	
}
