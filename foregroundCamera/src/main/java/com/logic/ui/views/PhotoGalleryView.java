package com.logic.ui.views;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.view.View;
import android.widget.GridView;

import com.controls.library.MultiItemListView;
import com.controls.library.adapters.MultiItemRowAdapter;
import com.controls.library.helpers.AdapterParams;
import com.logic.foregroundcamera.BaseHomeActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PhotoGalleryView {
	private static Uri[] mUrls = null;
    private static String[] strUrls = null;
    private String[] mNames = null;
    private Long[] mIds = null;
    private GridView gridview = null;
    private Cursor cc = null;
    private Context mContext;
    
    protected MultiItemListView mNewsListView;
	private MultiItemRowAdapter mMultiItemRowAdapter;
	private ArrayList<AdapterParams> mArrListAdapterParam;
	private AdapterParams mAdapterParam;
    private PhotoGalleryItemView mPhotoGalleryItemView;
	private BaseHomeActivity.GallerySelectionListener gallerySelectionListener;
    public PhotoGalleryView(Context context) {
    	this.mContext = context;
    }
    public void requestPhotoGalleryView() {
    	 final String[] projection = { MediaStore.Images.Media.DATA };
         final String selection = MediaStore.Images.Media.BUCKET_ID + " = ?";
         final String[] selectionArgs = { CAMERA_IMAGE_BUCKET_ID };
    	cc = mContext.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, selection, selectionArgs,
                MediaStore.Images.Media.DATE_TAKEN +" DESC LIMIT 20");
    	 try {
             cc.moveToFirst();
             mUrls = new Uri[cc.getCount()];
             strUrls = new String[cc.getCount()];
             mNames = new String[cc.getCount()];
             mIds = new Long[cc.getCount()];
             for (int i = 0; i < cc.getCount(); i++) {
                 cc.moveToPosition(i);
                 mIds[i] = (long) cc.getInt(cc.getColumnIndex(MediaStore.MediaColumns._ID));
                 mUrls[i] = Uri.parse(cc.getString(1));
                 strUrls[i] = cc.getString(1);
                 mNames[i] = cc.getString(3);
                 //Log.e("mNames[i]",mNames[i]+":"+cc.getColumnCount()+ " : " +cc.getString(3));
             }

         } catch (Exception e) {
         }
    }

    public void setGallerySelectionListener(BaseHomeActivity.GallerySelectionListener gallerySelectionListener) {
        this.gallerySelectionListener = gallerySelectionListener;
    }
    
    public View getListView() {
    	mPhotoGalleryItemView = new PhotoGalleryItemView(mContext);
        if (gallerySelectionListener != null)
            mPhotoGalleryItemView.setGallerySelectionListener(gallerySelectionListener);
    	mNewsListView =new MultiItemListView(mContext);
    	mNewsListView.isPullRefrshEnabled(false);
		mNewsListView.setScrollListnerBelowICS(true);
		mNewsListView.getListView().setDividerHeight(0);
		mMultiItemRowAdapter = new MultiItemRowAdapter(mContext);
		mArrListAdapterParam = new ArrayList<AdapterParams>();
		List<Uri> uriList = new ArrayList<Uri>(Arrays.asList(mUrls));
		List<Long> idsList = new ArrayList<Long>(Arrays.asList(mIds));
        List<PhotoGalleryItemView.CameraPhotoItem> cameraPhotoItemList = new ArrayList<PhotoGalleryItemView.CameraPhotoItem>();
        for (int i = 0; i < mIds.length; i++) {
            PhotoGalleryItemView.CameraPhotoItem cameraPhotoItem = new PhotoGalleryItemView.CameraPhotoItem();
            cameraPhotoItem.setSrcId(mIds[i]);
            cameraPhotoItem.setImageUri(mUrls[i]);
            cameraPhotoItemList.add(cameraPhotoItem);
        }
		mAdapterParam = new AdapterParams(cameraPhotoItemList, mPhotoGalleryItemView);
		//mAdapterParam = new AdapterParams(getCameraImages(mContext), mPhotoGalleryItemView);
		mAdapterParam.setNumOfColumn(4);
		mArrListAdapterParam.add(mAdapterParam);
		mMultiItemRowAdapter.setAdapterParams(mArrListAdapterParam);
		mNewsListView.setAdapter(mMultiItemRowAdapter);
    	return mNewsListView.getPopulatedView();
    }
    
    /********************* Get id's of camera picture only****************************/
    private final String CAMERA_IMAGE_BUCKET_NAME =
            Environment.getExternalStorageDirectory().toString()
            + "/DCIM/Camera";
    private final String CAMERA_IMAGE_BUCKET_ID =
            getBucketId(CAMERA_IMAGE_BUCKET_NAME);

    /**
     * Matches code in MediaProvider.computeBucketValues. Should be a common
     * function.
     */
    private String getBucketId(String path) {
        return String.valueOf(path.toLowerCase().hashCode());
    }
    
    private List<Long> getCameraImages(Context context) {
        final String[] projection = { MediaStore.Images.Media.DATA };
        final String selection = MediaStore.Images.Media.BUCKET_ID + " = ?";
        final String[] selectionArgs = { CAMERA_IMAGE_BUCKET_ID };
        final Cursor cursor = context.getContentResolver().query(Images.Media.EXTERNAL_CONTENT_URI, 
                projection, 
                selection, 
                selectionArgs, 
                null);
        ArrayList<Long> result = new ArrayList<Long>(cursor.getCount());
        if (cursor.moveToFirst()) {
            final int dataColumn = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            do {
                final String data = cursor.getString(dataColumn);
                int columnId = (int) cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID));
                result.add((long) columnId);
            } while (cursor.moveToNext());
        }
        cursor.close();
        return result;
    }
    
}
