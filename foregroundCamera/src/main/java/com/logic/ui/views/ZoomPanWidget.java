package com.logic.ui.views;

import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.logic.foregroundcamera.R;

/**
 * @author Dewesh Kumar
 * To be used for step controlling of Zoom Pan
 */
public class ZoomPanWidget extends LinearLayout {
    private final float minimumScale = 0.50f;
    private final float maximumScale = 4.0f;
    private final float trans_delta = 20.0f; //in pixel
    private final float trans_delta_continuous = 10.0f; //in pixel

    private final float scale_delta = 0.10f; //in pixel
    private final float scale_delta_continuous = 0.05f; //in pixel

    private final int CONTINUOUS_INTERVAL = 33; //in milis = 30 fps
    private final int CONTINUOUS_MAX_TIME = 5000; //5 Seconds
    private int[] mActionableIds = {R.id.pan_left_ib, R.id.pan_right_ib, R.id.pan_up_ib, R.id.pan_down_ib,
            R.id.zoom_increase, R.id.zoom_decrease};
    private View mView;
    private Handler mContinuousHandler;
    private int mLongPressTimeCounter = -1;
    public ZoomPanWidget(Context context) {
        this(context, null);
    }

    public ZoomPanWidget(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ZoomPanWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.widget_zoom_pan, this);
        mContinuousHandler = new Handler();
        for (int  i = 0; i < mActionableIds.length; i++) {
            findViewById(mActionableIds[i]).setOnClickListener(onClickListener);
            findViewById(mActionableIds[i]).setOnLongClickListener(onLongClickListener);
            findViewById(mActionableIds[i]).setOnTouchListener(onTouchListener);
        }
    }

    public void setView(View view) {
        this.mView = view;
    }

    OnClickListener onClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            //In case want to stop current movement
            mContinuousHandler.removeCallbacksAndMessages(null);
            mLongPressTimeCounter = 0;
            switch (v.getId()) {
                case R.id.pan_left_ib:
                    adjustTranslation(mView, -trans_delta, 0);
                    break;
                case R.id.pan_right_ib:
                    adjustTranslation(mView, trans_delta, 0);
                    break;
                case R.id.pan_up_ib:
                    adjustTranslation(mView, 0, -trans_delta);
                    break;
                case R.id.pan_down_ib:
                    adjustTranslation(mView, 0, trans_delta);
                    break;
                case R.id.zoom_increase:
                    adjustZoom(mView, 1 + scale_delta);
                    break;
                case R.id.zoom_decrease:
                    adjustZoom(mView, 1 - scale_delta);
                    break;
            }
        }
    };

    OnLongClickListener onLongClickListener = new OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            mContinuousHandler.removeCallbacksAndMessages(null);
            mLongPressTimeCounter = 0;
            switch (v.getId()) {
                case R.id.pan_left_ib:
                    adjustTranslationContinuous(mView, -trans_delta_continuous, 0);
                    break;
                case R.id.pan_right_ib:
                    adjustTranslationContinuous(mView, trans_delta_continuous, 0);
                    break;
                case R.id.pan_up_ib:
                    adjustTranslationContinuous(mView, 0, -trans_delta_continuous);
                    break;
                case R.id.pan_down_ib:
                    adjustTranslationContinuous(mView, 0, trans_delta_continuous);
                    break;
                case R.id.zoom_increase:
                    adjustZoomContinuous(mView, 1 + scale_delta_continuous);
                    break;
                case R.id.zoom_decrease:
                    adjustZoomContinuous(mView, 1 - scale_delta_continuous);
                    break;
            }
            return true;
        }
    };

    private void adjustTranslation(View view, float deltaX, float deltaY) {
        float[] deltaVector = { deltaX, deltaY };
        view.getMatrix().mapVectors(deltaVector);
        view.setTranslationX(view.getTranslationX() + deltaVector[0]);
        view.setTranslationY(view.getTranslationY() + deltaVector[1]);
    }

    /**
     * On Long press will continue translation for 2 seconds with 20 fps.
     * TODO: TO be replaced with touch events
     * @param view
     * @param deltaX
     * @param deltaY
     */
    private void adjustTranslationContinuous(final View view, final float deltaX, final float deltaY) {
        mContinuousHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mLongPressTimeCounter >= CONTINUOUS_MAX_TIME) {
                    mContinuousHandler.removeCallbacksAndMessages(null);
                    return;
                }
                mLongPressTimeCounter += CONTINUOUS_INTERVAL;
                float[] deltaVector = {deltaX, deltaY };
                view.getMatrix().mapVectors(deltaVector);
                view.setTranslationX(view.getTranslationX() + deltaVector[0]);
                view.setTranslationY(view.getTranslationY() + deltaVector[1]);
                adjustTranslationContinuous(view, deltaX, deltaY);
            }
        }, CONTINUOUS_INTERVAL);
    }

    private  void adjustZoom(View view, float deltaScale) {
        float scale = view.getScaleX() * deltaScale;
        scale = Math.max(minimumScale, Math.min(maximumScale, scale));
        view.setScaleX(scale);
        view.setScaleY(scale);
    }

    private  void adjustZoomContinuous(final View view, final float deltaScale) {
        if (mLongPressTimeCounter >= CONTINUOUS_MAX_TIME) {
            mContinuousHandler.removeCallbacksAndMessages(null);
            return;
        }
        mContinuousHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                float scale = view.getScaleX() * deltaScale;
                scale = Math.max(minimumScale, Math.min(maximumScale, scale));
                view.setScaleX(scale);
                view.setScaleY(scale);
                adjustZoomContinuous(view, deltaScale);
            }
        }, CONTINUOUS_INTERVAL);
    }

    OnTouchListener onTouchListener = new OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                mContinuousHandler.removeCallbacksAndMessages(null);
            }
            return false;
        }
    };


}
