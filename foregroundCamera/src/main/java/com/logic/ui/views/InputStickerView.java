package com.logic.ui.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.logic.foregroundcamera.R;
import com.logic.photo.filters.ColorChangeFilter;
import com.logic.photo.filters.ForegroundMaskFilter;
import com.logic.photo.filters.MultipleObjectFilter;
import com.logic.photo.filters.PaintFilter;
import com.logic.photo.gestures.MultiTouchListener;
import com.logic.photo.gestures.SingleTouchListener;
import com.logic.photo.gestures.Vector2D;
import com.logic.photo.interfaces.FilterInterfaces;
import com.logic.photo.models.InputFilterInfo;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;

/**
 * @author Dewesh Kumar
 */
public class InputStickerView extends LinearLayout implements FilterInterfaces.OutlineFilterListener,
        FilterInterfaces.ColorFilterListener, FilterInterfaces.PaintFilterListener{
    public static interface OnCopyListener {
        public void onCopy(InputFilterInfo inputFilterInfo);
    }
    private final int[] mActionableIds = {R.id.ivCopy, R.id.ivRemove};
    private MultiTouchListener mMultiTouchListener;
    protected InputFilterInfo mInputFilterInfo;
    protected MultipleObjectFilter mMultiObjectFilter;
    private Rect mBoundingRect;
    private Boolean isActive = false;

    private Bitmap mFilterAppliedBitmap;
    private ForegroundMaskFilter mMaskFilter = null;
    private int mFocusRadius = 15;
    private Context mContext;
    private ImageView mImageView;
    private OnCopyListener onCopyListener;
    private View mZoomRotateView;
    public InputStickerView(Context context) {
        this(context, null);
    }

    public InputStickerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public InputStickerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mContext = context;
        initView();
    }

    private void initView() {
        inflate(mContext, R.layout.view_sticker_input, this);
        mImageView = (ImageView)findViewById(R.id.ivSticker);
        for (int i = 0; i < mActionableIds.length; i++) {
            findViewById(mActionableIds[i]).setOnClickListener(onClickListener);
        }
        mMultiTouchListener = new MultiTouchListener();
        this.setOnTouchListener(mMultiTouchListener);
        int padding = getResources().getDimensionPixelSize(R.dimen.input_image_view_padding);
        this.setPadding(padding, padding, padding, padding);

        mZoomRotateView = findViewById(R.id.ivZoomRotate);
        mZoomRotateView.setOnTouchListener(new SingleTouchListener(new SingleTouchListener.OnTouchChanged() {
            @Override
            public void onTouchChanged(SingleTouchListener.TouchTransformInfo touchTransInfo) {
                rotateZoom(touchTransInfo);

            }
        }));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        /*if (isActive) {
            android.graphics.Rect r = canvas.getClipBounds() ;
            android.graphics.Rect outline = new android.graphics.Rect( 0,0,r.right+2, r.bottom+2) ;
            canvas.drawRect(outline, mPaint) ;
        }*/
    }

    public void setOnSingleTapConfirmedListener(MultiTouchListener.OnSingleTapConfirmed onSingleTapConfirmedListener) {
        mMultiTouchListener.setOnSingleTapConfirmedListener(onSingleTapConfirmedListener);
    }

    public void setBitmapFromMat(Mat mat) {
        trimAndSetBitmap(mat);
    }

    public void setBitmapFromMat(Mat mat, Boolean isRGBImage) {
        trimAndSetBitmap(trimMat(mat));
    }

    public InputFilterInfo getInputFilterInfo() {
        return mInputFilterInfo;
    }

    public void setInputFilterInfo(InputFilterInfo inputFilterInfo) {
        this.mInputFilterInfo = inputFilterInfo;
        this.mMultiObjectFilter = inputFilterInfo.getMultipleObjectFilter();
        mBoundingRect = mMultiObjectFilter.getBoundingRect();
        //this.setImageBitmap(mInputFilterInfo.getIntermediateBitmap());
        mFilterAppliedBitmap = Bitmap.createBitmap(mBoundingRect.width, mBoundingRect.height, Bitmap.Config.ARGB_8888);
    }

    public void setActiveState(Boolean isActive) {
        this.isActive = isActive;
        if (isActive) {
            mImageView.setBackgroundResource(R.drawable.shape_input_img_active_bg);
            findViewById(R.id.ivRemove).setVisibility(View.VISIBLE);
            findViewById(R.id.ivCopy).setVisibility(View.VISIBLE);
        }
        else {
            mImageView.setBackgroundResource(android.R.color.transparent);
            findViewById(R.id.ivRemove).setVisibility(View.GONE);
            findViewById(R.id.ivCopy).setVisibility(View.GONE);
        }
    }

    public void setOnCopyListener(OnCopyListener onCopyListener) {
        this.onCopyListener = onCopyListener;
    }

    OnClickListener onClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivCopy:
                    //Toast.makeText(mContext, "Copy", Toast.LENGTH_SHORT).show();
                    if (onCopyListener != null)
                        onCopyListener.onCopy(mInputFilterInfo);
                    break;
                case R.id.ivRemove:
                    ViewGroup container = ((ViewGroup) InputStickerView.this.getParent());
                    if (container.getChildCount() == 1) {
                        Toast.makeText(mContext, "At least 1 sticker is required. Please add 1 if you want to delete this.", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        container.removeView(InputStickerView.this);
                    }
                    break;
            }
        }
    };

    /************************************************************ Filters ********************************************/
    @Override
    public void applyOutlineFilter(int value) {
        if(mMaskFilter == null){
            mMaskFilter = new ForegroundMaskFilter();
            mMaskFilter.setOriginalMat(mMultiObjectFilter.getOriginalMat());
            mMaskFilter.setMaskMat(mMultiObjectFilter.getMaskMat());
            mMaskFilter.setBlurRadius(mFocusRadius);
        }
        mMaskFilter.applyFilterOnForeground();
        //trimAndSetBitmap(mMaskFilter.getProcessedMat());
        Utils.matToBitmap(mMultiObjectFilter.getTrimmedFGMat(), mFilterAppliedBitmap, true);
        mImageView.setImageBitmap(mFilterAppliedBitmap);
    }


    private ColorChangeFilter mColorChangeFilter = null;
    @Override
    public void applyColorFilter(int newColor) {
        if(mColorChangeFilter== null){
            mColorChangeFilter = new ColorChangeFilter();
            mColorChangeFilter.setOriginalMat(mMultiObjectFilter.getOriginalMat());
            mColorChangeFilter.setMaskMat(mMultiObjectFilter.getMaskMat());
            Scalar meanColor = mMultiObjectFilter.getCurrentMeanColor();
            mColorChangeFilter.setMeanColor((int)meanColor.val[0], (int)meanColor.val[1], (int)meanColor.val[2]);
        }
        //Now just apply filter and reset bitmap
        mColorChangeFilter.applyColorOnForeground(Color.red(newColor), Color.green(newColor), Color.blue(newColor));
        //Utils.matToBitmap(mColorChangeFilter.getProcessedMat(), mFilterAppliedBitmap);
        //this.resetResizedBitmap(mFilterAppliedBitmap);
        //Mat colorFilteredMat = trimMat(mColorChangeFilter.getProcessedMat().submat(mBoundingRect));
        Mat colorFilteredMat = mColorChangeFilter.getProcessedMat().submat(mBoundingRect);
        trimAndSetBitmap(trimMat(colorFilteredMat));
    }

    private PaintFilter mPaintFilter = null;
    private int mPaintRadius = 40;
    @Override
    public void applyPaintFilter(int value) {
        if(mPaintFilter == null){
            mPaintFilter = new PaintFilter();
            mPaintFilter.setOriginalMat(mMultiObjectFilter.getOriginalMat());
            mPaintFilter.setMaskMat(mMultiObjectFilter.getMaskMat());
            mPaintFilter.setBlurRadius(mPaintRadius);
        }
        mPaintFilter.applyFilterOnForeground();
        //Utils.matToBitmap(trimAndSetBitmap();
        //Mat paintAppliedMat = trimMat(mPaintFilter.getProcessedMat()).submat(mBoundingRect);
        //Mat paintAppliedMat = trimMat(mPaintFilter.getProcessedMat().submat(mBoundingRect));
        Mat paintAppliedMat = mPaintFilter.getProcessedMat().submat(mBoundingRect);
        trimAndSetBitmap(trimMat(paintAppliedMat));
        //this.resetResizedBitmap(mFilterAppliedBitmap);
    }

    public void setImageBitmap(Bitmap bitmap) {
        mImageView.setImageBitmap(bitmap);
    }



    private void trimAndSetBitmap(Mat inputMat) {
        //Utils.matToBitmap(trimMat(inputMat).submat(mBoundingRect), mFilterAppliedBitmap, true);
        //Utils.matToBitmap(trimMat(inputMat), mFilterAppliedBitmap, true);
        Utils.matToBitmap(inputMat, mFilterAppliedBitmap, true);
        mImageView.setImageBitmap(mFilterAppliedBitmap);
    }

    private static Mat trimMat(Mat inputMat) {
        return inputMat;
        /*
        Mat dst = new Mat(inputMat.rows(), inputMat.cols(), CvType.CV_8UC4);
        Mat tmp = new Mat(inputMat.rows(), inputMat.cols(), CvType.CV_8UC4);
        Mat alpha = new Mat(inputMat.rows(), inputMat.cols(), CvType.CV_8UC4);
        try {
            // convert image to grayscale
            Imgproc.cvtColor(inputMat, tmp, Imgproc.COLOR_BGR2GRAY);

            // threshold the image to create alpha channel with complete transparency in black background region and zero transparency in foreground object region.
            Imgproc.threshold(tmp, alpha, 100, 255, Imgproc.THRESH_BINARY);

            // split the original image into three single channel.
            List<Mat> rgb = new ArrayList<Mat>(3);
            Core.split(inputMat, rgb);

            // Create the final result by merging three single channel and alpha(BGRA order)
            List<Mat> rgba = new ArrayList<Mat>(4);
            rgba.add(rgb.get(0));
            rgba.add(rgb.get(1));
            rgba.add(rgb.get(2));
            rgba.add(alpha);
            Core.merge(rgba, dst);
            return dst;
        }
        finally {
            tmp.release();
            alpha.release();
        }*/
    }

    private float minimumScale = 0.50f;
    private float maximumScale = 3.0f;
    private int yOffSet = 0;
    private void rotateZoom(SingleTouchListener.TouchTransformInfo touchTransformInfo) {
        PointF currentPoint = touchTransformInfo.currentPoint;
        PointF prevPoint = touchTransformInfo.prevPoint;
        android.graphics.Rect rect = new android.graphics.Rect();
        if (yOffSet == 0)
            yOffSet = getResources().getDimensionPixelSize(R.dimen.yOffSet);
        int[] location = new int[2];
        this.getLocationOnScreen(location);
        //log("C1:"+(int)(this.getX() + this.getWidth()/2)+","+(int)(this.getY() + this.getHeight()/2)
          //      +"; C2:"+rect.centerX()+"," + rect.centerY());
        log("C1:"+(int)(this.getX() + this.getWidth()/2)+","+(int)(this.getY() + this.getHeight()/2)
                +"; C2:"+location[0]+this.getWidth()/2+"," + location[1] + this.getHeight()/2 + yOffSet);

        Vector2D c = new Vector2D(this.getX() + this.getWidth()/2, this.getY() + this.getHeight()/2 + yOffSet);
        //Vector2D c = new Vector2D(location[0]+this.getWidth()/2, location[1] + this.getHeight()/2);

        Vector2D p2 = new Vector2D(currentPoint.x, currentPoint.y);
        Vector2D p1 = new Vector2D(prevPoint.x, prevPoint.y);
        log("lx:" + (int) this.getX() + "; ly:" + (int) this.getY());
       // log("P1:"+(int)prevPoint.x + "," + (int)prevPoint.y + "; P2:"+(int)currentPoint.x + ","+(int)currentPoint.y+"; C:" +
                //(int)(this.getX() + this.getWidth()/2)+","+(int)(this.getY() + this.getHeight()/2));
        //log("P2:"+(int)currentPoint.x + ","+(int)currentPoint.y+"; C:" +
                //(int)(this.getX() + this.getWidth()/2)+","+(int)(this.getY() + this.getHeight()/2 + 80));

        float rotationAngle = Vector2D.getAngle(c, p1, p2);
        /*float rotationAngle =  (float)Geometry.computeAngle(new double[]{this.getX() + this.getWidth()/2,this.getY() + this.getHeight()/2,0},
                new double[]{currentPoint.x, currentPoint.y,0},
                new double[]{prevPoint.x, prevPoint.y,0});*/
        //float angle = this.getRotation() + (float)Math.toDegrees(rotationAngle);
        float angle = this.getRotation() + rotationAngle;
        //log("Angle : " + rotationAngle);
        //log("Current Rotation:" + this.getRotation());
        //log("PivotX="+this.getPivotX() + ";PivotY="+this.getPivotY());
        //angle = angle % 360;
        this.setRotation(angle);

        float scale = this.getScaleX()  *  Vector2D.getScale(c, p1, p2);
        scale = Math.max(minimumScale, Math.min(maximumScale, scale));
        this.setScaleX(scale);
        this.setScaleY(scale);


    }

    private float adjustAngle(float degrees) {
        if (degrees > 180.0f) {
            degrees -= 360.0f;
        } else if (degrees < -180.0f) {
            degrees += 360.0f;
        }

        return degrees;
    }

    private void log(String msg) {
        Log.i("SingleTouchListener", msg);
    }
}
