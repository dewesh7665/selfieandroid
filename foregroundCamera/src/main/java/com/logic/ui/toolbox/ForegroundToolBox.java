package com.logic.ui.toolbox;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.logic.foregroundcamera.R;
import com.logic.photo.filters.BaseObjectFilter;
import com.logic.photo.filters.ForegroundMaskFilter;
import com.logic.photo.filters.GrayFilter;
import com.logic.photo.filters.MultipleObjectFilter;
import com.logic.photo.filters.ToonFilter;
import com.logic.photo.models.InputFilterInfo;
import com.logic.ui.views.InputStickerView;

import org.opencv.android.Utils;
import org.opencv.core.Mat;

import afzkl.development.colorpickerview.view.ColorPickerView;

/**
 * @author Dewesh Kumar
 */
public class ForegroundToolBox extends BaseToolBox {
    private int[] mActionableIds = {R.id.btnOutline, R.id.btnPaint, R.id.btnCartoon,
            R.id.btnColor, R.id.btnClose};
    private LinearLayout mLLSeekBars = null;
    private LinearLayout mLLEdge = null;
    private LinearLayout mLLRadius = null;
    private SeekBar mEdgeSeek = null;
    private SeekBar mBlurSeek = null;

    //Color filter
    private LinearLayout mColorViewLayout = null;;
    private ColorPickerView mColorPickerView;

    private int mFocusRadius = 15;
    private int mCartoonRadius = 40;
    //This is edge radius for toon filter
    private int mEdgeRadius = 50;

    private ForegroundMaskFilter mMaskFilter = null;
    private GrayFilter mGrayFilter = null;
    private ToonFilter mCartoonFilter = null;
    private BaseObjectFilter mBaseFilter = null;
    private MultipleObjectFilter mMultiObjectFilter = null;
    private InputStickerView mRenderView = null;
    private Bitmap mProcessedBitmap = null;
    private InputFilterInfo mInputFilterInfo;

    public ForegroundToolBox(Context context) {
        this(context, null);
    }

    public ForegroundToolBox(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ForegroundToolBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mInflater.inflate(R.layout.toolbox_fg, this);
        init();
    }

    private void init() {
        //Seek bars
        mLLSeekBars = (LinearLayout)findViewById(R.id.llSeekBars);
        mLLEdge = (LinearLayout)findViewById(R.id.llEdge);
        mLLRadius = (LinearLayout)findViewById(R.id.llRadius);
        mEdgeSeek = (SeekBar)findViewById(R.id.seekEdge);
        mBlurSeek = (SeekBar)findViewById(R.id.seekRadius);

        //Color filter
        mColorPickerView = (ColorPickerView) findViewById(R.id.color_picker_view);
        mColorViewLayout = (LinearLayout)findViewById(R.id.llColorView);
        mColorViewLayout.setVisibility(View.GONE);

        setClickListeners();
        configureSeekBars();
        setColorChangeListener();
    }

    public void setImageRenderView(InputStickerView imageRenderView) {
        this.mRenderView = imageRenderView;
        mInputFilterInfo = this.mRenderView.getInputFilterInfo();
        mMultiObjectFilter = mInputFilterInfo.getMultipleObjectFilter();
        //Bitmap intermediateBitmap = mInputFilterInfo.getIntermediateBitmap();
        Mat mat = mInputFilterInfo.getIntermediateMat();
        mProcessedBitmap = Bitmap.createBitmap(mat.rows(), mat.cols(), Bitmap.Config.ARGB_8888);
        //initFilters();
    }

    public void refreshViews() {
        //this.mRenderView.setImageBitmap(mBitmapManager.getBitmapFromPath(mInputFilterInfo.getIntermediateBMPPath()));
        this.mRenderView.setBitmapFromMat(mInputFilterInfo.getIntermediateMat());
    }

    public void resetVisibility() {
        mLLSeekBars.setVisibility(GONE);
        mColorViewLayout.setVisibility(GONE);
    }

    private void setClickListeners() {
        for (int i = 0; i < mActionableIds.length; i++) {
            findViewById(mActionableIds[i]).setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickEvent(v);
                }
            });
        }
    }

    private void onClickEvent(View v) {
        switch (v.getId()) {
            case R.id.btnOutline:
                mRenderView.applyOutlineFilter(0);
                break;

            case R.id.btnPaint:
                //Reset seek bars
                hideSeekBars();
                mLLSeekBars.setVisibility(View.VISIBLE);
                mRenderView.applyPaintFilter(0);
                break;

            case R.id.btnColor:
                //Reset seek bars
                hideSeekBars();
                mColorViewLayout.setVisibility(View.VISIBLE);
                break;

            case R.id.btnClose:
                mColorViewLayout.setVisibility(View.GONE);
                break;
            case R.id.btnBW:
                hideSeekBars();
                if(mGrayFilter == null){
                    mGrayFilter = new GrayFilter();
                    mGrayFilter.setOriginalMat(mMultiObjectFilter.getOriginalMat());
                    mGrayFilter.setMaskMat(mMultiObjectFilter.getMaskMat());
                }
                //Set base filter to null here
                mBaseFilter = null;
                //Now just apply filter and reset bitmap
                mGrayFilter.applyFilterOnForeground();
                Utils.matToBitmap(mGrayFilter.getProcessedMat(), mProcessedBitmap);
                mRenderView.setImageBitmap(mProcessedBitmap);
                break;

            case R.id.btnCartoon:
                /*
                //Reset seek bars
                hideSeekBars();
                mLLSeekBars.setVisibility(View.VISIBLE);
                mLLEdge.setVisibility(View.VISIBLE);
                if(mCartoonFilter == null){
                    mCartoonFilter = new ToonFilter();
                    mCartoonFilter.setOriginalMat(mMultiObjectFilter.getOriginalMat());
                    mCartoonFilter.setMaskMat(mMultiObjectFilter.getMaskMat());
                    mCartoonFilter.setBlurRadius(mCartoonRadius);
                    mCartoonFilter.setEdgeRadius(mEdgeRadius);
                }
                //Set base filter
                mBaseFilter = mCartoonFilter;
                setSeekBarValues();
                applyFilter();
                */
                break;

            case R.id.btnFilter:
                //Filter listener is in holding activity. Code won't reach here because not declared in actionableIds.
                break;
        }
    }

    private void applyFilter(){
        isImageDirty = true;
        mBaseFilter.applyFilterOnForeground();
//        Utils.matToBitmap(mBaseFilter.getProcessedMat(), mProcessedBitmap
        //Utils.matToBitmap(mMultiObjectFilter.getProcessedFGMat(), mProcessedBitmap, true);
        //mRenderView.setImageBitmap(mProcessedBitmap);
        mInputFilterInfo.setIntermediateMat(mMultiObjectFilter.getProcessedFGMat());
        //mRenderView.setBitmapFromMat(mMultiObjectFilter.getProcessedFGMat());
        refreshViews();
        //((ImageView) findViewById(R.id.ivUserImage)).setImageBitmap(mProcessedBitmap);
    }

    private void configureSeekBars(){
        mLLSeekBars.setVisibility(View.GONE);
        mLLEdge.setVisibility(View.GONE);
        //mLLRadius.setVisibility(View.GONE);
        mEdgeSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                if(mBaseFilter != null){
                    int currentProgress = seekBar.getProgress();
                    int[] edgeIntervalVals = mBaseFilter.getEdgeIntervalArray();
                    int actualProgress = edgeIntervalVals != null ? currentProgress + edgeIntervalVals[0] : currentProgress;
                    mBaseFilter.setEdgeRadius(actualProgress);
                    applyFilter();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                // TODO Auto-generated method stub
                //Toast.makeText(ImagePreviewActivity.this, "Progress changed", Toast.LENGTH_LONG).show();
            }
        });

        mBlurSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
                if(mBaseFilter != null){
                    int currentProgress = seekBar.getProgress();
                    int[] blurIntervalVals = mBaseFilter.getBlurIntervalArray();
                    int actualProgress = blurIntervalVals != null ? currentProgress + blurIntervalVals[0] : currentProgress;
                    mBaseFilter.setBlurRadius(actualProgress);
                    applyFilter();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                // TODO Auto-generated method stub

            }
        });

    }

    private void hideSeekBars(){
        mLLSeekBars.setVisibility(View.GONE);
        mLLEdge.setVisibility(View.GONE);
        //mLLRadius.setVisibility(View.GONE);
    }

    private void setSeekBarValues(){
        if(mBaseFilter != null){
            int[] blurIntervalVals = mBaseFilter.getBlurIntervalArray();
            if(blurIntervalVals != null){
                if(blurIntervalVals.length>1)
                {
                    int diffValue = blurIntervalVals[1]- blurIntervalVals[0];
                    mBlurSeek.incrementProgressBy(diffValue);
                }
                int maxValue = blurIntervalVals[blurIntervalVals.length-1] - blurIntervalVals[0];
                mBlurSeek.setMax(maxValue);
                mBlurSeek.setProgress(mBaseFilter.getBlurRadius() - blurIntervalVals[0]);
            }

            int[]edgeIntervalVals = mBaseFilter.getEdgeIntervalArray();
            if(edgeIntervalVals != null){
                if(edgeIntervalVals.length>1)
                {
                    int diffValue = edgeIntervalVals[1]- edgeIntervalVals[0];
                    mBlurSeek.incrementProgressBy(diffValue);
                }
                int maxValue = edgeIntervalVals[blurIntervalVals.length-1] - edgeIntervalVals[0];
                mEdgeSeek.setMax(maxValue);
                mEdgeSeek.setProgress(mBaseFilter.getEdgeRadius()- edgeIntervalVals[0]);
            }
        }
    }

    private void setColorChangeListener(){
        mColorPickerView.setOnColorChangedListener(new ColorPickerView.OnColorChangedListener() {

            @Override
            public void onColorChanged(int newColor) {
                // TODO Auto-generated method stub
                mRenderView.applyColorFilter(newColor);
            }
        });
    }

    /**
     * Will be true when some operation are done on input image.
     * To be used avoid re save image already saved.
     *
     */
    private Boolean isImageDirty = false;
    public Boolean isImageDirty() {
        return isImageDirty;
    }
    public void setImageDirty(Boolean isImageDirty) {
        this.isImageDirty = isImageDirty;
    }



    /**
     * To be called after renderView set.
     */
    private void initFilters() {
        //setMaskFilter();
        //applyOutlineFilter();
    }

    private void applyOutlineFilter(){
        hideSeekBars();
        mLLSeekBars.setVisibility(View.VISIBLE);
        if(mMaskFilter == null){
            mMaskFilter = new ForegroundMaskFilter();
            mMaskFilter.setOriginalMat(mMultiObjectFilter.getOriginalMat());
            mMaskFilter.setMaskMat(mMultiObjectFilter.getMaskMat());
            mMaskFilter.setBlurRadius(mFocusRadius);
        }
        //Set base filter
        mBaseFilter = mMaskFilter;
        setSeekBarValues();
        applyFilter();
    }

    private void setMaskFilter(){

		/*if(mMaskFilter == null){
			mMaskFilter = new ForegroundMaskFilter();
			mMaskFilter.setOriginalMat(mMultiObjectFilter.getOriginalMat());
			mMaskFilter.setMaskMat(mMultiObjectFilter.getMaskMat());
			mMaskFilter.setBlurRadius(mOutlineRadius);
		}
		//Set base filter
		mBaseFilter = mMaskFilter;
		applyFilter();*/
    }

    private void applyReverseFilter(){
        mBaseFilter.applyFilter(true);
        Utils.matToBitmap(mBaseFilter.getProcessedMat(), mProcessedBitmap);
        mRenderView.setImageBitmap(mProcessedBitmap);
    }


}
