package com.logic.ui.toolbox;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;

import com.logic.foregroundcamera.R;
import com.logic.photo.application.PhotoGrabApplication;
import com.logic.ui.TextFilter.AddTextBottomView;
import com.logic.ui.TextFilter.ZoomPanEditTextView;
import com.logic.ui.utils.FontUtil;

/**
 * @author Dewesh Kumar
 */
public class AddTextToolView extends BaseToolBox {
    private ZoomPanEditTextView mZoomPanEditText;
    private EditText mEditText;
    private AddTextBottomView mAddTextBottomView;
    private PhotoGrabApplication mApplication;
    public AddTextToolView(Context context) {
        this(context, null);
    }

    public AddTextToolView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AddTextToolView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mInflater.inflate(R.layout.tool_add_text, this);
        mApplication = PhotoGrabApplication.getInstance();
    }

    public void setZoomPanEditText(ZoomPanEditTextView zoomPanEditText) {
        this.mZoomPanEditText = zoomPanEditText;
        mEditText = (EditText) zoomPanEditText.findViewById(R.id.zoomPanEditText);
        mAddTextBottomView = (AddTextBottomView) findViewById(R.id.addTextBottomView);
        mAddTextBottomView.setOnColorSelectionListener(new AddTextBottomView.OnColorSelectionListener() {
            @Override
            public void onColorSelected(int colorId) {
                mEditText.setTextColor(getResources().getColor(colorId));
            }
        });

        mAddTextBottomView.setOnFontSelectionListener(new AddTextBottomView.OnFontSelectionListener() {
            @Override
            public void onFontSelected(FontUtil.FontFamily fontFamily) {
                FontUtil.setFonts(mContext, mEditText, fontFamily);
            }
        });

        mAddTextBottomView.setOnFilterSelectionListener(new AddTextBottomView.OnFilterSelectionListener() {
            @Override
            public void onFilteringStart() {
                mZoomPanEditText.setEditMode(true);
            }

            @Override
            public void onFilteringEnd() {
                mZoomPanEditText.setEditMode(false);

            }
        });

        mAddTextBottomView.setOnOkCancelListener(new AddTextBottomView.OnOkCancelListener() {
            @Override
            public void onOk() {
                showHideTextTool(GONE);
            }

            @Override
            public void onCancel() {
                showHideTextTool(GONE);
            }
        });

        mAddTextBottomView.setOnSpeechBubbleSelectionListener(new AddTextBottomView.OnSpeechBubbleSelectionListener() {
            @Override
            public void onSpeechBubbleSelection(View view, AddTextBottomView.SpeechBubble speechBubble) {

            }
        });
    }

    private void showHideTextTool(int visibility) {
        this.setVisibility(visibility);
    }
}
