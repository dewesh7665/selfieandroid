package com.logic.ui.toolbox;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.logic.business.image.chooser.ImageChooser;
import com.logic.business.managers.BitmapManager;
import com.logic.foregroundcamera.R;
import com.logic.photo.application.PhotoGrabApplication;
import com.logic.ui.views.BGImageView;

import afzkl.development.colorpickerview.view.ColorPickerView;

/**
 * @author Dewesh Kumar
 */
public class BackgroundToolBox extends BaseToolBox{
    public static interface OnToolsSelectionListener {
        /**
         *
         * @param toolIndex 0 (BG), 1(Foreground Filter), 2(Text), 3(Background Filter)
         */
        public void onToolSelected(int toolIndex);
    }
    private BGImageView mBGImageView;
    private int[] mActionableIds = {R.id.btnChangeBG , R.id.btnChangeBGColor, R.id.btnPaint, R.id.btnColor, R.id.btnFilter};
    private ImageChooser mImageChooser;
    private ViewGroup mContainerProcessedImage;
    //private ImageView ivInput;
    private LinearLayout mColorViewLayout = null;
    private ImageButton mBtnClose = null;
    private ColorPickerView mColorPickerView;
    protected BitmapManager mBitmapManager;
    protected PhotoGrabApplication mApplication;
    private OnToolsSelectionListener mToolSelectionListener;

    public BackgroundToolBox(Context context) {
        this(context, null);
    }

    public BackgroundToolBox(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BackgroundToolBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mInflater.inflate(R.layout.toolbox_bg, this);
        init();
    }

    private void init() {
        mImageChooser = new ImageChooser(mContext);
        mApplication = (PhotoGrabApplication) mContext.getApplicationContext();
        mBitmapManager = new BitmapManager(mContext);
        mApplication = (PhotoGrabApplication)mContext.getApplicationContext();
        for (int i = 0; i < mActionableIds.length; i++) {
            findViewById(mActionableIds[i]).setOnClickListener(onClickListener);
        }

        //Color
        mBtnClose = (ImageButton) findViewById(R.id.btnClose);
        mColorPickerView = (ColorPickerView) findViewById(R.id.color_picker_view);
        mColorViewLayout = (LinearLayout) findViewById(R.id.llColorView);
        mColorViewLayout.setVisibility(View.GONE);
        setColorChangeListener();
    }

    /*public void setImageRenderView(InputImageView imageRenderView) {
        this.mRenderView = imageRenderView;
    }*/


    public void setBGRenderingIds(ViewGroup parent) {
        setBGRenderingIds(parent, null);
    }

    public void setBGRenderingIds(ViewGroup parent, ImageView bgImageView) {
        mContainerProcessedImage = parent;
        if (bgImageView != null)
            mBGImageView = (BGImageView)bgImageView;

        /*if (mApplication.getSelectedBGPath() != null)
            prepareImageWithSelectedBG(mApplication.getSelectedBGPath());
        if(mApplication.getSelectedBGColor() != -1)
            mBGImageView.setBackgroundColor(mApplication.getSelectedBGColor());*/
    }

    public BGImageView getBGImageView() {
        return mBGImageView;
    }

    public void setToolsSelectionListener(OnToolsSelectionListener onToolsSelectionListener) {
        this.mToolSelectionListener = onToolsSelectionListener;
    }

    public void refreshViews() {
       /* if (mApplication.getSelectedBGPath() != null)
            prepareImageWithSelectedBG(mApplication.getSelectedBGPath());*/
        if(mApplication.getSelectedBGColor() != -1)
            mContainerProcessedImage.setBackgroundColor(mApplication.getSelectedBGColor());
    }

    public void resetVisibility() {
        mColorViewLayout.setVisibility(GONE);
    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnChangeBG:
                    showBGOptionPopUp();
                    if (mToolSelectionListener != null)
                        mToolSelectionListener.onToolSelected(0);
                    break;
                case R.id.btnChangeBGColor:
                    mColorViewLayout.setVisibility(View.VISIBLE);
                    mColorPickerView.setOnColorChangedListener(bgColorChangeListener);
                    break;
                case R.id.btnPaint:
                    mBGImageView.applyPaintFilter(0);
                    break;
                case R.id.btnColor:
                    mColorViewLayout.setVisibility(View.VISIBLE);
                    mColorPickerView.setOnColorChangedListener(bgImageFilterColorChangeListener);
                    break;
                case R.id.btnFilter:

                    if (mToolSelectionListener != null)
                        mToolSelectionListener.onToolSelected(3);
                    break;
            }
        }
    };

    private void showBGOptionPopUp() {
        CharSequence options[] = new CharSequence[] {"Camera", "Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Selection Mode");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // the user clicked on colors[which]
                switch (which) {
                    case 0: //Camera
                        mImageChooser.chooseFromCamera(onImageSelectionListener);
                        break;
                    case 1: //Gallery
                        mImageChooser.chooseFromGallery(onImageSelectionListener);
                        break;
                    case 2://Color
                        mColorViewLayout.setVisibility(View.VISIBLE); //Not using since color moved to bottom bar
                        break;

                }
            }
        });
        builder.show();
    }

    ImageChooser.OnImageSelectionListener onImageSelectionListener = new ImageChooser.OnImageSelectionListener() {
        @Override
        public void onImageSelected(String filePath) {
            BackgroundToolBox.this.findViewById(R.id.llBtnContainer).setVisibility(GONE);
            BackgroundToolBox.this.findViewById(R.id.llFilterOption).setVisibility(VISIBLE);
            mApplication.setSelectedBGPath(filePath);
            prepareImageWithSelectedBG(filePath);
        }
    };

    private void prepareImageWithSelectedBG(String filePath) {
        if (mBGImageView == null) {
            mBGImageView = new BGImageView(mContext);
            mContainerProcessedImage.addView(mBGImageView, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }
        //mBGImageView.setImageBitmap(mBitmapManager.getBitmapFromPath(filePath));
        mBGImageView.setResetBitmap(mBitmapManager.getBitmapFromPath(filePath));
        //mBGImageView.setImageURI(Uri.fromFile(new File(filePath)));
        //BitmapUtils.bindBitmapToImageView(filePath, mBGImageView);
    }


    private void setColorChangeListener(){
        mBtnClose.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mColorViewLayout.setVisibility(View.GONE);
            }
        });
    }

    ColorPickerView.OnColorChangedListener bgColorChangeListener = new ColorPickerView.OnColorChangedListener() {
        @Override
        public void onColorChanged(int newColor) {
            mApplication.setSelectedBGColor(newColor);
            mContainerProcessedImage.setBackgroundColor(newColor);
        }
    };

    ColorPickerView.OnColorChangedListener bgImageFilterColorChangeListener = new ColorPickerView.OnColorChangedListener() {
        @Override
        public void onColorChanged(int newColor) {
            mBGImageView.applyColorFilter(newColor);
        }
    };

}
