package com.logic.ui.toolbox;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.logic.business.filters.GPUImageFilterTools;
import com.logic.business.managers.BitmapManager;
import com.logic.foregroundcamera.R;
import com.logic.photo.ui.views.OkCancelView;
import com.logic.ui.UIConstants;

import org.opencv.android.Utils;
import org.opencv.core.Mat;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageView;

/**
 * @author Dewesh Kumar
 */
public class GPUFilterToolView extends BaseToolBox implements OkCancelView.OnActionListner,
        SeekBar.OnSeekBarChangeListener, GPUImageView.OnPictureSavedListener{
    public interface OnFilterApplied {
        void onFilterApplied(Mat mat, Bitmap bitmap);
    }
    private GPUImageFilter mFilter;
    private GPUImageFilterTools.FilterAdjuster mFilterAdjuster;
    private GPUImageView mGPUImageView;
    private LinearLayout llParentScroll;
    private int mFilterThumbSize;
    //private Uri mOriginalImageUri;
    private GPUImage mGPUImage;
    private BitmapManager mBitmapManager;
    private Mat mInputMat;
    private OnFilterApplied onFilterApplied;

    public GPUFilterToolView(Context context) {
        this(context, null);
    }

    public GPUFilterToolView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public GPUFilterToolView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mInflater.inflate(R.layout.tool_gpu_filter, this);
        mBitmapManager = new BitmapManager(mContext);
        ((SeekBar) findViewById(R.id.seekBar)).setOnSeekBarChangeListener(this);
        ((OkCancelView) findViewById(R.id.viewOkCancel)).setOnActionListner(this);
        mGPUImage = new GPUImage(mContext);
        mFilterThumbSize = getResources().getDimensionPixelSize(R.dimen.filter_thumb_size);

    }

    public void setGPUImageView(GPUImageView gpuImageView) {
        this.mGPUImageView = gpuImageView;
    }

    /*public void initFilterOptions(InputFilterInfo inputFilterInfo) {
        //String interimPath = mInputFilterInfo.getIntermediateBMPPath();

        //Uri imageUri = FileIOUtil.getUriOfFile(interimPath);
        //Mat mat = inputFilterInfo.getIntermediateMat();
        Mat mat = inputFilterInfo.getMultipleObjectFilter().getProcessedFGMat();
        Bitmap bitmap = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(mat, bitmap);

        mGPUImageView.setImage(bitmap);
        ((ViewGroup)this.mGPUImageView.getParent()).setVisibility(VISIBLE);
        ((ViewGroup)this.mGPUImageView.getParent()).bringToFront();
        //mGPUImageView.bringToFront();
        //Bitmap bitmap = mBitmapManager.getBitmapFromPath(interimPath, mFilterThumbSize, mFilterThumbSize);
        //mGPUImage.setImage(mBitmapManager.getTrimmedBitmap(bitmap));
        Bitmap thumb = ThumbnailUtils.extractThumbnail(bitmap, mFilterThumbSize, mFilterThumbSize);
        mGPUImage.setImage(thumb);
        prepareHorScrollView();
    }*/

//    public void initFilterOptions(String filePath, OnFilterApplied onFilterApplied) {
//        //mOriginalImageUri = FileIOUtil.getUriOfFile(filePath);
//        this.onFilterApplied = onFilterApplied;
//        mGPUImageView.setImage(FileIOUtil.getUriOfFile(filePath));
//        Bitmap bitmap = mBitmapManager.getBitmapFromPath(filePath, mFilterThumbSize, mFilterThumbSize);
//        //mGPUImage.setImage(mBitmapManager.getTrimmedBitmap(bitmap));
//        mGPUImage.setImage(bitmap);
//        prepareHorScrollView();
//    }

    public void initFilterOptions(String filePath, OnFilterApplied onFilterApplied) {
        Mat mat = new Mat();
        Bitmap bitmap = mBitmapManager.getBitmapFromPath(filePath, mat.cols(), mat.rows());
        Utils.bitmapToMat(bitmap, mat);
        initFilterOptions(mat, onFilterApplied);
    }

    public void initFilterOptions(Mat mat, OnFilterApplied onFilterApplied) {
        //this.mInputFilterInfo = inputFilterInfo;
        //String interimPath = mInputFilterInfo.getIntermediateBMPPath();

        //Uri imageUri = FileIOUtil.getUriOfFile(interimPath);
        //Mat mat = inputFilterInfo.getIntermediateMat();
        //Mat mat = inputFilterInfo.getMultipleObjectFilter().getProcessedFGMat();
        this.onFilterApplied = onFilterApplied;
        mInputMat = mat;
        Bitmap bitmap = Bitmap.createBitmap(mat.cols(), mat.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(mInputMat, bitmap);
        mGPUImageView.setImage(bitmap);
        ((ViewGroup)this.mGPUImageView.getParent()).setVisibility(VISIBLE);
        ((ViewGroup)this.mGPUImageView.getParent()).bringToFront();
        //mGPUImageView.bringToFront();
        //Bitmap bitmap = mBitmapManager.getBitmapFromPath(interimPath, mFilterThumbSize, mFilterThumbSize);
        //mGPUImage.setImage(mBitmapManager.getTrimmedBitmap(bitmap));
        Bitmap thumb = ThumbnailUtils.extractThumbnail(bitmap, mFilterThumbSize, mFilterThumbSize);
        mGPUImage.setImage(thumb);
        prepareHorScrollView();
    }

    @Override
    public void onOk() {
        saveImage();
    }

    @Override
    public void onCancel() {
        goBack();
    }

    private void goBack(){
        this.setVisibility(GONE);
        this.mGPUImageView.getGPUImage().deleteImage();
        this.mGPUImageView.setVisibility(GONE);
        ((ViewGroup)this.mGPUImageView.getParent()).setVisibility(GONE);
    }

    @Override
    public void onPictureSaved(final Uri uri) {
        hideProgressDialog();
        //showToast(UIConstants.SUCCESS_MSG_DEFAULT);
        //PhotoGrabApplication.getInstance().setIntermediateBMPPath(mBitmapManager.getPathFromUri(uri));
        Mat mat = new Mat();
        Bitmap bitmap = mBitmapManager.getBitmapFromPath(mBitmapManager.getPathFromUri(uri), mInputMat.cols(), mInputMat.rows());
        Utils.bitmapToMat(bitmap, mat);
        if (this.onFilterApplied != null)
            this.onFilterApplied.onFilterApplied(mat, bitmap);
        //bitmap.recycle();
        //mInputFilterInfo.setIntermediateMat(mat);
        //mInputFilterInfo.setIntermediateBMPPath(mBitmapManager.getPathFromUri(uri));
        goBack();
    }

    private void saveImage() {
        showProgressDialog(UIConstants.PROGRESS_MSG_DEFAULT);
        String fileName = System.currentTimeMillis() + ".png";
        mGPUImageView.saveToPictures(UIConstants.FOLDER_NAME,fileName,mInputMat.cols(), mInputMat.rows(), this);
        //mGPUImageView.saveToPictures("GPUImage", fileName, 1600, 1600, this);
    }

    private void switchFilterTo(final GPUImageFilter filter) {
        if (mFilter == null
                || (filter != null && !mFilter.getClass().equals(filter.getClass()))) {
            mFilter = filter;
            //mGPUImageView.setFilter(mFilter);
            mGPUImageView.setFilter(mFilter);
            mFilterAdjuster = new GPUImageFilterTools.FilterAdjuster(mFilter);

            findViewById(R.id.seekBar).setVisibility(
                    mFilterAdjuster.canAdjust() ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onProgressChanged(final SeekBar seekBar, final int progress, final boolean fromUser) {
        if (mFilterAdjuster != null) {
            mFilterAdjuster.adjust(progress);
        }
        mGPUImageView.requestRender();
    }

    @Override
    public void onStartTrackingTouch(final SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(final SeekBar seekBar) {
    }

    private void prepareHorScrollView() {
        llParentScroll = (LinearLayout) findViewById(R.id.llScrollParent);
        llParentScroll.removeAllViews();
        GPUImageFilterTools.FilterList filterList = GPUImageFilterTools.getFilterList();
        for (int i = 0; i < filterList.filters.size(); i++) {
            GPUImageFilter gpuImageFilter = GPUImageFilterTools.createFilterForType(mContext, filterList.filters.get(i));
            ImageView imageView = new ImageView(mContext);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(mFilterThumbSize, mFilterThumbSize);
            layoutParams.setMargins(5,5,5,5);
            imageView.setLayoutParams(layoutParams);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setOnClickListener(filterOnClickListener);
            //imageView.setBackgroundResource(R.drawable.shape_round_rect_glass);
            imageView.setBackgroundColor(getResources().getColor(R.color.material_deep_teal_200));
            imageView.setTag(gpuImageFilter);
            new ThumbCreator(gpuImageFilter, imageView, i).execute();
        }
    }



    private class ThumbCreator extends AsyncTask<Void, Void, Bitmap> {
        GPUImageFilter gpuImageFilter;
        ImageView imageView;
        int filterIndex;
        private ThumbCreator(final GPUImageFilter gpuImageFilter, final ImageView imageView, final int index) {
            this.gpuImageFilter = gpuImageFilter;
            this.imageView = imageView;
            this.filterIndex = index;
        }
        @Override
        protected Bitmap doInBackground(Void... params) {
            mGPUImage.setFilter(gpuImageFilter);
            Bitmap bitmap = null;
            try {
                bitmap = mGPUImage.getBitmapWithFilterApplied();
            }
            catch (Exception ex) {}
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (bitmap != null) {
                imageView.setImageBitmap(bitmap);
                llParentScroll.addView(imageView);
            }
            /*if (filterIndex == mMaxFilterCount - 1) {
                onHorizontalScrollPrepared();
            }*/
        }
    }




    View.OnClickListener filterOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            GPUImageFilter gpuImageFilter = (GPUImageFilter) v.getTag();
            switchFilterTo(gpuImageFilter);
            mGPUImageView.requestRender();
            //mGPUImageView.requestRender();
        }
    };

    private void onHorizontalScrollPrepared() {
        //mGPUImageView.setImage(mOriginalImageUri);
    }

    public void showHideOkCancel(boolean isToBeShown) {
        if (isToBeShown)
            findViewById(R.id.viewOkCancel).setVisibility(VISIBLE);
        else
            findViewById(R.id.viewOkCancel).setVisibility(GONE);
    }

}
