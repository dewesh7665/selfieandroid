package com.logic.ui.toolbox;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.logic.business.managers.BitmapManager;

/**
 * @author Dewesh Kumar
 */
public class BaseToolBox extends LinearLayout {
    protected Context mContext;
    protected LayoutInflater mInflater;
    private ProgressDialog mProgressDialog;
    protected BitmapManager mBitmapManager;
    public BaseToolBox(Context context) {
        this(context, null);
    }

    public BaseToolBox(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseToolBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        mInflater = LayoutInflater.from(mContext);
        mBitmapManager = new BitmapManager(mContext);
    }

    public void showProgressDialog(String text) {
        try {
            if(mProgressDialog == null){
                mProgressDialog = ProgressDialog.show(mContext, "Alert!", text + "\t\t\t\t\t", true, false);
                mProgressDialog.setCancelable(true);
            }
            else{
                if(!mProgressDialog.isShowing()){
                    mProgressDialog.show();
                }
            }
        } catch (Exception ex) {
            mProgressDialog = null;
        }
    }

    public void hideProgressDialog() {
        try{
            if(mProgressDialog != null && mProgressDialog.isShowing())
                mProgressDialog.dismiss();
        }
        catch (IllegalArgumentException e) {
            mProgressDialog = null;
            //Never mind
            //Will throw if progress dialog has not attached to windows.If not attached then no need to hide
        }
    }

    public void showToast(String msg) {
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }
}
