package com.logic.ui.utils;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

/**
 * @author Dewesh Kumar
 */
public class UIUtil {

    public static void showHide(final View view) {
        Boolean isToBeShown = view.getVisibility() == View.VISIBLE ? false : true;
        showHide(view, isToBeShown);
    }

    static int viewVisibility = View.GONE;
    public static void showHide(final View view, Boolean isToBeShown) {
        int height = view.getHeight();
        if (height == 0) {
            viewVisibility = isToBeShown ? View.VISIBLE : View.GONE;
            view.setVisibility(viewVisibility);
            return;
        }
        TranslateAnimation animate = null;
        if (isToBeShown) {
            animate = new TranslateAnimation(0, 0, height, 0);
            viewVisibility = View.VISIBLE;
        }
        else {
            animate = new TranslateAnimation(0, 0, 0, height);
            viewVisibility = View.GONE;
        }
        animate.setDuration(300);
        animate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(viewVisibility);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animate);
    }
}
