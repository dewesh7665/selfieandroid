package com.logic.ui.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * @author Dewesh Kumar
 */
public class FontUtil {
   //public enum FontFamily{ARIAL,GEORGIA,HELVETICA,ROBOTO_REGULAR,MAYFLOWER,ROBOTO_LIGHT,ROBOTO_BOLD,ROBOTO_THIN,ROBOTO_SLAB_LIGHT,ROBOTO_SLAB_THIN,ROBOTO_SLAB_REGULAR,ROBOTO_MEDIUM,lANGUAGE_FONT};
    public enum FontFamily{ROBOTO_SLAB_REGULAR, PRECIUOS, TANGERINE_BOLD, TANGERINE_REGULAR};
//    private static Typeface robotoSlabRegular = null;
//    private static Typeface preciuos = null;
//    private static Typeface tangerine_bold = null;
//    private static Typeface tangerine_regular = null;
    private static Typeface[] typefaces = new Typeface[4];
    public static Typeface getTypeFace(Context context, String path) {
                return Typeface.createFromAsset(context.getAssets(),path);
    }

    public static void setFontToTextView(final Context context, final View v,FontFamily fontFamily) {
        if (v instanceof TextView) {
            if(((TextView) v).getTypeface() != null){
                ((TextView)v).setTypeface(getTypeface(context,fontFamily),((TextView) v).getTypeface().getStyle());
            }
            else{
                ((TextView)v).setTypeface(getTypeface(context,fontFamily));
            }
        }
    }

    /**
     * Will recursively search for views in root view and set the fontfamily suppiled
     * keeping rest of the style same as in xml.
     * @param context
     * @param v
     * @param fontFamily enum FontFamily.ARIAL,GEORGIA
     */
    public static void setFonts(final Context context, final View v,FontFamily fontFamily) {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    setFonts(context, child,fontFamily);
                }
            }
            else if (v instanceof TextView) {
                if(((TextView) v).getTypeface() != null){
                    ((TextView)v).setTypeface(getTypeface(context,fontFamily),((TextView) v).getTypeface().getStyle());
                }
                else{
                    ((TextView)v).setTypeface(getTypeface(context,fontFamily));
                }
            }
            else if (v instanceof Button) {
                ((Button)v).setTypeface(getTypeface(context,fontFamily),((Button) v).getTypeface().getStyle());
            }
            else if (v instanceof EditText) {
                ((EditText)v).setTypeface(getTypeface(context,fontFamily),((EditText) v).getTypeface().getStyle());
            }
        } catch (Exception e) {
            e.printStackTrace();
            // ignore
        }
    }

    private static Typeface getTypeface(final Context context,FontFamily fontFamily){
        Typeface typeFace = null;
        switch (fontFamily) {
            case ROBOTO_SLAB_REGULAR:
                if (typefaces[FontFamily.ROBOTO_SLAB_REGULAR.ordinal()] == null)
                    typefaces[FontFamily.ROBOTO_SLAB_REGULAR.ordinal()] = getTypeFace(context, "fonts/RobotoSlab-Regular.ttf");
                typeFace = typefaces[FontFamily.ROBOTO_SLAB_REGULAR.ordinal()];
                break;
            case PRECIUOS:
                if (typefaces[FontFamily.PRECIUOS.ordinal()] == null)
                    typefaces[FontFamily.PRECIUOS.ordinal()] = getTypeFace(context, "fonts/precious.ttf");
                typeFace = typefaces[FontFamily.PRECIUOS.ordinal()];
                break;
            case TANGERINE_BOLD:
                if (typefaces[FontFamily.TANGERINE_BOLD.ordinal()] == null)
                    typefaces[FontFamily.TANGERINE_BOLD.ordinal()] = getTypeFace(context, "fonts/tangerine_bold.ttf");
                typeFace = typefaces[FontFamily.TANGERINE_BOLD.ordinal()];
                break;
            case TANGERINE_REGULAR:
                if (typefaces[FontFamily.TANGERINE_REGULAR.ordinal()] == null)
                    typefaces[FontFamily.TANGERINE_REGULAR.ordinal()] = getTypeFace(context, "fonts/tangerine_regular.ttf");
                typeFace = typefaces[FontFamily.TANGERINE_REGULAR.ordinal()];
                break;
            default:
                break;
        }
        return typeFace;
    }
}
