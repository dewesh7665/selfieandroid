package com.logic.GPUFilters.UI;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.logic.business.image.chooser.ActivityResultManager;
import com.logic.business.image.chooser.ImageChooser;
import com.logic.foregroundcamera.R;

/**
 * @author Dewesh Kumar
 */
public class GPUHomeActivity extends GPUBaseActivity {
    private ImageChooser mImageChooser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gpu_act_home);
        mImageChooser = new ImageChooser(this);
        initListeners();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initListeners() {
        findViewById(R.id.abh_cusBtn_camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImageChooser.chooseFromCamera(new ImageChooser.OnImageSelectionListener() {
                    @Override
                    public void onImageSelected(String filePath) {
                        goToFilterPreview(filePath);
                    }
                });
            }
        });

        findViewById(R.id.abh_cusBtn_gallary).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mImageChooser.chooseFromGallery(new ImageChooser.OnImageSelectionListener() {
                    @Override
                    public void onImageSelected(String filePath) {
                        goToFilterPreview(filePath);
                    }
                });
            }
        });
    }

    private void goToFilterPreview(String filePath) {
        Intent intent = new Intent(GPUHomeActivity.this, GPUFilterPreviewActivity.class);
        intent.putExtra(UIConstants.EXTRA_IMAGE_PATH, filePath);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ActivityResultManager.getInstance().updateActivityResultListeners(requestCode, resultCode, data);
    }
}
