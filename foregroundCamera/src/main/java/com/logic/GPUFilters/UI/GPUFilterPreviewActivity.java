package com.logic.GPUFilters.UI;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logic.GPUFilters.business.Filters.MultiFilters.DailyFilter;
import com.logic.GPUFilters.business.Filters.MultiFilters.DramaFilter;
import com.logic.GPUFilters.business.Filters.MultiFilters.GrungeFilter;
import com.logic.GPUFilters.business.Filters.MultiFilters.RetroFilter;
import com.logic.GPUFilters.business.ImageProcessor;
import com.logic.GPUFilters.business.utils.ThumbCreator;
import com.logic.business.managers.BitmapManager;
import com.logic.foregroundcamera.R;
import com.logic.photo.ui.views.CustomButtonView;
import com.logic.ui.toolbox.GPUFilterToolView;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageView;

/**
 * @author Dewesh Kumar
 */
public class GPUFilterPreviewActivity extends GPUBaseActivity {
    private enum FilterOption {Daily,Paper,Vintage,Retro,Drama,BW,Glow};

    private GPUFilterToolView mGPUFilterToolView;
    private String mFilePath;
    private Bitmap mInputBitmap;
    private ImageProcessor mImageProcessor;
    private GPUImageView mGPUImageView;
    private ThumbCreator mThumbCreator;
    private LinearLayout llHSThumbs;
    private int mFilterThumbSize;
    private FilterOption mCurrentFilterOption = FilterOption.Retro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gpu_act_filter_preview);
        llHSThumbs = (LinearLayout) findViewById(R.id.llScrollParentV2);
        mFilterThumbSize = getResources().getDimensionPixelSize(R.dimen.filter_thumb_size);
        mImageProcessor = new ImageProcessor();
        mFilePath = getIntent().getStringExtra(UIConstants.EXTRA_IMAGE_PATH);
        mInputBitmap = new BitmapManager(this).getBitmapFromPath(mFilePath);
        mGPUImageView = (GPUImageView) findViewById(R.id.gpuImageView);
        mGPUImageView.setImage(mInputBitmap);
        mThumbCreator = new ThumbCreator(this, mInputBitmap);
        Log.i("FilterPreview", mFilePath + "");
        initGPUFilterOptions();
        setDefaultFilter();
        initGPUSubFilterOptions();
    }

    private void setDefaultFilter() {
        DailyFilter dailyFilter = new DailyFilter();
        dailyFilter.setInputImage(mInputBitmap);
        mGPUImageView.setFilter(dailyFilter.applyFilter(DailyFilter.FilterType.LBFilterPunch));
        mGPUImageView.requestRender();
        /*RetroFilter retroFilter = new RetroFilter();
        retroFilter.setInputImage(mInputBitmap);
        mGPUImageView.setFilter(retroFilter.applyFilter(RetroFilter.RetroFilterType.LBRetroFilter5));
        mGPUImageView.requestRender();*/
    }

    /*private void initGPUSubFilterOptions() {
        mGPUFilterToolView = (GPUFilterToolView) findViewById(R.id.gpuToolsView);
        mGPUFilterToolView.showHideOkCancel(false);
        mGPUFilterToolView.setGPUImageView(mGPUImageView);
        mGPUFilterToolView.initFilterOptions(mFilePath,
                new GPUFilterToolView.OnFilterApplied() {
                    @Override
                    public void onFilterApplied(Mat mat, Bitmap bitmap) {

                    }
                });
    }*/

    private void initGPUSubFilterOptions() {
        llHSThumbs.removeAllViews();
        Bitmap inputThumb = ThumbnailUtils.extractThumbnail(mInputBitmap, mFilterThumbSize, mFilterThumbSize);
        switch (mCurrentFilterOption) {
            case Retro:
                for (RetroFilter.RetroFilterType retroFilterType : RetroFilter.RetroFilterType.values()) {
                    RetroFilter retroFilter = new RetroFilter();
                    retroFilter.setInputImage(inputThumb);
                    createThumb(retroFilter.applyFilter(retroFilterType), retroFilterType.toString());
                }
                break;
            case Drama:
                for (DramaFilter.DramaMode dramaMode : DramaFilter.DramaMode.values()) {
                    DramaFilter dramaFilter = new DramaFilter();
                    dramaFilter.setInputImage(inputThumb);
                    createThumb(dramaFilter.applyFilter(dramaMode), dramaMode.toString());
                }
                break;
            case Paper:
                for (int i = 1; i <=10; i++) {
                    int textureId = getResources().getIdentifier("old"+i, "drawable", getPackageName());
                    GrungeFilter grungeFilter = new GrungeFilter();
                    grungeFilter.setInputImage(inputThumb);
                    GPUImageFilter gpuImageFilter = grungeFilter.applyFilterWithTextureImage(BitmapFactory.decodeResource(getResources(), textureId));
                    createThumb(gpuImageFilter, "old"+i);
                }
                break;
            case Daily:
                for (DailyFilter.FilterType dailyFilterType : DailyFilter.FilterType.values()) {
                    DailyFilter dailyFilter = new DailyFilter();
                    dailyFilter.setInputImage(inputThumb);
                    dailyFilter.setOrgInputImage(mInputBitmap);
                    createThumb(dailyFilter.applyFilter(dailyFilterType), dailyFilterType.toString());
                }
                //testDailyFilter();
                break;
            case Vintage:

                break;
        }
    }

    private void createThumb(GPUImageFilter gpuImageFilter) {
       createThumb(gpuImageFilter, null);
    }

    private void createThumb(GPUImageFilter gpuImageFilter, final String text) {
        mThumbCreator.createThumb(gpuImageFilter, new ThumbCreator.OnThumbCreated() {
            @Override
            public void onThumbCreated(GPUImageFilter gpuImageFilter, Bitmap bitmap) {
                appendToScrollView(gpuImageFilter, bitmap, text);
            }
        });
    }

    private void appendToScrollView(GPUImageFilter gpuImageFilter, Bitmap bitmap, String text) {
        /*ImageView imageView = new ImageView(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(mFilterThumbSize, mFilterThumbSize);
        layoutParams.setMargins(5, 5, 5, 5);
        imageView.setLayoutParams(layoutParams);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        imageView.setOnClickListener(filterOnClickListener);
        //imageView.setBackgroundResource(R.drawable.shape_round_rect_glass);
        imageView.setBackgroundColor(getResources().getColor(R.color.material_deep_teal_200));
        imageView.setTag(gpuImageFilter);
        imageView.setImageBitmap(bitmap);
        llHSThumbs.addView(imageView);*/

        View filterOptionView = LayoutInflater.from(this).inflate(R.layout.view_filter_option, null);
        filterOptionView.setTag(gpuImageFilter);
        filterOptionView.setOnClickListener(filterOnClickListener);
        ((ImageView)filterOptionView.findViewById(R.id.ivFilterThumb)).setImageBitmap(bitmap);
        if (text != null) {
            filterOptionView.findViewById(R.id.tvFilterText).setVisibility(View.VISIBLE);
            ((TextView) filterOptionView.findViewById(R.id.tvFilterText)).setText(text);
        }
        else
            filterOptionView.findViewById(R.id.tvFilterText).setVisibility(View.GONE);
        llHSThumbs.addView(filterOptionView);
        if (llHSThumbs.getChildCount() == 1) {
            llHSThumbs.getChildAt(0).performClick();
        }
    }

    View.OnClickListener filterOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            GPUImageFilter gpuImageFilter = (GPUImageFilter) v.getTag();
           renderFilter(gpuImageFilter);
        }
    };

    private void renderFilter(GPUImageFilter gpuImageFilter) {
        mGPUImageView.setFilter(gpuImageFilter);
        mGPUImageView.requestRender();
    }


    private View lastSelectedView = null;
    private void initGPUFilterOptions() {
        LinearLayout menuContainer = (LinearLayout) findViewById(R.id.llContainerButtons);

        for (final FilterOption filterOption : FilterOption.values()) {
            CustomButtonView customButtonView = new CustomButtonView(this);
            customButtonView.setText(filterOption.name());
            customButtonView.setTag(filterOption);
            customButtonView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (lastSelectedView != null) {
                        ((CustomButtonView) lastSelectedView).updateSelection(false);
                    }
                    lastSelectedView = v;
                    ((CustomButtonView) v).updateSelection(true);
                    onFilterOptionSelected((FilterOption)v.getTag());
                }
            });
            menuContainer.addView(customButtonView);
            if (menuContainer.getChildCount() == 1) {
                menuContainer.getChildAt(0).performClick();
            }
        }
    }

    private void onFilterOptionSelected(FilterOption filterOption) {
        mCurrentFilterOption = filterOption;
        initGPUSubFilterOptions();
        /*Toast.makeText(GPUFilterPreviewActivity.this, filterOption.name(), Toast.LENGTH_SHORT).show();
        //Test Retro filters
        GPUImageFilter imageFilter = mImageProcessor.processImageForRetro(mInputBitmap, RetroFilter.RetroFilterType.LBRetroFilter5);
        mGPUImageView.setFilter(imageFilter);
        mGPUImageView.requestRender();*/
    }



    /**************************** Tests *******************************/
    private void testGrungeFilter() {
        GrungeFilter grungeFilter = new GrungeFilter();
        grungeFilter.setInputImage(mInputBitmap);
        GPUImageFilter gpuImageFilter = grungeFilter.applyFilterWithTextureImage(BitmapFactory.decodeResource(getResources(), R.drawable.old1));
        renderFilter(gpuImageFilter);
    }

    private void testDailyFilter() {
        DailyFilter dailyFilter = new DailyFilter();
        dailyFilter.setInputImage(this.mInputBitmap);
        renderFilter(dailyFilter.applyFilter(DailyFilter.FilterType.LBFilterMorning));
    }

}
