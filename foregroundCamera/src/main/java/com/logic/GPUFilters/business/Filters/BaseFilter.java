package com.logic.GPUFilters.business.Filters;

import android.graphics.Bitmap;

/**
 * @author Dewesh Kumar
 */
public class BaseFilter {
    protected Bitmap inputImage;
    protected Bitmap orgInputImage;
    public Bitmap getInputImage() {
        return inputImage;
    }

    public void setInputImage(Bitmap inputImage) {
        this.inputImage = inputImage;
    }

    public Bitmap getOrgInputImage() {
        return orgInputImage;
    }

    public void setOrgInputImage(Bitmap orgInputImage) {
        this.orgInputImage = orgInputImage;
    }

    public Bitmap createImageFromColor(int color) {
        Bitmap bmp = Bitmap.createBitmap(inputImage.getWidth(), inputImage.getHeight(), Bitmap.Config.ARGB_8888); // this creates a MUTABLE bitmap
        //bmp.eraseColor(Color.HSVToColor(new float[]{1.0f, 0.4f, 0.7f}));
        //bmp.eraseColor(Color.HSVToColor(new float[]{this.hueVal * 360f, this.satVal, 0.7f}));
        //bmp.eraseColor(Color.HSVToColor(255, new float[]{this.hueVal * 360f * 1.5f, this.satVal, 0.7f}));
        bmp.eraseColor(color);
        return bmp;
    }
}
