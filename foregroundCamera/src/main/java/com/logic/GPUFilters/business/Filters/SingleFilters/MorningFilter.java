package com.logic.GPUFilters.business.Filters.SingleFilters;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PointF;

import com.logic.GPUFilters.business.Filters.BaseFilter;

import java.util.ArrayList;
import java.util.List;

import jp.co.cyberagent.android.gpuimage.GPUImageContrastFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilterGroup;
import jp.co.cyberagent.android.gpuimage.GPUImageGammaFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageMultiplyBlendFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageSaturationFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageToneCurveFilter;

/**
 * @author Dewesh Kumar
 */
public class MorningFilter extends BaseFilter {
    public GPUImageFilter applyFilter() {
        int textureColor = Color.argb(255, 252, 243, 214);
        Bitmap textureImage = createImageFromColor(textureColor, this.inputImage.getWidth(), this.inputImage.getWidth());
        List<GPUImageFilter> filterList = new ArrayList<>();
        GPUImageMultiplyBlendFilter multiplyBlendFilter = new GPUImageMultiplyBlendFilter();
        multiplyBlendFilter.setBitmap(textureImage);
        filterList.add(multiplyBlendFilter);

        //Now apply GPUImageSaturation filter and desaturate the image a bit
        GPUImageSaturationFilter gpuImageSaturationFilter = new GPUImageSaturationFilter();
        gpuImageSaturationFilter.setSaturation(0.7f);
        filterList.add(gpuImageSaturationFilter);

        //Now apply gamma filter and brightne up by 20%
        GPUImageGammaFilter gpuImageGammaFilter = new GPUImageGammaFilter();
        gpuImageGammaFilter.setGamma(0.85f);
        filterList.add(gpuImageGammaFilter);

        //Now apply red tone curve filter and brighten red part of shadows..
        GPUImageToneCurveFilter toneCurveFilter = new GPUImageToneCurveFilter();
        toneCurveFilter.setRedControlPoints(new PointF[]{new PointF(0.0f, 0.1f), new PointF(1.0f, 1.0f)});
        filterList.add(toneCurveFilter);

        //Now add and increase contrast a bit
        GPUImageContrastFilter gpuImageContrastFilter = new GPUImageContrastFilter();
        gpuImageContrastFilter.setContrast(1.20f);
        filterList.add(gpuImageContrastFilter);

        GPUImageSaturationFilter gpuImageSaturationFilterNext = new GPUImageSaturationFilter();
        gpuImageSaturationFilterNext.setSaturation(0.85f);
        filterList.add(gpuImageSaturationFilterNext);

        return new GPUImageFilterGroup(filterList);
    }

    private Bitmap createImageFromColor(int color, int width, int height) {
        Bitmap bmp = Bitmap.createBitmap(inputImage.getWidth(), inputImage.getHeight(), Bitmap.Config.ARGB_8888); // this creates a MUTABLE bitmap
        //bmp.eraseColor(Color.HSVToColor(new float[]{1.0f, 0.4f, 0.7f}));
        //bmp.eraseColor(Color.HSVToColor(new float[]{this.hueVal * 360f, this.satVal, 0.7f}));
        //bmp.eraseColor(Color.HSVToColor(255, new float[]{this.hueVal * 360f * 1.5f, this.satVal, 0.7f}));
        bmp.eraseColor(color);
        return bmp;
    }

    private float convertHueTo360(float hueVal) {
        return hueVal * 360f;
    }
}
