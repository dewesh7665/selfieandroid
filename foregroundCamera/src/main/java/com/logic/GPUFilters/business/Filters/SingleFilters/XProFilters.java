package com.logic.GPUFilters.business.Filters.SingleFilters;

import android.graphics.PointF;

import com.logic.GPUFilters.business.Filters.BaseFilter;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageToneCurveFilter;

/**
 * @author Dewesh Kumar
 */
public class XProFilters extends BaseFilter {
    public GPUImageFilter applyCrossProcessFilter() {
        GPUImageToneCurveFilter toneCurveFilter = new GPUImageToneCurveFilter();
        PointF[] rgbControlPoints = new PointF[]{new PointF(0.0f, 0.0f), new PointF(0.20f, 0.15f),
                new PointF(0.65f,0.70f), new PointF(1.0f, 1.0f)};
        PointF[] blueControlPoints = new PointF[]{new PointF(0.0f, 0.15f), new PointF(1.0f, 0.85f)};
        PointF[] redControlPoints = new PointF[]{new PointF(0.0f, 0.0f), new PointF(0.20f, 0.10f),
                new PointF(0.65f,0.75f), new PointF(1.0f, 1.0f)};
        toneCurveFilter.setRgbCompositeControlPoints(rgbControlPoints);
        toneCurveFilter.setBlueControlPoints(blueControlPoints);
        toneCurveFilter.setRedControlPoints(redControlPoints);
        return toneCurveFilter;
    }
}
