package com.logic.GPUFilters.business.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.os.AsyncTask;

import com.logic.foregroundcamera.R;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;

/**
 * @author Dewesh Kumar
 */
public class ThumbCreator {
    public interface OnThumbCreated{
        void onThumbCreated(GPUImageFilter gpuImageFilter, Bitmap bitmap);
    };

    private Context mContext;
    private GPUImage mGPUImage;
    private int mFilterThumbSize;
    private Bitmap mBitmap;

    public ThumbCreator(Context context, Bitmap bitmap) {
        mContext = context;
        mGPUImage = new GPUImage(mContext);
        mFilterThumbSize = mContext.getResources().getDimensionPixelSize(R.dimen.filter_thumb_size);
        this.mBitmap = bitmap;
        Bitmap thumb = ThumbnailUtils.extractThumbnail(bitmap, mFilterThumbSize, mFilterThumbSize);
        mGPUImage.setImage(thumb);
    }
    public void createThumb(GPUImageFilter gpuImageFilter, OnThumbCreated onThumbCreated) {
        new ThumbCreatorTask(gpuImageFilter, onThumbCreated).execute();
    }

    private class ThumbCreatorTask extends AsyncTask<Void, Void, Bitmap> {
        GPUImageFilter gpuImageFilter;
        OnThumbCreated onThumbCreated;
        private ThumbCreatorTask(final GPUImageFilter gpuImageFilter, OnThumbCreated onThumbCreated) {
            this.gpuImageFilter = gpuImageFilter;
            this.onThumbCreated = onThumbCreated;
        }
        @Override
        protected Bitmap doInBackground(Void... params) {
            mGPUImage.setFilter(gpuImageFilter);
            Bitmap bitmap = null;
            try {
                bitmap = mGPUImage.getBitmapWithFilterApplied();
            }
            catch (Exception ex) {}
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (bitmap != null) {
                this.onThumbCreated.onThumbCreated(this.gpuImageFilter, bitmap);
            }
            /*if (filterIndex == mMaxFilterCount - 1) {
                onHorizontalScrollPrepared();
            }*/
        }
    }
}
