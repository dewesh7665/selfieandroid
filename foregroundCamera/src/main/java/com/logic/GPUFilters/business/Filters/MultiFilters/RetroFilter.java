package com.logic.GPUFilters.business.Filters.MultiFilters;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PointF;

import com.logic.GPUFilters.business.Filters.BaseFilter;

import java.util.LinkedList;
import java.util.List;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilterGroup;
import jp.co.cyberagent.android.gpuimage.GPUImageSaturationFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageSoftLightBlendFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageToneCurveFilter;


/**
 * @author Dewesh Kumar
 */
public class RetroFilter extends BaseFilter {
    public enum RetroFilterType {
        LBRetroFilter1("Retro1"),
        LBRetroFilter2("Retro2"),
        LBRetroFilter3("Retro3"),
        LBRetroFilter4("Retro4"),
        LBRetroFilter5("Retro5"),
        LBRetroFilter6("Retro6"),
        LBRetroFilter7("Retro7"),
        LBRetroFilter8("Retro8"),
        LBRetroFilter9("Retro9"),
        LBRetroFilter10("Retro10");

        private String name;
        RetroFilterType(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return this.name;
        }
    }
    private float filterStrength;
    private float brightnessVal;
    private float contrastVal;
    private float hueVal;
    private float satVal;

    public float getFilterStrength() {
        return filterStrength;
    }

    public void setFilterStrength(float filterStrength) {
        this.filterStrength = filterStrength;
    }

    public float getBrightnessVal() {
        return brightnessVal;
    }

    public void setBrightnessVal(float brightnessVal) {
        this.brightnessVal = brightnessVal;
    }

    public float getContrastVal() {
        return contrastVal;
    }

    public void setContrastVal(float contrastVal) {
        this.contrastVal = contrastVal;
    }

    public float getHueVal() {
        return hueVal;
    }

    public void setHueVal(float hueVal) {
        this.hueVal = hueVal;
    }

    public float getSatVal() {
        return satVal;
    }

    public void setSatVal(float satVal) {
        this.satVal = satVal;
    }

    /************************************** Public Methods ***********************************************/

    public GPUImageFilter applyFilter(RetroFilterType lbFilterType) {
        this.brightnessVal = 0.0f;
        this.contrastVal = 0.0f;
        //Keep saturation at 20%
        this.satVal = 0.4f;
        this.hueVal = getHueValue(lbFilterType);

        List<GPUImageFilter> filters = new LinkedList<GPUImageFilter>();

        GPUImageSaturationFilter gpuImageSaturationFilter = new GPUImageSaturationFilter();
        gpuImageSaturationFilter.setSaturation(0.0f);
        filters.add(gpuImageSaturationFilter);

        //Create image and set to softLightBlendFilter
        GPUImageSoftLightBlendFilter softLightBlendFilter = new GPUImageSoftLightBlendFilter();
        softLightBlendFilter.setBitmap(createImageFromColor());
        filters.add(softLightBlendFilter);

        GPUImageToneCurveFilter toneCurveFilter = new GPUImageToneCurveFilter();
        PointF[] sigmoidPtsArray = new PointF[]{new PointF(0.0f, 0.15f), new PointF(70.0f/255.0f, 80.0f/255.0f),
                new PointF(170.0f/255.0f, 190.0f/255.0f), new PointF(1.0f, 0.90f)};
        toneCurveFilter.setRgbCompositeControlPoints(sigmoidPtsArray);
        filters.add(toneCurveFilter);

        return new GPUImageFilterGroup(filters);
    }

    private float getHueValue(RetroFilterType lbFilterType) {
        float hueValue = 0.0f;
        switch (lbFilterType) {
            case LBRetroFilter1:
                hueValue = 0.05f;
                break;
            case LBRetroFilter2:
                hueValue = 0.10f;
                break;
            case LBRetroFilter3:
                hueValue = 0.15f;
                break;
            case LBRetroFilter4:
                hueValue = 0.20f;
                break;
            case LBRetroFilter5:
                hueValue = 0.25f;
                break;
            case LBRetroFilter6:
                hueValue = 0.30f;
                break;
            case LBRetroFilter7:
                hueValue = 0.35f;
                break;
            case LBRetroFilter8:
                hueValue = 0.40f;
                break;
            case LBRetroFilter9:
                hueValue = 0.45f;
                break;
            case LBRetroFilter10:
                hueValue = 0.50f;
                break;
        }
        return hueValue;
    }

    private Bitmap createImageFromColor() {
        Bitmap bmp = Bitmap.createBitmap(inputImage.getWidth(), inputImage.getHeight(), Bitmap.Config.ARGB_8888); // this creates a MUTABLE bitmap
        //bmp.eraseColor(Color.HSVToColor(new float[]{1.0f, 0.4f, 0.7f}));
        //bmp.eraseColor(Color.HSVToColor(new float[]{this.hueVal * 360f, this.satVal, 0.7f}));
        bmp.eraseColor(Color.HSVToColor(255, new float[]{this.hueVal * 360f * 1.5f, this.satVal, 0.7f}));
        return bmp;
    }

    private float convertHueTo360(float hueVal) {
        return hueVal * 360f;
    }
}
