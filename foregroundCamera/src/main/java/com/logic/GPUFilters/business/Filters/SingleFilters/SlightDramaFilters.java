package com.logic.GPUFilters.business.Filters.SingleFilters;

import com.logic.GPUFilters.business.Filters.BaseFilter;
import com.logic.GPUFilters.business.Filters.GPUFilters.GPUImageUnsharpMaskFilter;

import java.util.ArrayList;
import java.util.List;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilterGroup;
import jp.co.cyberagent.android.gpuimage.GPUImageHighlightShadowFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageSaturationFilter;

/**
 * @author Dewesh Kumar
 */
public class SlightDramaFilters extends BaseFilter {
    public GPUImageFilter applyFilter() {
        List<GPUImageFilter> filterList = new ArrayList<>();
        GPUImageHighlightShadowFilter highlightShadowFilter = new GPUImageHighlightShadowFilter();
        highlightShadowFilter.setShadows(0.2f);
        highlightShadowFilter.setHighlights(0.8f);
        filterList.add(highlightShadowFilter);

        GPUImageUnsharpMaskFilter gpuImageUnsharpMaskFilter = new GPUImageUnsharpMaskFilter(this.getInputImage(), 6.0f, 1.3f);
        filterList.add(gpuImageUnsharpMaskFilter);

        GPUImageSaturationFilter saturationFilter = new GPUImageSaturationFilter();
        saturationFilter.setSaturation(1.2f);
        filterList.add(saturationFilter);
        return new GPUImageFilterGroup(filterList);
    }
}
