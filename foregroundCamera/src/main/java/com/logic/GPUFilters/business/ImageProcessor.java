package com.logic.GPUFilters.business;

import android.graphics.Bitmap;

import com.logic.GPUFilters.business.Filters.MultiFilters.DramaFilter;
import com.logic.GPUFilters.business.Filters.MultiFilters.RetroFilter;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;


/**
 * @author Dewesh Kumar
 */
public class ImageProcessor {
    public GPUImageFilter processImageForRetro(Bitmap bitmap, RetroFilter.RetroFilterType lbFilterType) {
        RetroFilter retroFilter = new RetroFilter();
        retroFilter.setInputImage(bitmap);
        return retroFilter.applyFilter(lbFilterType);
    }

    public GPUImageFilter processImageForDrama(Bitmap bitmap, DramaFilter.DramaMode dramaMode) {
        DramaFilter dramaFilter = new DramaFilter();
        return dramaFilter.applyFilter(dramaMode);
    }
}
