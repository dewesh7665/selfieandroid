package com.logic.GPUFilters.business.Filters.SingleFilters;

import android.graphics.Bitmap;

import com.logic.GPUFilters.business.Filters.BaseFilter;

import org.opencv.android.Utils;
import org.opencv.core.Mat;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageMultiplyBlendFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageScreenBlendFilter;

/**
 * @author Dewesh Kumar
 */
public class FashionFilter extends BaseFilter {
    public GPUImageFilter applyHighFashion() {
        GPUImageScreenBlendFilter blendFilter = new GPUImageScreenBlendFilter();
        //blendFilter.setBitmap(getImageWithAlpha(0.7f));
        blendFilter.setBitmap(this.inputImage);
        return blendFilter;
    }

    public GPUImageFilter applyGrungeFashion() {
        //GPUImageOverlayBlendFilter blendFilter = new GPUImageOverlayBlendFilter();
        GPUImageMultiplyBlendFilter blendFilter = new GPUImageMultiplyBlendFilter();
        blendFilter.setBitmap(this.inputImage);
        return blendFilter;
    }

    private Bitmap getImageWithAlpha(float alpha) {
        Mat source = new Mat();
        Utils.bitmapToMat(this.inputImage, source);
        Mat destination = new Mat(source.rows(), source.cols(), source.type());
        int rows = source.rows();
        int colms = source.cols();
        int channelCount = source.channels();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < colms; j++) {
                double[] rgba = source.get(i, j);
                //rgba[3] = rgba[3] * alpha;
                destination.put(i, j, rgba);
            }
        }
        Bitmap destinationBmp = Bitmap.createBitmap(source.cols(), source.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(source, destinationBmp);
        return destinationBmp;
    }
}
