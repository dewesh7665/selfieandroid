package com.logic.GPUFilters.business.Filters.SingleFilters;

import com.logic.GPUFilters.business.Filters.BaseFilter;

import java.util.ArrayList;
import java.util.List;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilterGroup;
import jp.co.cyberagent.android.gpuimage.GPUImageSaturationFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageWhiteBalanceFilter;

/**
 * @author Dewesh Kumar
 */
public class KelwinFilters extends BaseFilter {
    public GPUImageFilter applyWarmFilter() {
        List<GPUImageFilter> filterList = new ArrayList<>();

        GPUImageSaturationFilter saturationFilter = new GPUImageSaturationFilter();
        saturationFilter.setSaturation(1.5f);
        filterList.add(saturationFilter);

        GPUImageWhiteBalanceFilter whiteBalanceFilter = new GPUImageWhiteBalanceFilter();
        whiteBalanceFilter.setTemperature(6500f);
        filterList.add(whiteBalanceFilter);
        return new GPUImageFilterGroup(filterList);
    }

    public GPUImageFilter applyCoolFilter() {
        List<GPUImageFilter> filterList = new ArrayList<>();

        GPUImageSaturationFilter saturationFilter = new GPUImageSaturationFilter();
        saturationFilter.setSaturation(0.8f);
        filterList.add(saturationFilter);

        GPUImageWhiteBalanceFilter whiteBalanceFilter = new GPUImageWhiteBalanceFilter();
        whiteBalanceFilter.setTemperature(4500f);
        filterList.add(whiteBalanceFilter);
        return new GPUImageFilterGroup(filterList);
    }
}
