package com.logic.GPUFilters.business.Filters.MultiFilters;

import android.graphics.Bitmap;

import com.logic.GPUFilters.business.Filters.BaseFilter;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageMultiplyBlendFilter;

/**
 * @author Dewesh Kumar
 */
public class GrungeFilter extends BaseFilter {
    private float filterStrength;
    private float brightnessValue;
    private float contrastValue;

    public GPUImageFilter applyFilterWithTextureImage(Bitmap texture) {
        this.brightnessValue = 0.0f;
        this.contrastValue = 0.0f;
        //Resize texture image to size of input image
        Mat inputMat = new Mat();
        Utils.bitmapToMat(this.inputImage, inputMat);
        Mat textureMat = new Mat();
        Utils.bitmapToMat(texture, textureMat);
        Mat resizedTextureMat = new Mat();
        Imgproc.resize(textureMat, resizedTextureMat, inputMat.size());
        Bitmap textureBitmap = Bitmap.createBitmap(resizedTextureMat.cols(), resizedTextureMat.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(resizedTextureMat, textureBitmap);

        //Now blend this texture image with main image
        GPUImageMultiplyBlendFilter multiplyBlendFilter = new GPUImageMultiplyBlendFilter();
        multiplyBlendFilter.setBitmap(textureBitmap);
        return multiplyBlendFilter;
    }

    public GPUImageFilter applyFilterWithTwoTextureImage(Bitmap texture1, Bitmap texture2) {
        return null;
    }
}
