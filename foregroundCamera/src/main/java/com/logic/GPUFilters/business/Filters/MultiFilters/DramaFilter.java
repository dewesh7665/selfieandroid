package com.logic.GPUFilters.business.Filters.MultiFilters;

import com.logic.GPUFilters.business.Filters.BaseFilter;
import com.logic.GPUFilters.business.Filters.GPUFilters.GPUImageUnsharpMaskFilter;

import java.util.LinkedList;
import java.util.List;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilterGroup;
import jp.co.cyberagent.android.gpuimage.GPUImageGammaFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageHighlightShadowFilter;

/**
 * @author Dewesh Kumar
 */
public class DramaFilter extends BaseFilter {
    public enum DramaMode {
        DRAMA_NORMAL_MODE("Normal"),
        DRAMA_BRIGHT_MODE("Bright"),
        DRAMA_DARK_MODE("Dark");
        //DRAMA_CONTRAST_MODE

        private String name;
        DramaMode(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return this.name;
        }
    }
    private float filterStrength;
    private float shadowLevel;
    private float highlightLevel;
    private float midToneLevel;
    private float saturationVal;
    private DramaMode selectedDramaMode;

    public GPUImageFilter applyFilter(DramaMode dramaMode) {
        List<GPUImageFilter> filters = new LinkedList<GPUImageFilter>();
        GPUImageFilter firstLevelFilter = null;
        //GPUImageGaussianBlurFilter highPassFilter = new GPUImageGaussianBlurFilter();
        GPUImageFilter highPassFilter = null;
        switch (dramaMode) {
            case DRAMA_NORMAL_MODE:
                firstLevelFilter = new GPUImageHighlightShadowFilter();
                ((GPUImageHighlightShadowFilter) firstLevelFilter).setShadows(0.3f);
                ((GPUImageHighlightShadowFilter) firstLevelFilter).setHighlights(0.7f);
                highPassFilter = getGPUImageUnsharpMaskFilter(15.0f, 3.0f);
                break;
            case DRAMA_BRIGHT_MODE:
                firstLevelFilter = new GPUImageGammaFilter();
                ((GPUImageGammaFilter)firstLevelFilter).setGamma(0.5f);
                highPassFilter = getGPUImageUnsharpMaskFilter(10.0f, 3.0f);
                break;
            case DRAMA_DARK_MODE:
                firstLevelFilter = new GPUImageGammaFilter();
                ((GPUImageGammaFilter)firstLevelFilter).setGamma(2.0f);
                highPassFilter = getGPUImageUnsharpMaskFilter(10.0f, 3.0f);
                break;
            /*case DRAMA_CONTRAST_MODE:
                firstLevelFilter = new GPUImageHighlightShadowFilter();
                ((GPUImageHighlightShadowFilter) firstLevelFilter).setShadows(0.2f);
                ((GPUImageHighlightShadowFilter) firstLevelFilter).setHighlights(0.8f);
                highPassFilter = getGPUImageUnsharpMaskFilter(15.0f, 1.4f);
                break;*/
        }
        filters.add(firstLevelFilter);
        filters.add(highPassFilter);
        return new GPUImageFilterGroup(filters);
    }

    private GPUImageFilter getGPUImageUnsharpMaskFilter(float blurSize, float intensity) {
        GPUImageUnsharpMaskFilter gpuImageUnsharpMaskFilter = new GPUImageUnsharpMaskFilter(inputImage,blurSize, intensity);
        gpuImageUnsharpMaskFilter.setIntensity(intensity);
       return gpuImageUnsharpMaskFilter;
    }
}
