package com.logic.GPUFilters.business.Filters.SingleFilters;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PointF;

import com.logic.GPUFilters.business.Filters.BaseFilter;

import java.util.ArrayList;
import java.util.List;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilterGroup;
import jp.co.cyberagent.android.gpuimage.GPUImageMultiplyBlendFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageToneCurveFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageVignetteFilter;

/**
 * @author Dewesh Kumar
 */
public class MemoryFilter extends BaseFilter {
    public GPUImageFilter applyFilter() {
        List<GPUImageFilter> filterList = new ArrayList<>();
        GPUImageToneCurveFilter blueCurveFilter = new GPUImageToneCurveFilter();
        PointF[] blueControlPoints = new PointF[]{new PointF(0.0f/255.0f, 50.0f/255.0f),
                new PointF(1.0f, 1.0f)};
        blueCurveFilter.setBlueControlPoints(blueControlPoints);
        filterList.add(blueCurveFilter);

        int blendColor = Color.argb(255, 246, 221, 173);
        Bitmap colorImage = createImageFromColor(blendColor);
        GPUImageMultiplyBlendFilter multiplyBlendFilter = new GPUImageMultiplyBlendFilter();
        multiplyBlendFilter.setBitmap(colorImage);
        filterList.add(multiplyBlendFilter);

        GPUImageToneCurveFilter rgbToneCurveFilter = new GPUImageToneCurveFilter();
        PointF[] rgbControlPoints = new PointF[]{new PointF(0.0f, 35.0f/255.0f),
                new PointF(70.0f/255.0f, 80.0f/255.0f),
                new PointF(170.0f/255.0f,210.0f/255.0f),
                new PointF(1.0f, 1.0f)};
        rgbToneCurveFilter.setRgbCompositeControlPoints(rgbControlPoints);
        filterList.add(rgbToneCurveFilter);

        GPUImageVignetteFilter vignetteFilter = new GPUImageVignetteFilter();
        vignetteFilter.setVignetteCenter(new PointF(0.5f,0.5f));
        vignetteFilter.setVignetteStart(0.3f);
        vignetteFilter.setVignetteEnd(0.75f);
        filterList.add(vignetteFilter);

        return new GPUImageFilterGroup(filterList);

    }
}
