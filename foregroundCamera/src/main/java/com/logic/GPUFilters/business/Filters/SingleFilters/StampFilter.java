package com.logic.GPUFilters.business.Filters.SingleFilters;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PointF;

import com.logic.GPUFilters.business.Filters.BaseFilter;

import java.util.ArrayList;
import java.util.List;

import jp.co.cyberagent.android.gpuimage.GPUImageContrastFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilterGroup;
import jp.co.cyberagent.android.gpuimage.GPUImageSaturationFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageSoftLightBlendFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageToneCurveFilter;

/**
 * @author Dewesh Kumar
 */
public class StampFilter extends BaseFilter {
    public GPUImageFilter applyFilter() {
        List<GPUImageFilter> filterList = new ArrayList<>();
        GPUImageSaturationFilter saturationFilter = new GPUImageSaturationFilter();
        saturationFilter.setSaturation(0.0f);
        filterList.add(saturationFilter);

        int blendColor = Color.HSVToColor((int)(0.3f*360.0f), new float[]{0.06f, 0.5f, 0.7f});
        Bitmap textureImage = createImageFromColor(blendColor);
        GPUImageSoftLightBlendFilter softLightBlendFilter = new GPUImageSoftLightBlendFilter();
        softLightBlendFilter.setBitmap(textureImage);
        filterList.add(softLightBlendFilter);

        GPUImageToneCurveFilter sigmoidFilter = new GPUImageToneCurveFilter();
        PointF[] rgbControlPoints = new PointF[]{new PointF(0.0f, 0.15f),
                new PointF(70.0f/255.0f, 80.0f/255.0f),
                new PointF(170.0f/255.0f,190.0f/255.0f),
                new PointF(1.0f, 0.90f)};
        sigmoidFilter.setRgbCompositeControlPoints(rgbControlPoints);
        filterList.add(sigmoidFilter);

        GPUImageContrastFilter contrastFilter = new GPUImageContrastFilter();
        contrastFilter.setContrast(1.25f);
        filterList.add(contrastFilter);

        return new GPUImageFilterGroup(filterList);
    }
}
