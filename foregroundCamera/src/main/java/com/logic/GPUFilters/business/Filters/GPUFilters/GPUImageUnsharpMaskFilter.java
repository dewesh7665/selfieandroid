package com.logic.GPUFilters.business.Filters.GPUFilters;

import android.graphics.Bitmap;
import android.opengl.GLES20;

import com.logic.photo.application.PhotoGrabApplication;

import jp.co.cyberagent.android.gpuimage.GPUImage;
import jp.co.cyberagent.android.gpuimage.GPUImageContrastFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageFilterGroup;
import jp.co.cyberagent.android.gpuimage.GPUImageGaussianBlurFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageTwoInputFilter;

/**
 * @author Dewesh Kumar
 */
public class GPUImageUnsharpMaskFilter extends GPUImageFilterGroup {

    private static final String UNIFORM_INTENSITY = "intensity";
    private float intensity = 1.0f;// The strength of the sharpening, from 0.0 on up, with a default of 1.0
    private GPUImageGaussianBlurFilter blurFilter;
    GPUImageTwoInputFilter unSharpMaskFilter = null;
    private GPUImage mGpuImage;
    private int mIntensityLocation;

    @Override
    public void onInit() {
        super.onInit();
        mIntensityLocation = GLES20.glGetUniformLocation(getProgram(), UNIFORM_INTENSITY);
    }

    public void setIntensity(float intensity) {
        this.intensity = intensity;
        setFloat(mIntensityLocation, intensity);
    }

    public GPUImageUnsharpMaskFilter(Bitmap inputBitmap, float blurSize, float intensity) {
        mGpuImage = new GPUImage(PhotoGrabApplication.getInstance());
        mGpuImage.setImage(inputBitmap);
        this.intensity = intensity;
        blurFilter = new GPUImageGaussianBlurFilter();
        blurFilter.setBlurSize(blurSize);
        mGpuImage.setFilter(blurFilter);
        Bitmap blurredBitmap = mGpuImage.getBitmapWithFilterApplied();

        unSharpMaskFilter = new GPUImageTwoInputFilter(FRAGMENT_SHADER_V2);
        unSharpMaskFilter.setBitmap(blurredBitmap);

        addFilter(new GPUImageContrastFilter());
        addFilter(unSharpMaskFilter);
        addFilter(new GPUImageFilter());
        //addFilter(blurFilter);
        //addFilter(unSharpMaskFilter);


    }


    private static final String ATTRIBUTE_POSITION = "a_Position";
    private static final String ATTRIBUTE_TEXCOORD = "a_TexCoord";
    private static final String VARYING_TEXCOORD = "v_TexCoord";
    private static final String UNIFORM_TEXTUREBASE = "inputImageTexture";
    private static final String UNIFORM_TEXTURE0 = UNIFORM_TEXTUREBASE+0;


    public static String FRAGMENT_SHADER = "precision mediump float;\n"
            +"uniform sampler2D inputImageTexture;\n"
            +"uniform sampler2D inputImageTexture2\n"
            //+"varying vec2 textureCoordinate2;\n"
            +"uniform float "+UNIFORM_INTENSITY+";\n"


            +"void main(){\n"
            +"   vec4 sharpImageColor = texture2D(inputImageTexture, textureCoordinate);\n"
            +"   vec4 blurredImageColor = texture2D(inputImageTexture2, textureCoordinate2);\n"
            +"   gl_FragColor = vec4(mix(sharpImageColor.rgb, blurredImageColor.rgb, "+UNIFORM_INTENSITY+"), sharpImageColor.a);\n"
            +"}\n";


    public static String FRAGMENT_SHADER_V2 =
            "varying highp vec2 textureCoordinate;\n" +
            " varying highp vec2 textureCoordinate2;\n" +
            " \n" +
            " uniform sampler2D inputImageTexture;\n" +
            " uniform sampler2D inputImageTexture2;\n" +
            " \n" +
            " uniform float intensity;\n" +
            " \n" +
            " void main()\n" +
            " {\n" +
            "     mediump vec4 sharpImageColor = texture2D(inputImageTexture, textureCoordinate);\n" +
            "     mediump vec4 blurredImageColor = texture2D(inputImageTexture2, textureCoordinate2);\n" +
            "     \n" +
            //"     gl_FragColor = vec4(sharpImageColor.rgb * intensity + blurredImageColor * (1.0 - intensity), sharpImageColor.a);\n" +
                    "    gl_FragColor = vec4(mix(sharpImageColor.rgb, blurredImageColor.rgb, "+UNIFORM_INTENSITY+"), sharpImageColor.a);\n"+
            " }";

    public static final String COLOR_BURN_BLEND_FRAGMENT_SHADER =
            "varying highp vec2 textureCoordinate;\n" +
            " varying highp vec2 textureCoordinate2;\n" +
            "\n" +
            " uniform sampler2D inputImageTexture;\n" +
            " uniform sampler2D inputImageTexture2;\n" +
            " \n" +
            " void main()\n" +
            " {\n" +
            "    mediump vec4 textureColor = texture2D(inputImageTexture, textureCoordinate);\n" +
            "    mediump vec4 textureColor2 = texture2D(inputImageTexture2, textureCoordinate2);\n" +
            "    mediump vec4 whiteColor = vec4(1.0);\n" +
            "    gl_FragColor = whiteColor - (whiteColor - textureColor) / textureColor2;\n" +
            " }";
}
