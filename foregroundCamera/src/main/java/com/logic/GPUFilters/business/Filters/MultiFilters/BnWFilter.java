package com.logic.GPUFilters.business.Filters.MultiFilters;

import com.logic.GPUFilters.business.Filters.BaseFilter;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;

/**
 * @author Dewesh Kumar
 */
public class BnWFilter extends BaseFilter {
    public enum BWMode {
        BW_NORMAL_MODE,
        BW_DRAMA_MODE,
        //Little bit of drama with vignette..2-3 rounds of little sharpening with radial blur and slight vignette
        BW_NOIR_MODE,
        BW_WASH_MODE,
        //Based on gamma
        BW_LIGHTER_DRAMA_MODE,
        //Based on gamma
        BW_DARKER_DRAMA_MODE,
        //Simple contrast adjustment
        BW_CONTRAST_MODE,
        //Simple vintage look.. difference blend a light blue color with 0.2 opacity..
        BW_VINTAGE_MODE,
        //Simple female fashion mode
        BW_FASHION_CLEAR,
        //Simple male fashion mode
        BW_FASHION_ROUGH
    }

    public GPUImageFilter appFilter(BWMode bwMode) {
        return null;
    }
}
