package com.logic.GPUFilters.business.Filters.SingleFilters;

import android.graphics.PointF;

import com.logic.GPUFilters.business.Filters.BaseFilter;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;
import jp.co.cyberagent.android.gpuimage.GPUImageToneCurveFilter;

/**
 * @author Dewesh Kumar
 */
public class LomoFilter extends BaseFilter {
    public GPUImageFilter applyFilter() {
        GPUImageToneCurveFilter toneCurveFilter = new GPUImageToneCurveFilter();
        PointF[] rgbControlPoints = new PointF[]{new PointF(0.0f, 0.0f),
                new PointF(46.0f/255.0f, 40.0f/255.0f),
                new PointF(95.0f/255.0f,105.0f/255.0f),
                new PointF(200.0f/255.0f,205.0f/255.0f),
                new PointF(1.0f, 1.0f)};
        PointF[] redControlPoints = new PointF[]{new PointF(0.0f, 0.0f),
                new PointF(63.0f/255.0f, 46.0f/255.0f),
                new PointF(190.0f/255.0f,210.0f/255.0f),
                new PointF(1.0f, 1.0f)};
        PointF[] greenControlPoints = new PointF[]{new PointF(0.0f, 0.0f),
                new PointF(70.0f/255.0f, 60.0f/255.0f),
                new PointF(190.0f/255.0f,205.0f/255.0f),
                new PointF(1.0f, 1.0f)};
        PointF[] blueControlPoints = new PointF[]{new PointF(0.0f, 0.0f),
                new PointF(60.0f/255.0f, 70.0f/255.0f),
                new PointF(190.0f/255.0f,170.0f/255.0f),
                new PointF(1.0f, 1.0f)};
        toneCurveFilter.setRgbCompositeControlPoints(rgbControlPoints);
        toneCurveFilter.setRedControlPoints(redControlPoints);
        toneCurveFilter.setGreenControlPoints(greenControlPoints);
        toneCurveFilter.setBlueControlPoints(blueControlPoints);
        return toneCurveFilter;
    }
}
