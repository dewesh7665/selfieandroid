package com.logic.GPUFilters.business.Filters.MultiFilters;

import com.logic.GPUFilters.business.Filters.BaseFilter;
import com.logic.GPUFilters.business.Filters.SingleFilters.FashionFilter;
import com.logic.GPUFilters.business.Filters.SingleFilters.KelwinFilters;
import com.logic.GPUFilters.business.Filters.SingleFilters.LomoFilter;
import com.logic.GPUFilters.business.Filters.SingleFilters.MemoryFilter;
import com.logic.GPUFilters.business.Filters.SingleFilters.MorningFilter;
import com.logic.GPUFilters.business.Filters.SingleFilters.SlightDramaFilters;
import com.logic.GPUFilters.business.Filters.SingleFilters.StampFilter;
import com.logic.GPUFilters.business.Filters.SingleFilters.XProFilters;

import jp.co.cyberagent.android.gpuimage.GPUImageFilter;

/**
 * @author Dewesh Kumar
 */
public class DailyFilter extends BaseFilter {

    public enum FilterType {
        //Daily best filters
        LBFilterHighFashion("Clarity"),
        LBFilterLowFashion("Grunge"),
        LBFilterPunch("Punch"),
        LBFilterXPro("XPro"),
        LBFilterLomo("Lomo"),
        LBFilterCool("Pluto"),
        LBFilterMorning("Morning"),
        //LBFilterDreamy("Dreamy"),
        //Similar to Kelvin of Instagram
        LBFilterWarm("Warm"),
        //Vintage default for nashville
        LBFilterMemory("Memory"),
        //Desaturate and hue,sat value for light brown/sepia tone
        LBFilterStamp("Postcard");
        //LBFilterBleach("Bleach"),
        //LBFilterCold("Cold");


        private String name;

        FilterType(String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return this.name;
        }
    }

    public GPUImageFilter applyFilter(FilterType filterType) {
        GPUImageFilter gpuImageFilter = null;
        switch (filterType) {
            case LBFilterPunch:
                SlightDramaFilters slightDramaFilters = new SlightDramaFilters();
                slightDramaFilters.setInputImage(this.inputImage);
                gpuImageFilter =  slightDramaFilters.applyFilter();
                break;
            case LBFilterMorning:
                MorningFilter morningFilter = new MorningFilter();
                morningFilter.setInputImage(this.inputImage);
                gpuImageFilter = morningFilter.applyFilter();
                break;
            case LBFilterWarm:
                KelwinFilters kelwinFiltersWarm = new KelwinFilters();
                kelwinFiltersWarm.setInputImage(this.inputImage);
                gpuImageFilter = kelwinFiltersWarm.applyWarmFilter();
                break;
            case LBFilterCool:
                KelwinFilters kelwinFiltersCold = new KelwinFilters();
                kelwinFiltersCold.setInputImage(this.inputImage);
                gpuImageFilter = kelwinFiltersCold.applyCoolFilter();
                break;
            case LBFilterXPro:
                XProFilters xProFilters = new XProFilters();
                xProFilters.setInputImage(this.inputImage);
                gpuImageFilter = xProFilters.applyCrossProcessFilter();
                break;
            case LBFilterLomo:
                LomoFilter lomoFilter = new LomoFilter();
                lomoFilter.setInputImage(this.inputImage);
                gpuImageFilter = lomoFilter.applyFilter();
                break;
            case LBFilterMemory:
                MemoryFilter memoryFilter = new MemoryFilter();
                memoryFilter.setInputImage(this.inputImage);
                gpuImageFilter = memoryFilter.applyFilter();
                break;
            case LBFilterStamp:
                StampFilter stampFilter = new StampFilter();
                stampFilter.setInputImage(this.inputImage);
                gpuImageFilter = stampFilter.applyFilter();
                break;
            case LBFilterHighFashion:
                FashionFilter highFashionFilter = new FashionFilter();
                highFashionFilter.setInputImage(this.orgInputImage);
                gpuImageFilter = highFashionFilter.applyHighFashion();
                break;
            case LBFilterLowFashion:
                FashionFilter lowFashionFilter = new FashionFilter();
                lowFashionFilter.setInputImage(this.orgInputImage);
                gpuImageFilter = lowFashionFilter.applyGrungeFashion();
                break;
            /*case LBFilterBleach:
                break;
             case LBFilterDreamy:
                break;
            case LBFilterCold:
                break;*/
        }
        return gpuImageFilter;
    }
}
