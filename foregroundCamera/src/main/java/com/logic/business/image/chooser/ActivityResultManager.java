package com.logic.business.image.chooser;

import android.content.Intent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 */
public class ActivityResultManager {
	private static ActivityResultManager mInstance = new ActivityResultManager();;
	private List<OnActivityResultObserver> arrListListener = Collections.synchronizedList(new ArrayList<OnActivityResultObserver>());

	private ActivityResultManager() {

	}
	public static ActivityResultManager getInstance() {
		return mInstance;
	}

	public void registerListener(OnActivityResultObserver onActivityResultListener) {
		arrListListener.add(onActivityResultListener);
	}

	public void unRegisterListener(
            OnActivityResultObserver onActivityResultListener) {
		arrListListener.remove(onActivityResultListener);
	}

	public void updateActivityResultListeners(int requestCode, int resultCode,
                                              Intent data) {
            for (int i = 0; i < arrListListener.size(); i++) {
                arrListListener.get(i).onActivityResultObtained(requestCode, resultCode, data);
            }
	}

    public static interface OnActivityResultObserver {
        public void onActivityResultObtained(int requestCode, int resultCode, Intent data);
    }
}
