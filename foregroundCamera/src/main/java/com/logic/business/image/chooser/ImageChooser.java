package com.logic.business.image.chooser;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;

import com.logic.business.FileIOUtil;

import java.io.File;

public class ImageChooser implements ActivityResultManager.OnActivityResultObserver {
	public static interface OnImageSelectionListener {
		public void onImageSelected(String filePath);
	}

	private static final int CAMERA_REQUEST_CODE = 1000;
	private static final int PICK_REQUEST_CODE = 1001;
	private Context mContext;
	private OnImageSelectionListener onImageSelectionListener;
	private String mOutputFilePath;

	public ImageChooser(Context context) {
		this.mContext = context;
	}

	public void chooseFromGallery(OnImageSelectionListener onImageSelectionListener) {
		this.onImageSelectionListener = onImageSelectionListener;
		ActivityResultManager.getInstance().registerListener(this);
		((Activity) mContext).startActivityForResult(new Intent(
				Intent.ACTION_PICK,
				MediaStore.Images.Media.INTERNAL_CONTENT_URI),
				PICK_REQUEST_CODE);
	}

	public void chooseFromCamera(OnImageSelectionListener onImageSelectionListener) {
		this.onImageSelectionListener = onImageSelectionListener;
		ActivityResultManager.getInstance().registerListener(this);
		Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		File newImageFile = FileIOUtil.getNewFileBasedOnTimeStamp();
		mOutputFilePath = newImageFile.getAbsolutePath();
		cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,
				Uri.fromFile(newImageFile));
		((Activity) mContext).startActivityForResult(cameraIntent,
				CAMERA_REQUEST_CODE);
	}

	@Override
	public void onActivityResultObtained(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		switch (requestCode) {
		case CAMERA_REQUEST_CODE:
			if (resultCode == Activity.RESULT_OK) {
				// Handle URI
				this.onImageSelectionListener.onImageSelected(mOutputFilePath);
				ActivityResultManager.getInstance().unRegisterListener(this);
			}
			break;
		case PICK_REQUEST_CODE:
			if (resultCode == Activity.RESULT_OK) {
				// Handle URI
				Uri selectedImageURI = data.getData();
				this.onImageSelectionListener.onImageSelected(FileIOUtil
						.getPathFromURI(mContext, selectedImageURI));
				ActivityResultManager.getInstance().unRegisterListener(this);
			}

			break;

		default:
			break;
		}
	}
}
