package com.logic.business.effects;

import android.graphics.Color;
import android.media.effect.EffectFactory;

import com.logic.foregroundcamera.R;

import java.util.ArrayList;

public class FilterParams {
	private ArrayList<FilterParam> arrlistFilterParam;
	public ArrayList<FilterParam> getFilterParams(){
		if(arrlistFilterParam == null)
			init();
		return arrlistFilterParam;
	}
	private void init(){
		arrlistFilterParam = new ArrayList<FilterParam>();

		arrlistFilterParam.add(new FilterParam(R.drawable.effects_autofixeffect, EffectFactory.EFFECT_AUTOFIX,
				new ExtraParam(new String[]{"scale"}, new Object[]{0.5f})));

		arrlistFilterParam.add(new FilterParam(R.drawable.effects_blackwhiteeffect, EffectFactory.EFFECT_BLACKWHITE,
				new ExtraParam(new String[]{"black","white"}, new Object[]{0.1f,.7f})));

		arrlistFilterParam.add(new FilterParam(R.drawable.effects_brightnesseffect, EffectFactory.EFFECT_BRIGHTNESS,
				new ExtraParam(new String[]{"brightness"}, new Object[]{2.0f})));

		arrlistFilterParam.add(new FilterParam(R.drawable.effects_colortemperatureeffect, EffectFactory.EFFECT_TEMPERATURE,
				new ExtraParam(new String[]{"scale"}, new Object[]{0.5f})));

		arrlistFilterParam.add(new FilterParam(R.drawable.effects_contrasteffect, EffectFactory.EFFECT_CONTRAST,
				new ExtraParam(new String[]{"contrast"}, new Object[]{1.4f})));

		arrlistFilterParam.add(new FilterParam(R.drawable.effects_crossprocesseffect, EffectFactory.EFFECT_CROSSPROCESS));
		arrlistFilterParam.add(new FilterParam(R.drawable.effects_documentaryeffect, EffectFactory.EFFECT_DOCUMENTARY));

		arrlistFilterParam.add(new FilterParam(R.drawable.effects_duotoneeffect, EffectFactory.EFFECT_DUOTONE,
				new ExtraParam(new String[]{"first_color","second_color"}, new Object[]{Color.YELLOW,Color.DKGRAY})));

		arrlistFilterParam.add(new FilterParam(R.drawable.effects_filllighteffect, EffectFactory.EFFECT_FILLLIGHT,
				new ExtraParam(new String[]{"strength"}, new Object[]{0.8f})));

		arrlistFilterParam.add(new FilterParam(R.drawable.effects_fisheyeeffect, EffectFactory.EFFECT_FISHEYE,
				new ExtraParam(new String[]{"scale"}, new Object[]{0.5f})));

		arrlistFilterParam.add(new FilterParam(R.drawable.effects_flipeffect, EffectFactory.EFFECT_FLIP,
				new ExtraParam(new String[]{"horizontal"}, new Object[]{true})));

		arrlistFilterParam.add(new FilterParam(R.drawable.effects_graineffect, EffectFactory.EFFECT_GRAIN,
				new ExtraParam(new String[]{"strength"}, new Object[]{1.0f})));

		arrlistFilterParam.add(new FilterParam(R.drawable.effects_grayscaleeffect, EffectFactory.EFFECT_GRAYSCALE));
		arrlistFilterParam.add(new FilterParam(R.drawable.effects_lomoisheffect, EffectFactory.EFFECT_LOMOISH));
		arrlistFilterParam.add(new FilterParam(R.drawable.effects_negativeeffect, EffectFactory.EFFECT_NEGATIVE));
		arrlistFilterParam.add(new FilterParam(R.drawable.effects_posterizeeffect, EffectFactory.EFFECT_POSTERIZE));

		arrlistFilterParam.add(new FilterParam(R.drawable.effects_rotateeffect, EffectFactory.EFFECT_ROTATE,
				new ExtraParam(new String[]{"angle"}, new Object[]{180})));

		arrlistFilterParam.add(new FilterParam(R.drawable.effects_saturateeffect, EffectFactory.EFFECT_SATURATE,
				new ExtraParam(new String[]{"scale"}, new Object[]{0.5f})));

		arrlistFilterParam.add(new FilterParam(R.drawable.effects_sepiaeffect, EffectFactory.EFFECT_SEPIA));
		arrlistFilterParam.add(new FilterParam(R.drawable.effects_sharpeneffect, EffectFactory.EFFECT_SHARPEN));

		arrlistFilterParam.add(new FilterParam(R.drawable.effects_tinteffect, EffectFactory.EFFECT_TINT,
				new ExtraParam(new String[]{"tint"}, new Object[]{Color.MAGENTA})));

		arrlistFilterParam.add(new FilterParam(R.drawable.effects_vignetteeffect, EffectFactory.EFFECT_VIGNETTE,
				new ExtraParam(new String[]{"scale"}, new Object[]{0.5f})));
	}
	public static class FilterParam{
		public int drawableId;
		public String effectName;
		public ExtraParam extraParam;
		public FilterParam(int drawableId, String effectName) {
			this(drawableId,effectName,null);
		}
		public FilterParam(int drawableId, String effectName,ExtraParam extraParam) {
			this.drawableId =drawableId;
			this.effectName = effectName;
			if(extraParam != null)
				this.extraParam = extraParam;
		}
	}

	public static class ExtraParam{
		public String[] names;
		public Object[] values;
		public ExtraParam(String[] names,Object[] values) {
			// TODO Auto-generated constructor stub
			this.names = names;
			this.values = values;
		}
	}
}
