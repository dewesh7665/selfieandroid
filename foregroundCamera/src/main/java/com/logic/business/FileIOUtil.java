package com.logic.business;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.provider.MediaStore.MediaColumns;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileIOUtil {
	private static final String TAG = "FileUIUtils";
	private static final String DIR_TEMP = "photo_lb_dir_temp_lib";
	private static final String DIR_DEFAULT = "LB_Images_LIB";
	public static final String MERGED_FILE_PREFIX = "IMG_";
	public static final String JPEG_FILE_SUFFIX = ".jpg";
	private static final long LOW_STORAGE_THRESHOLD = 1024 * 1024 * 10;

	// private static final String SDCARD_ROOT =
	// Environment.getExternalStorageDirectory()//Defailt directory for API 1 to
	// Latest
	// .getAbsolutePath() + "/";
	// private static String mAppStorageDirectory =
	// Environment.getExternalStorageDirectory().toString();
	// private static Boolean hasAppDirSet = false;

	public static File getNewFile(String fileName) {
		return getNewFileInDir(fileName, DIR_DEFAULT);
	}

	public static File getNewTempFile(String fileName) {
		return getNewFileInDir(fileName, DIR_TEMP);
	}

	/**
	 * Beware : Subsequent calling this function may result in same file name
	 * @return
	 */
	public static File getNewFileBasedOnTimeStamp() {
		return getNewFileBasedOnTimeStamp(null);
	}
	
	public static File getNewFileBasedOnTimeStamp(String prefix) {
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
		.format(new Date());
		String fileName = null;
		if(prefix != null)
			fileName = prefix + "_"+MERGED_FILE_PREFIX + timeStamp + "_"+ JPEG_FILE_SUFFIX;
		else
			fileName = MERGED_FILE_PREFIX + timeStamp + "_"+ JPEG_FILE_SUFFIX;
		return getNewFileInDir(fileName, DIR_DEFAULT);
	}

	public static File getAlbumDir(String albumName) {
		File storageDir = null;

		if (Environment.MEDIA_MOUNTED.equals(Environment
				.getExternalStorageState())) {

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
				storageDir = new File(
						Environment
						.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
						albumName);
			} else {
				storageDir = new File(Environment.getExternalStorageDirectory()
						+ "/dcim/" + albumName);
			}

			if (storageDir != null) {
				if (!storageDir.mkdirs()) {
					if (!storageDir.exists()) {
						Log.d(TAG, "failed to create directory");
						return null;
					}
				}
			}

		} else {
			Log.v(TAG, "External storage is not mounted READ/WRITE.");
		}

		return storageDir;
	}

	/**
	 * Will return null in case of IO exception else path of file
	 * 
	 * @param bitmap
	 * @return
	 */
	public static String saveBitmap(Bitmap bitmap) {
		File dataFile = FileIOUtil.getNewFileBasedOnTimeStamp();
		try {
			FileOutputStream out = new FileOutputStream(dataFile, false);
			bitmap.compress(CompressFormat.PNG, 95, out);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return dataFile.getAbsolutePath();
	}
	
	public static Boolean saveBitmap(String filePath,Bitmap bitmap) {
		File dataFile = new File(filePath);
		if(!dataFile.exists()) Log.e(TAG, filePath + "file don't exist");
		try {
			FileOutputStream out = new FileOutputStream(dataFile, false);
			bitmap.compress(CompressFormat.PNG, 95, out);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * Will broadcast gallery
	 * 
	 * @param photoPath
	 * @param context
	 */
	public static void notifyGallery(String photoPath, Context context) {
		Intent mediaScanIntent = new Intent(
				"android.intent.action.MEDIA_SCANNER_SCAN_FILE");
		File f = new File(photoPath);
		Uri contentUri = Uri.fromFile(f);
		mediaScanIntent.setData(contentUri);
		context.sendBroadcast(mediaScanIntent);
	}

	public static void notifyGallery(Uri uri, Context context) {
		Intent mediaScanIntent = new Intent(
				"android.intent.action.MEDIA_SCANNER_SCAN_FILE");
		mediaScanIntent.setData(uri);
		context.sendBroadcast(mediaScanIntent);
	}

	public static String getPathFromURI(Context context, Uri contentUri) {
		Cursor cursor = null;
		try {
			String[] proj = { MediaColumns.DATA };
			cursor = context.getContentResolver().query(contentUri, proj, null,
					null, null);
			int column_index = cursor
					.getColumnIndexOrThrow(MediaColumns.DATA);
			cursor.moveToFirst();
			return cursor.getString(column_index);
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}
	}

	public static Uri getUriOfFile(String filePath) {
		return Uri.fromFile(new File(filePath));
	}

	public static Bitmap getBitmapFromURI(String imageUri, int width, int height) {
		/* There isn't enough memory to open up more than a couple camera photos */
		/* So pre-scale the target bitmap into which the file is decoded */

		/* Get the size of the ImageView */
		int targetW = width;
		int targetH = height;

		/* Get the size of the image */
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(imageUri, bmOptions);
		int photoW = bmOptions.outWidth;
		int photoH = bmOptions.outHeight;

		/* Figure out which way needs to be reduced less */
		int scaleFactor = 1;
		if ((targetW > 0) || (targetH > 0)) {
			scaleFactor = Math.min(photoW / targetW, photoH / targetH);
		}

		/* Set bitmap options to scale the image decode target */
		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scaleFactor;

		/* Decode the JPEG file into a Bitmap */
		Bitmap bitmap = BitmapFactory.decodeFile(imageUri, bmOptions);

		return bitmap;
	}
	
	public static void copy(String srcPath, String dstPath) throws IOException {
		copy(new File(srcPath),new File(dstPath));
		//copy(srcPath,dstPath);
	}
	
	public static void copy(File src, File dst) throws IOException {
	    FileInputStream inStream = new FileInputStream(src);
	    FileOutputStream outStream = new FileOutputStream(dst);
	    FileChannel inChannel = inStream.getChannel();
	    FileChannel outChannel = outStream.getChannel();
	    inChannel.transferTo(0, inChannel.size(), outChannel);
	    inStream.close();
	    outStream.close();
	}
	/**
	 * 
	 * @param filePath
	 * @return true if file exist and deleted. False if file not exist, exception during deletion
	 */
	public static Boolean deleteFile(String filePath){
		File file = new File(filePath);
		if(file.exists())
			return file.delete();
		else{
			Log.i(TAG, "File not exist");
			return false;
		}
	}

	/************************************** Private Methods *************************/
	private static File getNewFileInDir(String fileName, String dirName) {
		try {
			// String mBaseFolderPath = mAppStorageDirectory +"/"+ dirName;
			// File imageDir = new File(mBaseFolderPath);//Make the image folder
			File imageDir = getAlbumDir(dirName);
			if (!imageDir.exists() || !imageDir.isDirectory())
				imageDir.mkdir();
			File imageFile = new File(imageDir, URLEncoder.encode(fileName,
					"UTF-8"));
			return imageFile;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	/*************************************************** Not Used Methods. Intentionally Left for reference ********************************/
	private static boolean isSdCardWrittenable() {

		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			return true;
		}
		return false;
	}

	/**
	 * Iterative call is fine.It will set only once. Here,not checking SD card
	 * available/mounted/MAX threshold per application These checks are just
	 * before storing the file in SD card.
	 * {@link ImageCacheManager.ImageDiskCache}
	 * 
	 * @param appSpecificRootDir
	 */
	private static void setAppStorageDirectory(File appSpecificRootDir) {
		// if(!hasAppDirSet){
		// if(appSpecificRootDir != null){
		// /**
		// appSpecificRootDir will be null in case of :
		// 1). API level less then 8
		// 2). SD card unmounted while storing operation is going on.
		// */
		// mAppStorageDirectory = appSpecificRootDir.toString();
		// hasAppDirSet = true;
		// }
		// }
	}

	private static long getAvailableStorage() {

		String storageDirectory = null;
		storageDirectory = Environment.getExternalStorageDirectory().toString();

		try {
			StatFs stat = new StatFs(storageDirectory);
			long avaliableSize = ((long) stat.getAvailableBlocks() * (long) stat
					.getBlockSize());
			return avaliableSize;
		} catch (RuntimeException ex) {
			return 0;
		}
	}

	private static int getAvailableStorageMB() {

		String storageDirectory = null;
		storageDirectory = Environment.getExternalStorageDirectory().toString();

		try {
			StatFs stat = new StatFs(storageDirectory);
			long avaliableSize = ((long) stat.getAvailableBlocks() * (long) stat
					.getBlockSize());
			return (int) (avaliableSize / (1024 * 1024));
		} catch (RuntimeException ex) {
			return 0;
		}
	}

	private static boolean checkAvailableStorage() {

		if (getAvailableStorage() < LOW_STORAGE_THRESHOLD) {
			return false;
		}
		return true;
	}

	private static boolean isSDCardPresent() {

		return Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED);
	}

	private static Bitmap getLoacalBitmap(String url) {

		try {
			FileInputStream fis = new FileInputStream(url);
			return BitmapFactory.decodeStream(fis);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}

	private static String size(long size) {

		if (size / (1024 * 1024) > 0) {
			float tmpSize = (float) (size) / (float) (1024 * 1024);
			DecimalFormat df = new DecimalFormat("#.##");
			return "" + df.format(tmpSize) + "MB";
		} else if (size / 1024 > 0) {
			return "" + (size / (1024)) + "KB";
		} else
			return "" + size + "B";
	}

	/**
	 * 
	 * @param bm
	 * @param url
	 * @param directoryName
	 *            Should be fully qualified e.g String mAppImageFolderPath =
	 *            <context>.getExternalFilesDir(null) + directoryName;
	 * @return
	 */
	private static Boolean putBitmapInDisk(Bitmap bm, String url,
			String directoryName) {
		try {
			File imageFile = getNewFile(URLEncoder.encode(url, "UTF-8"));
			FileOutputStream stream = new FileOutputStream(
					imageFile.getAbsolutePath());
			if (!bm.compress(CompressFormat.PNG, 100, stream))
				Log.w(TAG, "Compression Failed.Url is " + url);
			stream.flush();
			stream.close();
			return true;
		} catch (Exception e) {
			Log.w(TAG, "Exception cached" + url + e.toString());
			return false;
		}
	}

	/**
	 * 
	 * @param url
	 *            directoryName Should be fully qualified e.g String
	 *            mAppImageFolderPath = <context>.getExternalFilesDir(null) +
	 *            directoryName;
	 * @return
	 */
	private static Bitmap getBitmapFromDisk(String url, String directoryName) {
		try {
			// String mFilePath = directoryName+"/" + URLEncoder.encode(url,
			// "UTF-8");
			String mFilePath = getAlbumDir(directoryName) + "/"
					+ URLEncoder.encode(url, "UTF-8");
			if (!new File(mFilePath).exists())
				return null;
			BitmapFactory.Options options = new BitmapFactory.Options();
			// Note:@D:Use Bitmap.Config.ARGB_8888 if only you need very high
			// quality images but
			// It will convert 9 kb ob image to Approx : 120 KB.
			options.inPreferredConfig = Bitmap.Config.RGB_565;
			Bitmap bitmap = BitmapFactory.decodeFile(mFilePath, options);
			if (bitmap == null)
				Log.w(TAG, "Fetching failed from Disc.Url is " + url);
			return bitmap;
		} catch (Exception ex) {
			Log.w(TAG, "EXCEPTION:Error : " + ex.getMessage());
			return null;
		}
	}

	/**
	 * Will delete file or all files of folder
	 * 
	 * @param path
	 * @return
	 */
	private static synchronized boolean delete(File path) {

		boolean result = true;
		if (path.exists()) {
			if (path.isDirectory()) {
				for (File child : path.listFiles()) {
					result &= delete(child);
				}
				result &= path.delete();
			}
			if (path.isFile()) {
				result &= path.delete();
			}
			if (!result) {
				Log.e(null, "Delete failed;");
			}
			return result;
		} else {
			Log.e(null, "File does not exist.");
			return false;
		}
	}

	/**
	 * Returns File object pointing to requested fileName in app's internal
	 * storage, creating the file if it doesn't exists
	 * 
	 * @param context
	 * @param directoryName
	 * @param fileName
	 * @return
	 */
	private static File getFileInInternalStorage(Context context,
			String directoryName, String fileName) {
		File topLevelDir = context.getDir(directoryName, Context.MODE_PRIVATE);
		File tempFile = new File(topLevelDir, fileName);

		if (!tempFile.exists()) {
			try {
				tempFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return tempFile;
	}

	/**
	 * To be used for creating ne file in SD card as output
	 * 
	 * @param fileName
	 * @param directoryName
	 * @return
	 */
	private static File getNewTempFile(String fileName, String directoryName,
			String suffix) {
		try {
			File imageFile = new File(getAlbumDir(directoryName),
					URLEncoder.encode(fileName, "UTF-8"));
			return imageFile;
		} catch (IOException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

}
