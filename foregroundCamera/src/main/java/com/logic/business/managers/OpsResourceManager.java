package com.logic.business.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.logic.business.FileIOUtil;
import com.logic.foregroundcamera.R;

public class OpsResourceManager {
	public static final String INPUT_PATH_KEY = "input_path_key";
	public static final String OUTPUT_PATH_KEY = "output_path_key";
	public static final String INTERMEDIATE_PATH_KEY = "intermediate_path_key";
	public static final String EDITING_ON_OFF = "editing_on_off";
	private Context mContext = null;
	private SharedPreferences mPref = null;
	private Editor mEditor = null;

	private String inputFilePath;
	private String outputFilePath;
	private String intmFilePath;
	public OpsResourceManager(Context context) {
		// TODO Auto-generated constructor stub
		mContext = context;
		mPref = mContext.getSharedPreferences(mContext.getString(R.string.pref_name), Context.MODE_PRIVATE);
	}

	public void setIOFilePaths(String inputFilePath,String outputFilePath){
		this.inputFilePath = inputFilePath;
		this.outputFilePath = outputFilePath;
		addToSharedPref(INPUT_PATH_KEY, inputFilePath);
		addToSharedPref(OUTPUT_PATH_KEY, outputFilePath);
	}
	/**
	 * To be set true when user activates any image operation like cropping,effects etc.
	 * Based on this preview on HomeFragment will be shown
	 * Set it false when user exist.
	 * @param isEditingOn
	 */
	public void setEditing(Boolean isEditingOn){
		addToSharedPref(EDITING_ON_OFF, isEditingOn);
	}
	public Boolean isEditingOn(){
		return getBooleanFromSharedPref(EDITING_ON_OFF, false);
	}
	private void setIntmFilePath(String filePath){
		this.intmFilePath = filePath;
		addToSharedPref(INTERMEDIATE_PATH_KEY, filePath);
	}
	private void addToSharedPref(String key,String value){
		mEditor = mPref.edit();
		mEditor.putString(key, value);
		mEditor.commit();
	}
	public String getInputFilePath(){
		if(inputFilePath == null)
			inputFilePath = getDataFromSharedPref(INPUT_PATH_KEY);
		return inputFilePath;
	}
	public String getOutputFilePath(){
		if(outputFilePath == null)
			outputFilePath = getDataFromSharedPref(OUTPUT_PATH_KEY);
		return outputFilePath;
	}
	public String getRunningIntmFilePath(){
		if(intmFilePath == null)
			intmFilePath = getDataFromSharedPref(INTERMEDIATE_PATH_KEY);
		if(intmFilePath == null){//Still null. Create file. Put it in shared pref 
			intmFilePath = FileIOUtil.getNewFileBasedOnTimeStamp().getAbsolutePath();
			setIntmFilePath(intmFilePath);
		}
		return intmFilePath;
	}
	/**
	 * Will be used when app enters into library
	 * @return
	 */
	public String getNewIntmFilePath(){
		if(intmFilePath == null)
			intmFilePath = getDataFromSharedPref(INTERMEDIATE_PATH_KEY);
		if(intmFilePath != null){
			//Delete it
			FileIOUtil.deleteFile(intmFilePath);
			intmFilePath = null;
		}
		intmFilePath = FileIOUtil.getNewFileBasedOnTimeStamp().getAbsolutePath();
		setIntmFilePath(intmFilePath);
		return intmFilePath;
	}
	public void clearSharedPref(String key){
		mEditor = mPref.edit();
		mEditor.remove(key);
		mEditor.commit();
	}
	/**
	 * Default value is null
	 * @param key
	 * @return
	 */
	private String getDataFromSharedPref(String key){
		return mPref.getString(key, null);
	}

	public Boolean getBooleanFromSharedPref(String key,Boolean defaultValue){
		return mPref.getBoolean(key, defaultValue);
	}
	public void addToSharedPref(String key,Boolean value){
		mEditor = mPref.edit();
		mEditor.putBoolean(key, value);
		mEditor.commit();
	}
}
