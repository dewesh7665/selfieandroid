package com.logic.business.managers;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.View;

import com.logic.business.FileIOUtil;
import com.logic.photo.utils.BitmapUtils;
import com.logic.ui.UIConstants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Dewesh Kumar
 */
public class BitmapManager {
    public static interface OnBitmapSavedListener {
        public void onBitmapSaved(String path);
        public void onError(String msg);
    }
    private Context mContext;
    private String mFilePath;
    public BitmapManager(Context context) {
        mContext = context;
    }

    public void requestBitmapFromView(final View view, final OnBitmapSavedListener onBitmapSavedListener) {
        mFilePath = null;
        TaskManager.getInstanse().queueJob(new TaskManager.TaskListner() {
            @Override
            public void doBackGroundTask() {
               mFilePath = createBitmapFromView(view);
            }

            @Override
            public void onBackGroundTaskCompleted() {
                if (mFilePath == null)
                    onBitmapSavedListener.onError(UIConstants.BMP_ERROR);
                else
                    onBitmapSavedListener.onBitmapSaved(mFilePath);
            }
        });
    }

    /**
     * Save bitmap on background thread.
     * @param bitmap
     * @param onBitmapSavedListener
     */
    public void saveBitmap(final Bitmap bitmap, final OnBitmapSavedListener onBitmapSavedListener) {
        mFilePath = null;
        TaskManager.getInstanse().queueJob(new TaskManager.TaskListner() {
            @Override
            public void doBackGroundTask() {
                mFilePath = saveBitmap(bitmap);
            }

            @Override
            public void onBackGroundTaskCompleted() {
                if (mFilePath == null)
                    onBitmapSavedListener.onError(UIConstants.BMP_ERROR);
                else
                    onBitmapSavedListener.onBitmapSaved(mFilePath);
            }
        });
    }

    private String createBitmapFromView(View view) {
        try {
            File dataFile = FileIOUtil.getNewFileBasedOnTimeStamp();
            Bitmap bitmap = BitmapUtils.createBitmap(view);
            FileOutputStream out = new FileOutputStream(dataFile, false);
            bitmap.compress(Bitmap.CompressFormat.PNG, 95, out);
            return dataFile.getAbsolutePath();

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String saveBitmap(Bitmap bitmap) {
        try {
            File dataFile = FileIOUtil.getNewFileBasedOnTimeStamp();
            FileOutputStream out = new FileOutputStream(dataFile, false);
            bitmap.compress(Bitmap.CompressFormat.PNG, 95, out);
            return dataFile.getAbsolutePath();

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Bitmap getBitmapFromPath(String imageUri) {
        //In any case bitmap can't be larger then device width/height
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity)mContext).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenWidth = displaymetrics.widthPixels;
        int screenHeight = displaymetrics.heightPixels;
        return getBitmapFromPath(imageUri, screenWidth, screenHeight);
    }

    public Bitmap getBitmapFromPath(String imageUri, int width, int height) {
		/* There isn't enough memory to open up more than a couple camera photos */
		/* So pre-scale the target bitmap into which the file is decoded */

		/* Get the size of the ImageView */
        int targetW = width;
        int targetH = height;

		/* Get the size of the image */
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imageUri, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

		/* Figure out which way needs to be reduced less */
        int scaleFactor = 1;
        if ((targetW > 0) || (targetH > 0)) {
            scaleFactor = Math.min(photoW / targetW, photoH / targetH);
        }

		/* Set bitmap options to scale the image decode target */
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inMutable = true;

		/* Decode the JPEG file into a Bitmap */
        Bitmap bitmap = BitmapFactory.decodeFile(imageUri, bmOptions);

        return bitmap;
    }

    public String getPathFromUri(Uri uri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.MediaColumns.DATA };
            cursor = mContext.getContentResolver().query(uri, proj, null,
                    null, null);
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void changeBlackToTransparent(Bitmap bitmap) {
        int [] allPixels = new int [ bitmap.getHeight()*bitmap.getWidth()];
        bitmap.getPixels(allPixels, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(),bitmap.getHeight());
        for(int i =0; i<bitmap.getHeight()*bitmap.getWidth();i++) {
            if( allPixels[i] == Color.BLACK)
                allPixels[i] = Color.TRANSPARENT;
        }
        bitmap.setPixels(allPixels, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());
    }

    public Bitmap getTrimmedBitmap(Bitmap bitmap){
        Bitmap bmp = null;
        Rect bmpRegion = getBitmapRegion(bitmap);
        bmp = Bitmap.createBitmap(bmpRegion.width(), bmpRegion.height(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bmp);
        canvas.drawBitmap(bitmap, bmpRegion, bmpRegion, null);
        return bmp;
    }

    private Rect getBitmapRegion(Bitmap bmp) {
        //BitmapDrawable bmpDrawable = (BitmapDrawable) imageView.getDrawable();
        int bmpWidth = bmp.getWidth();
        int bmpHeight = bmp.getHeight();
        Rect bounds = new Rect(0, 0, bmpWidth, bmpHeight);
        //int drawableWidth = bounds.right - bounds.left;
        //int drawableHeight = bounds.bottom - bounds.top;
        Bitmap bitmap = bmp;
        Rect transparentRect = new Rect(0, 0, 0, 0);
        int horizonPixels[] = new int[bmpWidth + 1];// Max sized array
        int verticalPixels[] = new int[bmpHeight + 1];// Max sized array.

        for (int i = bounds.left; i < bounds.right; i++) {
            for (int j = bounds.top; j < bounds.bottom; j++) {
                int argbColor = bitmap.getPixel(i, j);
                if (argbColor != 0) {
                    if (transparentRect.left == 0 || i < transparentRect.left)
                        transparentRect.left = i;
                    if (transparentRect.right == 0 || i > transparentRect.right)
                        transparentRect.right = i;

                    if (transparentRect.top == 0 || j < transparentRect.top)
                        transparentRect.top = j;
                    if (transparentRect.bottom == 0
                            || j > transparentRect.bottom)
                        transparentRect.bottom = j;
                    horizonPixels[i] = i;
                    verticalPixels[j] = j;
                    // horizonPixelCount++;
                    // verticalPixelCount++;
                }
            }
        }
        //Rect transformedRect = getTransparentRectOnScreens(imageView,
        //transparentRect);
        return transparentRect;
    }
}
