package com.logic.business.managers;

import android.os.Handler;
import android.os.Message;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * For long running image processing
 * @author Dewesh_MAC
 *
 */
public class TaskManager {
	private final String TAG = "Task_Manager";
	public static interface TaskListner{
		public void doBackGroundTask();
		public void onBackGroundTaskCompleted();
	}
	private final int POOL_SIZE = 5;
	private final ExecutorService mThreadPool;
	private static TaskManager mInstanse;

	private TaskManager(){
		mThreadPool = Executors.newFixedThreadPool(POOL_SIZE);
	}

	public static TaskManager getInstanse(){
		if(mInstanse == null)
			mInstanse = new TaskManager();
		return mInstanse;
	}

	public void queueJob(final TaskListner taskListner){
		/*final Handler handler = new Handler(){
			@Override  
			public void handleMessage(android.os.Message msg) {
				taskListner.onBackGroundTaskCompleted();
			};
		};*/
		final TaskHandler taskHandler = new TaskHandler(taskListner);
		mThreadPool.submit(new Runnable() {

			@Override
			public void run() {
				taskListner.doBackGroundTask();
				Message message = Message.obtain();  
				message.obj = "Task Performed";   
				taskHandler.sendMessage(message);  
			}
		});
	}
	/**
	 * NonStatic inner handlers won't be eligible for garbage collection 
	 * @author deweshkumar
	 *
	 */
	static class TaskHandler extends Handler{
		//private final WeakReference<TaskListner> taskListner;
		private final TaskListner taskListner;
		TaskHandler(TaskListner taskListner){
			//this.taskListner = new WeakReference<TaskListner>(taskListner);
			this.taskListner = taskListner;
		}

		@Override
		public void handleMessage(Message msg) {
			if(this.taskListner != null)
				this.taskListner.onBackGroundTaskCompleted();
		}
	}
}
