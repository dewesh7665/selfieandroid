//
//  ImageUtility.h
//  ScannerDemo
//
//  Created by Sharad Shankar on 20/07/13.
//  Copyright (c) 2013 Sharad Shankar. All rights reserved.
//

#ifndef OpenCV_Tutorial_ImageUtility_h
#define OpenCV_Tutorial_ImageUtility_h

#include "Line.h"
#include "Quadrilateral.h"

class ImageUtility
{
public:
    ImageUtility();
    static void drawLines(vector<Line> lines,cv::Mat& inputFrame,int scaleFactor,cv::Scalar color);
    static void drawQuadrilateral(Quadrilateral quad,cv::Mat& inputFrame,int scaleFactor,cv::Scalar color);
    static cv::Point2f getPointOfIntersection(Line line1, Line line2);
    static float findAngleBetweenTwoLines(Line line1,Line line2);
    static float findDistanceBetweenTwoPoints(cv::Point2f pt1,cv::Point2f pt2);
private:
   
};

#endif

