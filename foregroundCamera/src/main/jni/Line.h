//
//  Line.h
//  ScannerDemo
//
//  Created by Sharad Shankar on 20/07/13.
//  Copyright (c) 2013 Sharad Shankar. All rights reserved.
//


#ifndef OpenCV_Tutorial_Line_h
#define OpenCV_Tutorial_Line_h
#include "ScanConfigurator.h"
class Line
{
public:
    Line();
    Line(cv::Point2f startPt,cv::Point2f endPt);
    Line( const Line &obj);  // copy constructor
    cv::Point2f getStartPoint();
    cv::Point2f getEndPoint();
    
private:
    cv::Point2f pt1;
    cv::Point2f pt2;
};

#endif
