//
//  Line.m
//  ScannerDemo
//
//  Created by Sharad Shankar on 20/07/13.
//  Copyright (c) 2013 Sharad Shankar. All rights reserved.
//

#include "Line.h"

Line::Line(){
    
}
Line::Line(cv::Point2f startPt,cv::Point2f endPt){
    pt1 = startPt;
    pt2 = endPt;
}

Line::Line(const Line &obj)
{
    pt1 = obj.pt1;
    pt2 = obj.pt2;
}

cv::Point2f Line::getStartPoint(){
    return pt1;
}

cv::Point2f Line:: getEndPoint(){
    return pt2;
}