//
//  Quadrilateral.h
//  ScannerDemo
//
//  Created by Sharad Shankar on 20/07/13.
//  Copyright (c) 2013 Sharad Shankar. All rights reserved.
//

#ifndef OpenCV_Tutorial_Quadrilateral_h
#define OpenCV_Tutorial_Quadrilateral_h

#include "Line.h"

class Quadrilateral
{
public:
    Quadrilateral();
    Quadrilateral( const Quadrilateral &obj);  // copy constructor
    cv::Point2f getPt1();
    void setPt1(cv::Point2f pt);
    cv::Point2f getPt2();
    void setPt2(cv::Point2f pt);
    cv::Point2f getPt3();
    void setPt3(cv::Point2f pt);
    cv::Point2f getPt4();
    void setPt4(cv::Point2f pt);
    
    float getFirstHorLineLength();
    void setFirstHorLineLength(float firsthorLineLength);
    float getLeftVertLineLength();
    void setLeftVertLineLength(float leftvertLineLength);
    float getRightVertLineLength();
    void setRightVertLineLength(float rightvertLineLength);
    float getLeftFirstVertOverlapPercentage();
    void setLeftFirstVertOverlapPercentage(
                                           float leftfirstVertOverlapPercentage);
    float getRightFirstVertOverlapPercentage();
    void setRightFirstVertOverlapPercentage(
                                            float rightfirstVertOverlapPercentage);
    Line getHorLine1();
    void setHorLine1(Line horline1);
    Line getHorLine2();
    void setHorLine2(Line horline2);
    Line getVertLine1();
    void setVertLine1(Line vertline1);
    Line getVertLine2();
    void setVertLine2(Line vertline2);
    float getSecondHoriOverlapPercentage();
    void setSecondHoriOverlapPercentage(float secondhoriOverlapPercentage);
    bool isAllLinesFound();
    void setAllLinesFound(bool alllinesFound);
    
private:
    cv::Point2f pt1;
    cv::Point2f pt2;
    cv::Point2f pt3;
    cv::Point2f pt4;
    //Other representation can be set of 4 lines
    Line horLine1;
    Line horLine2;
    Line vertLine1;
    Line vertLine2;
    //Keep overlap percentage to maximum when initialized
    float leftFirstVertOverlapPercentage;
    float rightFirstVertOverlapPercentage;
    float secondHoriOverlapPercentage;
    
    float firstHorLineLength;
    float leftVertLineLength;
    float rightVertLineLength;
    
    bool allLinesFound;
};

#endif