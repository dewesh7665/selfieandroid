//
//  ScanFrameDetector.h
//  LBScanner
//
//  Created by Sharad Shankar on 28/04/15.
//  Copyright (c) 2015 Vineet Jain. All rights reserved.
//

#ifndef LBScanner_ScanFrameDetector_h
#define LBScanner_ScanFrameDetector_h
#include "Quadrilateral.h"

typedef enum{LINE_HORIZONTAL,LINE_VERTICAL}LineDirection;
class ScanFrameDetector{
public:
    ScanFrameDetector();
    ~ScanFrameDetector();
    //void initializeDetector(cv::Mat& inputFrame);
    int getProcessedFrameWidth();
    int getProcessedFrameHeight();
    Quadrilateral findFrameQuad(Mat& originalFrame);
    
private:
    Mat mOriginalMat;
    Mat mGrayMat;
    Mat mProcessedGrayMat;
    Mat mHorizontalEdgeMap;
    Mat mVerticalEdgeMap;
    Mat mAbshorizontalMat;
    Mat mAbsVerticalMat;
    
    vector<Line> mHorizontalLines;
    vector<Line> mVerticalLines;
    std::vector<cv::Vec4i> mLines;
    
    Mat mHorizontalLinesMat;
    Mat mVerticalLinesMat;
    int mScaleFactor;
    vector<Quadrilateral> mPlaneQuads;
    vector<Quadrilateral> mVerticalQuads;
    int mThreshold;
    int mMinLineSize;
    //Private methods
    void resetPreviousState();
    void configureMats();
    void configureScaleFactor();
    void findLines(vector<cv::Vec4i>& lines,LineDirection direction);
    
    cv::Mat m_rgbaOriginal,m_rgbaProcessed;
    int m_maxFameWidth;
    int m_processedFrameWidth;
    int m_processedFrameHeight;
    float m_resizeRatio;
    int m_gaussianKernelSize;
    int m_notDetectedFrameCount;

    int mHoughScale;
    cv::Mat mScaledHorizontalEdgeMap;
    cv::Mat mScaledVerticalEdgeMap;
};


#endif
