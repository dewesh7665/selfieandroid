//
//  QuadrilateralFinder.m
//  LBScanner
//
//  Created by Sharad Shankar on 28/04/15.
//  Copyright (c) 2015 Vineet Jain. All rights reserved.
//

#include "QuadrilateralFinder.h"
#include "ImageUtility.h"

QuadrilateralFinder::QuadrilateralFinder(){
    
}

QuadrilateralFinder::~QuadrilateralFinder(){
    
}

vector<Quadrilateral> QuadrilateralFinder::findQuadrialaterals(vector<Line>& baseLines,vector<Line>& comparisonLines,bool isHorizontalMode)
{
    vector<Quadrilateral> probableQuads;
    for(int i=0;i<baseLines.size();i++){
        //Calculate the minimumoverlap for each perpendicualr lines..
        //First find point of intersection
        Line horizontalLine = baseLines[i];
        int horx1 = (int)horizontalLine.getStartPoint().x,hory1 = (int)horizontalLine.getStartPoint().y;
        int horx2 = (int)horizontalLine.getEndPoint().x, hory2 = (int)horizontalLine.getEndPoint().y;
        double centerX = (horizontalLine.getStartPoint().x + horizontalLine.getEndPoint().x)/2.0f;
        double centerY =  (horizontalLine.getStartPoint().y + horizontalLine.getEndPoint().y)/2.0f;
        
        float horizontalLineLength = (float)sqrtf((hory2-hory1)*(hory2- hory1) +(horx2-horx1)*(horx2-horx1));
        
        Line* leftVerticalLine = NULL;
        Line* rightVerticalLine = NULL;
        float leftVerticalDistancePercentage = 2.0f;
        float rightVerticalDistancePercentage = 2.0f;
        float leftVerticalLineLength = 0.0f;
        float rightVerticalLineLength = 0.0f;
        
        for(int j=0;j<comparisonLines.size();j++){
            Line verticalLine = comparisonLines[j];
            cv::Point2f ptIntersection = ImageUtility::getPointOfIntersection(horizontalLine, verticalLine);
            if(ptIntersection.x ==0 && ptIntersection.y==0.0)
                continue;
            
            //Get distance from vertical line
            int vertX1 = (int)verticalLine.getStartPoint().x,vertY1 = (int)verticalLine.getStartPoint().y;
            int vertX2 = (int)verticalLine.getEndPoint().x,vertY2 = (int)verticalLine.getEndPoint().y;
            double verticalLineLength = (float)sqrtf((vertY2 - vertY1)*(vertY2 - vertY1) + (vertX2 - vertX1)*(vertX2 - vertX1));
            float overlapPercentage = findOverlapPercentage(horizontalLine,verticalLine,ptIntersection,horizontalLineLength,verticalLineLength,false);
            if(overlapPercentage == 0.0f)
                continue;
            //Now check whether this lies on left or on right of horizontal line
            bool doesthislieOnLeft = false;
            if(isHorizontalMode){
                if(ptIntersection.x < centerX)
                    doesthislieOnLeft = true;
            }
            else{
                if(ptIntersection.y < centerY)
                    doesthislieOnLeft= true;
            }
            if(doesthislieOnLeft){
                //This lies on left
                if(overlapPercentage < leftVerticalDistancePercentage){
                    leftVerticalDistancePercentage = overlapPercentage;
                    leftVerticalLine = new Line(verticalLine.getStartPoint(), verticalLine.getEndPoint());
                    leftVerticalLineLength = verticalLineLength;
                }
            }
            else{
                //This lies on right
                if(overlapPercentage < rightVerticalDistancePercentage){
                    rightVerticalDistancePercentage = overlapPercentage;
                    rightVerticalLine = new Line(verticalLine.getStartPoint(), verticalLine.getEndPoint());
                    rightVerticalLineLength = verticalLineLength;
                }
            }
        }
        //Now if left vertical line and right vertical line is found then create quad from them else move to next base line
        if(leftVerticalLine != NULL && rightVerticalLine!= NULL){
            //First check if both are on same side of horizontal line only then consider them
            bool isValidCombination = true;
            if(isHorizontalMode){
                //Lines center y difference multiplication should be positive
                float leftVerticalCenterY = (float)(leftVerticalLine->getStartPoint().y + leftVerticalLine->getEndPoint().y)/2;
                float rightVerticalCenterY = (float)(rightVerticalLine->getStartPoint().y + rightVerticalLine->getEndPoint().y)/2;
                if((leftVerticalCenterY - centerY)*(rightVerticalCenterY - centerY) < 0.0f)
                    isValidCombination = false;
            }
            else{
                //Lines center y difference multiplication should be positive
                float leftVerticalCenterX = (float)(leftVerticalLine->getStartPoint().x + leftVerticalLine->getEndPoint().x)/2;
                float rightVerticalCenterX = (float)(rightVerticalLine->getStartPoint().x + rightVerticalLine->getEndPoint().x)/2;
                if((leftVerticalCenterX - centerX)*(rightVerticalCenterX - centerX) < 0.0f)
                    isValidCombination = false;
            }
            //This is a probable quad
            if(isValidCombination)
            {
                Quadrilateral quad;
                quad.setHorLine1(horizontalLine);
                quad.setVertLine1(Line(leftVerticalLine->getStartPoint(),leftVerticalLine->getEndPoint()));
                quad.setVertLine2(Line(rightVerticalLine->getStartPoint(),rightVerticalLine->getEndPoint()));
                quad.setLeftFirstVertOverlapPercentage(leftVerticalDistancePercentage);
                quad.setRightFirstVertOverlapPercentage(rightVerticalDistancePercentage);
                quad.setLeftVertLineLength(leftVerticalLineLength);
                quad.setRightVertLineLength(rightVerticalLineLength);
                quad.setFirstHorLineLength(horizontalLineLength);
                probableQuads.push_back(quad);
            }
        }
        delete leftVerticalLine; delete rightVerticalLine;
    }
    //Now we have set of all probable quads we need to find the one with best overlap
    vector<Quadrilateral> bestQuads;
    float sumOverlap = 20.0f;
    Quadrilateral currentBestQuad;
    for(int i=0;i<probableQuads.size();i++){
        Quadrilateral currentQuad = probableQuads[i];
        float sum = currentQuad.getLeftFirstVertOverlapPercentage() + currentQuad.getRightFirstVertOverlapPercentage()+ currentQuad.getSecondHoriOverlapPercentage();
        if(sum <sumOverlap){
            sumOverlap = sum;
            currentBestQuad = currentQuad;
        }
    }
    if(sumOverlap < 20.0)
        bestQuads.push_back(currentBestQuad);
    return bestQuads;
}

float QuadrilateralFinder::findOverlapPercentage(Line horizontalLine,Line verticalLine,cv::Point2f ptIntersection,float horizontalLineLength,float verticalLineLength,bool applyCheck)
{
    int horx1 = (int)horizontalLine.getStartPoint().x,hory1 = (int)horizontalLine.getStartPoint().y;
    int horx2 = (int)horizontalLine.getEndPoint().x, hory2 = (int)horizontalLine.getEndPoint().y;
    //First find point of intersection
    //ptIntersection = PhotoUtils.getPointOfIntersection(horizontalLine, verticalLine);
    
    //Get distance from first point on horizontal line
    int firstXDistance =(int) ((ptIntersection.x-horx1)*((int)ptIntersection.x-horx1)+(ptIntersection.y -hory1)*(ptIntersection.y - hory1));
    //Get distance from second point on horizontal line
    int secondXDistance = (int) ((ptIntersection.x-horx2)*(ptIntersection.x-horx2)+(ptIntersection.y -hory2)*(ptIntersection.y - hory2));
    int minHorDistance = firstXDistance <secondXDistance ? firstXDistance : secondXDistance;
    if(applyCheck && minHorDistance > horizontalLineLength/2)
        return 0.0f;
    
    //Get vertical distance from point of intersection
    int vertX1 = (int)verticalLine.getStartPoint().x,vertY1 = (int)verticalLine.getStartPoint().y;
    int vertX2 = (int)verticalLine.getEndPoint().x,vertY2 = (int)verticalLine.getEndPoint().y;
    int firstYDistance = ((int)ptIntersection.x - vertX1)*((int)ptIntersection.x - vertX1) + ((int)ptIntersection.y - vertY1)*((int)ptIntersection.y - vertY1);
    int secondYDistance = (int) ((ptIntersection.x - vertX2)*(ptIntersection.x - vertX2) + (ptIntersection.y - vertY2)*(ptIntersection.y - vertY2));
    int minVertDistance = firstYDistance < secondYDistance ? firstYDistance : secondYDistance;
    if(applyCheck && minVertDistance > verticalLineLength/2)
        return 0.0f;
    //Find overlap percentage and get
    float overlapPercentage = (float)((sqrtf(minHorDistance)/horizontalLineLength) + (sqrtf(minVertDistance)/verticalLineLength)) ;
    return overlapPercentage;
}

Quadrilateral QuadrilateralFinder::findMergedQuadrilateral(Quadrilateral* horizontalQuad, Quadrilateral* verticalQuad)
{
    if(horizontalQuad == NULL && verticalQuad== NULL)
        return Quadrilateral();
    //Handle null cases
    if(horizontalQuad == NULL || verticalQuad == NULL){
        Quadrilateral baseQuad;
        if(horizontalQuad == NULL)
            baseQuad = *verticalQuad;
        else
            baseQuad = *horizontalQuad;
        
        cv::Point2f firstPoint =ImageUtility::getPointOfIntersection(baseQuad.getHorLine1(), baseQuad.getVertLine1());
        cv::Point2f secondPoint = ImageUtility::getPointOfIntersection(baseQuad.getHorLine1(), baseQuad.getVertLine2());
        
        //Third and forth point will be points on vertical line which are away from horizontal line
        //Find the point farthest from second point on vertical line 2
        float firstVertDist =(float) ((secondPoint.x - baseQuad.getVertLine2().getStartPoint().x)* (secondPoint.x - baseQuad.getVertLine2().getStartPoint().x) + (secondPoint.y - baseQuad.getVertLine2().getStartPoint().y)* (secondPoint.y - baseQuad.getVertLine2().getStartPoint().y));
        float secondVertDist =(float) ((secondPoint.x - baseQuad.getVertLine2().getEndPoint().x)* (secondPoint.x - baseQuad.getVertLine2().getEndPoint().x) + (secondPoint.y - baseQuad.getVertLine2().getEndPoint().y)* (secondPoint.y - baseQuad.getVertLine2().getEndPoint().y));
        
        cv::Point2f thirdPoint;
        if(firstVertDist > secondVertDist)
            thirdPoint = baseQuad.getVertLine2().getStartPoint();
        else
            thirdPoint = baseQuad.getVertLine2().getEndPoint();
        
        firstVertDist =(float) ((firstPoint.x - baseQuad.getVertLine1().getStartPoint().x)* (firstPoint.x - baseQuad.getVertLine1().getStartPoint().x) + (firstPoint.y - baseQuad.getVertLine1().getStartPoint().y)* (firstPoint.y - baseQuad.getVertLine1().getStartPoint().y));
        secondVertDist =(float) ((firstPoint.x - baseQuad.getVertLine1().getEndPoint().x)* (firstPoint.x - baseQuad.getVertLine1().getEndPoint().x) + (firstPoint.y - baseQuad.getVertLine1().getEndPoint().y)* (firstPoint.y - baseQuad.getVertLine1().getEndPoint().y));
        
        cv::Point2f forthPoint;
        if(firstVertDist > secondVertDist)
            forthPoint = baseQuad.getVertLine1().getStartPoint();
        else
            forthPoint = baseQuad.getVertLine1().getEndPoint();
        
        baseQuad.setPt1(firstPoint);baseQuad.setPt2(secondPoint);baseQuad.setPt3(thirdPoint);baseQuad.setPt4(forthPoint);
        return baseQuad;
    }
    
    //If both lines are found then find the most appropriate quad
    float horizontalOverlapSum = horizontalQuad->getLeftFirstVertOverlapPercentage()+ horizontalQuad->getRightFirstVertOverlapPercentage();
    float verticalOverlapSum = verticalQuad->getLeftFirstVertOverlapPercentage() + verticalQuad->getRightFirstVertOverlapPercentage();
    //keep one quad as a base quad -- one with minimum overlap
    Quadrilateral baseQuad;
    if(horizontalOverlapSum < verticalOverlapSum){
        baseQuad = *horizontalQuad;
        //Now set the fourth side of the quad as farthest horizontal line in second quad
        Line firstHorizontalLine = verticalQuad->getVertLine1();
        Line secondHorizontalLine = verticalQuad->getVertLine2();
        Line baseHorizontalLine = baseQuad.getHorLine1();
        Line baseSecondHorizontalLine;
        //Check which one is closest line
        float baseCenterY = (float)(baseHorizontalLine.getStartPoint().y + baseHorizontalLine.getEndPoint().y)/2;
        float firstCenterY = (float)(firstHorizontalLine.getStartPoint().y + firstHorizontalLine.getEndPoint().y)/2;
        float secondCenterY = (float)(secondHorizontalLine.getStartPoint().y + secondHorizontalLine.getEndPoint().y)/2;
        if(fabs(firstCenterY - baseCenterY) > fabs(secondCenterY - baseCenterY))
            baseSecondHorizontalLine = firstHorizontalLine;
        else
            baseSecondHorizontalLine = secondHorizontalLine;
        baseQuad.setHorLine2(baseSecondHorizontalLine);
    }
    else{
        baseQuad = *verticalQuad;
        //Now set the fourth side of the quad as farthest horizontal line in second quad
        Line firstVerticalLine = horizontalQuad->getVertLine1();
        Line secondVerticalLine = horizontalQuad->getVertLine2();
        Line baseVerticalLine = baseQuad.getHorLine1();
        Line baseSecondHorizontalLine;
        //Check which one is closest line
        float baseCenterX = (float)(baseVerticalLine.getStartPoint().x + baseVerticalLine.getEndPoint().x)/2;
        float firstCenterX = (float)(firstVerticalLine.getStartPoint().x + firstVerticalLine.getEndPoint().x)/2;
        float secondCenterX = (float)(secondVerticalLine.getStartPoint().x + secondVerticalLine.getEndPoint().x)/2;
        if(fabs(firstCenterX - baseCenterX) > fabs(secondCenterX - baseCenterX))
            baseSecondHorizontalLine = firstVerticalLine;
        else
            baseSecondHorizontalLine = secondVerticalLine;
        baseQuad.setHorLine2(baseSecondHorizontalLine);
    }
    
    cv::Point2f firstPoint = ImageUtility::getPointOfIntersection(baseQuad.getHorLine1(), baseQuad.getVertLine1());
    cv::Point2f secondPoint = ImageUtility::getPointOfIntersection(baseQuad.getHorLine1(), baseQuad.getVertLine2());
    cv::Point2f thirdPoint = ImageUtility::getPointOfIntersection(baseQuad.getVertLine2(), baseQuad.getHorLine2());
    cv::Point2f forthPoint = ImageUtility::getPointOfIntersection(baseQuad.getHorLine2(), baseQuad.getVertLine1());
    baseQuad.setPt1(firstPoint);baseQuad.setPt2(secondPoint);baseQuad.setPt3(thirdPoint);baseQuad.setPt4(forthPoint);
    
    return baseQuad;
}
