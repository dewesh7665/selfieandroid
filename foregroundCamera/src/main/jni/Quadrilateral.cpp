//
//  Quadrilateral.m
//  ScannerDemo
//
//  Created by Sharad Shankar on 20/07/13.
//  Copyright (c) 2013 Sharad Shankar. All rights reserved.
//

#include "Quadrilateral.h"
Quadrilateral::Quadrilateral(){
    pt1 = cv::Point2f(0,0);
    pt2 = cv::Point2f(0,0);
    pt3 = cv::Point2f(0,0);
    pt4 = cv::Point2f(0,0);
    leftFirstVertOverlapPercentage = 5.0f;
    rightFirstVertOverlapPercentage = 5.0f;
    secondHoriOverlapPercentage = 5.0f;
}

Quadrilateral::Quadrilateral( const Quadrilateral &obj){
    pt1 = obj.pt1;
    pt2 = obj.pt2;
    pt3 = obj.pt3;
    pt4 = obj.pt4;
    horLine1 = obj.horLine1;
    horLine2 = obj.horLine2;
    vertLine1 = obj.vertLine1;
    vertLine2 = obj.vertLine2;
    
    leftFirstVertOverlapPercentage = obj.leftFirstVertOverlapPercentage;
    rightFirstVertOverlapPercentage = obj.rightFirstVertOverlapPercentage;
    secondHoriOverlapPercentage = obj.secondHoriOverlapPercentage;
    firstHorLineLength = obj.firstHorLineLength;
    leftVertLineLength = obj.leftVertLineLength;
    rightVertLineLength = obj.rightVertLineLength;
}
cv::Point2f Quadrilateral::getPt1(){
    return pt1;
}

void Quadrilateral::setPt1(cv::Point2f pt){
    pt1 = pt;
}

cv::Point2f Quadrilateral::getPt2(){
    return pt2;
}

void Quadrilateral::setPt2(cv::Point2f pt){
    pt2 = pt;
}

cv::Point2f Quadrilateral::getPt3(){
    return pt3;
}

void Quadrilateral::setPt3(cv::Point2f pt){
    pt3 = pt;
}

cv::Point2f Quadrilateral::getPt4(){
    return pt4;
}

void Quadrilateral::setPt4(cv::Point2f pt){
    pt4 = pt;
}

float Quadrilateral::getFirstHorLineLength(){
    return firstHorLineLength;
}

void Quadrilateral::setFirstHorLineLength(float firsthorLineLength){
    firstHorLineLength = firsthorLineLength;
}

float Quadrilateral::getLeftVertLineLength(){
    return leftVertLineLength;
}

void Quadrilateral::setLeftVertLineLength(float leftvertLineLength){
    leftVertLineLength = leftvertLineLength;
}

float Quadrilateral::getRightVertLineLength(){
    return rightVertLineLength;
}

void Quadrilateral::setRightVertLineLength(float rightvertLineLength){
    rightVertLineLength = rightvertLineLength;
}

float Quadrilateral::getLeftFirstVertOverlapPercentage(){
    return leftFirstVertOverlapPercentage;
}

void Quadrilateral::setLeftFirstVertOverlapPercentage(
                                                      float leftfirstVertOverlapPercentage){
    leftFirstVertOverlapPercentage = leftfirstVertOverlapPercentage;
}

float Quadrilateral::getRightFirstVertOverlapPercentage(){
    return rightFirstVertOverlapPercentage;
}
void Quadrilateral::setRightFirstVertOverlapPercentage(
                                                       float rightfirstVertOverlapPercentage){
    rightFirstVertOverlapPercentage = rightfirstVertOverlapPercentage;
}

Line Quadrilateral::getHorLine1(){
    return horLine1;
}

void Quadrilateral::setHorLine1(Line horline1){
    horLine1 = horline1;
}

Line Quadrilateral::getHorLine2(){
    return horLine2;
}

void Quadrilateral::setHorLine2(Line horline2){
    horLine2 = horline2;
}

Line Quadrilateral::getVertLine1(){
    return vertLine1;
}

void Quadrilateral::setVertLine1(Line vertline1){
    vertLine1 = vertline1;
}

Line Quadrilateral::getVertLine2(){
    return vertLine2;
}

void Quadrilateral::setVertLine2(Line vertline2){
    vertLine2 = vertline2;
}

float Quadrilateral::getSecondHoriOverlapPercentage(){
    return secondHoriOverlapPercentage;
}

void Quadrilateral::setSecondHoriOverlapPercentage(float secondhoriOverlapPercentage){
    secondHoriOverlapPercentage = secondhoriOverlapPercentage;
}

bool Quadrilateral::isAllLinesFound(){
    return allLinesFound;
}

void Quadrilateral::setAllLinesFound(bool alllinesFound){
    allLinesFound = alllinesFound;
}
