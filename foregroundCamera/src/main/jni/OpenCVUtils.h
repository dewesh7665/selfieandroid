//
//  OpenCVUtils.h
//  VisionDemo
//
//  Created by Sharad Shankar on 24/05/13.
//  Copyright (c) 2013 Sharad Shankar. All rights reserved.
//

#ifndef __VisionDemo__OpenCVUtils__
#define __VisionDemo__OpenCVUtils__
#include <iostream>
#include "ScanConfigurator.h"
using namespace cv;
class OpenCVutils{
public:
    static void getGray(const cv::Mat& input, cv::Mat& gray);
    static int sum_line_pixels( IplImage*image, CvPoint pt1, CvPoint pt2);
    static void downsampleOriginalMat(Mat& originalMat,Mat& downsampledMat,int scaleFactor);
};
#endif /* defined(__VisionDemo__OpenCVUtils__) */
