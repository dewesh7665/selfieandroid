//
//  QuadrilateralFinder.h
//  LBScanner
//
//  Created by Sharad Shankar on 28/04/15.
//  Copyright (c) 2015 Vineet Jain. All rights reserved.
//

#ifndef LBScanner_QuadrilateralFinder_h
#define LBScanner_QuadrilateralFinder_h

#include "Quadrilateral.h"
#include "Line.h"

#include <vector>
using namespace cv;
using namespace std;

class QuadrilateralFinder{
public:
    QuadrilateralFinder();
    ~QuadrilateralFinder();
    static vector<Quadrilateral> findQuadrialaterals(vector<Line>& baseLines,vector<Line>& comparisonLines,bool isHorizontalMode);
    static Quadrilateral findMergedQuadrilateral(Quadrilateral* horizontalQuad, Quadrilateral* verticalQuad);
private:
    static float findOverlapPercentage(Line horizontalLine,Line verticalLine,cv::Point2f ptIntersection,float horizontalLineLength,float verticalLineLength,bool applyCheck);
};

#endif

