//
//  ImageUtility.m
//  ScannerDemo
//
//  Created by Sharad Shankar on 20/07/13.
//  Copyright (c) 2013 Sharad Shankar. All rights reserved.
//

#include "ImageUtility.h"

ImageUtility::ImageUtility(){
    
}

void ImageUtility::drawLines(vector<Line> lines,cv::Mat& inputFrame,int scaleFactor,cv::Scalar color){
    for(int i=0;i<lines.size();i++){
        Line line = lines[i];
        cv::line(inputFrame,cv::Point2f(line.getStartPoint().x*scaleFactor,line.getStartPoint().y*scaleFactor) ,cv::Point2f(line.getEndPoint().x*scaleFactor,line.getEndPoint().y*scaleFactor),color,4);
    }
}

cv::Point2f ImageUtility::getPointOfIntersection(Line line1, Line line2){
    int x1 = (int)line1.getStartPoint().x,x2 = (int)line1.getEndPoint().x,y1=(int)line1.getStartPoint().y,y2=(int)line1.getEndPoint().y;
    int x3 = (int)line2.getStartPoint().x,x4 = (int)line2.getEndPoint().x,y3=(int)line2.getStartPoint().y,y4=(int)line2.getEndPoint().y;
    int d = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4);
    if (d == 0)
        return cv::Point2f(0,0);
    
    float xi = ((x3-x4)*(x1*y2-y1*x2)-(x1-x2)*(x3*y4-y3*x4))/d;
    float yi = ((y3-y4)*(x1*y2-y1*x2)-(y1-y2)*(x3*y4-y3*x4))/d;
    return cv::Point2f(xi,yi);
}

float ImageUtility::findDistanceBetweenTwoPoints(cv::Point2f pt1,cv::Point2f pt2){
    return sqrtf((pt1.x-pt2.x)*(pt1.x - pt2.x)+(pt1.y-pt2.y)*(pt1.y-pt2.y));
}

float ImageUtility::findAngleBetweenTwoLines(Line line1,Line line2){
    float x1 = line1.getStartPoint().x,x2 = line1.getEndPoint().x,y1=line1.getStartPoint().y,y2=line1.getEndPoint().y;
    float x3 = line2.getStartPoint().x,x4 = line2.getEndPoint().x,y3=line2.getStartPoint().y,y4=line2.getEndPoint().y;
    float a = x1-x2,b= y1-y2,c=x3-x4,d=y3-y4;
    float cos_angle , angle;
    float mag_v1 = sqrtf(a*a + b*b);
    float mag_v2 = sqrtf(c*c + d*d);
    cos_angle = (a*c + b*d) / (mag_v1 * mag_v2);
    angle = acosf(cos_angle);
    return angle;
}

void ImageUtility::drawQuadrilateral(Quadrilateral quad,cv::Mat& inputFrame,int scaleFactor,cv::Scalar color){
    cv::line(inputFrame,cv::Point2f(quad.getPt1().x*scaleFactor,quad.getPt1().y*scaleFactor) ,cv::Point2f(quad.getPt2().x*scaleFactor,quad.getPt2().y*scaleFactor),color,4);
    cv::line(inputFrame,cv::Point2f(quad.getPt2().x*scaleFactor,quad.getPt2().y*scaleFactor) ,cv::Point2f(quad.getPt3().x*scaleFactor,quad.getPt3().y*scaleFactor),color,4);
    cv::line(inputFrame,cv::Point2f(quad.getPt3().x*scaleFactor,quad.getPt3().y*scaleFactor) ,cv::Point2f(quad.getPt4().x*scaleFactor,quad.getPt4().y*scaleFactor),color,4);
    cv::line(inputFrame,cv::Point2f(quad.getPt4().x*scaleFactor,quad.getPt4().y*scaleFactor) ,cv::Point2f(quad.getPt1().x*scaleFactor,quad.getPt1().y*scaleFactor),color,4);
    
}