//
//  ScanFrameDetector.m
//  LBScanner
//
//  Created by Sharad Shankar on 28/04/15.
//  Copyright (c) 2015 Vineet Jain. All rights reserved.
//

#include <iostream>
#include "ScanFrameDetector.h"
#include "OpenCVUtils.h"
#include "QuadrilateralFinder.h"

ScanFrameDetector::ScanFrameDetector(){
    mScaleFactor = 1;
    mHoughScale = 2;
    m_maxFameWidth = MAX_IMAGE_WIDTH;
    m_processedFrameWidth = 0;
    m_processedFrameHeight = 0;
    m_gaussianKernelSize = GAUSSIAN_KERNEL_DIMENSION;
    m_notDetectedFrameCount = 0;

    
    
}

ScanFrameDetector::~ScanFrameDetector(){
    if(!mOriginalMat.empty())
        mOriginalMat.release();
    if(!mGrayMat.empty())
        mGrayMat.release();
    if(!mProcessedGrayMat.empty())
        mProcessedGrayMat.release();
    if(!mHorizontalEdgeMap.empty())
        mHorizontalEdgeMap.release();
    if(!mVerticalEdgeMap.empty())
        mVerticalEdgeMap.release();
    if(!mAbshorizontalMat.empty())
        mAbshorizontalMat.release();
    if(!mAbsVerticalMat.empty())
        mAbsVerticalMat.release();
    if(!mHorizontalLinesMat.empty()){
        mHorizontalLinesMat.release();
    }
    if(!mVerticalLinesMat.empty()){
        mVerticalLinesMat.release();
    }
    if(!m_rgbaOriginal.empty()){
        m_rgbaOriginal.release();
    }
    if(!m_rgbaProcessed.empty()){
        m_rgbaProcessed.release();
    }
    if(!mScaledHorizontalEdgeMap.empty())
        mScaledHorizontalEdgeMap.release();
    if(!mScaledVerticalEdgeMap.empty())
        mScaledVerticalEdgeMap.release();
}

void ScanFrameDetector::resetPreviousState(){
    mHorizontalLines.clear();
    mVerticalLines.clear();
    mLines.clear();
    
}

int ScanFrameDetector::getProcessedFrameWidth(){
    return m_processedFrameWidth;
}

int ScanFrameDetector::getProcessedFrameHeight(){
    return m_processedFrameHeight;
}

/*void ScanFrameDetector::initializeDetector(cv::Mat& inputFrame){
    m_rgbaOriginal.create(inputFrame.rows,inputFrame.cols,inputFrame.type());
    int width = inputFrame.cols;
    int height = inputFrame.rows;
    m_processedFrameWidth = width;
    int factor = 1;
    while(m_processedFrameWidth > m_maxFameWidth)
    {
        m_processedFrameWidth /=2;
        factor *=2;
    }
    m_resizeRatio = factor;
    m_processedFrameHeight = height/factor;
    
    m_processedFrameHeight = m_processedFrameHeight%2 == 0 ? m_processedFrameHeight : m_processedFrameHeight-1;
}*/

Quadrilateral ScanFrameDetector::findFrameQuad(Mat& originalFrame){

    if(mOriginalMat.empty())
        mOriginalMat.create(originalFrame.rows, originalFrame.cols, CV_8UC1);
    //cv::cvtColor(originalFrame, mOriginalMat, COLOR_BGRA2BGR);
    originalFrame.copyTo(mGrayMat);
    
    //Configure scale factor
    configureScaleFactor();
    //Configure all mats based on original mat and scale factor
    configureMats();
    //Now run the algorithm to find quads
    //mHorizontalEdgeMap.setTo(Scalar(0));
    //mAbshorizontalMat.setTo(Scalar(0));
    cv::Sobel(mProcessedGrayMat, mAbshorizontalMat, CV_16S, 0, 1);
    cv::convertScaleAbs(mAbshorizontalMat, mHorizontalEdgeMap);
    
    cv::Sobel(mProcessedGrayMat, mAbsVerticalMat, CV_16S, 1, 0);
    cv::convertScaleAbs(mAbsVerticalMat, mVerticalEdgeMap);
    
    cv::resize(mHorizontalEdgeMap, mScaledHorizontalEdgeMap, mScaledHorizontalEdgeMap.size());
    cv::resize(mVerticalEdgeMap, mScaledVerticalEdgeMap, mScaledVerticalEdgeMap.size());

    cv::threshold(mScaledHorizontalEdgeMap, mScaledHorizontalEdgeMap, MAX_HORIZONTAL_THRESH_VALUE, 255, THRESH_BINARY);
    cv::threshold(mScaledVerticalEdgeMap,mScaledVerticalEdgeMap,MAX_VERTICAL_THRESH_VALUE,255,THRESH_BINARY);

    //First reset previous state
    resetPreviousState();

    /*if(originalFrame.rows > 100){
                       Quadrilateral quad;
                       quad.setPt1(cv::Point2f(50,50));quad.setPt1(cv::Point2f(50,150));
                       quad.setPt1(cv::Point2f(150,150));
                       quad.setPt1(cv::Point2f(150,50));
                       return quad;
                    }*/
    /*Mat resizedMat;resizedMat.create(mOriginalMat.rows, mOriginalMat.cols, CV_8UC1);
     cv::resize(mHorizontalEdgeMap, resizedMat,resizedMat.size());
     int edgeType = resizedMat.type();
     int grayMatType = mGrayMat.type();
     if(CV_8UC1 == grayMatType)
     //cv::resize(mHorizontalEdgeMap, resizedMat, cv::Size(resizedMat.cols,resizedMat.rows));
     cv::cvtColor(resizedMat,outputFrame, CV_GRAY2BGRA);*/
    //std::vector<cv::Vec4i> lines;
    //cv::Mat outputLines;
    std::vector<cv::Vec4i> lines;
    float scaleFactor = mHoughScale > 1 ?  mHoughScale/3.0 : mHoughScale;
    cv::HoughLinesP(mScaledHorizontalEdgeMap, lines, 1, CV_PI/60, mThreshold*scaleFactor,mMinLineSize*scaleFactor,MAXIMUM_LINE_GAP*scaleFactor);
    //outputLines.release();
    findLines(lines, LINE_HORIZONTAL);

    lines.clear();
     //Now find prominent vertical lines
     std::vector<cv::Vec4i> vertlines;
     cv::HoughLinesP(mScaledVerticalEdgeMap, vertlines, 1, CV_PI/60, mThreshold*scaleFactor,mMinLineSize*scaleFactor,MAXIMUM_LINE_GAP*scaleFactor);
     findLines(vertlines, LINE_VERTICAL);
     vertlines.clear();


    //Now find quadrilaterals using these lines
    mPlaneQuads.clear();
    mPlaneQuads = QuadrilateralFinder::findQuadrialaterals(mHorizontalLines, mVerticalLines, true);
    
    //Find vertical quads
    mVerticalQuads.clear();
    mVerticalQuads = QuadrilateralFinder::findQuadrialaterals(mVerticalLines, mHorizontalLines, false);
    
    Quadrilateral *firstQuad = mPlaneQuads.size()>0 ? &mPlaneQuads[0]: NULL;
    Quadrilateral *secondQuad = mVerticalQuads.size()>0 ? &mVerticalQuads[0]: NULL;
    Quadrilateral minOverlapQuad;
    vector<Line> baseLines;
    if(firstQuad != NULL && secondQuad!= NULL){
        if((firstQuad->getLeftFirstVertOverlapPercentage()+ firstQuad->getRightFirstVertOverlapPercentage()) < (secondQuad->getLeftFirstVertOverlapPercentage()+ secondQuad->getRightFirstVertOverlapPercentage()))
        {
            minOverlapQuad = *firstQuad;
            mVerticalQuads.clear();
            baseLines.push_back(minOverlapQuad.getVertLine1());
            baseLines.push_back(minOverlapQuad.getVertLine2());
            mVerticalQuads = QuadrilateralFinder::findQuadrialaterals(baseLines, mHorizontalLines, false);
        }
        else
        {
            minOverlapQuad = *secondQuad;
            mPlaneQuads.clear();
            baseLines.push_back(minOverlapQuad.getVertLine1());
            baseLines.push_back(minOverlapQuad.getVertLine2());
            mPlaneQuads = QuadrilateralFinder::findQuadrialaterals(baseLines, mVerticalLines, true);
        }
    }
    //Now merge two quads to form one quad and return that quad
    firstQuad = mPlaneQuads.size()>0 ? &mPlaneQuads[0]: NULL;
    secondQuad = mVerticalQuads.size()>0 ? &mVerticalQuads[0]: NULL;
    Quadrilateral bestQuad = QuadrilateralFinder::findMergedQuadrilateral(firstQuad, secondQuad);
    //Update quad by scale factor
    cv::Point2f pt1 = bestQuad.getPt1(); pt1.x *= mScaleFactor*mHoughScale;pt1.y *=mScaleFactor*mHoughScale; cv::Point2f pt2= bestQuad.getPt2();pt2.x *=mScaleFactor*mHoughScale;pt2.y *=mScaleFactor*mHoughScale;
    cv::Point2f pt3 = bestQuad.getPt3(); pt3.x *= mScaleFactor*mHoughScale;pt3.y *=mScaleFactor*mHoughScale; cv::Point2f pt4= bestQuad.getPt4();pt4.x *=mScaleFactor*mHoughScale;pt4.y *=mScaleFactor*mHoughScale;
    
    Quadrilateral scaledQuad; scaledQuad.setPt1(pt1); scaledQuad.setPt2(pt2);scaledQuad.setPt3(pt3);scaledQuad.setPt4(pt4);
    return scaledQuad;
    //return Quadrilateral();
    
}

void ScanFrameDetector::configureScaleFactor(){
    int processedframeWidth = mOriginalMat.cols;
    mScaleFactor = 1;
    while(processedframeWidth > MAX_IMAGE_WIDTH)
    {
        processedframeWidth = MAX_IMAGE_WIDTH/2;
        mScaleFactor *=2;
    }
    
}

void ScanFrameDetector::configureMats(){
    //cv::cvtColor(mOriginalMat, mGrayMat, CV_BGR2GRAY);
    //mOriginalMat.copyTo(mGrayMat);
    if(mScaleFactor == 1)
        mGrayMat.copyTo(mProcessedGrayMat);
    else
        OpenCVutils::downsampleOriginalMat(mGrayMat, mProcessedGrayMat, mScaleFactor);
    m_processedFrameWidth = mProcessedGrayMat.cols;
    m_processedFrameHeight = mProcessedGrayMat.rows;
    //Blur the image before processing
    GaussianBlur(mProcessedGrayMat, mProcessedGrayMat, cv::Size(GAUSSIAN_KERNEL_DIMENSION,GAUSSIAN_KERNEL_DIMENSION),0.0);
    int minimumDimension = mProcessedGrayMat.cols < mProcessedGrayMat.rows ? mProcessedGrayMat.cols : mProcessedGrayMat.rows;
    mThreshold = minimumDimension/4;
    mMinLineSize = minimumDimension/4;
    //Set horizontal and vertical mats
    if(mHorizontalEdgeMap.empty())
        mHorizontalEdgeMap.create(mProcessedGrayMat.size(), CV_8UC1);
    if(mVerticalEdgeMap.empty())
        mVerticalEdgeMap.create(mProcessedGrayMat.size(), CV_8UC1);

    if(mScaledHorizontalEdgeMap.empty())
            mScaledHorizontalEdgeMap.create(mProcessedGrayMat.rows/mHoughScale, mProcessedGrayMat.cols/mHoughScale, CV_8UC1);
        if(mScaledVerticalEdgeMap.empty())
            mScaledVerticalEdgeMap.create(mProcessedGrayMat.rows/mHoughScale, mProcessedGrayMat.cols/mHoughScale, CV_8UC1);


    if(mAbshorizontalMat.empty())
        mAbshorizontalMat.create(mProcessedGrayMat.rows, mProcessedGrayMat.cols, CV_16S);
    if(mAbsVerticalMat.empty())
        mAbsVerticalMat.create(mProcessedGrayMat.rows, mProcessedGrayMat.cols, CV_16S);
    
    //mHorizontalEdgeMap.setTo(Scalar(0));
    //mVerticalEdgeMap.setTo(Scalar(0));
    //mAbshorizontalMat.setTo(Scalar(0));
    //mAbsVerticalMat.setTo(Scalar(0));
    
}

void ScanFrameDetector::findLines(vector<cv::Vec4i>& lines,LineDirection direction){
    int countIndex = 0;
    for(int x=0; x<lines.size(); x++){
        if(countIndex > MAX_PROMINENT_LINES_TO_BE_FOUND)
            break;
        cv::Vec4i line = lines[x];
        float x1 = line[0];
        float y1 = line[1];
        float x2 = line[2];
        float y2 = line[3];
        
        if(direction == LINE_HORIZONTAL && (abs(x2-x1) > abs(y2-y1)))
        {
	        	  mHorizontalLines.push_back(Line(cv::Point2f(x1,y1), cv::Point2f(x2,y2)));
	        	  countIndex++;
        }
        else if(direction == LINE_VERTICAL && (abs(y2-y1) > abs(x2-x1))){
	        	  mVerticalLines.push_back(Line(cv::Point2f(x1,y1), cv::Point2f(x2,y2)));
	        	  countIndex++;
        }
    }
}