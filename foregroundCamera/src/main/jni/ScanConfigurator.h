//
//  ScanConfigurator.h
//  ScannerDemo
//
//  Created by Sharad Shankar on 25/01/14.
//  Copyright (c) 2014 Sharad Shankar. All rights reserved.
//

#ifndef ScannerDemo_ScanConfigurator_h
#define ScannerDemo_ScanConfigurator_h
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <string>
#include <vector>

using namespace std;
using namespace cv;
//Keep it at less than 320*240 for speedy
//These variables will vary depending on size of the image
/*Ideal values in case image size is between 300-500 is
 
 */
#define MAX_IMAGE_WIDTH 400
#define CAPTURE_MODE_WIDTH 500
#define GAUSSIAN_KERNEL_DIMENSION 3
#define MAXIMUM_LINE_GAP 6
#define CANNY_MIN_THRESH_VALUE 25
#define CANNY_MAX_THRESH_VALUE 50

#define MAX_VERTICAL_THRESH_VALUE 40
#define MAX_HORIZONTAL_THRESH_VALUE 40
#define MAX_PROMINENT_LINES_TO_BE_FOUND 20

#define MIN_HORIZONTAL_THRESH_VALUE  20
#define MIN_VERTICAL_THRESH_VALUE  20
#endif
