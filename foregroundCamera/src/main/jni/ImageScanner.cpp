#include "ImageScanner.h"

#include <opencv2/core/core.hpp>
#include <opencv2/objdetect.hpp>
#include "ScanFrameDetector.h"

#include <jni.h>

#define LOG_TAG "Scanner"
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))

using namespace std;
using namespace cv;

void vector_Point2f_to_Mat(vector < Point2f > &v_point, Mat & mat) {
    mat = Mat(v_point, true);
}

JNIEXPORT jlong JNICALL Java_com_logic_scanner_Utils_ImageScanner_nativeCreateObject
        (JNIEnv *, jclass) {
                jlong result = 0;
                result = (jlong) new ScanFrameDetector();
                return result;
}

JNIEXPORT void JNICALL Java_com_logic_scanner_Utils_ImageScanner_nativeDestroyObject
(JNIEnv *, jclass, jlong thiz){
        if(thiz != 0)
            delete (ScanFrameDetector *) thiz;
}

JNIEXPORT void JNICALL Java_com_logic_scanner_Utils_ImageScanner_nativeDetect
(JNIEnv*, jclass, jlong thiz, jlong inputImage, jlong quadMat){
        Quadrilateral quad = ((ScanFrameDetector *) thiz)->findFrameQuad(*((Mat *) inputImage));
        vector <cv::Point2f> quadPts;
        quadPts.push_back(quad.getPt1());
        quadPts.push_back(quad.getPt2());
        quadPts.push_back(quad.getPt3());
        quadPts.push_back(quad.getPt4());

        *((Mat*)quadMat) = Mat(quadPts,true);
}
