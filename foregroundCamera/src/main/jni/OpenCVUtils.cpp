//
//  OpenCVUtils.cpp
//  VisionDemo
//
//  Created by Sharad Shankar on 24/05/13.
//  Copyright (c) 2013 Sharad Shankar. All rights reserved.
//

#include "OpenCVUtils.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

void OpenCVutils::getGray(const cv::Mat& input, cv::Mat& gray)
{
   /* const int numChannes = input.channels();
    
    if (numChannes == 4)
    {
//#if TARGET_IPHONE_SIMULATOR
        cv::cvtColor(input, gray, CV_BGRA2GRAY);
//#else
  //      cv::neon_cvtColorBGRA2GRAY(input, gray);
//#endif
        
    }
    else if (numChannes == 3)
    {
        cv::cvtColor(input, gray, CV_BGR2GRAY);
    }
    else if (numChannes == 1)
    {
        gray = input;
    }*/
}

void OpenCVutils::downsampleOriginalMat(Mat& originalMat,Mat& downsampledMat,int scaleFactor){
    if(scaleFactor == 1)
        originalMat.copyTo(downsampledMat);
    else{
        Mat tempResizedMat;
        originalMat.copyTo(tempResizedMat);
        while(scaleFactor > 1){
            Mat tempDownMat;
            cv::pyrDown(tempResizedMat,tempDownMat);
            tempResizedMat.release();
            tempDownMat.copyTo(tempResizedMat);
            scaleFactor = scaleFactor/2;
            tempDownMat.release();
        }
        //Finally copy tempDownMat to downsampledMat
        tempResizedMat.copyTo(downsampledMat);
        tempResizedMat.release();
    }
}

int OpenCVutils::sum_line_pixels(IplImage *image, CvPoint pt1, CvPoint pt2 )
{
    CvLineIterator iterator;
    int gray_sum =0;
    int count = cvInitLineIterator( image, pt1, pt2, &iterator, 8, 0 );
    for(int i=0;i< count;i++){
        gray_sum += iterator.ptr[0];
        CV_NEXT_LINE_POINT(iterator);
    }
    if(count == 0)
        return 0;
    return gray_sum/count;
    /*for
     (
     int
     i = 0; i < count; i++ ){blue_sum += iterator.ptr[0];green_sum += iterator.ptr[1];red_sum += iterator.ptr[2];CV_NEXT_LINE_POINT(iterator);
     
     {
     int
     offset, x, y;
     
     offset = iterator.ptr - (uchar*)(image->imageData);y = offset/image->widthStep;x = (offset - y*image->widthStep)/(3*
     sizeof
     (uchar)
     
     
     );printf("(%d,%d)\n", x, y );}}
     return
     cvScalar( blue_sum, green_sum, red_sum );*/
    
    
}
