package com.controls.library.util;

import android.graphics.Rect;
import android.view.TouchDelegate;
import android.view.View;

public class TouchUtil {

	public static void increaseTouchArea(final View delegate,final int extraPadding){
		View parent = (View) delegate.getParent();
        parent.post(new Runnable() {
            public void run() {
                // Post in the parent's message queue to make sure the parent
                // lays out its children before we call getHitRect()
                Rect delegateArea = new Rect();

                delegate.getHitRect(delegateArea);
                delegateArea.top -= extraPadding;
                delegateArea.bottom += extraPadding;
                delegateArea.left -= extraPadding;
                delegateArea.right += extraPadding;
                TouchDelegate expandedArea = new TouchDelegate(delegateArea,
                        delegate);
                // give the delegate to an ancestor of the view we're
                // delegating the
                // area to
                if (View.class.isInstance(delegate.getParent())) {
                    ((View) delegate.getParent())
                            .setTouchDelegate(expandedArea);
                }
            };
        });
	}	
}
