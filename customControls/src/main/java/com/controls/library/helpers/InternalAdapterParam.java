package com.controls.library.helpers;

public class InternalAdapterParam {
	private Object singelObject;
	public Object getSingelObject() {
		return singelObject;
	}
	public Object getListObject() {
		return listObject;
	}
	public String getViewClassName() {
		return viewClassName;
	}
	public void setSingelObject(Object singelObject) {
		this.singelObject = singelObject;
	}
	public void setListObject(Object listObject) {
		this.listObject = listObject;
	}
	public void setViewClassName(String viewClassName) {
		this.viewClassName = viewClassName;
	}
	private Object listObject;
	private String viewClassName;
}
