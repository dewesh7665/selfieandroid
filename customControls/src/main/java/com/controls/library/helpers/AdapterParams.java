package com.controls.library.helpers;

import com.controls.library.interfaces.MultiListInterfaces;
import com.controls.library.interfaces.MultiListInterfaces.OnMultiListScrollPositionListener;


public class AdapterParams {
	private Object dataObject;
	private Object collectionObject;
	private MultiListInterfaces.OnMultiListGetViewCalledListener onGetViewCalledListner;
	private Boolean isFirstLast = false;
	//private HeaderParam headerParam;

	/**
	 * If view is Horizontal Scroll View, Pager or any collection view
	 */
	private Boolean isCompositeView = false;//Default value
	private int numOfColumn = 1;//Default value
	private OnMultiListScrollPositionListener onMultiListItemScrollPositionListner;
	/**
	 * TO be used for Sticky header. For deciding if null to be send for Non header objects.
	 */
	private Object stickyHeaderObject = null;

	public AdapterParams(Object dataObject,MultiListInterfaces.OnMultiListGetViewCalledListener onGetViewCalledListner) {
		// TODO Auto-generated constructor stub
		this.dataObject = dataObject;
		this.onGetViewCalledListner = onGetViewCalledListner;
	}

	public Object getDataObject() {
		return dataObject;
	}
	public int getNumOfColumn() {
		return numOfColumn;
	}
	public MultiListInterfaces.OnMultiListGetViewCalledListener getViewClassName() {
		return this.onGetViewCalledListner;
	}
	public void setNumOfColumn(int numOfColumn) {
		this.numOfColumn = numOfColumn;
	}
	public Boolean getCompositeViewStatus() {
		return isCompositeView;
	}
	public void isCompositeView(Boolean isCompositeView) {
		this.isCompositeView = isCompositeView;
	}
	/*public Object getCollectionObject() {
		return collectionObject;
	}

	public void setCollectionObject(Object collectionObject) {
		this.collectionObject = collectionObject;
	}*/

	/*************************************************Below Methods to be used for sticky header implementation***********************************/

	public OnMultiListScrollPositionListener getOnMultiListItemScrollPositionListner() {
		return onMultiListItemScrollPositionListner;
	}
	
	/**
	 * Will be called with data object. For providing data object use {@link #setOnMultiListItemAtFirstPositionListner(com.controls.library.interfaces.MultiListInterfaces.OnMultiListScrollPositionListener, Object)}
	 * @param onMultiListItemScrollPositionListner
	 */
	public void setOnMultiListItemAtFirstPositionListner(
			OnMultiListScrollPositionListener onMultiListItemScrollPositionListner) {
		this.onMultiListItemScrollPositionListner = onMultiListItemScrollPositionListner;
	}
	
	/**
	 * Will be called with dataobject provided
	 * @param onMultiListItemScrollPositionListner
	 * @param stickyHeaderObject
	 */
	public void setOnMultiListItemAtFirstPositionListner(
			OnMultiListScrollPositionListener onMultiListItemScrollPositionListner,Object stickyHeaderObject) {
		this.onMultiListItemScrollPositionListner = onMultiListItemScrollPositionListner;
		this.stickyHeaderObject = stickyHeaderObject;
	}
	/**
	 * To be used for identify when to start/stop scroll callbacks
	 * @param isFirstItem
	 */
	public void setItemAsFirstVisible(Boolean isFirstVisibleItem){
		this.isFirstLast = isFirstVisibleItem;
	}

	public Boolean isFirstLast(){
		return this.isFirstLast;
	}

	public Object getStickyHeaderObject() {
		return stickyHeaderObject;
	}

	public void setStickyHeaderObject(Object stickyHeaderObject) {
		this.stickyHeaderObject = stickyHeaderObject;
	}
}
