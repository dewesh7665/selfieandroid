package com.controls.library.helpers;

import java.util.ArrayList;
import java.util.List;

import com.controls.library.interfaces.MultiListInterfaces.OnMultiListScrollPositionListener;

public class AdapterParamInfo {
	private ArrayList<AdapterParams> mArrListAdpaterParams;
	public AdapterParamInfo(ArrayList<AdapterParams> arrListAdpaterParams) {
		// TODO Auto-generated constructor stub
		this.mArrListAdpaterParams = arrListAdpaterParams;
	}

	public int getSingleItemRowViewTypeCount(){
		return 0;
	}

	public int getMultipleItemRowViewTypeCount(){
		return 0;
	}

	public List<AdapterParams> getListForSingleItemAdapter(){
		List<AdapterParams> listSingleItemParam = new ArrayList<AdapterParams>();
		//Next variables to be used for sticky header view
		OnMultiListScrollPositionListener onMultiListScrollPositionListener = null;
		Object stickyHeaderObject = null;
		for(int i = 0;i < mArrListAdpaterParams.size();i++){
			AdapterParams adapterParam = mArrListAdpaterParams.get(i);
			if(adapterParam.getDataObject() instanceof List<?>){ //Arraylist or any list
				List<?> listDataObject = (List<?>) adapterParam.getDataObject();
				if(!adapterParam.getCompositeViewStatus()){
					for(int j = 0;j < listDataObject.size();j++){
						AdapterParams newAdapterParam = new AdapterParams(listDataObject.get(j), adapterParam.getViewClassName());
						if(adapterParam.getOnMultiListItemScrollPositionListner() != null){
							onMultiListScrollPositionListener = adapterParam.getOnMultiListItemScrollPositionListner();
							//newAdapterParam.setFakeObjectForStickyHeader(false);
							if(adapterParam.getStickyHeaderObject() != null)
								stickyHeaderObject = adapterParam.getStickyHeaderObject();
							else
								stickyHeaderObject = adapterParam.getDataObject();
						}
						newAdapterParam.setOnMultiListItemAtFirstPositionListner(onMultiListScrollPositionListener);
						newAdapterParam.setStickyHeaderObject(stickyHeaderObject);
						newAdapterParam.setItemAsFirstVisible(adapterParam.isFirstLast());
						listSingleItemParam.add(newAdapterParam);
					}
				}
				else{
					//Its composite view HScrollView/Pager etc
					AdapterParams newAdapterParam = new AdapterParams(listDataObject, adapterParam.getViewClassName());
					if(adapterParam.getOnMultiListItemScrollPositionListner() != null){
						onMultiListScrollPositionListener = adapterParam.getOnMultiListItemScrollPositionListner();
						//newAdapterParam.setFakeObjectForStickyHeader(false);
						if(adapterParam.getStickyHeaderObject() != null)
							stickyHeaderObject = adapterParam.getStickyHeaderObject();
						else
							stickyHeaderObject = adapterParam.getDataObject();
					}
					newAdapterParam.setOnMultiListItemAtFirstPositionListner(onMultiListScrollPositionListener);
					newAdapterParam.setStickyHeaderObject(stickyHeaderObject);
					newAdapterParam.setItemAsFirstVisible(adapterParam.isFirstLast());
					listSingleItemParam.add(newAdapterParam);
				}
			}
			else{// Single object for example string
				AdapterParams newAdapterParam = new AdapterParams(adapterParam.getDataObject(), adapterParam.getViewClassName());
				if(adapterParam.getOnMultiListItemScrollPositionListner() != null){
					onMultiListScrollPositionListener = adapterParam.getOnMultiListItemScrollPositionListner();
					//newAdapterParam.setFakeObjectForStickyHeader(false);
					if(adapterParam.getStickyHeaderObject() != null)
						stickyHeaderObject = adapterParam.getStickyHeaderObject();
					else
						stickyHeaderObject = adapterParam.getDataObject();
				}
				newAdapterParam.setOnMultiListItemAtFirstPositionListner(onMultiListScrollPositionListener);
				newAdapterParam.setStickyHeaderObject(stickyHeaderObject);
				newAdapterParam.setItemAsFirstVisible(adapterParam.isFirstLast());
				listSingleItemParam.add(newAdapterParam);
			}
		}
		return listSingleItemParam;
	}

	public List<AdapterParams> getListForMultiItemAdapter(){
		List<AdapterParams> listMultiItemParam = new ArrayList<AdapterParams>();
		//Next variables to be used for sticky header view
		OnMultiListScrollPositionListener onMultiListScrollPositionListener = null;
		Object stickyHeaderObject = null;
		for(int i = 0;i < mArrListAdpaterParams.size();i++){
			AdapterParams adapterParam = mArrListAdpaterParams.get(i);
			if(adapterParam.getDataObject() instanceof List<?>){ //Arraylist or any list

				/**
				 * Iterate through listDataObject,prepare row with multiple item info (With list of object that count will 
				 * be num of column, and view class name) and add prepared row into list i.e listMultiItemParam
				 */

				List<?> listDataObject = (List<?>) adapterParam.getDataObject();
				if(!adapterParam.getCompositeViewStatus()){//It has multiple columns per row
					int numOfColumn = adapterParam.getNumOfColumn();
					int numOfRows = (int)Math.ceil(1.0f * listDataObject.size() / numOfColumn);
					for(int j = 0;j < numOfRows; j++){
						List<Object> rowListItems = new ArrayList<Object>();//Will contains numOfColoumn items
						for(int k = 0;k < numOfColumn;k++){
							int position = j * numOfColumn + k;
							if(position < listDataObject.size())//Check for nth row call but odd number of total item
								rowListItems.add(listDataObject.get(position));
						}

						AdapterParams newAdapterParam = new AdapterParams(rowListItems, adapterParam.getViewClassName());
						newAdapterParam.setNumOfColumn(numOfColumn);
						if(adapterParam.getOnMultiListItemScrollPositionListner() != null){
							onMultiListScrollPositionListener = adapterParam.getOnMultiListItemScrollPositionListner();
							if(adapterParam.getStickyHeaderObject() != null)
								stickyHeaderObject = adapterParam.getStickyHeaderObject();
							else
								stickyHeaderObject = adapterParam.getDataObject();
							//newAdapterParam.setFakeObjectForStickyHeader(false);
						}
						newAdapterParam.setOnMultiListItemAtFirstPositionListner(onMultiListScrollPositionListener);
						newAdapterParam.setStickyHeaderObject(stickyHeaderObject);
						newAdapterParam.setItemAsFirstVisible(adapterParam.isFirstLast());
						listMultiItemParam.add(newAdapterParam);
					}
				}
				else{
					//Its composite view HScrollView/Pager etc. Object count will be 1
					AdapterParams newAdapterParam = new AdapterParams(listDataObject, adapterParam.getViewClassName());
					if(adapterParam.getOnMultiListItemScrollPositionListner() != null){
						onMultiListScrollPositionListener = adapterParam.getOnMultiListItemScrollPositionListner();
						//newAdapterParam.setFakeObjectForStickyHeader(false);
						if(adapterParam.getStickyHeaderObject() != null)
							stickyHeaderObject = adapterParam.getStickyHeaderObject();
						else
							stickyHeaderObject = adapterParam.getDataObject();
					}
					newAdapterParam.setOnMultiListItemAtFirstPositionListner(onMultiListScrollPositionListener);
					newAdapterParam.setStickyHeaderObject(stickyHeaderObject);
					newAdapterParam.setItemAsFirstVisible(adapterParam.isFirstLast());
					listMultiItemParam.add(newAdapterParam);
				}
			}
			else{// Single object for example string, header,  Object count will be 1
				AdapterParams newAdapterParam = new AdapterParams(adapterParam.getDataObject(), adapterParam.getViewClassName());
				if(adapterParam.getOnMultiListItemScrollPositionListner() != null){
					onMultiListScrollPositionListener = adapterParam.getOnMultiListItemScrollPositionListner();
					//newAdapterParam.setFakeObjectForStickyHeader(false);
					if(adapterParam.getStickyHeaderObject() != null)
						stickyHeaderObject = adapterParam.getStickyHeaderObject();
					else
						stickyHeaderObject = adapterParam.getDataObject();
				}
				newAdapterParam.setOnMultiListItemAtFirstPositionListner(onMultiListScrollPositionListener);
				newAdapterParam.setStickyHeaderObject(stickyHeaderObject);
				newAdapterParam.setItemAsFirstVisible(adapterParam.isFirstLast());
				listMultiItemParam.add(newAdapterParam);
			}
		}
		return listMultiItemParam;
	}

	public Boolean isAllRowSingleItem(){
		Boolean isAllRowSingleItem = true;
		for(AdapterParams adapterParam : mArrListAdpaterParams){
			if(adapterParam.getNumOfColumn() > 1){
				isAllRowSingleItem = false;
				break;
			}
		}
		return isAllRowSingleItem;
	}
}
