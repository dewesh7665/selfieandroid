package com.controls.library;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import com.controls.library.adapters.CustomGridViewAdapter;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;

public class CustomGridView extends PullToRefreshGridView {

	protected int mItemCount;

	private CustomGridViewAdapter mAdapter;
	protected OnGetViewCalledListner onGetViewCalledListner;
	private int mCountPerPage = 10;
	protected OnLoadMoreCalledListner onLoadMoreListner;
	public static interface OnGetViewCalledListner{
		/**
		 * View newView = mInflater.inflate(R.layout.test_view,container,false);
		 * or
		 * if extending ViewGroup(LinearLayout or RelativeLayout),then this indicating that view
		 * View newView = mInflater.inflate(R.layout.test_view, this,false);
		 * @param position
		 * @param container
		 * @return
		 */
		public View onGetViewCalled(View convertView, int position,ViewGroup container);
	}
	
	public static interface OnLoadMoreCalledListner{
		public void loadMoreData(int currentMaxCount);
	}
	
	public CustomGridView(Context context) {
		super(context);
	}

	public CustomGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}


	public void setNumColumns(int numColumns) {
		this.getRefreshableView().setNumColumns(numColumns);
	}
	
	public void setOnLoadMoreListner(OnLoadMoreCalledListner onLoadMoreListner,int countPerPage){
		this.onLoadMoreListner = onLoadMoreListner;
		mCountPerPage = countPerPage;
	}
	
	public Boolean isLoadMoreImplemented(){
		if(this.onLoadMoreListner == null)
			return false;
		return true;
	}
	
	public void removeOnLoadMoreListner(){
		if(mAdapter != null) mAdapter.removeOnLoadMoreListner();
	}

	public void setAdapterParams(int itemCount,OnGetViewCalledListner onGetViewCalledListner){
		this.mItemCount = itemCount;
		this.onGetViewCalledListner = onGetViewCalledListner;
		mAdapter = new CustomGridViewAdapter(mItemCount, this.onGetViewCalledListner);
		mAdapter.setGridView(this);
		this.setAdapter(mAdapter);
	}

	//Loading layout to be handled in the activity implementing onLoadMoreListner
	public void updateGridContent(int itemCount) {
		if(null != mAdapter) {
			mAdapter.setCount(itemCount);
			if(this.onLoadMoreListner != null){
				mAdapter.setOnLoadMoreListner(onLoadMoreListner);
			}
			mAdapter.notifyDataSetChanged();
		}
	}

	/**
	 * In gen To be used internally.
	 */
	public CustomGridViewAdapter getPagerAdapter(){
		return this.mAdapter;
	}

}
