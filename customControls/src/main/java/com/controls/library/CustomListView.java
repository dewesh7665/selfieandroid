
package com.controls.library;


import java.util.ArrayList;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.controls.library.adapters.CustomListAdapter;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class CustomListView{
	public static interface OnGetViewCalledListner{
		public View onGetViewCalled(int position,Object object,View convertView,ViewGroup viewGroup);
	}
	public static interface OnLoadMoreCalledListner{
		public void loadMoreData(int currentMaxCount);
	}
	public static interface OnGetStickyHeaderViewCalledListner{
		public String getHeader(int firstVisibleItem);
		//TODO:To be implemented
		public void onStickyHeaderListner(TextView headerView);
		//public View getHeaderView(int position);
	}
	private Context mContext;
	private LayoutInflater mInflater = null;
	private LinearLayout mEmptyView = null;
	private LayoutParams mLayoutParams = null;
	private View mView = null;

	protected ListView mListViewHome;
	protected CustomListAdapter mAdapter = null;
	private LinearLayout mParentStickyHeaedr;
	private TextView mHeaderText;
	private PullToRefreshListView pullToRefreshlistView = null;

	protected ArrayList<?> mListItems;
	protected OnGetViewCalledListner onGetViewCalledListner;
	protected OnLoadMoreCalledListner onLoadMoreListner;
	private OnGetStickyHeaderViewCalledListner onGetStickyHeaderViewCalledListner;
	private String mStrHeaderText = "";
	protected LinearLayout mParentLoadingRow;
	protected View mListFooterView = null;
	private int mCountPerPage = 10;
	public CustomListView(Context context)
	{
		mContext = context;
		mEmptyView = new LinearLayout(mContext);
		mLayoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		mEmptyView.setLayoutParams(mLayoutParams);
		mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mView = mInflater.inflate(R.layout.view_custom_listview,null);
		pullToRefreshlistView = (PullToRefreshListView)mView.findViewById(R.id.listViewHome);
		mParentLoadingRow = (LinearLayout)mView.findViewById(R.id.llParentLoading);
		mParentLoadingRow.setVisibility(View.VISIBLE);
		mListViewHome = pullToRefreshlistView.getRefreshableView();
		setPullRefreshListner();
		//mListViewHome = (ListView)mView.findViewById(R.id.listViewHome);
		mParentStickyHeaedr = (LinearLayout)mView.findViewById(R.id.llParentStickyHeader);
	}

	public void isPullRefrshEnabled(Boolean isEnabled){
		if(isEnabled) pullToRefreshlistView.setMode(Mode.PULL_FROM_START);
		else pullToRefreshlistView.setMode(Mode.DISABLED);
	}

	public void setOnGetViewCalledListner(OnGetViewCalledListner onGetViewCalledListner){
		this.onGetViewCalledListner = onGetViewCalledListner;
	}

	public Boolean isLoadMoreImplemented(){
		if(this.onLoadMoreListner == null)
			return false;
		return true;
	}

	public void hideProgressBar(){
		mParentLoadingRow.setVisibility(View.GONE);
	}
	public void setOnLoadMoreListner(OnLoadMoreCalledListner onLoadMoreListner,int countPerPage){
		this.onLoadMoreListner = onLoadMoreListner;
		mCountPerPage = countPerPage;
	}

	public void removeOnLoadMoreListner(){
		if(mAdapter != null) mAdapter.removeOnLoadMoreListner();
		if(mListViewHome != null) mListViewHome.removeFooterView(mListFooterView);
	}

	public void setOnGetStickyHeaderCalledListner(OnGetStickyHeaderViewCalledListner listner){
		if(listner != null){
			mHeaderText = (TextView)mView.findViewById(R.id.tvHeaderText);
			this.mListViewHome.setOnScrollListener(onScrollListner);
			this.onGetStickyHeaderViewCalledListner = listner;
		}
	}
	/**
	 * Will initialize adapter if already not initialized else will update currentarraylist.
	 * If calling it for first time,make sure to call 
	 * {@link #setOnGetViewCalledListner(OnGetViewCalledListner)}
	 * before calling this.
	 * If calling for updating listview only,then its fine
	 * @param listItems
	 * @param isFreshListing : true : will create a new adapter with listItems provided
	 * 		False : Will update the existing data structure.Same as NotifyDataSetChanged
	 */
	public void setUpdateListItems(ArrayList<?> listItems,Boolean isFreshListing){
		mParentLoadingRow.setVisibility(View.GONE);
		if(listItems == null || listItems.size() == 0)
			throw new NullPointerException("Please make sure your arraylist is not null and empty.");
		this.mListItems = listItems;
		if(mAdapter == null || isFreshListing){
			if(this.onGetViewCalledListner == null)
				throw new NullPointerException("Please make sure to call setOnGetViewCalledListner before calling setUpdateListItems.");
			mAdapter = new CustomListAdapter(mContext);
			mAdapter.setParamaters(mListItems, this.onGetViewCalledListner);
			if(this.onLoadMoreListner != null){
				mAdapter.setOnLoadMoreListner(onLoadMoreListner);
				if(mListViewHome.getFooterViewsCount() == 0){
					mListFooterView = ((LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.list_loading_row, null);
					mListViewHome.addFooterView(mListFooterView);
				}
			}
			mListViewHome.setAdapter(mAdapter);
		}
		else
			mAdapter.updateAdapterArrayList(listItems);
		
		//Remove footer and load more listner based on array size
		if(this.mListItems.size() < mCountPerPage){
			removeOnLoadMoreListner();
		}
	}

	public ListView getListView(){
		return mListViewHome;
	}

	public View getPopulatedView(){
		return mView;
	}

	public CustomListAdapter getListAdapter()
	{
		return mAdapter;
	}




	OnScrollListener onScrollListner = new OnScrollListener() 
	{

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) 
		{
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) 
		{

			if(firstVisibleItem >0){
				String headerText = onGetStickyHeaderViewCalledListner.getHeader(firstVisibleItem);
				if(headerText == null)
					throw new IllegalStateException("You can't return null from onGetStickyHeaderViewCalledListner.getHeader.Please init with empty string.");

				if(hasHeaderTextChanged(headerText)){
					mParentStickyHeaedr.setVisibility(View.VISIBLE);
					mHeaderText.setText(mStrHeaderText);
					onGetStickyHeaderViewCalledListner.onStickyHeaderListner(mHeaderText);
				}
				else if(mStrHeaderText.length() == 0){
					mParentStickyHeaedr.setVisibility(View.GONE);
				}
				
			}
			/*if(firstVisibleItem >= 1)
			{
				if(mParentListViewTabs.getVisibility() != View.VISIBLE)
					mParentListViewTabs.setVisibility(View.VISIBLE);
			}
			else
			{
				if(mParentListViewTabs.getVisibility() != View.GONE)
					mParentListViewTabs.setVisibility(View.GONE);
			}*/
		}
	};

	private Boolean hasHeaderTextChanged(String newString){
		if(mStrHeaderText.compareTo(newString) == 0)
			return false;
		else{
			mStrHeaderText = newString;
			return true;
		}
	}

	//	@Override
	//	public void loadMoreData(int currentCount) 
	//	{
	//		if (!hasDataEnded)
	//		{
	//			//mLisitngButton.getUrlManager().setFinalUrl(getNewUrl(mLisitngButton.getUrlManager().getFinalUrl(),currentCount+1,Constants.LIST_ITEM_PER_PAGE));		// Commented in Rev. 278 (was skipping one record)
	//			//mLisitngButton.getUrlManager().setFinalUrl(getNewUrl(mLisitngButton.getUrlManager().getFinalUrl(),currentCount,Constants.LIST_ITEM_PER_PAGE));			// Added in Rev. 278 (older code was skipping one record)
	//			//((BaseActivity)mContext).startFeedRetreival(onLoadMoreDataFinished,mLisitngButton.getUrlManager(),false);
	//		}
	//	}

	public PullToRefreshListView getPullToRefreshListView() {
		return this.pullToRefreshlistView;
	}
	
	Handler handler;//Temp : To be implemented
	private void setPullRefreshListner()
	{
		pullToRefreshlistView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			public void onRefresh(PullToRefreshBase<ListView> refreshView){
				handler = new Handler();
				handler.postDelayed(new Runnable() {

					@Override
					public void run() {
						pullToRefreshlistView.onRefreshComplete();

					}
				}, 1000);
			}
		});
	}


}
