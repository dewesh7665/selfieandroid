package com.controls.library.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class BaseView1 extends LinearLayout implements OnClickListener{

	public BaseView1(Context context) {
		this(context,null);
	}

	public BaseView1(Context context,AttributeSet attrs) {
		super(context,attrs);
	}


	public View getPopulatedView(View view,ViewGroup parent,Object businessObject){
		view.setOnClickListener(this);
		return null;
	}


	@Override
	public void onClick(View v) {

	}
}


