
package com.controls.library;


import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.controls.library.adapters.MultiItemRowAdapter;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

public class MultiItemListView{
	/*public static interface OnLoadMoreCalledListner{
		public void loadMoreData(int currentMaxCount);
	}
	public static interface OnGetStickyHeaderViewCalledListner{
		public String getHeader(int firstVisibleItem);
		//TODO:To be implemented
		public void onStickyHeaderListner(TextView headerView);
		//public View getHeaderView(int position);
	}*/

	private Boolean isPullToRefreshCalled = false;
	public static interface OnPullToRefreshListener {
		public void onPulltoRefreshCalled();
	}
	private Context mContext;
	private OnPullToRefreshListener mPullToRefreshListener = null;
	private LayoutInflater mInflater = null;
	private View mView = null;
	private ListAdapter mAdapter;

	protected ListView mListView;
	private PullToRefreshListView pullToRefreshlistView = null;

	protected ProgressBar mParentLoadingRow;
	/**
	 * Will decide if we need to set scoll listner only below ICS
	 */
	private Boolean isScrollListnerOnlyBelowICS = false;

	protected View mListFooterView = null;
	public MultiItemListView(Context context)
	{
		mContext = context;
		mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mView = mInflater.inflate(R.layout.view_multiitem_listview,null);
		pullToRefreshlistView = (PullToRefreshListView)mView.findViewById(R.id.listViewHome);
		mParentLoadingRow = (ProgressBar)mView.findViewById(R.id.progress_image_topbar);
		mParentLoadingRow.setVisibility(View.VISIBLE);
		mListView = pullToRefreshlistView.getRefreshableView();
		//TODO:@Dewesh : Check is specially for TOI where business want images to be bind instantly if already downloaded.
		//if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
		//mListView.setOnScrollListener(onScrollListner);
		setPullRefreshListner();
		//mListViewHome = (ListView)mView.findViewById(R.id.listViewHome);
	}

	/**
	 * Some business required not to wait image binding if image already downloaded. Cant be done due to performence issue below ICS
	 */
	public void setScrollListnerBelowICS(Boolean isScrollListnerOnlyBelowICS){
		this.isScrollListnerOnlyBelowICS = isScrollListnerOnlyBelowICS;
	}

	public void setAdapter(ListAdapter adapter){
		hideProgressBar();
		mAdapter = adapter;
		mListView.setAdapter(adapter);
		pullToRefreshComplete();
		if(adapter instanceof MultiItemRowAdapter)
			setMultiItemRowAdapterSpecificParams();
	}

	public void pullToRefreshComplete(){
		if(isPullToRefreshCalled){
			isPullToRefreshCalled = false;
			pullToRefreshlistView.onRefreshComplete();
		}
	}

	public void isPullRefrshEnabled(Boolean isEnabled){
		if(isEnabled) pullToRefreshlistView.setMode(Mode.PULL_FROM_START);
		else pullToRefreshlistView.setMode(Mode.DISABLED);
	}

	public void hideProgressBar(){
		mParentLoadingRow.setVisibility(View.GONE);
	}

	public ListView getListView(){
		return mListView;
	}

	public View getPopulatedView(){
		return mView;
	}

	public PullToRefreshListView getPullToRefreshListView() {
		return this.pullToRefreshlistView;
	}

	private void addFooterLoader(){
		if(mListFooterView == null)
			mListFooterView = ((LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.list_loading_row, null);
		mListView.removeFooterView(mListFooterView);
		mListView.addFooterView(mListFooterView);
	}

	public void removeFooterLoader(){
		if(mListFooterView != null) mListView.removeFooterView(mListFooterView);
	}

	/**
	 * Will set only MultiRowAdapter specific params here so that it can be used with any adapter
	 */
	private void setMultiItemRowAdapterSpecificParams(){
		MultiItemRowAdapter multiItemAdapter = (MultiItemRowAdapter)mAdapter;
		multiItemAdapter.setMultiItemListView(this);
		if(multiItemAdapter.getOnLoadMoreListner() != null)
			addFooterLoader();
		if(isScrollListnerOnlyBelowICS){//
			if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH)
				//mListView.setOnScrollListener(onScrollListner);
				mListView.setOnScrollListener(((MultiItemRowAdapter)mAdapter).getListOnScrollListner());
		}
		else{
			mListView.setOnScrollListener(((MultiItemRowAdapter)mAdapter).getListOnScrollListner());
		}
	}

	/*private OnScrollListener onScrollListner = new OnScrollListener() {

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			// TODO Auto-generated method stub
			//if(view instanceof ListAdapter){
			if(scrollState != 0)
				((MultiItemRowAdapter)mAdapter).setScrollStatus(true);
			else{
				((MultiItemRowAdapter)mAdapter).setScrollStatus(false);
				((MultiItemRowAdapter)mAdapter).notifyDataSetChanged();
			}
			//}
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
			// TODO Auto-generated method stub

		}
	};*/

	/**
	 * 50 Milisecond delay if PullToRefresh return quick
	 */
	Handler handler;//Temp : To be implemented
	private void setPullRefreshListner()
	{
		pullToRefreshlistView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			public void onRefresh(PullToRefreshBase<ListView> refreshView){
				handler = new Handler();
				handler.postDelayed(new Runnable() {

					@Override
					public void run() {
						isPullToRefreshCalled = true;
						if(mPullToRefreshListener != null)
							mPullToRefreshListener.onPulltoRefreshCalled();
					}
				}, 50);
			}
		});
	}

	public void setPullToRefreshListener(OnPullToRefreshListener pullToRefreshListener) {
		this.mPullToRefreshListener= pullToRefreshListener; 
	}
}

