package com.controls.library.adapters;

import java.util.ArrayList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.controls.library.CustomListView;
import com.controls.library.CustomListView.OnGetViewCalledListner;
import com.controls.library.CustomListView.OnLoadMoreCalledListner;

public class CustomListAdapter extends BaseAdapter{
	protected ArrayList<Object> mArrrListItems;
	protected Boolean hasLoadMoreListnerRemoved = false;
	protected CustomListView.OnGetViewCalledListner onGetViewCalledListner;
	protected CustomListView.OnLoadMoreCalledListner onLoadMoreCalledListner;

	public CustomListAdapter(Context pContext){

	}
	public void setOnGetViewCalledListner(OnGetViewCalledListner onGetViewCalledListner){
		this.onGetViewCalledListner = onGetViewCalledListner;
	}
	public void setOnLoadMoreListner(OnLoadMoreCalledListner onLoadMoreCalledListner){
		this.onLoadMoreCalledListner = onLoadMoreCalledListner;
	}
	public void removeOnLoadMoreListner(){
		hasLoadMoreListnerRemoved = true;
	}

	/**
	 * 
	 * @param pArrListItems : Arraylist of items or model to be added in Listview.
	 * @param pLayoutId	: LayoutId of the view in which model to be added. 
	 * @param pAddListItemView 
	 */
	public void setParamaters(ArrayList<?> pArrListItems,OnGetViewCalledListner onGetViewCalledListner){
		//mArrrListItems = (ArrayList<Object>) pArrListItems;
		mArrrListItems = new ArrayList<Object>();
		mArrrListItems.addAll(pArrListItems);
		this.onGetViewCalledListner = onGetViewCalledListner;
	}
	public void updateAdapterArrayList(ArrayList<?> pNewArrayList){
		if(pNewArrayList != null && pNewArrayList.size() != 0){
			for(int i = 0;i < pNewArrayList.size();i++){
				Object newObject = (Object)pNewArrayList.get(i);
				this.mArrrListItems.add(newObject);
			}
		}
		this.notifyDataSetChanged();
	}
	public ArrayList<Object> getAdapterArrayList(){
		return mArrrListItems;
	}
	public void removeItem(Object pObject){
		this.mArrrListItems.remove(pObject);
		//this.notifyDataSetChanged();
	}
	public void clearAdapter(){
		this.mArrrListItems.clear();
		this.notifyDataSetChanged();
	}

	public int getCount() {
		return this.mArrrListItems.size();
	}

	public Object getItem(int position) {
		return this.mArrrListItems.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		if(this.onLoadMoreCalledListner != null){
			Boolean addMore = (position == (getCount() - 1));
			if(addMore && !hasLoadMoreListnerRemoved){
				this.onLoadMoreCalledListner.loadMoreData(getCount());
			}
		}
		return this.onGetViewCalledListner.onGetViewCalled(position,getItem(position), convertView, parent);
	}
}

