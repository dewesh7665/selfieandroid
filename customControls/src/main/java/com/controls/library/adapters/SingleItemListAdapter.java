package com.controls.library.adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;

import com.controls.library.MultiItemListView;
import com.controls.library.helpers.AdapterParamInfo;
import com.controls.library.helpers.AdapterParams;
import com.controls.library.interfaces.MultiListInterfaces;
import com.controls.library.interfaces.MultiListInterfaces.OnMultiListScrollPositionListener;

public class SingleItemListAdapter extends BaseAdapter{
	public static interface MultiListLoadMoreListner{
		/**
		 * If 2 passed means url to be constructed for page 2.
		 * @param pageNumberToBeLoaded
		 */
		public void loadMoreData(int pageNumberToBeLoaded);
	}

	protected MultiListLoadMoreListner onLoadMoreListner;
	private Boolean hasLoadMoreListnerRemoved = false;

	private final String TAG = "SingleItemAdapter";
	private Boolean isDebuggingEnabled = false;
	//private Context mContext;
	private ArrayList<AdapterParams> mArrListAdpaterParams;
	private List<AdapterParams> mArrListSingleItemAdpaterParams;
	private AdapterParamInfo mAdapterParamInfo;
	private List<MultiListInterfaces.OnMultiListGetViewCalledListener> mListViewClassName;
	private Boolean isScrolling = false;

	private MultiItemListView multiItemListView;

	private int mCurrentPageNumber = 0;//Default initialisation

	public SingleItemListAdapter(Context context){
		//mContext = context;
		isDebuggingEnabled = (context.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
	}

	public SingleItemListAdapter(Context context,ArrayList<AdapterParams> arrListAdpaterParams){
		//mContext = context;
		isDebuggingEnabled = (context.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
		setAdapterParams(arrListAdpaterParams);
	}

	public void setAdapterParams(ArrayList<AdapterParams> arrListAdpaterParams){
		this.mArrListAdpaterParams = arrListAdpaterParams;
		mAdapterParamInfo = new AdapterParamInfo(mArrListAdpaterParams);
		mArrListSingleItemAdpaterParams = mAdapterParamInfo.getListForSingleItemAdapter();
		mListViewClassName = getListOfViewClassName();
	}

	/**
	 * To be used for situation where data is to come later e.z in pagination load more
	 * This must be called after {@link #setAdapterParams(ArrayList)}
	 * @param onMultiListCalledListner
	 */
	public void addAdditionalView(MultiListInterfaces.OnMultiListGetViewCalledListener onMultiListCalledListner){
		if(mListViewClassName == null)
			throw new IllegalStateException("This must be called after setAdapterParams and before setAdapter to list view");
		if(!mListViewClassName.contains(onMultiListCalledListner))
			mListViewClassName.add(onMultiListCalledListner);
	}

	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub
		mAdapterParamInfo = new AdapterParamInfo(mArrListAdpaterParams);
		mArrListSingleItemAdpaterParams = mAdapterParamInfo.getListForSingleItemAdapter();
		//if(mListViewClassName.size() != getListOfViewClassName().size())
		//Listview call view type count only once.
		//throw new IllegalStateException("@Dev:You can't add new view class instance in notifydatasetchanged. Please use view classes that already been initalized.");
		//TODO:@Dev : May be can be merged based on object equality
		super.notifyDataSetChanged();
	}

	public void setOnLoadMoreListner(MultiListLoadMoreListner onLoadMoreListner){
		this.onLoadMoreListner = onLoadMoreListner;
	}

	public void setScrollStatus(Boolean isScrolling){
		this.isScrolling = isScrolling;
	}

	private List<MultiListInterfaces.OnMultiListGetViewCalledListener> getListOfViewClassName(){
		MultiListInterfaces.OnMultiListGetViewCalledListener currentViewClassName = null;
		List<MultiListInterfaces.OnMultiListGetViewCalledListener> listViewClassName = new ArrayList<MultiListInterfaces.OnMultiListGetViewCalledListener>();
		for(int i = 0;i < mArrListSingleItemAdpaterParams.size();i++){
			MultiListInterfaces.OnMultiListGetViewCalledListener viewClassName = mArrListSingleItemAdpaterParams.get(i).getViewClassName();
			if(currentViewClassName == null || (!currentViewClassName.equals(viewClassName) && !listViewClassName.contains(viewClassName))){
				currentViewClassName = viewClassName;
				listViewClassName.add(currentViewClassName);
			}
		}
		return listViewClassName;
	}

	public void removeOnLoadMoreListner(){

	}

	public void updateAdapterArrayList(ArrayList<?> pNewArrayList){

		this.notifyDataSetChanged();
	}
	public List<AdapterParams> getAdapterArrayList(){
		return mArrListSingleItemAdpaterParams;
	}
	/*public void removeItem(Object pObject){
		this.mArrrListItems.remove(pObject);
		//this.notifyDataSetChanged();
	}
	public void clearAdapter(){
		this.mArrrListItems.clear();
		this.notifyDataSetChanged();
	}
	 */
	@Override
	public int getCount() {
		return this.mArrListSingleItemAdpaterParams.size();
	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		int count = mListViewClassName.size();
		return count;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		MultiListInterfaces.OnMultiListGetViewCalledListener viewClassName = mArrListSingleItemAdpaterParams.get(position).getViewClassName();
		return mListViewClassName.indexOf(viewClassName);
	}

	public Object getItem(int position) {
		return this.mArrListSingleItemAdpaterParams.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public int getPosition(Object obj){
		int position = 0;
		for(int i = 0;i < mArrListSingleItemAdpaterParams.size();i++){
			if(mArrListSingleItemAdpaterParams.get(i).getDataObject() == obj){
				position = i;
				break;
			}
		}
		return position;
	}

	public void removeLoadMoreListner(){
		hasLoadMoreListnerRemoved = true;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		if(this.onLoadMoreListner != null){
			Boolean addMore = (position == (getCount() - 1));
			if(addMore && !hasLoadMoreListnerRemoved){
				mCurrentPageNumber ++;
				this.onLoadMoreListner.loadMoreData(mCurrentPageNumber);
			}
		}
		MultiListInterfaces.OnMultiListGetViewCalledListener viewInterface = mListViewClassName.get(getItemViewType(position));
		/*BaseView1 baseView;
		if(hmpClaseInstanseName.containsKey(viewClassName))
			baseView = hmpClaseInstanseName.get(viewClassName);
		else
			baseView = getConstructor(viewClassName);
		return baseView.getPopulatedView(convertView, parent, mArrListSingleItemAdpaterParams.get(position).getDataObject());
		 */
		if(convertView != null)
			if(isDebuggingEnabled) Log.i(TAG, "GetView called .Position : "+position);
		return viewInterface.onGetViewCalled(convertView, parent, mArrListSingleItemAdpaterParams.get(position).getDataObject(), isScrolling);
	}

	public String getViewTypeClassName(int viewTypePosition){
		return mListViewClassName.get(viewTypePosition).getClass().getSimpleName();
	}

	public OnScrollListener getListOnScrollListner(){
		return onScrollListner;
	}

	/*private void checkForItemScrollListner(int position){
		AdapterParams adapterParam = mArrListSingleItemAdpaterParams.get(position);
		OnMultiListScrollPositionListner listner = adapterParam.getOnMultiListItemScrollPositionListner();
		if(listner != null)
			listner.onMultiListItemAtFirstVisiblePosition(adapterParam.getDataObject(),adapterParam.isFirstLast());
	}*/

	private int mLastFirstVisibleItem = -1;
	private int mStartStickyIndex = -1;
	private OnMultiListScrollPositionListener mMultiScrollListner = null;
	private OnScrollListener onScrollListner = new OnScrollListener() {

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			// TODO Auto-generated method stub
			if(scrollState != 0)
				setScrollStatus(true);
			else{
				setScrollStatus(false);
				notifyDataSetChanged();
			}
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
			// TODO Auto-generated method stub
			/**
			 * TODO: Try catch to be removed. Added because crash of TOI/NBT
			 */
			try{
				//For avoiding multiple scroll calls for same position
				if(mLastFirstVisibleItem == firstVisibleItem)
					return;
				else
					mLastFirstVisibleItem = firstVisibleItem;
				if(firstVisibleItem == 0){//TODO:To be handled for show sticky on 0th index and hide when scrolling down.
					if(mMultiScrollListner != null)
						mMultiScrollListner.onMultiListItemAtFirstVisiblePosition("",false);//Passing string for avoiding NPE
				}

				if(firstVisibleItem > 0){
					int realFirstVisibleItem = firstVisibleItem - getMultiItemListView().getListView().getHeaderViewsCount();

					/*-2 to make sure header visiblity gone. On slow scrolling and in boundry conditions, some times
					 * boundry index getting escaped. This will call listner 2 times more. 
					 */
					/*if(realFirstVisibleItem < mStartStickyIndex - 2)
					return;*/

					AdapterParams adapterParam = mArrListSingleItemAdpaterParams.get(realFirstVisibleItem);
					if(mMultiScrollListner == null) //Get first listner defined in adapter params.
						mMultiScrollListner = adapterParam.getOnMultiListItemScrollPositionListner();
					if(mMultiScrollListner != null){
						//Get first index of sticky header
						if(mStartStickyIndex == -1 && adapterParam.isFirstLast())
							mStartStickyIndex = realFirstVisibleItem;

						Object object = adapterParam.getStickyHeaderObject();
						if(realFirstVisibleItem < mStartStickyIndex)
							mMultiScrollListner.onMultiListItemAtFirstVisiblePosition(object,false);//Backward scrolling. Visiblity false
						else
							mMultiScrollListner.onMultiListItemAtFirstVisiblePosition(object,true);

					}
				}

			}
			catch(Exception neverMind){

			}}
	};

	public MultiItemListView getMultiItemListView() {
		return multiItemListView;
	}

	public void setMultiItemListView(MultiItemListView multiItemListView) {
		this.multiItemListView = multiItemListView;
	}

	/*private BaseView1 getConstructor(String className){

		try 
		{
			mClassName = Class.forName(className);
			mCoctructor = mClassName.getConstructor(Context.class);
			mBaseView = (BaseView1) mCoctructor.newInstance(mContext);

		} 
		catch (ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		catch (SecurityException e) 
		{
			e.printStackTrace();
		}
		catch (NoSuchMethodException e) 
		{
			e.printStackTrace();
		}
		catch (IllegalArgumentException e) 
		{
			e.printStackTrace();
		} 
		catch (InstantiationException e) 
		{
			e.printStackTrace();
		} 
		catch (IllegalAccessException e) 
		{
			e.printStackTrace();
		} 
		catch (InvocationTargetException e) 
		{
			e.printStackTrace();
		}
		return mBaseView;
	}*/
}