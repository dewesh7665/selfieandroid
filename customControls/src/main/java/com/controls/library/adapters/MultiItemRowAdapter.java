package com.controls.library.adapters;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.database.DataSetObserver;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.WrapperListAdapter;

import com.controls.library.MultiItemListView;
import com.controls.library.R;
import com.controls.library.adapters.SingleItemListAdapter.MultiListLoadMoreListner;
import com.controls.library.helpers.AdapterParamInfo;
import com.controls.library.helpers.AdapterParams;
import com.controls.library.interfaces.MultiListInterfaces;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class MultiItemRowAdapter implements WrapperListAdapter {
    protected MultiListLoadMoreListner onLoadMoreListner;
    private final String TAG = "MultiRowAdapter";
    private Boolean isDebuggingEnabled = false;
    private SingleItemListAdapter mSingleItemAdapter = null;
    //private final int mItemsPerRow = 2;
    private int mCellSpacing = 0;
    private final WeakReference<Context> mContextReference;
    private final LinearLayout.LayoutParams mItemLayoutParams;
    private final LinearLayout.LayoutParams m1stItemLayoutParams;
    private final LinearLayout.LayoutParams mLastItemLayoutParams;
    private final LinearLayout.LayoutParams mRestItemLayoutParams;
    private final AbsListView.LayoutParams mRowLayoutParams;
    private ViewGroup.LayoutParams mSingleItemLayoutParams = null;
    private Context mContext;

    private ArrayList<AdapterParams> mArrListAdpaterParams;
    private List<AdapterParams> mArrListMultiItemAdpaterParams;//This will providing all info about adapter i.e count,getview etc

    private AdapterParamInfo mAdapterParamInfo;
    private Boolean isAllRowSingleItem = false;

    private MultiItemListView multiItemListView;

    //private Boolean isScrolling = false;
    public void setSingleItemRow(Boolean isSingleItemRow) {
        this.isAllRowSingleItem = isSingleItemRow;
    }
    public MultiItemRowAdapter(Context context) {
        mContext = context;
        isDebuggingEnabled = (context.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
        mCellSpacing = mContext.getResources().getDimensionPixelSize(R.dimen.multirowadapter_cell_spacing);
        mContextReference = new WeakReference<Context>(context);
        mItemLayoutParams = new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT);
        mItemLayoutParams.weight = 1.0f;

        m1stItemLayoutParams = new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT);
        mLastItemLayoutParams = new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT);
        mRestItemLayoutParams = new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT);

        m1stItemLayoutParams.setMargins(0, 0, mCellSpacing/2, 0);
        mLastItemLayoutParams.setMargins(mCellSpacing/2, 0, 0, 0);
        mRestItemLayoutParams.setMargins(mCellSpacing/2, 0, mCellSpacing/2, 0);

        m1stItemLayoutParams.weight = 1.0f;
        mLastItemLayoutParams.weight = 1.0f;
        mRestItemLayoutParams.weight = 1.0f;
        //mItemLayoutParams.setMargins(mCellSpacing, 0, 0, 0);

        mRowLayoutParams = new AbsListView.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
    }

    /**
     * This to be called just before {@link MultiItemListView#setAdapter(android.widget.ListAdapter)}
     * All other initialization must be done before calling this function.
     * @param arrListAdpaterParams
     */
    public void setAdapterParams(ArrayList<AdapterParams> arrListAdpaterParams){
        this.mArrListAdpaterParams = arrListAdpaterParams;
        mAdapterParamInfo = new AdapterParamInfo(mArrListAdpaterParams);
        mArrListMultiItemAdpaterParams = mAdapterParamInfo.getListForMultiItemAdapter();
        isAllRowSingleItem = mAdapterParamInfo.isAllRowSingleItem();
        mSingleItemAdapter = (SingleItemListAdapter) getListAdapter();
        //mItemsPerRow = itemsPerRow;
        //mCellSpacing = cellSpacing;
    }

    /**
     * This function must be called before {@link #setAdapterParams(ArrayList)}
     * @param onLoadMoreListner
     */
    public void setOnLoadMoreListner(MultiListLoadMoreListner onLoadMoreListner){
        if(mSingleItemAdapter != null)
            throw new IllegalStateException("You must call setOnLoadMoreListner before calling setAdapterParams.");
        this.onLoadMoreListner = onLoadMoreListner;
    }

    public void removeLoadMoreListner(){
        if(mSingleItemAdapter != null){
            mSingleItemAdapter.removeLoadMoreListner();
            multiItemListView.removeFooterLoader();
        }
    }

    /**
     * To be used for situation where new view type is to be added on pagination/load more.
     * This must be called after {@link #setAdapterParams(ArrayList)}
     * @param onMultiListCalledListner
     */
    public void addAdditionalView(MultiListInterfaces.OnMultiListGetViewCalledListener onMultiListCalledListner){
        if(mSingleItemAdapter == null)
            throw new IllegalStateException("This must be called after setAdapterParams and before setAdapter to list view");
        mSingleItemAdapter.addAdditionalView(onMultiListCalledListner);
    }

	/*public void setAdapterParams(ArrayList<AdapterParams> arrListAdpaterParams,Boolean isNewAdapter){
		this.mArrListAdpaterParams = arrListAdpaterParams;
		mAdapterParamInfo = new AdapterParamInfo(mArrListAdpaterParams);
		mArrListMultiItemAdpaterParams = mAdapterParamInfo.getListForMultiItemAdapter();
		isAllRowSingleItem = mAdapterParamInfo.isAllRowSingleItem();
		if(isNewAdapter)
			mSingleItemAdapter = (SingleItemListAdapter) getListAdapter();
		else
			mSingleItemAdapter.notifyDataSetChanged();
		//mItemsPerRow = itemsPerRow;
		//mCellSpacing = cellSpacing;
	}*/

    public void notifyDataSetChanged(){
        mAdapterParamInfo = new AdapterParamInfo(mArrListAdpaterParams);
        mArrListMultiItemAdpaterParams = mAdapterParamInfo.getListForMultiItemAdapter();
        isAllRowSingleItem = mAdapterParamInfo.isAllRowSingleItem();
        mSingleItemAdapter.notifyDataSetChanged();
    }



    public MultiListLoadMoreListner getOnLoadMoreListner(){
        return this.onLoadMoreListner;
    }



    public void setScrollStatus(Boolean isScrolling){
        mSingleItemAdapter.setScrollStatus(isScrolling);
    }

    public OnScrollListener getListOnScrollListner(){
        return mSingleItemAdapter.getListOnScrollListner();
    }

	/*private OnScrollListener onScrollListner = new OnScrollListener() {

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
			// TODO Auto-generated method stub
			if(scrollState != 0)
				setScrollStatus(true);
			else{
				setScrollStatus(false);
				notifyDataSetChanged();
			}
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {
			// TODO Auto-generated method stub

		}
	};*/

    private BaseAdapter getListAdapter(){
        SingleItemListAdapter listAdapter = new SingleItemListAdapter(mContext,this.mArrListAdpaterParams);
        if(this.onLoadMoreListner != null){
            listAdapter.setOnLoadMoreListner(onLoadMoreListner);
            //multiItemListView.addFooterLoader();
        }
        return listAdapter;
    }

    @Override
    public boolean isEmpty() {
        return (mSingleItemAdapter == null || mSingleItemAdapter.isEmpty());
    }

    @Override
    public int getCount() {
		/*if (mSingleItemAdapter != null) {
			return (int)Math.ceil(1.0f * mSingleItemAdapter.getCount() / mItemsPerRow);
		}
		return 0;*/
        return mArrListMultiItemAdpaterParams.size();
    }

    @Override
    public boolean areAllItemsEnabled() {
        if (mSingleItemAdapter != null) {
            return mSingleItemAdapter.areAllItemsEnabled();
        } else {
            return true;
        }
    }

    @Override
    public boolean isEnabled(int position) {
		/*if (mSingleItemAdapter != null) {
			// the cell is enabled if at least one item is enabled
			boolean enabled = false;
			for (int i = 0; i < mItemsPerRow; ++i) {
				int p = position * mItemsPerRow + i;
				if (p < mSingleItemAdapter.getCount()) {
					enabled |= mSingleItemAdapter.isEnabled(p);
				}
			}
			return enabled;
		}
		return true;
		 */
        return mSingleItemAdapter.isEnabled(position);
    }

    @Override
    public Object getItem(int position) {
		/*if (mSingleItemAdapter != null) {
			int itemCountInThatRow = getItemCountInRow(position);
			List<Object> items = new ArrayList<Object>(itemCountInThatRow);
			for (int i = 0; i < itemCountInThatRow; ++i) {
				int p = position * itemCountInThatRow + i;
				if (p < mSingleItemAdapter.getCount()) {
					items.add(mSingleItemAdapter.getItem(p));
				}
			}
			return items;
		}
		return null;
		 */
        //mSingleItemAdapter.getItem(position);
        return mArrListMultiItemAdpaterParams.get(position);
    }

    @Override
    public long getItemId(int position) {
        if (mSingleItemAdapter != null) {
            return position;
        }
        return -1;
    }

    @Override
    public boolean hasStableIds() {
        if (mSingleItemAdapter != null) {
            return mSingleItemAdapter.hasStableIds();
        }
        return false;
    }

    @Override
    public int getItemViewType(int position) {
        if (mSingleItemAdapter != null) {
            return mSingleItemAdapter.getItemViewType(getPositionOfSingleAdapter(position, 0));
        }

        return IGNORE_ITEM_VIEW_TYPE;
    }

    @Override
    public int getViewTypeCount() {
        if (mSingleItemAdapter != null) {
            return mSingleItemAdapter.getViewTypeCount();
        }
        return 1;
    }

    /**
     * Will check if number of column for particular view class has been changed.
     * @param wrapperViewTag
     * @param viewClassName
     * @param numOfColumns
     * @return
     */
    private Boolean hasNumOfColumnChanged(WrapperViewTag wrapperViewTag,String viewClassName,int numOfColumns){
        return !(wrapperViewTag.viewClassName == viewClassName && wrapperViewTag.noOfCols == numOfColumns);
    }

    private String mLastViewType = null;
    private int mLastRowColCount = -1;
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context c = mContextReference.get();
        if (c == null || mSingleItemAdapter == null) return null;
        if(isAllRowSingleItem)
            return mSingleItemAdapter.getView(position, convertView, parent);
        int itemCountInThatRow = getItemCountInRow(position);
        Log.i("HomeFrag",itemCountInThatRow+"");
        LinearLayout view = null;
        String viewClassName = mSingleItemAdapter.getViewTypeClassName(getItemViewType(position));

//		if (convertView == null
//				|| !(convertView instanceof LinearLayout)
//				|| hasNumOfColumnChanged((WrapperViewTag)convertView.getTag(),viewClassName,itemCountInThatRow)) {
//			/*if (convertView == null
//				|| !(convertView instanceof LinearLayout)) {*/
//			// create a linear Layout
//			view = new LinearLayout(c);
//			//view.setPadding(0, 0, mCellSpacing, 0);
//			view.setLayoutParams(mRowLayoutParams);
//            if(mLastViewType != null && mLastViewType.equalsIgnoreCase(viewClassName) && mLastRowColCount != -1 && mLastRowColCount > itemCountInThatRow){
//                view.setWeightSum((float) mLastRowColCount);
//            }
//            else
//                view.setWeightSum(0.0f);
//
//			view.setOrientation(LinearLayout.HORIZONTAL);
//			//view.setBaselineAligned(true);
//
//			view.setTag(new WrapperViewTag(viewClassName,itemCountInThatRow));
//		}
        if(convertView == null || Integer.parseInt(convertView.getTag().toString()) != itemCountInThatRow){
            view = (LinearLayout) LayoutInflater.from(c).inflate(R.layout.view_multilist_wrapper, null);
            view.setTag(itemCountInThatRow);
            view.setWeightSum(mArrListMultiItemAdpaterParams.get(position).getNumOfColumn());
        }else {
            if(isDebuggingEnabled) Log.i(TAG, "GetView called .Position : "+position);
            view = (LinearLayout) convertView;
        }
        mLastViewType = viewClassName;
        mLastRowColCount = itemCountInThatRow;
        //view.setBackgroundResource(R.drawable.card_background);

        for (int i = 0; i < itemCountInThatRow; ++i) {
            View subView = i < view.getChildCount() ? view.getChildAt(i) : null;
            //int p = position * itemCountInThatRow + i;
            int p = getPositionOfSingleAdapter(position,i);

            View newView = subView;
            if (p < mSingleItemAdapter.getCount()) {
                newView = mSingleItemAdapter.getView(p, subView, view);
            } else if (subView == null || !(subView instanceof PlaceholderView)) {
                newView = new PlaceholderView(c);
            }
            if (newView != subView || i >= view.getChildCount()) {
                if (i < view.getChildCount()) {
                    view.removeView(subView);
                }
                //mItemLayoutParams.height = newView.getLayoutParams().height;
                if(itemCountInThatRow > 1){
                    if(i == 0) //First item
                        newView.setLayoutParams(m1stItemLayoutParams);
                    else if( i == itemCountInThatRow-1) //Last item
                        newView.setLayoutParams(mLastItemLayoutParams);
                    else //Rest item
                        newView.setLayoutParams(mRestItemLayoutParams);
                }
                else{
                    mItemLayoutParams.setMargins(0, 0, 0, 0);//intentionally set 0 so that won't pick any margin in changing coloumn numbers
                    newView.setLayoutParams(mItemLayoutParams);
                }
                view.addView(newView, i);
            }
        }
        return view;
    }


    /**
     * Will return position of  single row adapter corresponding to Multi item adapter
     * @param
     * @return
     */
    public int getPositionOfSingleAdapter(int positionMultiRowAdapter,int colPositionInRow){
        int singleItemAdapterPosition = 0;
        Object objRow = mArrListMultiItemAdpaterParams.get(positionMultiRowAdapter).getDataObject();
        if(objRow instanceof List<?>){
            List<?> listObjRow = (List<?>) objRow;
            //if(colPositionInRow < listObjRow.size())
            singleItemAdapterPosition = mSingleItemAdapter.getPosition(listObjRow.get(colPositionInRow));
        }
        else{
            singleItemAdapterPosition = mSingleItemAdapter.getPosition(objRow);
        }
        return singleItemAdapterPosition;
    }

    /**
     * Will return position of  Multiitem adapter corresponding to Singleitem adapter
     * @param
     * @return
     */
    public int getPositionOfMultiRowAdapter(int positionSingleRowAdapter){
        int multiItemAdapterPosition = 0;
        Object objOfSingleItemAdapter = ((AdapterParams)mSingleItemAdapter.getItem(positionSingleRowAdapter)).getDataObject();
        for(int i = 0;i < mArrListMultiItemAdpaterParams.size();i++){
            Object objRow = mArrListMultiItemAdpaterParams.get(i).getDataObject();
            Object objOfMultiItemAdapter = null;
            if(objRow instanceof List<?>)
                objOfMultiItemAdapter =  ((List<?>) objRow).get(0);//0th index item
            else
                objOfMultiItemAdapter = objRow;
            if(objOfSingleItemAdapter == objOfMultiItemAdapter){
                multiItemAdapterPosition = i;
                break;
            }
        }
        return multiItemAdapterPosition;
    }

    /**
     * Will itrate through adapterParamArraylist and return position corrosonding to object
     * @param object
     * @return
     */
    public int getPositionInCurrentAdapter(Object object){
        int adapterPosition = 0;
        for(int i = 0;i < mArrListMultiItemAdpaterParams.size();i++){
            Object objRow = mArrListMultiItemAdpaterParams.get(i).getDataObject();
            //Object objOfMultiItemAdapter = null;
            if(objRow instanceof List<?>){
                List<?> listObjRow = (List<?>) objRow;{
                    for(int j = 0;j < listObjRow.size();j++){
                        if(listObjRow.get(j) == object){
                            adapterPosition = i;
                            break;
                        }
                    }
                }
            }
            else{
                if(objRow == object){
                    adapterPosition = i;
                    break;
                }
            }
        }
        return adapterPosition;
    }

    /**
     * Will return object at position irrespective of SingleItem(FirstColumn will be only column)/MultiItem
     * @param position
     * @return
     */
    public Object getDataObjectAtFirstColumn(int position){
        //TODO:Null check.Big Hack. It shouldn't be here.But some how mArrListMultiItemAdpaterParams refrence getting null on orientation changed
        if(mArrListMultiItemAdpaterParams == null || mArrListMultiItemAdpaterParams.size() == 0)
            return null;
        Object objRow = mArrListMultiItemAdpaterParams.get(position).getDataObject();
        Object objOfAdapter = null;
        if(objRow instanceof List<?>)
            objOfAdapter =  ((List<?>) objRow).get(0);//0th index item
        else
            objOfAdapter = objRow;
        return objOfAdapter;
    }

	/*@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		//return mSingleItemAdapter.getView(position, convertView, parent);
		Context c = mContextReference.get();
		if (c == null || mSingleItemAdapter == null) return null;

		int itemCountInThatRow = getItemCountInRow(position);
		LinearLayout view = null;
		if (convertView == null) {
			// create a linear Layout
			view = new LinearLayout(c);
			view.setPadding(0, 0, mCellSpacing, 0);
			view.setLayoutParams(mRowLayoutParams);
			view.setOrientation(LinearLayout.HORIZONTAL);
			view.setBaselineAligned(false);
			//view.setTag(Integer.valueOf(itemCountInThatRow));
		} else {
			view = (LinearLayout) convertView;
		}

		for (int i = 0; i < itemCountInThatRow; i++) {
			View subView = i < view.getChildCount() ? view.getChildAt(i) : null;
			int p = position * itemCountInThatRow + i;

			View newView = subView;
			if (p < mSingleItemAdapter.getCount()) {
				newView = mSingleItemAdapter.getView(p, subView, view);
			} else if (subView == null || !(subView instanceof PlaceholderView)) {
				newView = new PlaceholderView(c);
			}
			if (newView != subView || i >= view.getChildCount()) {
				if (i < view.getChildCount()) {
					view.removeView(subView);
				}
				//newView.setLayoutParams(mItemLayoutParams);
				view.addView(newView, i);
			}
		}

		return view;
	}*/

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        if (mSingleItemAdapter != null) {
            mSingleItemAdapter.registerDataSetObserver(observer);
        }
    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
		/*if (mSingleItemAdapter != null) {
			mSingleItemAdapter.unregisterDataSetObserver(observer);
		}*/
    }

    @Override
    public BaseAdapter getWrappedAdapter() {
        return mSingleItemAdapter;
    }

    public static class PlaceholderView extends View {

        public PlaceholderView(Context context) {
            super(context);
        }

    }

    private int getItemCountInRow(int position){
        Object dataObjectInThatRow = mArrListMultiItemAdpaterParams.get(position).getDataObject();
        int itemCountInThatRow = 1;//Default
        if(dataObjectInThatRow instanceof List<?>){
            itemCountInThatRow = ((List<?>)dataObjectInThatRow).size();
        }
        return itemCountInThatRow;
        //return mArrListMultiItemAdpaterParams.get(position).getNumOfColumn();
    }
    public MultiItemListView getMultiItemListView() {
        return multiItemListView;
    }
    public void setMultiItemListView(MultiItemListView multiItemListView) {
        this.multiItemListView = multiItemListView;
        mSingleItemAdapter.setMultiItemListView(this.multiItemListView);
    }

    private static class WrapperViewTag{
        public String viewClassName;
        public int noOfCols = 1;
        public WrapperViewTag(String viewClassName,int numOfCols){
            this.viewClassName = viewClassName;
            this.noOfCols = numOfCols;
        }
    }

}