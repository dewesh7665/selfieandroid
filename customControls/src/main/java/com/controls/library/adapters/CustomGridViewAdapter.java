package com.controls.library.adapters;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.controls.library.CustomGridView;
import com.controls.library.CustomGridView.OnGetViewCalledListner;
import com.controls.library.CustomGridView.OnLoadMoreCalledListner;

public class CustomGridViewAdapter extends BaseAdapter {

	protected int mItemCount;
	private CustomGridView mGrid;

	protected OnGetViewCalledListner onGetViewCalledListner;
	protected Boolean hasLoadMoreListnerRemoved = false;
	protected CustomGridView.OnLoadMoreCalledListner onLoadMoreCalledListner;

	public CustomGridViewAdapter(int count, OnGetViewCalledListner onGetViewCalledListner2) {
		mItemCount = count;
		this.onGetViewCalledListner = onGetViewCalledListner2;
	}
	
	public void removeOnLoadMoreListner(){
		hasLoadMoreListnerRemoved = true;
	}

	public void setOnLoadMoreListner(OnLoadMoreCalledListner onLoadMoreCalledListner){
		this.onLoadMoreCalledListner = onLoadMoreCalledListner;
	}
	
	public void setGridView(CustomGridView grid){
		this.mGrid = grid;
	}

	@Override
	public int getCount() {
		return mItemCount;
	}

	public void setCount(int itemCount) {
		this.mItemCount = itemCount;
	}
	
	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		if(this.onLoadMoreCalledListner != null){
			Boolean addMore = (position == (getCount() - 1));
			if(addMore && !hasLoadMoreListnerRemoved){
				this.onLoadMoreCalledListner.loadMoreData(getCount());
			}
		}
		convertView = this.onGetViewCalledListner.onGetViewCalled(convertView, position, parent);
		return convertView;
	}
}

