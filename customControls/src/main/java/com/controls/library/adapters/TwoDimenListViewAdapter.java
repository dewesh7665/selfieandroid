package com.controls.library.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.controls.library.TwoDimenListView.OnLoadMoreCalledListner;

public class TwoDimenListViewAdapter extends BaseAdapter {

	private int mViewCount = -1;
	private int mCount = -1;
	private OnGetViewCalledListener mListener;
	protected Boolean hasLoadMoreListnerRemoved = false;
	protected OnLoadMoreCalledListner onLoadMoreCalledListner;

	private boolean isScrolling = false;
	private int firstVisibleIndex = -1;
	private int endVisibleIndex = -1;

	private int listPageNumber = 1;
	
	boolean scrollingState = false;
	public void setScrolling(Boolean isScrolling) {
		this.isScrolling = isScrolling;
	}

	public void setIndex(int last, int first,boolean scrollState){
		this.firstVisibleIndex = first;
		this.endVisibleIndex = last;
		scrollingState = scrollState;
	}

	public Boolean isScrolling() {
		return isScrolling;
	}

	public interface OnGetViewCalledListener {
		public View onGetViewCalled(int position, View convertView, ViewGroup parent, Boolean isScrolling/*, boolean lastRowCalled*/);
		public int getItemViewType(int position);
	}

	public TwoDimenListViewAdapter(Context context) {

	}
	
	public void setOnLoadMoreListner(OnLoadMoreCalledListner onLoadMoreCalledListner){
		this.onLoadMoreCalledListner = onLoadMoreCalledListner;
	}
	public void removeOnLoadMoreListner(){
		hasLoadMoreListnerRemoved = true;
	}

	public void addOnLoadMoreListner(){
		hasLoadMoreListnerRemoved = false;
	}
	
	public int getListPageNumber(){
		return listPageNumber;
	}
	
	public void resetListPageNumber(){
		this.listPageNumber = 1;
	}

	public void incListPageNumber(){
		this.listPageNumber++;
	}
	
	@Override
	public boolean isEnabled(int position) {
		return false;
	}

	public TwoDimenListViewAdapter(Context context, int viewCount, int count, OnGetViewCalledListener _listener) {
		this.mViewCount = viewCount;
		this.mCount = count;
		this.mListener = _listener;
	}

	@Override
	public int getCount() {
		return mCount;
	}

	public void updateGaanaAdapter(int count) {
		this.mCount = count;		
		notifyDataSetChanged();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {		
		isScrolling = shouldBindDefaultImage(position);
		if(this.onLoadMoreCalledListner != null){
			Boolean addMore = (position == (getCount() - 1));
			if(addMore && !hasLoadMoreListnerRemoved){
				this.onLoadMoreCalledListner.loadMoreData(listPageNumber);
			}
		}
		return mListener.onGetViewCalled(position, convertView, parent, isScrolling);
	}

	private boolean shouldBindDefaultImage(int position){
//		boolean isFlinging = false;

		/*if(firstVisibleIndex < endVisibleIndex){
			if(position >= firstVisibleIndex && position <= endVisibleIndex)
				isFlinging = false;
			else
				isFlinging = true;
		}
		else {
			if(position <= firstVisibleIndex && position >= endVisibleIndex)
				isFlinging = false;
			else
				isFlinging = true;
		}
		 */

//		if(position >= firstVisibleIndex  && position <= endVisibleIndex + 2){
//			isFlinging = false;
//
//		}else if(position <= firstVisibleIndex && position >= endVisibleIndex - 2){
//			isFlinging = false;
//		}else{
//			isFlinging = true;
//		}
//
//		if(firstVisibleIndex == 0 || endVisibleIndex == 0)
//			isFlinging = false;

		//		return isFlinging;


		if(!scrollingState&&firstVisibleIndex==0&&endVisibleIndex==0)
			return false;
		else
			return true;
	}

	public void clearAdapter(){
		mCount = 0;
		this.notifyDataSetChanged();
	}

	@Override
	public int getItemViewType(int position) {
		if(null!=mListener)
			return mListener.getItemViewType(position);
		else 
			return 0;
	}

	@Override
	public int getViewTypeCount() {
		return mViewCount;
	}
}
