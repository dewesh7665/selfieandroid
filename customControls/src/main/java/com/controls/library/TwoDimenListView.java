
package com.controls.library;


import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.controls.library.adapters.TwoDimenListViewAdapter;
import com.controls.library.adapters.TwoDimenListViewAdapter.OnGetViewCalledListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
//import android.text.TextUtils;
//import android.util.Log;
//import com.gaana.models.BusinessObject;
//import com.gaana1.BaseGaanaFragment;
//import com.managers.URLManager;



public class TwoDimenListView {// implements LoadMoreListner{
	public static interface OnGetViewCalledListner{
		public View onGetViewCalled(int position,View convertView,ViewGroup viewGroup);
	}
	public static interface OnLoadMoreCalledListner{
		public void loadMoreData(int currentMaxCount);
	}
	public static interface OnGetStickyHeaderViewCalledListner{
		public String getHeader(int firstVisibleItem);
		//TODO:To be implemented
		//public View getHeaderView(int position);
	}
	public interface OnPullToRefreshListener {
		public void onPulltoRefreshCalled();
	}

	private Context mContext;
	private LayoutInflater mInflater = null;
	private LinearLayout mEmptyView = null;
	private LayoutParams mLayoutParams = null;
	private View mView = null;

	private ListView mListViewHome;
	private TwoDimenListViewAdapter mAdapter = null;
	//private LinearLayout mParentStickyHeaedr;
	private TextView mHeaderText;
	private PullToRefreshListView pullToRefreshlistView = null;

	private OnGetViewCalledListner onGetViewCalledListner;
	private OnGetStickyHeaderViewCalledListner onGetStickyHeaderViewCalledListner;
	private String mStrHeaderText = "";
	private LinearLayout mParentLoadingRow;
//	private GridViewHelper mGridViewHelper = null;
	private int listViewRowCount;
//	private GaanaApplication mAppState;
	private Boolean hasDataEnded = false;
	public View mLoadMoreProgressBar;
	private boolean mIsLoadMoreProgressBarVisible = false;
	public Boolean mIsQueryDone = true;
//	private BusinessObject mBusinessObject;
	private Boolean isGridStarted = false;
	protected View mListFooterView = null;
	private int mCountPerPage = 10;
	protected OnLoadMoreCalledListner onLoadMoreListner;
	private OnPullToRefreshListener mPullToRefreshListener = null;

	public void setPullToRefreshListener(OnPullToRefreshListener pullToRefreshListener) {
		this.mPullToRefreshListener= pullToRefreshListener; 
	}

	public TwoDimenListView(Context context) {
		mContext = context;
		mEmptyView = new LinearLayout(mContext);
		mLayoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		mEmptyView.setLayoutParams(mLayoutParams);
		mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mView = mInflater.inflate(R.layout.view_custom_listview,null);
		pullToRefreshlistView = (PullToRefreshListView)mView.findViewById(R.id.listViewHome);
		//pullToRefreshlistView.setPullToRefreshEnabled(false);
		mParentLoadingRow = (LinearLayout)mView.findViewById(R.id.llParentLoading);
		mParentLoadingRow.setVisibility(View.GONE);
		mListViewHome = pullToRefreshlistView.getRefreshableView();
		mListViewHome.setOnScrollListener(onScrollListener);
		mListViewHome.setClickable(false);
//		mAppState = (GaanaApplication)mContext.getApplicationContext();
		setPullRefreshListner();
	}

	public PullToRefreshListView getRefreshableView(){
		return pullToRefreshlistView;
	}
	public Boolean isLoadMoreImplemented(){
		if(this.onLoadMoreListner == null)
			return false;
		return true;
	}

	public void hideProgressBar(){
		mParentLoadingRow.setVisibility(View.GONE);
	}
	public void setOnLoadMoreListner(OnLoadMoreCalledListner onLoadMoreListner,int countPerPage){
		this.onLoadMoreListner = onLoadMoreListner;
		mCountPerPage = countPerPage;
	}

	public void removeOnLoadMoreListner(){
		if(mAdapter != null) mAdapter.removeOnLoadMoreListner();
		if(mListViewHome != null) mListViewHome.removeFooterView(mListFooterView);
	}


	public void showHideLoading(boolean showLoading){
		if(showLoading){
			mParentLoadingRow.setVisibility(View.VISIBLE);
			//	pullToRefreshlistView.setVisibility(View.GONE);
		}
		else{
			mParentLoadingRow.setVisibility(View.GONE);
			//	pullToRefreshlistView.setVisibility(View.VISIBLE);
		}
	}

	public void setOnGetViewCalledListner(OnGetViewCalledListner onGetViewCalledListner){
		this.onGetViewCalledListner = onGetViewCalledListner;
	}

	public void setRemoveListViewDivider() {
		mListViewHome.setDivider(null);
		mListViewHome.setDividerHeight(0);
	}

	public void setOnGetStickyHeaderCalledListner(OnGetStickyHeaderViewCalledListner listner){
		if(listner != null){
			this.mListViewHome.setOnScrollListener(onScrollListner);
			this.onGetStickyHeaderViewCalledListner = listner;
		}
	}

//	public void setBusinessObject(BusinessObject businessObject){
//		
//		if(mGridViewHelper!=null)
//			mGridViewHelper.setBusinessObject(mBusinessObject);
//	}

//	public View getPopulatedGridRowView(int position, View convertView, ViewGroup parent, Boolean isScrolling) {
//		if(null != mGridViewHelper)
//			convertView = mGridViewHelper.getPopulatedView(position, convertView, parent, isScrolling);	
//		return convertView;
//	}

	private int mStartIndex = 0;
	private int mColumnCount = 0;

//	public void addGridViewContent(BusinessObject businessObject, String className, int columnCount, int startIndex, boolean loadMoreEnabled) {			
//		this.mStartIndex = startIndex;
//		this.mBusinessObject = businessObject;
//		this.mColumnCount = columnCount;
//		if(businessObject.getArrListBusinessObj() == null || businessObject.getArrListBusinessObj().size() == 0)
//			return;
//
//		if(loadMoreEnabled) {
//			mLoadMoreProgressBar = ((LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.list_loading_row, null);
//			mGridViewHelper = new GridViewHelper(mContext, mFragment ,this, businessObject, className, columnCount, startIndex, this);
//
//			if(businessObject.getArrListBusinessObj() != null && businessObject.getArrListBusinessObj().size() <= Constants.LIST_ITEM_PER_PAGE)
//				mLoadMoreProgressBar.setVisibility(View.GONE);
//		}
//		else {
//			mGridViewHelper = new GridViewHelper(mContext, mFragment ,this, businessObject, className, columnCount, startIndex, null);
//		}
//	}


	boolean updatingGridViewContent = false;
//	public void updateGridViewContent(BusinessObject businessObject) {
//		ArrayList<BusinessObject> mArrListItems = (ArrayList<BusinessObject>) mBusinessObject.getArrListBusinessObj();
//		ArrayList<BusinessObject> pNewArrayList = (ArrayList<BusinessObject>) businessObject.getArrListBusinessObj();
//
//		mArrListItems.addAll(pNewArrayList);
//
//		//this.mBusinessObject.setArrListBusinessObj(mArrListItems);
//
//		if(null != mGridViewHelper)
//			mGridViewHelper.updateData(mBusinessObject);
//
//		int gridRows = mBusinessObject.getArrListBusinessObj().size()/mColumnCount + mBusinessObject.getArrListBusinessObj().size() % mColumnCount;
//		listViewRowCount = mStartIndex + gridRows;
//		updatingGridViewContent = true;
//		mAdapter.updateGaanaAdapter(listViewRowCount);
//		updatingGridViewContent = false;
//
//	}

	public void setLoadMoreProgressBar(View view)
	{
		mLoadMoreProgressBar = view;
	}
	public void onLoadMoreStarted() {
		if(!mIsLoadMoreProgressBarVisible) {
			mIsLoadMoreProgressBarVisible = true;
			mListViewHome.addFooterView(mLoadMoreProgressBar);
			mLoadMoreProgressBar.setVisibility(View.VISIBLE);
		}
	}

	public void onLoadMoreComplete() {
		if(mIsLoadMoreProgressBarVisible) {
			mIsLoadMoreProgressBarVisible = false;
			mListViewHome.removeFooterView(mLoadMoreProgressBar);
		}
	}

	public ListView getListView(){
		return mListViewHome;
	}

	public View getPopulatedView(){
		return mView;
	}

	public TwoDimenListViewAdapter getListAdapter() {
		return mAdapter;
	}

	
	OnScrollListener onScrollListner = new OnScrollListener() 
	{

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState)  {
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) 
		{
			if(firstVisibleItem >0){
				//				Log.e("PeeyushKS", "firstVisibleItem: "+firstVisibleItem+", visibleItemCount: "+visibleItemCount+"; totalItemCount: "+totalItemCount);
				String headerText = onGetStickyHeaderViewCalledListner.getHeader(firstVisibleItem);
				if(headerText == null)
					throw new IllegalStateException("You can't return null from onGetStickyHeaderViewCalledListner.getHeader.Please init with empty string.");

				if(hasHeaderTextChanged(headerText)){
					//mParentStickyHeaedr.setVisibility(View.VISIBLE);
					mHeaderText.setText(mStrHeaderText);
				}
				else if(mStrHeaderText.length() == 0){
					//mParentStickyHeaedr.setVisibility(View.GONE);
				}
			}
		}
	};

	private Boolean hasHeaderTextChanged(String newString){
		if(mStrHeaderText.compareTo(newString) == 0)
			return false;
		else{
			mStrHeaderText = newString;
			return true;
		}
	}

	Handler handler;//Temp : To be implemented
	public void setPullRefreshListner() {
		pullToRefreshlistView.setOnRefreshListener(new OnRefreshListener<ListView>() {
			public void onRefresh(PullToRefreshBase<ListView> refreshView){

				handler = new Handler();
				handler.postDelayed(new Runnable() {

					@Override
					public void run() {
						if(null != mPullToRefreshListener) {
							hasDataEnded = false;
							mPullToRefreshListener.onPulltoRefreshCalled();
						}
					}
				}, 1000);
			}
		});
	}

	public void setAdapterParams(Context context, int viewCount, int count, OnGetViewCalledListener _listener){
		mParentLoadingRow.setVisibility(View.GONE);

		int gridSizeRow = count/2 + count%2;

		count = viewCount-1+gridSizeRow;
		if(count == 0)
			throw new NullPointerException("Please make sure your arraylist is not null and empty.");
		this.listViewRowCount = count;
		//	if(mAdapter == null){
		//	if(this.onGetViewCalledListner == null)
		//	throw new NullPointerException("Please make sure to call setOnGetViewCalledListner before calling setUpdateListItems.");
		mAdapter = new TwoDimenListViewAdapter(mContext, viewCount, count, _listener);
		if(this.onLoadMoreListner != null){
			mAdapter.setOnLoadMoreListner(onLoadMoreListner);
			if(mListViewHome.getFooterViewsCount() == 0){
				mListFooterView = ((LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.list_loading_row, null);
				mListViewHome.addFooterView(mListFooterView);
			}
		}
		mListViewHome.setAdapter(mAdapter);
		pullToRefreshlistView.onRefreshComplete();
		showHideLoading(false);
	}

	
	public int getListViewRowCount()
	{
		return listViewRowCount;
	}
	
	public void setListViewRowCount(int rowViewCount)
	{
		listViewRowCount = rowViewCount;
	}
	
//	public void setGaanaListHomeAdapter(int gridSizeRow,final ArrayList<HomeScrollerView> mArrScrollerView,boolean isRefreshing) {
	
//	public void setGaanaListHomeAdapter(BusinessObject businessObj,final ArrayList<HomeScrollerView> mArrScrollerView,boolean isRefreshing, String className) {
//	
//		setBusinessObject(businessObj);
//		int gridSize=0;
//		
//		if(null != businessObj && businessObj.getArrListBusinessObj() !=null && businessObj.getArrListBusinessObj().size() > 0)
//			gridSize = businessObj.getArrListBusinessObj().size();
//		int gridSizeRow = gridSize/2 + gridSize%2;
//		
////		mGaanaListView.setAdapterParams(mContext, 9, (8 + gridSizeRow), new OnGetViewCalledListener() {
//		setAdapterParams(mContext, mArrScrollerView.size()+1, (mArrScrollerView.size() + gridSizeRow), new OnGetViewCalledListener() {
//
//			@Override
//			public View onGetViewCalled(int position, View convertView, ViewGroup parent, Boolean isScrolling) {
//				if(position < mArrScrollerView.size()) {
//					
//					convertView = mArrScrollerView.get(position).getPopulatedView(convertView, parent);
//				}
//				else {
//					convertView = getPopulatedGridRowView(position, convertView, parent, isScrolling);
//				}
//				return convertView;
//			}
//			
//			@Override
//			public int getItemViewType(int position) {				
//				if(position >= 0 && position <= mArrScrollerView.size()-1)
//					return position;
//				else 
//					return mArrScrollerView.size();
//			}
//		});	
//
//		
////		if(!isRefreshing) {
////			addGridViewContent(businessObj, className, 2, mArrScrollerView.size(), true);
////			setRemoveListViewDivider();
////		}
//
////		resetLoginStatus();
//	}
	
	
//	public URLManager getUrlManager() {
//		return mBusinessObject.getUrlManager();
//	}

//	private String getNewUrl(String baseUrl, int start, int end, Object object) {
//		if(!TextUtils.isEmpty(baseUrl))
//			baseUrl = removeTokenFromUrl(baseUrl);
//
//		if(object != null && object instanceof UserActivity){
//			UserActivity lastSeenUserActivity = (UserActivity)object;
//			if(baseUrl.contains("last_seen_id")) {
//				String paginationUrl = baseUrl.substring(0, baseUrl.lastIndexOf("&"));
//				return  paginationUrl + "&last_seen_id="+lastSeenUserActivity.getActivityId();
//
//			} else{
//				return baseUrl + "&last_seen_id="+lastSeenUserActivity.getActivityId();
//			}
//		} 
//		else{
//			if(baseUrl.contains("limit")) {
//				String paginationUrl = baseUrl.split("&limit") [0];
//				return paginationUrl + "&limit="+start+","+end;
//			}
//			else
//				return baseUrl + "&limit="+start+","+end;
//		}
//	}

//	public String removeTokenFromUrl(String url)
//	{
//		String[] params = url.split("&");
//		String tokenRemovedUrl = null;
//		for (String param : params)  
//		{  
//			String name = param.split("=")[0];
//			if(name.compareToIgnoreCase("token") == 0){
//				String value = param.split("=")[1];
//				tokenRemovedUrl = url.replace("&"+name+"="+value, " ").trim();
//			}
//		}
//		if (tokenRemovedUrl == null)
//			tokenRemovedUrl = url;
//		return tokenRemovedUrl;
//	}

//	@Override
//	public vo1id loadMoreData(int currentCount, Object object) {
//		if(mAppState.isAppInOfflineMode() || !Util.hasInternetAccess(mContext))
//			return;
//
//		if (!hasDataEnded && mIsQueryDone) {
//			onLoadMoreStarted();
//			mIsQueryDone = false;
//			getUrlManager().setFinalUrl(getNewUrl(getUrlManager().getFinalUrl(),currentCount,Constants.LIST_ITEM_PER_PAGE, object));			// Added in Rev. 278 (older code was skipping one record)
//			((GaanaActivity)mContext).startFeedRetreival(onLoadMoreDataFinished,getUrlManager(),false);
//		}
//	}

//	OnBusinessObjectRetrieved onLoadMoreDataFinished = new OnBusinessObjectRetrieved() {
//		@Override
//		public void onRetreivalComplete(BusinessObject businessObj) {
//			onLoadMoreComplete();
//			mIsQueryDone = true;
//			if(businessObj == null || businessObj.getArrListBusinessObj() == null || businessObj.getArrListBusinessObj().size() == 0) {
//				hasDataEnded = true;
//			}
//			else {
//				if(businessObj.getArrListBusinessObj().size() < Constants.LIST_ITEM_PER_PAGE)
//					hasDataEnded = true;
//				updateGridViewContent(businessObj);
//			}
//		}
//	};

	int firstVisibleIndex = 0;
	int endVisibleIndex = 0;
	//int currentVisibleIndex = 0;
	int visibleCount = 0;
	int firstvalue=0;
	int visibleValue = 0;
	
	
	public void setUpdatingGridViewContent(boolean updatingViewContent)
	{
		updatingGridViewContent = updatingViewContent;
	}
	
	public int getStartIndex()
	{
		return mStartIndex;
	}
	public void setStartIndex(int startIndex) {
		mStartIndex = startIndex;
	}
	
	public int getColumnCount()
	{
		return mColumnCount;
	}
	
	public void setColumnCount(int columnCount)
	{
		mColumnCount = columnCount;
	}
	
//	public void setGridBusinessObject(BusinessObject businessObject)
//	{
//		mBusinessObject = businessObject;
//	}
	
	
//	public void setGridViewHelper(GridViewHelper gridViewHelper)
//	{
//		mGridViewHelper = gridViewHelper;
//	}
	
	public void setHasDataEnded(boolean dataStatus)
	{
		hasDataEnded = dataStatus;
	}
	
	public boolean getHasDataEnded()
	{
		return hasDataEnded;
	}
	
	private OnScrollListener onScrollListener = new OnScrollListener() {

		private Boolean isScrolling = false;

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {

			//			firstvalue = firstVisibleItem;
			//			visibleValue = visibleItemCount;
			//			if( (firstVisibleItem+visibleItemCount) >= mStartIndex) {
			if( (firstVisibleItem+visibleItemCount) > mStartIndex+1) {
				isGridStarted = true;
			}
			else {
				isGridStarted = false;
			}
			//			Log.e("PeeyushKS", isGridStarted+"--"+mStartIndex+" :firstVisibleItem: "+firstVisibleItem+", visibleItemCount: "+visibleItemCount+", totalItemCount: "+totalItemCount);

			if(isGridStarted){
				if(TwoDimenListView.this.firstVisibleIndex == 0 ) {
					TwoDimenListView.this.firstVisibleIndex = firstVisibleItem;
					endVisibleIndex = firstVisibleIndex + visibleItemCount;
					visibleCount = visibleItemCount;
				}		

				if(TwoDimenListView.this.firstVisibleIndex > firstVisibleItem){
					//currentVisibleIndex = firstVisibleItem;
					endVisibleIndex = firstVisibleItem - visibleItemCount;

				}
				else{
					//currentVisibleIndex = firstVisibleItem;
					endVisibleIndex = firstVisibleItem + visibleItemCount;
				}
			}
		}

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {


			if(null != mAdapter && isGridStarted) {

				//				Log.e("PeeyushKS", "isGridStarted: "+isGridStarted);

				switch (scrollState) {
				case OnScrollListener.SCROLL_STATE_IDLE:
					TwoDimenListView.this.firstVisibleIndex = 0;
					endVisibleIndex = 0;
					isScrolling = false;
					mAdapter.setIndex(0, 0,true);
					break;

				case OnScrollListener.SCROLL_STATE_TOUCH_SCROLL:
					isScrolling = false;
					mAdapter.setIndex(0, 0,true);
					break;

				case OnScrollListener.SCROLL_STATE_FLING:
					//					isScrolling = true;
					isScrolling = false;
					//					mAdapter.setIndex(endVisibleIndex, firstVisibleIndex);
					//					mAdapter.setIndex(endVisibleIndex, endVisibleIndex-4);
					break;
				}

				//				if(scrollState==0&&firstvalue>7){
				if(scrollState==0&&!updatingGridViewContent){
					if(isScrolling != mAdapter.isScrolling()){
						if(endVisibleIndex>3)
							mAdapter.setIndex(endVisibleIndex, endVisibleIndex-4,false);
						else
							mAdapter.setIndex(endVisibleIndex, 0,false);	

						mAdapter.setScrolling(isScrolling);
						//mAdapter.notifyDataSetChanged();
					}
				}
			}
		}
	};


//	@Override
//	public void loadMoreData(int currentCount) {
//		// TODO Auto-generated method stub
//
//	}

	private View getEmptyView(String message, ViewGroup parent) {
		View view = mInflater.inflate(R.layout.view_user_msg,parent,false);
		((TextView)view.findViewById(R.id.tvUserMsg)).setText(message);
		return view;
	}	

	public void populateEmptyMsgInListView(final String message){
		if(mAdapter != null)
			mAdapter.clearAdapter();
		mAdapter = new TwoDimenListViewAdapter(mContext,1,1, new OnGetViewCalledListener() {

			@Override
			public View onGetViewCalled(int position, View convertView,
					ViewGroup parent, Boolean isScrolling) {
				return getEmptyView(message, parent);
			}

			@Override
			public int getItemViewType(int position) {
				return 0;
			}
		});
		mListViewHome.setAdapter(mAdapter);
	}
	
}
