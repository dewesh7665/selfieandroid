package com.controls.library.interfaces;

import android.view.View;
import android.view.ViewGroup;

public class MultiListInterfaces {
	/**
	 * 
	 * MultiList added for avoiding wrong import since we are using OnGetViewCalledListener in many files :)
	 *
	 */
	public static interface OnMultiListGetViewCalledListener {
		/**
		 * 
		 * @param view
		 * @param parent
		 * @param object
		 * @param isScrolling
		 * @return
		 */
		public View onGetViewCalled(View view, ViewGroup parent,Object object,Boolean isScrolling);
	}
	
	public static interface OnMultiListScrollPositionListener {
		/**
		 * 
		 * @param object
		 * @return
		 */
		public void onMultiListItemAtFirstVisiblePosition(Object object,Boolean isHeaderToBeShown);
	}
}
