package com.logic.photo.gestures;

import android.graphics.PointF;
import android.util.Log;

public class Vector2D extends PointF {

	public Vector2D() {
		super();
	}

	public Vector2D(float x, float y) {
		super(x, y);
	}

	public static float getAngle(Vector2D vector1, Vector2D vector2) {
		vector1.normalize();
		vector2.normalize();
		double degrees = (180.0 / Math.PI)
				* (Math.atan2(vector2.y, vector2.x) - Math.atan2(vector1.y,
						vector1.x));
		return (float) degrees;
	}

	private static double getAngleInRad(Vector2D vector1, Vector2D vector2) {
		return Math.atan2(vector2.y, vector2.x) - Math.atan2(vector1.y,
				vector1.x);
	}


	/*public static float getAngle(Vector2D refPoint, Vector2D vector1, Vector2D vector2) {
		Vector2D cp1 = new Vector2D(vector1.x - refPoint.x , (vector1.y - refPoint.y ));
		Vector2D cp2 = new Vector2D(vector2.x - refPoint.x , (vector2.y - refPoint.y ));
		double degree = getAngle(cp1, cp2);
		Log.i("SingleTouchListener","Computed Angle = "+degree+"");
		return (float) (degree);
	}*/


	public static float getAngle(Vector2D refPoint, Vector2D vector1, Vector2D vector2) {
		Vector2D cp1 = new Vector2D(vector1.x - refPoint.x , -(vector1.y - refPoint.y ));
		Vector2D cp2 = new Vector2D(vector2.x - refPoint.x , -(vector2.y - refPoint.y ));
		double inRads = getAngleInRad(cp1, cp2);

		// We need to map to coord system when 0 degree is at 3 O'clock, 270 at 12 O'clock
		if (inRads < 0)
			inRads = Math.abs(inRads);
		else
			inRads = 2*Math.PI - inRads;

		return (float)Math.toDegrees(inRads);
	}

	public static float getAngleV3(Vector2D c, Vector2D p1, Vector2D p2) {
		//double

		//arcos((P122 + P132 - P232) / (2 * P12 * P13));
		//sqrt((P1x - P2x)2 + (P1y - P2y)2);
		double angle = (Math.pow(distance(c,p1),2) + Math.pow(distance(c,p2),2) - Math.pow(distance(p1,p2),2))/
				(2*distance(c,p1)*distance(c,p2));
		double degree1 = Math.atan2(p1.y - c.y, p1.x - c.x);
		double degree2 = Math.atan2(p2.y - c.y, p2.x - c.x);
		//double degree = Math.toDegrees(degree2 - degree1);
		double degree = Math.toDegrees(Math.acos(angle));
		Log.i("SingleTouch","Computed Angle = "+degree+"");
		return (float) (degree);
	}

	static double lastComAngle = 0;
	public static float getAngleV2(Vector2D c, Vector2D p1, Vector2D p2) {
		float fz = (((p1.x - c.x) * (p2.x - c.x)) + ((p1.y - c.y) * (p2.y - c.y)));
		float fm = getDistance(c, p1) * getDistance(c, p2);
		double comAngle = (180 * Math.acos(fz / fm) / 3.14);
		if (Double.isNaN(comAngle)) {
			comAngle = (lastComAngle < 90 || lastComAngle > 270) ? 0 : 180;
		} else if ((p2.y - c.y) * (p1.x - c.x) < (p1.y - c.y) * (p2.x - c.x)) {
			comAngle = 360 - comAngle;
		}
		lastComAngle = comAngle;

		//float angle = (float) (this.getRotation() + comAngle);
		//angle = angle % 360;
		return (float)comAngle;
	}

	public static float getScale(Vector2D center, Vector2D p1, Vector2D p2) {
		float distanceCP1 = getDistance(center, p1);
		float distanceCP2 = getDistance(center, p2);
		float scale = distanceCP2 / distanceCP1;
		return scale;
	}

	public static float getDistance(Vector2D a, Vector2D b) {
		float v = ((a.x - b.x) * (a.x - b.x)) + ((a.y - b.y) * (a.y - b.y));
		return ((int) (Math.sqrt(v) * 100)) / 100f;
	}

	public static double distance(Vector2D a, Vector2D b) {
		float v = ((a.x - b.x) * (a.x - b.x)) + ((a.y - b.y) * (a.y - b.y));
		return ((int) (Math.sqrt(v) * 100)) / 100f;
	}

	public void normalize() {
		float length = (float) Math.sqrt(x * x + y * y);
		x /= length;
		y /= length;
	}
}