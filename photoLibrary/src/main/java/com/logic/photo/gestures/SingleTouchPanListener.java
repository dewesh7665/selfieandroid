package com.logic.photo.gestures;

import android.graphics.PointF;
import android.view.View;

/**
 * @author Dewesh Kumar
 */
public class SingleTouchPanListener extends SingleTouchListener {
    public SingleTouchPanListener(OnTouchChanged onTouchChanged) {
        super(onTouchChanged);
    }

    @Override
    protected void adjustTranslation(View view, float deltaX, float deltaY, PointF prevPoint, PointF currentPoint) {
        super.adjustTranslation(view, deltaX, deltaY, prevPoint, currentPoint);
        if (isValidTouchMove) {
            view.setTranslationX(view.getTranslationX() + deltaVector[0]);
            view.setTranslationY(view.getTranslationY() + deltaVector[1]);
        }
    }
}
