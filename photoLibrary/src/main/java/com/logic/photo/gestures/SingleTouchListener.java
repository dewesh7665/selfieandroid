package com.logic.photo.gestures;

import android.graphics.PointF;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

/**
 * Detects gesture of canvas.
 *
 */
public class SingleTouchListener implements OnTouchListener {
	private final Boolean DBG = true;
	private final String DBG_TAG = "SingleTouchListener";
	protected float[] deltaVector = new float[2];
	public interface OnTouchChanged {
		void onTouchChanged(TouchTransformInfo touchTransInfo);
	}

	private static final int INVALID_POINTER_ID = -1;
	private int mActivePointerId = INVALID_POINTER_ID;
	private float mPrevX;
	private float mPrevY;
	private OnTouchChanged onTouchChanged;
	protected Boolean isValidTouchMove = true; //Touch change greater then 5 px or so

	public SingleTouchListener(OnTouchChanged onTouchChanged) {
		this.onTouchChanged = onTouchChanged;
	}

	public void setOnTouchChangeListener(OnTouchChanged onTouchChanged) {
		this.onTouchChanged = onTouchChanged;
	}

	@Override
	public boolean onTouch(View view, MotionEvent event) {
		int action = event.getAction();
		switch (action & event.getActionMasked()) {
			case MotionEvent.ACTION_DOWN: {
				mPrevX = event.getRawX();
				mPrevY = event.getRawY();

				// Save the ID of this pointer.
				mActivePointerId = event.getPointerId(0);
				break;
			}

			case MotionEvent.ACTION_MOVE: {
				// Find the index of the active pointer and fetch its position.
				int pointerIndex = event.findPointerIndex(mActivePointerId);
				if (pointerIndex != -1) {
					float currX = event.getX(pointerIndex);
					float currY = event.getY(pointerIndex);
					//adjustTranslation(view, currX - mPrevX, currY - mPrevY, new PointF(currX, currY));
					PointF prevPoint = new PointF(mPrevX, mPrevY);
					adjustTranslation(view, event.getRawX() - mPrevX, event.getRawY() - mPrevY, prevPoint, new PointF(event.getRawX(), event.getRawY()));
					mPrevX = event.getRawX();
					mPrevY = event.getRawY();
				}

				break;
			}

			case MotionEvent.ACTION_CANCEL:
				mActivePointerId = INVALID_POINTER_ID;
				break;

			case MotionEvent.ACTION_UP:
				mActivePointerId = INVALID_POINTER_ID;
				break;

			case MotionEvent.ACTION_POINTER_UP: {
				// Extract the index of the pointer that left the touch sensor.
				int pointerIndex = (action & MotionEvent.ACTION_POINTER_INDEX_MASK) >> MotionEvent.ACTION_POINTER_INDEX_SHIFT;
				int pointerId = event.getPointerId(pointerIndex);
				if (pointerId == mActivePointerId) {
					// This was our active pointer going up. Choose a new
					// active pointer and adjust accordingly.
					int newPointerIndex = pointerIndex == 0 ? 1 : 0;
					mPrevX = event.getRawX();
					mPrevY = event.getRawY();
					mActivePointerId = event.getPointerId(newPointerIndex);
				}

				break;
			}
		}

		return true;
	}

	private static float adjustAngle(float degrees) {
		if (degrees > 180.0f) {
			degrees -= 360.0f;
		} else if (degrees < -180.0f) {
			degrees += 360.0f;
		}

		return degrees;
	}


	protected void adjustTranslation(View view, float deltaX, float deltaY,PointF prevPoint,  PointF currentPoint) {

		/*
		 * float[] deltaVector = {deltaX, deltaY};
		 * view.getMatrix().mapVectors(deltaVector);
		 * view.setTranslationX(view.getTranslationX() + deltaVector[0]);
		 * view.setTranslationY(view.getTranslationY() + deltaVector[1]);
		 */

		/*if (Math.abs(deltaX) < 1 && Math.abs(deltaY) < 1) {
			isValidTouchMove = false;
			return;
		}*/
		isValidTouchMove = true;
		deltaVector[0] = deltaX;
		deltaVector[1] = deltaY;
		view.getMatrix().mapVectors(deltaVector);

		//view.setTranslationX(view.getTranslationX() + deltaVector[0]);
		//view.setTranslationY(view.getTranslationY() + deltaVector[1]);

		TouchTransformInfo transInfo = new TouchTransformInfo();
		transInfo.deltaX = view.getTranslationX() + deltaVector[0];
		transInfo.deltaY = view.getTranslationY() + deltaVector[1];
		transInfo.currentPoint = currentPoint;
		transInfo.prevPoint = prevPoint;
		//transInfo.currentPoint = new PointF(transInfo.deltaX, transInfo.deltaY);
		//transInfo.prevPoint = new PointF(view.getTranslationX(),  view.getTranslationY());
		onTouchChanged.onTouchChanged(transInfo);
	}

	public static class TouchTransformInfo {
		public float deltaX;
		public float deltaY;
		public PointF prevPoint;
		public PointF currentPoint;
	}
}
