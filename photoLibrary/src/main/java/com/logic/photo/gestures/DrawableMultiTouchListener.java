package com.logic.photo.gestures;

import android.view.MotionEvent;
import android.view.View;

public class DrawableMultiTouchListener extends MultiTouchListener {
	public enum TouchMode{DRAW_MODE,ZOOM_TRANSLATE_MODE};
	private TouchMode mSelectedMode = TouchMode.DRAW_MODE;
	
	public TouchMode getSelectedMode() {
		return mSelectedMode;
	}

	public void setSelectedMode(TouchMode mSelectedMode) {
		this.mSelectedMode = mSelectedMode;
	}

	@Override
	public boolean onTouch(View view, MotionEvent event) {
		// TODO Auto-generated method stub
		switch (mSelectedMode) {
		case DRAW_MODE:
			view.onTouchEvent(event);
			break;
		case ZOOM_TRANSLATE_MODE:
			 super.onTouch(view, event);
			 break;
		default:
			break;
		}
		return true;
	}
}
