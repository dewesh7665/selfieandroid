package com.logic.photo.algorithms;

import com.logic.photo.filters.BaseObjectFilter;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class FocusFilter extends BaseObjectFilter{
	
	
	public FocusFilter() {
		// TODO Auto-generated constructor stub
		super();
		mBlurIntervalArray = new int[]{5,10,15,20,25,30,35,40,45};
		mBlurRadius = 20;
		mEdgeRadius = 50;
	}
	
	
	@Override
	public void applyFilter(Boolean isReverseMode) {
		// TODO Auto-generated method stub
		super.applyFilter(isReverseMode);
		Imgproc.GaussianBlur(mOriginalMat, mProcessedMat, new Size(2*mBlurRadius+1, 2*mBlurRadius+1), 0);
		//Apply mask where the filter is not to be applied
		if(isReverseMode){
			Mat reverseMaskMat = new Mat();
			reverseMaskMat.create(mMaskMat.size(), CvType.CV_8UC1);
			Core.bitwise_not(mMaskMat, reverseMaskMat);
			mOriginalMat.copyTo(mProcessedMat, reverseMaskMat);
			reverseMaskMat.release();
		}
		else{
			if(!mMaskMat.empty()){
				mOriginalMat.copyTo(mProcessedMat, mMaskMat);
			}
		}
	}
	
}
