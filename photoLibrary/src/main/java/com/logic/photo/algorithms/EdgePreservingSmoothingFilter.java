package com.logic.photo.algorithms;

import org.opencv.android.Utils;
import org.opencv.imgproc.Imgproc;

public class EdgePreservingSmoothingFilter extends ImageContourMaskFilter{
	
	public EdgePreservingSmoothingFilter(){
		super();
	}
	
	/*@Override
	public void getContourMaskResult(java.util.ArrayList<android.graphics.PointF> contourPoints, int radius) {
		//check if foreground mat is empty then first run prior edge filter
		if(mForegroundMat.empty())
			runFilterPrioriOnImage(false);
		//Now invoke masking method in super class
		super.getContourMaskResult(contourPoints, radius);
		Utils.matToBitmap(mForegroundMat, mForegroundBitmap);
		if(mImageFilterCompleteListener != null)
			mImageFilterCompleteListener.OnImageFilterComplete(mForegroundBitmap);
	};*/

	
	@Override
	public void applyFilter(){
		//Imgproc.medianBlur(mRGBImageMat, mSmoothMat, 11);
		//processFilter();
		getCurrentContourMaskResult();
		Utils.matToBitmap(mForegroundMat, mForegroundBitmap);
		if(mImageFilterCompleteListener != null)
			mImageFilterCompleteListener.OnImageFilterComplete(mForegroundBitmap);
	}

	@Override
	protected void processFilter() {
		// TODO Auto-generated method stub
		Imgproc.bilateralFilter(mRGBImageMat, mProcessedMat, 15, 80, 80);
		mProcessedMat.copyTo(mForegroundMat);
	}

	@Override
	public void undoFilter() {
		// TODO Auto-generated method stub
		getContourUndoMaskResult();
		Utils.matToBitmap(mForegroundMat, mForegroundBitmap);
		if(mImageFilterCompleteListener != null)
			mImageFilterCompleteListener.OnImageFilterComplete(mForegroundBitmap);
	}
}
