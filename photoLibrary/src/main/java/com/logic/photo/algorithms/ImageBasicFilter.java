package com.logic.photo.algorithms;

import android.graphics.Bitmap;
import android.graphics.PointF;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

public abstract class ImageBasicFilter {
	public enum ColorSpace {RGB_COLORSPACE,HSV_COLORSPACE,YCRCB_COLORSPACE};
	public enum UndoMode {FILTER_UNDO,ERASE_UNDO,PAINT_UNDO}
	protected int mColorCode;
	protected Mat mImageMat;
	protected Mat mRGBImageMat;
	protected Mat mColorSpaceMat;
	protected Mat mOriginalMat;
	protected Mat mAccumulatedMaskMat;
	//This is final processed mat
	protected Mat mProcessedMat;
	//Foreground outputs
	protected Bitmap mForegroundBitmap;
	protected Mat mForegroundMat;
	protected ArrayList<Point> mContourPoints;
	protected ArrayList<ArrayList<Point>> mArrayOfContourPoints;
	protected ColorSpace mColorSpace;
	private Scalar meanColor;
	
	private Mat maskColorMat;
	
	protected Mat mProcessedMaskMat;
	protected ArrayList<UndoMode> mUndoModes = new ArrayList<ImageBasicFilter.UndoMode>();
	
	
	public ImageBasicFilter(){
		mImageMat = new Mat();
		mRGBImageMat = new Mat();
		mProcessedMat = new Mat();
		mForegroundMat = new Mat();
		mColorSpaceMat = new Mat();
		mColorCode = Imgproc.COLOR_RGBA2RGB;
		mForegroundBitmap = null;
		mContourPoints = new ArrayList<Point>();
		mColorSpace = ColorSpace.RGB_COLORSPACE;
		//Mask Mats
		maskColorMat = new Mat();
		mAccumulatedMaskMat = new Mat();
		mArrayOfContourPoints = new ArrayList<ArrayList<Point>>();
		mOriginalMat = new Mat();
		mProcessedMaskMat = new Mat();
	}
	
	public interface ImageFilterCompleteListener{
		public void OnImageFilterComplete(Bitmap bmp);
	}
	protected ImageFilterCompleteListener mImageFilterCompleteListener;
	
	public void setImageFilterListener(ImageFilterCompleteListener listener){
		this.mImageFilterCompleteListener = listener;
	}
	
	
	public void setImageMatFromBitmap(Bitmap bitmap,ColorSpace colorSpace){
		Utils.bitmapToMat(bitmap, mImageMat);
		mColorSpace = colorSpace;
		Imgproc.cvtColor(mImageMat, mRGBImageMat, Imgproc.COLOR_RGBA2RGB);
		if(colorSpace == colorSpace.RGB_COLORSPACE)
			mRGBImageMat.copyTo(mColorSpaceMat);
		if(colorSpace == ColorSpace.HSV_COLORSPACE)
			Imgproc.cvtColor(mRGBImageMat, mColorSpaceMat, Imgproc.COLOR_RGB2HSV);	
		else if(colorSpace == ColorSpace.YCRCB_COLORSPACE)
			Imgproc.cvtColor(mRGBImageMat, mColorSpaceMat, Imgproc.COLOR_RGB2YCrCb);
		
		if(mOriginalMat.empty())
			mRGBImageMat.copyTo(mOriginalMat);
		//Foreground outputs.. which will eventually go
		if(mForegroundBitmap == null)
			mForegroundBitmap = Bitmap.createBitmap(bitmap);
		if(mForegroundMat.empty())
			mForegroundMat.create(new Size(mImageMat.width(), mImageMat.height()), CvType.CV_8UC3);
	}
	
	public void setOriginalMat(Mat originalMat){
		mOriginalMat = originalMat;
		//set separate foreground mats for each of these which we will remove eventually
		//if(mForegroundMat.empty())
			//mForegroundMat.create(new Size(originalMat.width(), originalMat.height()), CvType.CV_8UC3);
		if(mProcessedMaskMat.empty())
			mProcessedMaskMat.create(new Size(originalMat.width(), originalMat.height()), CvType.CV_8UC1);
	}
	
	public void setRGBImageMat(Mat rgbMat){
		mRGBImageMat = rgbMat;
	}
	
	public void setColorSpaceMat(Mat colorSpaceMat){
		mColorSpaceMat = colorSpaceMat;
	}
	
	public Mat getForegroundMaskMat(){
		return mForegroundMat;
	}
	
	public Mat getProcessedMaskMat(){
		return mProcessedMaskMat;
	}
	
	
	//Code to do filter processing should go here in 
	protected void processFilter(){
		
	}
	
	protected ArrayList<Point> getUndoContourMask(){
		if(mArrayOfContourPoints.size() == 0)
			return null;
		ArrayList<Point> currentContourPoints = new ArrayList<Point>();
		for(int i=0;i< mArrayOfContourPoints.size()-1;i++){
			for(Point point:mArrayOfContourPoints.get(i)){
				currentContourPoints.add(point);
			}
		}
		int lastIndex = mArrayOfContourPoints.size()-1;
		mArrayOfContourPoints.remove(lastIndex);
		return currentContourPoints;
	}
	
	
	//call this if you know the mask radius and want to get the response immediately
	public void setContourPoints(ArrayList<PointF> contourPoints){
		mContourPoints.clear();
		ArrayList<Point> allPoints = new ArrayList<Point>();
		for(PointF point:contourPoints)
		{
			mContourPoints.add(new Point(point.x, point.y));
			allPoints.add(new Point(point.x, point.y));
		}
		mArrayOfContourPoints.add(allPoints);
		
	}
	
	
	
	public void runFilterPrioriOnImage(Boolean isMultiPassAllowed){
		if(isMultiPassAllowed)
			processFilter();
		else if(mProcessedMat.empty())
			processFilter();
	}
	
	//This is assuming we have processed mat in place
	protected void populateColorMaskMatOld(){
		if(!mAccumulatedMaskMat.empty()){
			mRGBImageMat.copyTo(maskColorMat,mProcessedMat);
			//Get mean value of accumulated mask
			meanColor = Core.mean(mOriginalMat, mAccumulatedMaskMat);
			//Keep a temp mask mat for intermediate effects
			Mat tempMaskMat = new Mat();
			mRGBImageMat.copyTo(tempMaskMat, mProcessedMat);
			Core.subtract(mRGBImageMat, tempMaskMat, mForegroundMat);
			//Scalar meanColor = Core.mean(mRGBImageMat, mProcessedMat);
			Core.multiply(tempMaskMat, new Scalar(1.0,0.3, 0.5), tempMaskMat);
			Core.add(tempMaskMat, mForegroundMat, mForegroundMat);
			tempMaskMat.release();
		}
	}
	
	protected void populateColorMaskMat(){
		if(!mAccumulatedMaskMat.empty()){
			//Copy accumulated mask mat to processedmat
			mProcessedMaskMat.setTo(new Scalar(0));
			mAccumulatedMaskMat.copyTo(mProcessedMaskMat);
			
		}
	}
	
	
	protected void populateUndoMaskMatOld(){
		if(!mAccumulatedMaskMat.empty()){
			mOriginalMat.copyTo(maskColorMat,mProcessedMat);
			//Get mean value of accumulated mask
			//meanColor = Core.mean(mOriginalMat, mAccumulatedMaskMat);
			//Keep a temp mask mat for intermediate effects
			Mat tempMaskMat = new Mat();
			mOriginalMat.copyTo(tempMaskMat, mProcessedMat);
			Core.subtract(mOriginalMat, tempMaskMat, mForegroundMat);
			//Scalar meanColor = Core.mean(mRGBImageMat, mProcessedMat);
			Core.multiply(tempMaskMat, new Scalar(1.0,0.3, 0.5), tempMaskMat);
			Core.add(tempMaskMat, mForegroundMat, mForegroundMat);
			tempMaskMat.release();
		}
	}
	
	protected void populateUndoMaskMat(){
		if(!mAccumulatedMaskMat.empty()){
			//Copy accumulated mask mat to processedmat
			mProcessedMaskMat.setTo(new Scalar(0));
			mAccumulatedMaskMat.copyTo(mProcessedMaskMat);	
		}
	}
	
	protected void paintOriginalMatOld(Mat accumulatedMaskMat){
		mOriginalMat.copyTo(maskColorMat,accumulatedMaskMat);
		//Get mean value of accumulated mask
		//meanColor = Core.mean(mOriginalMat, mAccumulatedMaskMat);
		//Keep a temp mask mat for intermediate effects
		Mat tempMaskMat = new Mat();
		mOriginalMat.copyTo(tempMaskMat, accumulatedMaskMat);
		Core.subtract(mOriginalMat, tempMaskMat, mForegroundMat);
		//Scalar meanColor = Core.mean(mRGBImageMat, mProcessedMat);
		Core.multiply(tempMaskMat, new Scalar(1.0,0.3, 0.5), tempMaskMat);
		Core.add(tempMaskMat, mForegroundMat, mForegroundMat);
		tempMaskMat.release();
	}
	
	protected void paintOriginalMat(Mat accumulatedMaskMat){
		mProcessedMaskMat.setTo(new Scalar(0));
		accumulatedMaskMat.copyTo(mProcessedMaskMat);
	}
	
	
	protected void populateClearMaskMatOld(){
		if(!mAccumulatedMaskMat.empty()){
			mOriginalMat.copyTo(maskColorMat,mAccumulatedMaskMat);
			//Get mean value of accumulated mask
			//meanColor = Core.mean(mOriginalMat, mAccumulatedMaskMat);
			//Keep a temp mask mat for intermediate effects
			Mat tempMaskMat = new Mat();
			mOriginalMat.copyTo(tempMaskMat, mAccumulatedMaskMat);
			Core.subtract(mOriginalMat, tempMaskMat, mForegroundMat);
			//Scalar meanColor = Core.mean(mRGBImageMat, mProcessedMat);
			Core.multiply(tempMaskMat, new Scalar(1.0,0.3, 0.5), tempMaskMat);
			Core.add(tempMaskMat, mForegroundMat, mForegroundMat);
			tempMaskMat.release();
		}
	}
	
	protected void populateClearMaskMat(){
		if(!mAccumulatedMaskMat.empty()){
			mProcessedMaskMat.setTo(new Scalar(0));
			mAccumulatedMaskMat.copyTo(mProcessedMaskMat);
		}
	}
	
	public void applyRGBColor(int red,int green,int blue){
		/*if(maskColorMat.empty()){
			maskColorMat = new Mat();
		}*/
		//It is assumed that we
		double rFactor = 1.0, gFactor = 1.0, bFactor = 1.0;
		if(meanColor.val[0]> 0)
			rFactor = red/meanColor.val[0];
		if(meanColor.val[1] > 0)
			gFactor = green/meanColor.val[1];
		if(meanColor.val[2]> 0)
			bFactor = blue/meanColor.val[2];
		Mat tempMaskMat = new Mat();
		Core.multiply(maskColorMat,new Scalar(rFactor, gFactor, bFactor), tempMaskMat);
		Core.subtract(mRGBImageMat, maskColorMat, mForegroundMat);
		Core.add(tempMaskMat, mForegroundMat, mForegroundMat);
		tempMaskMat.release();
		Utils.matToBitmap(mForegroundMat, mForegroundBitmap);
		if(mImageFilterCompleteListener != null)
			mImageFilterCompleteListener.OnImageFilterComplete(mForegroundBitmap);
	}
	
	public void applyFilter(){
		//If there is something specific write here otherwise just override this in derived classes
	}
	
	public abstract void undoFilter();
	
	
	
}
