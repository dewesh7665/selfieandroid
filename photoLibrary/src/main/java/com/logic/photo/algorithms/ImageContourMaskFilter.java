package com.logic.photo.algorithms;

import android.graphics.PointF;
import android.graphics.Rect;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;


public abstract class ImageContourMaskFilter extends ImageBasicFilter{
		private Mat mContourMaskMat;
		private int mCircleRadius;
		private Point mCenter;
		private org.opencv.core.Rect mRect;
		public enum ImageMaskMode{MASK_CIRCLE, MASK_RECT, MASK_CONTOUR};
		protected ImageMaskMode maskMode;
		
		public ImageContourMaskFilter(){
			super();
			mContourMaskMat = new Mat();
			mCircleRadius = 15;
		}
		
		public void setRadius(int radius){
			mCircleRadius = radius;
		}
		
		protected void getCurrentContourMaskResult(){
			getContourMaskResult(mContourPoints,false);
		}
		
		protected void getContourUndoMaskResult(){
			ArrayList<Point> contourPoints = getUndoContourMask();
			if(contourPoints != null)
				getContourMaskResult(contourPoints,true);
		}
		
		private void getContourMaskResult(ArrayList<Point> contourPoints,Boolean isUndoMode){
			//check if foreground mat is empty then first run prior edge filter
			if(mForegroundMat.empty())
				runFilterPrioriOnImage(false);
			
			
			if(mContourMaskMat.empty())
				mContourMaskMat.create(mImageMat.size(), CvType.CV_8UC1);
			mContourMaskMat.setTo(new Scalar(0));
			for(Point point:contourPoints)
			{
				Imgproc.circle(mContourMaskMat, new org.opencv.core.Point(point.x, point.y), mCircleRadius, new Scalar(255), -1);
			}
			
			mProcessedMat.copyTo(mForegroundMat);
			//Core.su
			Core.bitwise_not(mContourMaskMat, mContourMaskMat);
			//Core.invert(mContourMaskMat, mContourMaskMat);
			//Now filter processed mat based on that
			if(isUndoMode)
				mOriginalMat.copyTo(mForegroundMat, mContourMaskMat);
			else
				mColorSpaceMat.copyTo(mForegroundMat, mContourMaskMat);
			//Output the processed bitmap
			/*Utils.matToBitmap(mForegroundMat, mForegroundBitmap);
			if(mImageFilterCompleteListener != null)
				mImageFilterCompleteListener.OnImageFilterComplete(mForegroundBitmap);*/
			
		}
		
		
		public void getCircleMaskResult(PointF center, int radius){
			maskMode = ImageMaskMode.MASK_CIRCLE;
			mCircleRadius = radius;
			mCenter = new Point(center.x, center.y);
			//get immediate result
		}
		
		public void getMaskRect(Rect maskRect){
			maskMode = ImageMaskMode.MASK_RECT;
			mRect = new org.opencv.core.Rect(maskRect.left, maskRect.top, maskRect.right - maskRect.left, maskRect.bottom-maskRect.top);//new RectF(maskRect.left, maskRect.top, maskRect.right, maskRect.bottom);
		}
}
