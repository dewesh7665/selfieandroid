package com.logic.photo.algorithms;

import android.graphics.Bitmap;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.photo.Photo;

public class ImageInPaint {
	//Declare a public interafce to notify on grab cut completion with extracted bitmap
		public interface InpaintCompleteListener{
			public void OnInpaintComplete(Bitmap bmp);
		}
		
		//Variables for grabcut separation
		private Mat mImageMat;	
		//Set mat without transparency
		private Mat mRGBImageMat;
		private Mat mRGBMaskImageMat;
		private Mat mResultMat;
		private Mat mResultRGBAMat;
		private Mat mScaledResultMat;
		//Final copy of bitmap
		private Bitmap mForegroundBitmap;
		private Mat mImageMask;
		private InpaintCompleteListener mInPaintCompleted;
		//Variables for operation on resized mode
		private int mScaleFactor = 2;
		private Mat mDownsampledMat;
		private int mNumberOfPasses = 1;
		public ImageInPaint(){
			mImageMat = new Mat();
			mRGBImageMat = new Mat();
			mImageMask = new Mat();
			mRGBMaskImageMat = new Mat();
			mResultMat = new Mat();
			mResultRGBAMat = new Mat();
			mScaledResultMat = new Mat();
			mDownsampledMat = new Mat();
		}
		
		public void setOnInpaintCompleteListener(InpaintCompleteListener inPaintCompleted){
			this.mInPaintCompleted = inPaintCompleted;
		}
		public void setImageMatFromBitmap(Bitmap bitmap) {
			Utils.bitmapToMat(bitmap, mImageMat);
			Imgproc.cvtColor(mImageMat, mRGBImageMat, Imgproc.COLOR_RGBA2RGB);
			//Also make a copy of the bitmap
			mForegroundBitmap = Bitmap.createBitmap(bitmap);
			
			mResultRGBAMat.create(mImageMat.size(), mImageMat.type());
			
			if(mScaleFactor == 1){
				mRGBImageMat.copyTo(mDownsampledMat);
			}
			else
				Imgproc.pyrDown(mRGBImageMat, mDownsampledMat, new Size(mRGBImageMat.cols()/mScaleFactor, mRGBImageMat.rows()/mScaleFactor));
			
			if(mResultMat.empty())
				mResultMat.create(new Size(mImageMat.width()/mScaleFactor, mImageMat.height()/mScaleFactor),  mRGBImageMat.type());
			
			
			if(mImageMask.empty())
				mImageMask.create(new Size(mImageMat.width()/mScaleFactor, mImageMat.height()/mScaleFactor), CvType.CV_8UC1);
			
			//reset the mask data and all 
			mImageMask.setTo(new Scalar(0));
		}
		
		public void setMaskImage(Bitmap bitmap){
			//Update mImageMask and copy 
			Utils.bitmapToMat(bitmap, mRGBMaskImageMat);
			Mat rMat = new Mat();
			Core.extractChannel(mRGBMaskImageMat, rMat, 1);
			Imgproc.pyrDown(rMat, mImageMask, new Size(mRGBImageMat.cols()/mScaleFactor, mRGBImageMat.rows()/mScaleFactor));
		}
		
		public void runInPaint(){
			Photo.inpaint(mDownsampledMat, mImageMask, mResultMat, 5, Photo.INPAINT_NS);
			//Now can merge this with ImageMat and 
			if(mScaleFactor == 1)
			{
				Imgproc.cvtColor(mResultMat, mResultRGBAMat, Imgproc.COLOR_RGB2RGBA);
			}
			else{
				Imgproc.resize(mResultMat, mRGBImageMat,mRGBImageMat.size());
				Imgproc.cvtColor(mRGBImageMat, mResultRGBAMat, Imgproc.COLOR_RGB2RGBA);
			}
			
			Utils.matToBitmap(mResultRGBAMat, mForegroundBitmap);
			if(this.mInPaintCompleted != null)
				mInPaintCompleted.OnInpaintComplete(mForegroundBitmap);
		}
}
