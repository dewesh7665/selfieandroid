package com.logic.photo.algorithms;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Range;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.photo.Photo;

import java.util.ArrayList;

//It can either use inpainting algorithm or dilation algorithm
public class InPaintFilter extends ImageBasicFilter{
	private Rect mOriginalRect;
	private Rect mMaskRect;
	private Mat mRectMaskMat;
	private Mat mRectROIMat;
	private Range mRowRange;
	private Range mColRange;
	private int mMaskRadius;
	private ArrayList<Rect> mRects;
	
	private Boolean applyDilation = false;
	private Boolean applyInPaintingAlgorithm = false;
	public InPaintFilter() {
		// TODO Auto-generated constructor stub
		super();
		mRectMaskMat = new Mat();
		mRectROIMat = new Mat();
		applyInPaintingAlgorithm = true;
		mRects = new ArrayList<Rect>();
	}
	
	@Override
	protected void processFilter() {
		// TODO Auto-generated method stub
		//Imgproc.GaussianBlur(mRectROIMat, mRectROIMat, new Size(7, 7), 0);
		if(applyDilation){
			Imgproc.dilate(mRectROIMat, mRectROIMat,Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(mMaskRadius/2, mMaskRadius/2)));
			mColorSpaceMat.copyTo(mProcessedMat);
			mRectROIMat.copyTo(mProcessedMat.submat(mMaskRect));
		}
		else
			Photo.inpaint(mColorSpaceMat, mRectMaskMat, mProcessedMat, 7, Photo.INPAINT_TELEA);
	}
	
	public void setDilationMethod(){
		applyDilation = true;
	}
	
	public void setRectMask(Rect rect){
		mOriginalRect = rect;
		if(mRectMaskMat.empty()){
			mRectMaskMat.create(mRGBImageMat.size(), CvType.CV_8UC1);
			
		}
		mRectMaskMat.setTo(new Scalar(0));
		if(mProcessedMat.empty())
			mProcessedMat.create(mRGBImageMat.size(), mRGBImageMat.type());
		//mRectROIMat = mColorSpaceMat.submat(rowRange, colRange)
		//keep rect as 3/4th of original rect
		
		int centerX = rect.x + rect.width/2;
		int centerY = rect.y + rect.height/2;
		int newWidth = (int)(rect.width*3)/4;
		int newHeight = (int)(rect.height*3)/4;
		mMaskRadius = newWidth/2;
		mMaskRect = new Rect(centerX - mMaskRadius, centerY - mMaskRadius, newWidth, newHeight);
		mRects.add(new Rect(mMaskRect.x, mMaskRect.y, mMaskRect.width, mMaskRect.height));
		if(applyDilation)
			mColorSpaceMat.submat(mMaskRect).copyTo(mRectROIMat);
		else
			Imgproc.circle(mRectMaskMat, new org.opencv.core.Point(centerX, centerY), mMaskRadius, new Scalar(255), -1);
		//get immediate result
	}
	
	@Override
	public void applyFilter() {
		// TODO Auto-generated method stub
		super.applyFilter();
		processFilter();
		Utils.matToBitmap(mProcessedMat, mForegroundBitmap);
		if(mImageFilterCompleteListener != null)
			mImageFilterCompleteListener.OnImageFilterComplete(mForegroundBitmap);
	}

	@Override
	public void undoFilter() {
		// TODO Auto-generated method stub
		int size = mRects.size();
		if(size == 0)
			return;
		Rect currentRect = mRects.get(size-1);
		int centerX = currentRect.x + currentRect.width/2;
		int centerY = currentRect.y + currentRect.height/2;
		int radius = currentRect.width/2;
		mRectMaskMat.setTo(new Scalar(0));
		Imgproc.circle(mRectMaskMat, new org.opencv.core.Point(centerX, centerY), radius, new Scalar(255), -1);
		
		mOriginalMat.copyTo(mProcessedMat, mRectMaskMat);
		//mOriginalMat.cop
		Utils.matToBitmap(mProcessedMat, mForegroundBitmap);
		if(mImageFilterCompleteListener != null)
			mImageFilterCompleteListener.OnImageFilterComplete(mForegroundBitmap);
		
		mRects.remove(size-1);
	}
}
