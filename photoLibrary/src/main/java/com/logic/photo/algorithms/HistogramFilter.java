package com.logic.photo.algorithms;

import android.graphics.Bitmap;
import android.graphics.PointF;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.Point;
import org.opencv.core.Range;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.Arrays;

public class HistogramFilter extends ImageBasicFilter{

	//Masking related variables
	private int mCircleRadius = 2;
	private Mat mSingleChannelMaskMat;
	//Bins for HSV color space
	private int hbins = 30, sbins = 32;
	private int[] hbinSizes = new int[]{hbins,sbins};
	//Bins for YCrCb color space 
	private int crbins = 32, cbbins = 32;
	private int[] crcbbinsizes = new int[]{crbins,cbbins};
	//Bins for selected color space
	private MatOfInt mSelectedBinSizes;
	private MatOfInt mSelectedChannels;
	private MatOfFloat mRangesMat;
	//Histogram calculation variables
	private Mat mHistogramMat;
	private Mat mBackProjMat;
	//Matrices for flood fill segmentation
	private Mat mAggregatedMaskMat;
	private Mat mSubMaskMat;
	private Range mRowRange;
	private Range mColRange;
	private int mLowVal = 20;
	private int mHighVal = 20;
	private Boolean isFloodFillProcessingEnabled;
	private Boolean resetAllowedOnEachpass;
	
	public HistogramFilter() {
		// TODO Auto-generated constructor stub
		super();
		mHistogramMat = new Mat();
		mBackProjMat = new Mat();
		mAggregatedMaskMat = new Mat();
		mSubMaskMat = new Mat();
		mSingleChannelMaskMat = new Mat();
		isFloodFillProcessingEnabled = true;
		resetAllowedOnEachpass = false;
	}
	
	public void setChannels(int[] channels,int[] binSizes,float[] dataRanges){
		mSelectedChannels = new MatOfInt(channels);
		mSelectedBinSizes = new MatOfInt(binSizes);
		mRangesMat = new MatOfFloat(dataRanges);
		
	}
	
	public void setRangeValueForFilling(int lowerRange,int higherRange){
		mLowVal = lowerRange;
		mHighVal = higherRange;
	}
	
	public void resetFilterOnNewMask(Boolean alwaysReset){
		resetAllowedOnEachpass = alwaysReset;
	}
	@Override
	public void setContourPoints(java.util.ArrayList<android.graphics.PointF> contourPoints) {
		super.setContourPoints(contourPoints);
		//We can add points on mask image by drawing circles of radius 2 around the point.
		if(resetAllowedOnEachpass)
			mSingleChannelMaskMat.setTo(new Scalar(0));
		for(PointF point:contourPoints)
			Imgproc.circle(mSingleChannelMaskMat, new org.opencv.core.Point(point.x, point.y), mCircleRadius, new Scalar(255), -1);
	}
	
	@Override
	public void setImageMatFromBitmap(Bitmap bitmap,ColorSpace colorSpace){
		super.setImageMatFromBitmap(bitmap, colorSpace);
		//Create mAggregatedMaskMat. Set it to zero only in the beginning. Else this will keep accumulating
		//if(mAggregatedMaskMat.empty())
		mAggregatedMaskMat = Mat.zeros(mImageMat.rows()+2, mImageMat.cols()+2, CvType.CV_8UC1);	
		mRowRange = new Range(1, mAggregatedMaskMat.rows()-1);
		mColRange = new Range(1, mAggregatedMaskMat.cols()-1);
		if(mAccumulatedMaskMat.empty())
			mAccumulatedMaskMat = Mat.zeros(mImageMat.size(), CvType.CV_8UC1);
		if(mSingleChannelMaskMat.empty())
			mSingleChannelMaskMat.create(new Size(mImageMat.width(), mImageMat.height()), CvType.CV_8UC1);
		mRowRange = new Range(1, mAggregatedMaskMat.rows()-1);
		mColRange = new Range(1, mAggregatedMaskMat.cols()-1);
		
	}
	
	private void perfromFloodFillSegmentation(){
		Scalar threshScalar = Core.mean(mBackProjMat, mSingleChannelMaskMat);
		Mat threshMat = new Mat();
		Imgproc.threshold(mBackProjMat, threshMat, threshScalar.val[0]*3/4, 255, Imgproc.THRESH_BINARY);
		//Copy mBackProjMat into mAggregated Mask mat in mRowRange to mColRange
		//Mat tempMat = mAggregatedMaskMat.rowRange(mRowRange).colRange(mColRange);
		//threshMat.copyTo(tempMat);
		//	mBackProjMat.copyTo(mAggregatedMaskMat.submat(mRowRange,mColRange));
		for(Point point:mContourPoints){
			int newMaskVal = 255;
			int connectivity = 8;
			int flags = connectivity + (newMaskVal << 8 ) + Imgproc.FLOODFILL_FIXED_RANGE + Imgproc.FLOODFILL_MASK_ONLY;		
			
			Rect rect = null;
			Imgproc.floodFill(mBackProjMat, mAggregatedMaskMat, point, new Scalar(255),rect,new Scalar(mLowVal),new Scalar(mHighVal),flags);
		}
		mSubMaskMat = mAggregatedMaskMat.submat(mRowRange,mColRange);
		Core.bitwise_and(mSubMaskMat, threshMat , mSubMaskMat);
		//Set processed mat and accumulated mask mat
		Core.subtract(mSubMaskMat, mAccumulatedMaskMat, mProcessedMat);
		Core.add(mAccumulatedMaskMat, mSubMaskMat,mAccumulatedMaskMat);
		threshMat.release();
		
	}
	
	@Override
	protected void processFilter() {
		// TODO Auto-generated method stub
		//Now get histogram from mask image
		Imgproc.calcHist(Arrays.asList(mColorSpaceMat), mSelectedChannels, mSingleChannelMaskMat, mHistogramMat, mSelectedBinSizes, mRangesMat);
		Core.normalize(mHistogramMat,mHistogramMat,0,255,Core.NORM_MINMAX,-1,new Mat());
		//And then do the backprojection
		Imgproc.calcBackProject(Arrays.asList(mColorSpaceMat), mSelectedChannels, mHistogramMat, mBackProjMat, mRangesMat, 1);
		if(isFloodFillProcessingEnabled){
			perfromFloodFillSegmentation();
			/*mTransparentForegroundMat.copyTo(mForegroundMat,mSubMaskMat);
			//Core.add(mForegroundMat, new Scalar(20,20,20), mForegroundMat);
			Core.addWeighted(mImageMat,1.0f, mForegroundMat, 0.5f, 0.0f, mForegroundMat);
			Utils.matToBitmap(mForegroundMat, mForegroundBitmap);*/
		}
		else{
			//else just threshold with average of mask image and threshold on the basis of that
			Scalar threshScalar = Core.mean(mBackProjMat, mSingleChannelMaskMat);
			Imgproc.threshold(mBackProjMat, mBackProjMat, threshScalar.val[0], 255, Imgproc.THRESH_BINARY);
			/*Imgproc.cvtColor(mBackProjMat, mForegroundMat, Imgproc.COLOR_GRAY2RGB);
			Utils.matToBitmap(mForegroundMat, mForegroundBitmap);*/
		}
	}
	
	@Override 
	public void applyFilter(){
		super.applyFilter();
		processFilter();
		//Now we have processed mat we can do any post processing like increasing whiteness etc..
		populateColorMaskMat();
	}

	@Override
	public void undoFilter() {
		// TODO Auto-generated method stub
		
	}
}
