package com.logic.photo.algorithms;

import android.graphics.Bitmap;
import android.graphics.PointF;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Range;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

//Can apply floodfill in YCRCB or HSV plane or RGB plane 
public class FloodFillSegmentation {
	public enum ColorSpace {RGB_COLORSPACE,HSV_COLORSPACE,YCRCB_COLORSPACE};
	private int mColorCode;
	private Mat mImageMat;	
	//Set mat without transparency
	private Mat mRGBImageMat;
	private Mat mRGBMaskImageMat;
	private Mat mColorSpaceMat;
	private Mat mAggregatedMaskMat;
	private Mat mSubMaskMat;
	//Foreground outputs
	private Mat mTransparentForegroundMat;
	private Bitmap mForegroundBitmap;
	private Mat mForegroundMat;
	//Channel based floodfill
	private Mat mFirstChannelMat;
	private Mat mSecondChannelMat;
	private Mat mSecondChannelMaskMat;
	private Boolean isChannelBasedSegmentation;
	private int mFirstChannelIndex;
	private int mSecondChannelIndex;
	
	//Single channel segmentation
	private Boolean isSingleChannelSegmentation;
	
	public interface SegmentationCompleteListener{
		public void OnSegmentationComplete(Bitmap bmp);
	}
	private SegmentationCompleteListener mSegmentationCompleteListener;
	private ArrayList<PointF> mFgdPixels;
	private ArrayList<Point> mCenterPoints;
	private ColorSpace mSelectedColorSpace;
	//Flood fill variables
	//Everything depends on color space and threshold values
	private int mLowVal = 20;
	private int mHighVal = 20;
	private Scalar mNewVal;
	private Scalar mLowDiff;
	private Scalar mHighDiff;
	private Range mRowRange;
	private Range mColRange;
	
	public void setColorSpace(ColorSpace selectedColorSpace){
		mSelectedColorSpace = selectedColorSpace;
		if(mSelectedColorSpace == ColorSpace.HSV_COLORSPACE){
			mColorCode = Imgproc.COLOR_RGB2HSV;
		}
		else if(mSelectedColorSpace == ColorSpace.YCRCB_COLORSPACE){
			mColorCode = Imgproc.COLOR_RGB2YCrCb;
		}
		else{
			mColorCode = Imgproc.COLOR_RGBA2RGB;
		}
	}
	
	public void setRangeValue(int lowerRange,int higherRange){
		mLowVal = lowerRange;
		mHighVal = higherRange;
		mLowDiff = new Scalar(mLowVal, mLowVal, mLowVal);
		mHighDiff = new Scalar(mHighVal, mHighVal, mHighVal);
	}
	public void setFloodFillCompleteListner(SegmentationCompleteListener listener){
		this.mSegmentationCompleteListener = listener;
	}
	
	public FloodFillSegmentation(){
		mImageMat = new Mat();
		mRGBImageMat = new Mat();
		mColorSpaceMat = new Mat();
		mFgdPixels = new ArrayList<PointF>();
		//mColorCode = Imgproc.COLOR_RGB2HSV;
		mRGBMaskImageMat = new Mat();
		mAggregatedMaskMat = new Mat();
		//Set default color space to HSV
		setColorSpace(ColorSpace.RGB_COLORSPACE);
		//Set the variables
		mLowDiff = new Scalar(mLowVal, mLowVal, mLowVal);
		mHighDiff = new Scalar(mHighVal, mHighVal, mHighVal);
		mNewVal = new Scalar(255, 255, 255);
		mCenterPoints = new ArrayList<Point>();
		mSubMaskMat = new Mat();
		
		mTransparentForegroundMat = new Mat();
		mForegroundMat = new Mat();
		
		//Set channels info
		mFirstChannelMat = new Mat();
		mSecondChannelMat = new Mat();
		isChannelBasedSegmentation = false;
		mSecondChannelMaskMat = new Mat();
		
		//Single channel variables
		isSingleChannelSegmentation = false;
	}
	
	public void setImageMatFromBitmap(Bitmap bitmap){
		Utils.bitmapToMat(bitmap, mImageMat);
		//Foreground outputs.. which will eventually go
		mForegroundBitmap = Bitmap.createBitmap(bitmap);
		//Also set foregroundMat with transparent red color
		if(mTransparentForegroundMat.empty())
		{
			mTransparentForegroundMat.create(mImageMat.size(), CvType.CV_8UC4);
			mTransparentForegroundMat.setTo(new Scalar(new double[]{255,0,0,255}));
		}
		
		//Can do hsv segmentation here as well
		if(mColorCode != Imgproc.COLOR_RGBA2RGB)
		{
			Imgproc.cvtColor(mImageMat, mRGBImageMat, Imgproc.COLOR_RGBA2RGB);
			Imgproc.cvtColor(mRGBImageMat, mColorSpaceMat, mColorCode);
		}
		else{
			//Just need to have one color space transformation
			Imgproc.cvtColor(mImageMat, mColorSpaceMat,mColorCode);
		}
		//Create mAggregatedMaskMat
		mAggregatedMaskMat = Mat.zeros(mImageMat.rows()+2, mImageMat.cols()+2, CvType.CV_8UC1);
		
		mRowRange = new Range(1, mAggregatedMaskMat.rows()-1);
		mColRange = new Range(1, mAggregatedMaskMat.cols()-1);
		//Set channel mats if channel based segmentation is supported
		if(isChannelBasedSegmentation)
			populateChannelMats();
		if(isSingleChannelSegmentation)
			populateSingleChannelMat();
	}
	
	private void populateChannelMats(){
		Core.extractChannel(mColorSpaceMat, mFirstChannelMat, mFirstChannelIndex);
		Core.extractChannel(mColorSpaceMat, mSecondChannelMat, mSecondChannelIndex);
		//Also populate second channel mask mat
		mSecondChannelMaskMat = Mat.zeros(mImageMat.rows()+2, mImageMat.cols()+2, CvType.CV_8UC1);
		
	}
	
	private void populateSingleChannelMat(){
		Core.extractChannel(mColorSpaceMat, mFirstChannelMat, mFirstChannelIndex);
	}
	
	public void setSingleChannelMode(int channelIndex){
		isSingleChannelSegmentation = true;
		mFirstChannelIndex = channelIndex;
	}
	
	public void setChannels(int firstChannelIndex, int secondChannelIndex){
		isChannelBasedSegmentation = true;
		mFirstChannelIndex = firstChannelIndex;
		mSecondChannelIndex = secondChannelIndex;
	}
	
	public void addForegroundPixels(ArrayList<PointF> points){
		int index = 0;
		mCenterPoints.clear();
		for(PointF point:points)
		{
			mCenterPoints.add(new Point(point.x, point.y));
			//mFgdPixels.add(point);
		}
	}
	
	private void resetFloodFill(){
		//Reset mask mats
		mForegroundMat.release();
		mForegroundMat = new Mat();
		mAggregatedMaskMat.setTo(new Scalar(0));
		
		if(isChannelBasedSegmentation)
			mSecondChannelMaskMat.setTo(new Scalar(0));
		
	}
	public void applyFloodFill(){
		//Use the mask image to avoid overlap
		//We can go through each point in centerPoints and try to do floodfill based on the mask
		
		for(Point point:mCenterPoints){
			int newMaskVal = 255;
			int connectivity = 8;
			int flags = connectivity + (newMaskVal << 8 ) + Imgproc.FLOODFILL_FIXED_RANGE + Imgproc.FLOODFILL_MASK_ONLY;		
			
			Rect rect = null;
			if(isChannelBasedSegmentation)
			{
				Imgproc.floodFill(mFirstChannelMat, mAggregatedMaskMat, point, new Scalar(255),rect,new Scalar(mLowVal),new Scalar(mHighVal),flags);
				Imgproc.floodFill(mSecondChannelMat, mSecondChannelMaskMat,point,new Scalar(255),rect,new Scalar(mLowVal),new Scalar(mHighVal),flags);
			}
			else if(isSingleChannelSegmentation)
				Imgproc.floodFill(mFirstChannelMat, mAggregatedMaskMat, point, new Scalar(255),rect,new Scalar(mLowVal),new Scalar(mHighVal),flags);
			else
				Imgproc.floodFill( mColorSpaceMat, mAggregatedMaskMat, point,mNewVal, rect, mLowDiff, mHighDiff, flags);		
		}
		//Now get mask mat from extended mask mat
		if(isChannelBasedSegmentation)
			Core.bitwise_and(mAggregatedMaskMat, mSecondChannelMaskMat, mAggregatedMaskMat);
		mSubMaskMat = mAggregatedMaskMat.submat(mRowRange,mColRange);
		mTransparentForegroundMat.copyTo(mForegroundMat,mSubMaskMat);
		//Core.add(mForegroundMat, new Scalar(20,20,20), mForegroundMat);
		Core.addWeighted(mImageMat,1.0f, mForegroundMat, 0.5f, 0.0f, mForegroundMat);
		Utils.matToBitmap(mForegroundMat, mForegroundBitmap);
		if(this.mSegmentationCompleteListener != null)
			mSegmentationCompleteListener.OnSegmentationComplete(mForegroundBitmap);
		resetFloodFill();
	}
	
}
