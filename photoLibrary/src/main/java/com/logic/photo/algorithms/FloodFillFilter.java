package com.logic.photo.algorithms;

import android.graphics.Bitmap;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Range;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

public class FloodFillFilter extends ImageBasicFilter{

	private Mat mAggregatedMaskMat;
	
	//Channel based mats
	private Mat mFirstChannelMat;
	private Mat mSecondChannelMat;
	private Mat mSecondChannelMaskMat;
	private int mFirstChannelIndex;
	private int mSecondChannelIndex;
	//channel based segmentation
	private Boolean isChannelBasedSegmentation;
	private Boolean isSingleChannelSegmentation;
	
	//Everything depends on color space and threshold values
	private int mLowVal = 20;
	private int mHighVal = 20;
	private Scalar mNewVal;
	private Scalar mLowDiff;
	private Scalar mHighDiff;
	private Range mRowRange;
	private Range mColRange;
	
	public FloodFillFilter(){
		super();
		mAggregatedMaskMat = new Mat();
		
		mFirstChannelMat = new Mat();
		mSecondChannelMat = new Mat();
		mSecondChannelMaskMat = new Mat();
		isChannelBasedSegmentation = false;
		isSingleChannelSegmentation = false;
		//Set the variables
		mLowDiff = new Scalar(mLowVal, mLowVal, mLowVal);
		mHighDiff = new Scalar(mHighVal, mHighVal, mHighVal);
		mNewVal = new Scalar(255, 255, 255);
		
	}
	
	public void setRangeValue(int lowerRange,int higherRange){
		mLowVal = lowerRange;
		mHighVal = higherRange;
		mLowDiff = new Scalar(mLowVal, mLowVal, mLowVal);
		mHighDiff = new Scalar(mHighVal, mHighVal, mHighVal);
	}
	
	public void setSingleChannelMode(int channelIndex){
		isSingleChannelSegmentation = true;
		mFirstChannelIndex = channelIndex;
	}
	
	public void setChannels(int firstChannelIndex, int secondChannelIndex){
		isChannelBasedSegmentation = true;
		mFirstChannelIndex = firstChannelIndex;
		mSecondChannelIndex = secondChannelIndex;
	}
	
	private void populateChannelMats(){
		Core.extractChannel(mColorSpaceMat, mFirstChannelMat, mFirstChannelIndex);
		Core.extractChannel(mColorSpaceMat, mSecondChannelMat, mSecondChannelIndex);
		//Also populate second channel mask mat
		mSecondChannelMaskMat = Mat.zeros(mImageMat.rows()+2, mImageMat.cols()+2, CvType.CV_8UC1);
		
	}
	
	private void populateSingleChannelMat(){
		Core.extractChannel(mColorSpaceMat, mFirstChannelMat, mFirstChannelIndex);
	}
	
	
	
	@Override
	public void setImageMatFromBitmap(Bitmap bitmap,ColorSpace colorSpace){
		super.setImageMatFromBitmap(bitmap, colorSpace);
		//Create mAggregatedMaskMat. Set it to zero only in the beginning. Else this will keep accumulating
		//if(mAggregatedMaskMat.empty())
		mAggregatedMaskMat = Mat.zeros(mImageMat.rows()+2, mImageMat.cols()+2, CvType.CV_8UC1);	
		mRowRange = new Range(1, mAggregatedMaskMat.rows()-1);
		mColRange = new Range(1, mAggregatedMaskMat.cols()-1);
		if(mAccumulatedMaskMat.empty())
			mAccumulatedMaskMat = Mat.zeros(mImageMat.size(), CvType.CV_8UC1);
		//Set channel mats if channel based segmentation is supported
		if(isChannelBasedSegmentation)
			populateChannelMats();
		if(isSingleChannelSegmentation)
			populateSingleChannelMat();
		
	}
	
	
	
	private void process(ArrayList<Point> contourPoints) {
		// TODO Auto-generated method stub
		//Here apply the filter code
		for(Point point:contourPoints){
			int newMaskVal = 255;
			int connectivity = 8;
			int flags = connectivity + (newMaskVal << 8 ) + Imgproc.FLOODFILL_FIXED_RANGE + Imgproc.FLOODFILL_MASK_ONLY;		
			
			Rect rect = null;
			if(isChannelBasedSegmentation)
			{
				Imgproc.floodFill(mFirstChannelMat, mAggregatedMaskMat, point, new Scalar(255),rect,new Scalar(mLowVal),new Scalar(mHighVal),flags);
				Imgproc.floodFill(mSecondChannelMat, mSecondChannelMaskMat,point,new Scalar(255),rect,new Scalar(mLowVal),new Scalar(mHighVal),flags);
			}
			else if(isSingleChannelSegmentation)
				Imgproc.floodFill(mFirstChannelMat, mAggregatedMaskMat, point, new Scalar(255),rect,new Scalar(mLowVal),new Scalar(mHighVal),flags);
			else
				Imgproc.floodFill( mColorSpaceMat, mAggregatedMaskMat, point,mNewVal, rect, mLowDiff, mHighDiff, flags);		
		}
		//Now get mask mat from extended mask mat
		if(isChannelBasedSegmentation)
			Core.bitwise_and(mAggregatedMaskMat, mSecondChannelMaskMat, mAggregatedMaskMat);
		Mat subMat = mAggregatedMaskMat.submat(mRowRange, mColRange);
		Core.subtract(subMat, mAccumulatedMaskMat, mProcessedMat);
		Core.add(mAccumulatedMaskMat, subMat,mAccumulatedMaskMat);
	}
	
	private void processFloodFill(Boolean isUndoMode){
		if(isUndoMode){
			//Also clear some of previously populated mats
			mAggregatedMaskMat.setTo(new Scalar(0));
			mAccumulatedMaskMat.setTo(new Scalar(0));
			ArrayList<Point> contourPoints = getUndoContourMask();
			if(contourPoints != null)
				process(contourPoints);
		}
		else{
			process(mContourPoints);
		}
	}
	@Override 
	public void applyFilter(){
		super.applyFilter();
		processFloodFill(false);
		//Now we have processed mat we can do any post processing like increasing whiteness etc..
		populateColorMaskMat();
	}

	@Override
	public void undoFilter() {
		// TODO Auto-generated method stub
		processFloodFill(true);
		populateUndoMaskMat();
	}

}
