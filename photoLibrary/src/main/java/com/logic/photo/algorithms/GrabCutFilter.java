package com.logic.photo.algorithms;

import android.graphics.PointF;

import com.logic.photo.utils.PhotoUtils;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Range;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GrabCutFilter extends ImagePaintFilter {
	
	//Mask mats
	private Mat mBinaryMask;
	private Mat mImageMask;
	private Mat mBgdModel;
	private Mat mFgdModel;
	private ArrayList<Point> mFgdPixels, mBgdPixels,mPrFgdPixels,mPrBgdPixels;
	private int mIterCount;
	private int mRadius;
	//Scaling related variables
	private int mScaleFactor = 4;
	private float mScaleMultiplier;
	private Mat mDownsampledMat;
	private Boolean isFirstPass = true;
	private Boolean isBgMode = false;
	private Rect maskRect;
	private ArrayList<ArrayList<Point>> mArrayOfForegroundPoints;
	private ArrayList<ArrayList<Point>> mArrayOfBackgroundPoints;
	private ArrayList<Mat> mImageMaskMats;
    private Rect maxBoundingRect;
	//private Mat mAccumulatedPaintMaskMat;
	//Erase and paint masks
	//private ArrayList<Point> mTempErasePoints;
	//private ArrayList<Point> mTempPaintPoints;
	//private int mTempPointThresh = 2;
	
	public GrabCutFilter() {
		// TODO Auto-generated constructor stub
		super();
		//Image masking
		mImageMask = new Mat();
		mBinaryMask = new Mat();
		mFgdModel = new Mat(); 
		mBgdModel = new Mat() ;
		mFgdPixels = new ArrayList<Point>();
		mBgdPixels = new ArrayList<Point>();
		mPrFgdPixels = new ArrayList<Point>();
		mPrBgdPixels = new ArrayList<Point>();
		mIterCount = 1;
		//Set the value of all mats
		mFgdModel.setTo(new Scalar(255));
		mBgdModel.setTo(new Scalar(255));
		//This is circle radius
		mRadius = 2;
		mDownsampledMat = new Mat();
		mScaleMultiplier = 1.0f/mScaleFactor;
		maskRect = new Rect(0, 0, 0, 0);
		//Initialization of array
		mArrayOfForegroundPoints = new ArrayList<ArrayList<Point>>();
		//This is for undo operation
		mImageMaskMats = new ArrayList<Mat>();
		
		//Temp variables
		//mTempErasePoints = new ArrayList<Point>();
		//mTempPaintPoints = new ArrayList<Point>();
		
		//Accumulated paint mask mat
		//mAccumulatedPaintMaskMat = new Mat();
	}
	
	//Create binary mask based on imagemask 
	private void createBinaryMask(){
		if(mBinaryMask.empty()){
			mBinaryMask.create(mImageMask.size(), CvType.CV_8UC1);
		}	
		mBinaryMask.setTo(new Scalar(1.0));
		Core.bitwise_and(mImageMask, mBinaryMask,mBinaryMask);
			
	}
	
	private void resetData(){
    
	    mBgdPixels.clear(); mFgdPixels.clear();
	    mPrBgdPixels.clear(); mPrFgdPixels.clear();
	   
	   
	}
	
	public void setBackgroundPoints(ArrayList<PointF> contourPoints){
		isBgMode = true;
		for(PointF point:contourPoints)
		{
			mBgdPixels.add(new Point(point.x, point.y));
			//allPoints.add(new Point(point.x, point.y));
		}
		//mArrayOfForegroundPoints.add(allPoints);
		//Set mask mat as well here
		mUndoModes.add(UndoMode.FILTER_UNDO);
		resetPaintMaskMats();
		
	}
	
	private void resetPaintMaskMats(){
		if(!mAccumulatedPaintMaskMat.empty())
		{
			mAccumulatedPaintMaskMat.copyTo(mAccumulatedMaskMat);
			//Need to change image mask as well
			//First get paint points and set mask to foreground points
			ArrayList<ArrayList<Point>> allPaintedPoints = getAllPaintedPoints();
			ArrayList<ArrayList<Point>> allErasedPoints = getAllErasedPoints();
			//Now iterate through all of them and set image mask appropriately
			Point startPt = new Point(0, 0), endPt = new Point(0, 0);
			
			int paintRadius = getPaintRadius();
			int eraseRadius = getEraseRadius();
			
			for(ArrayList<Point> paintPoints: allPaintedPoints){
				for(int i=0;i< paintPoints.size();i++){
					if(i==0)
					{
						startPt = new Point(paintPoints.get(i).x*mScaleMultiplier,paintPoints.get(i).y*mScaleMultiplier);
						continue;
					}
					endPt = new Point(paintPoints.get(i).x*mScaleMultiplier,paintPoints.get(i).y*mScaleMultiplier);
					Imgproc.line(mImageMask, startPt, endPt, new Scalar(Imgproc.GC_PR_FGD), (int) (paintRadius*mScaleMultiplier));
					//Core.line(mProcessedMat, startPt, endPt, new Scalar(255), paintRadius);
					startPt = endPt;
				}
			}
			
			for(ArrayList<Point> erasePoints: allErasedPoints){
				for(int i=0;i< erasePoints.size();i++){
					if(i==0)
					{
						startPt = new Point(erasePoints.get(i).x*mScaleMultiplier,erasePoints.get(i).y*mScaleMultiplier);
						continue;
					}
					endPt = new Point(erasePoints.get(i).x*mScaleMultiplier,erasePoints.get(i).y*mScaleMultiplier);
					Imgproc.line(mImageMask, startPt, endPt, new Scalar(Imgproc.GC_PR_BGD), (int) (eraseRadius*mScaleMultiplier));
					startPt = endPt;
					
				}
			}
		}
	}
	
	public Point getCenterPoint(){
		Mat tempBinaryMat = new Mat();
		tempBinaryMat.create(mAccumulatedMaskMat.size(), mAccumulatedMaskMat.type());
		mAccumulatedMaskMat.copyTo(tempBinaryMat);
		Mat hierarchyMat = new Mat();
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        
		//First find largest bounding rect and then apply flood fill to find 
		Imgproc.findContours(tempBinaryMat, contours, hierarchyMat, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
		double maxArea = 0;
        Iterator<MatOfPoint> each = contours.iterator();
        Rect maxBoundingRect = new Rect(0, 0, 0, 0);
        while (each.hasNext()) {
            MatOfPoint wrapper = each.next();
            double area = Imgproc.contourArea(wrapper);
            if (area > maxArea)
            {
                maxArea = area;
                maxBoundingRect = Imgproc.boundingRect(wrapper);
            }
        }
        tempBinaryMat.release();
        hierarchyMat.release();
        return new Point(maxBoundingRect.x+maxBoundingRect.width/2, maxBoundingRect.y+maxBoundingRect.height/2);
	}
	
	public void setForegroundPoints(ArrayList<PointF> contourPoints){
		//ArrayList<Point> allPoints = new ArrayList<Point>();
		for(PointF point:contourPoints)
		{
			mFgdPixels.add(new Point(point.x, point.y));
			//allPoints.add(new Point(point.x, point.y));
		}
		
		//mArrayOfForegroundPoints.add(allPoints);
		//Set mask mat as well here
		mUndoModes.add(UndoMode.FILTER_UNDO);
		//Also reset accumulated mask mats and binary mask mats
		resetPaintMaskMats();
		
	}
	
	
	
	@Override
	public void setImageMatFromBitmap(android.graphics.Bitmap bitmap, ColorSpace colorSpace) {
		super.setImageMatFromBitmap(bitmap, ColorSpace.RGB_COLORSPACE);

		//reset the mask data and all 
		if(isFirstPass){
			PhotoUtils.downSampleOriginalMat(mRGBImageMat, mDownsampledMat, mScaleFactor);
			mImageMask.create(mDownsampledMat.size(), CvType.CV_8UC1);
			mImageMask.setTo(new Scalar(Imgproc.GC_PR_BGD));
			mAccumulatedMaskMat = Mat.zeros(mRGBImageMat.size(), CvType.CV_8UC1);
			mProcessedMat.setTo(new Scalar(0));
			isFirstPass = false;
		}
		resetData();
	}
	
	public Mat getAccumulatedMaskMat(){
		return mAccumulatedMaskMat;
	}
	
	public void updateMaskMat(Mat imageMaskMat){
		imageMaskMat.copyTo(mImageMask);
	}
	
	public void setMaskMatsAndDownSampledMat(Mat originalMat, Mat downsampledMat){
		mDownsampledMat = downsampledMat;
		mImageMask.create(mDownsampledMat.size(), CvType.CV_8UC1);
		mImageMask.setTo(new Scalar(Imgproc.GC_PR_BGD));
		mAccumulatedMaskMat = Mat.zeros(originalMat.size(), CvType.CV_8UC1);
		mProcessedMat.setTo(new Scalar(0));
		isFirstPass = false;
	}
	
	private void drawLabels(){
		//mask value 0
		for(Point point:mBgdPixels)
			Imgproc.circle(mImageMask, new org.opencv.core.Point(point.x*mScaleMultiplier, point.y*mScaleMultiplier), mRadius, new Scalar(Imgproc.GC_BGD), -1);
		//mask value 1
		for(Point point:mFgdPixels)
			Imgproc.circle(mImageMask, new org.opencv.core.Point(point.x*mScaleMultiplier, point.y*mScaleMultiplier), mRadius, new Scalar(Imgproc.GC_FGD), -1);
		//mask value 2
		for(Point point:mPrBgdPixels)
			Imgproc.circle(mImageMask, new org.opencv.core.Point(point.x*mScaleMultiplier, point.y*mScaleMultiplier), mRadius, new Scalar(Imgproc.GC_PR_BGD), -1);
		//mask value 3
		for(Point point:mPrFgdPixels)
			Imgproc.circle(mImageMask, new org.opencv.core.Point(point.x*mScaleMultiplier, point.y*mScaleMultiplier), mRadius, new Scalar(Imgproc.GC_PR_FGD), -1);
		
	}
	
	private void processFilter(Boolean isUndoMode) {
		// TODO Auto-generated method stub
		if(!isUndoMode){
			drawLabels();
			Imgproc.grabCut(mDownsampledMat, mImageMask, maskRect, mBgdModel, mFgdModel, mIterCount,Imgproc.GC_INIT_WITH_MASK);
			//Add in undo mats list
			Mat tempUndoMat = new Mat();
			mImageMask.copyTo(tempUndoMat);
			mImageMaskMats.add(tempUndoMat);			
		}
		createBinaryMask();
		//Now can merge this with ImageMat and 
		if(mScaleFactor == 1)
			mBinaryMask.copyTo(mProcessedMat);
		else
		{
			//Perform Pyramidal Up Sampling.. keep this for now. This is for 4 times sampling
			Mat tempMat = new Mat();
			Imgproc.pyrUp(mBinaryMask, tempMat);
			Mat doubletempMat = new Mat();
			Imgproc.pyrUp(tempMat, doubletempMat);
			//mProcessedMat.setTo(new Scalar(0));
			Range rowRange,colRange;
			Mat tempProcessedMat = new Mat();
			tempProcessedMat.create(mRGBImageMat.size(), CvType.CV_8UC1);
			if(doubletempMat.width() < tempProcessedMat.width() || doubletempMat.height() < tempProcessedMat.height()){
				rowRange = new Range(0, doubletempMat.rows());
				colRange = new Range(0, doubletempMat.cols());
				Mat subMat = tempProcessedMat.submat(rowRange, colRange);
				doubletempMat.copyTo(subMat);
			}
			else{
				rowRange = new Range(0, tempProcessedMat.rows());
				colRange = new Range(0, tempProcessedMat.cols());
				Mat subMat = doubletempMat.submat(rowRange, colRange);
				subMat.copyTo(tempProcessedMat);
			}
			
			
			
			
			//Mat tempProcessedMat = new Mat();
			//Imgproc.resize(mBinaryMask, tempProcessedMat,mImageMat.size());
			mProcessedMat.setTo(new Scalar(0));
			//Lets first dilate and then erode to smooth out the edges
			Imgproc.dilate(tempProcessedMat, tempProcessedMat, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3)));
			Imgproc.erode(tempProcessedMat, tempProcessedMat, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3)));
			if(isUndoMode){
				tempProcessedMat.copyTo(mProcessedMat);
				tempProcessedMat.copyTo(mAccumulatedMaskMat);
			}
			else if(isBgMode){
				tempProcessedMat.copyTo(mProcessedMat);
				tempProcessedMat.copyTo(mAccumulatedMaskMat);
				//Core.subtract(tempProcessedMat, mAccumulatedMaskMat, mProcessedMat);
				//Core.add(mAccumulatedMaskMat, tempProcessedMat,mAccumulatedMaskMat);
			}
			else{
				//As of now it just kept on adding stuff and it cann cause problems in case of background subtraction
				Core.subtract(tempProcessedMat, mAccumulatedMaskMat, mProcessedMat);
				Core.add(mAccumulatedMaskMat, tempProcessedMat,mAccumulatedMaskMat);
			}
			mAccumulatedMaskMat.copyTo(mAccumulatedPaintMaskMat);
			tempProcessedMat.release();
			//Imgproc.resize(mBinaryMask, mProcessedMat, mImageMat.size(), 0,0 , Imgproc.INTER_CUBIC);
			//Imgproc.GaussianBlur(mProcessedMat, mProcessedMat, new Size(7, 7), 0);
			
		}
		
		resetData();	
	}
	
	@Override
	public void applyFilter() {
		// TODO Auto-generated method stub
		super.applyFilter();
		processFilter(false);
		if(isBgMode)
		{
			populateUndoMaskMat();
			isBgMode = false;
		}
		else
			populateColorMaskMat();
	}
	
	private ArrayList<Point> getUndoForegoundPoints(){
		if(mArrayOfForegroundPoints.size() == 1)
			return null;
		ArrayList<Point> currentContourPoints = new ArrayList<Point>();
		for(int i=0;i< mArrayOfForegroundPoints.size()-1;i++){
			for(Point point:mArrayOfForegroundPoints.get(i)){
				currentContourPoints.add(point);
			}
		}
		int lastIndex = mArrayOfForegroundPoints.size()-1;
		mArrayOfForegroundPoints.remove(lastIndex);
		return currentContourPoints;
	}
	
	private Mat getUndoMaskMat(){
		if(mImageMaskMats.size() <= 1)
		{
			if(mImageMaskMats.size() == 1)
				mImageMaskMats.remove(0);
			//Also reset accumulated mask mats and binary mask
			mImageMask.setTo(new Scalar(Imgproc.GC_PR_BGD));
			mAccumulatedMaskMat.setTo(new Scalar(0));
			mAccumulatedPaintMaskMat.setTo(new Scalar(0));
			mProcessedMat.setTo(new Scalar(0));
			resetData();
			return null;
			
		}
		Mat tempMat = mImageMaskMats.get(mImageMaskMats.size()-2);
		mImageMaskMats.remove(mImageMaskMats.size()-2);
		return tempMat;
	}
	
	public boolean isUndoModeApplicable(){
		if(mUndoModes.size() > 0)
			return true;
		else
			return false;
	}
	
	@Override
	public void undoFilter() {
		// TODO Auto-generated method stub
		if(mUndoModes.size() == 0)
			return;
		
		int eraseRadius = getEraseRadius();
		int paintRadius = getPaintRadius();
		
		UndoMode undoMode = mUndoModes.get(mUndoModes.size()-1);
		Point startPt = new Point(0, 0), endPt = new Point(0, 0);
		switch (undoMode) {
		case FILTER_UNDO:
			resetData();
			Mat tempMat = getUndoMaskMat();
			mAccumulatedMaskMat.setTo(new Scalar(0));
			if(tempMat == null){
				//Can comment this later
				//mOriginalMat.copyTo(mForegroundMat);
				mProcessedMaskMat.setTo(new Scalar(0));
				mImageMask.setTo(new Scalar(Imgproc.GC_PR_BGD));
				mUndoModes.remove(mUndoModes.size()-1);
				return;
			}
			//mImageMask.copyTo(tempMat);
			tempMat.copyTo(mImageMask);
			processFilter(true);
			populateUndoMaskMat();
			break;
		
		case ERASE_UNDO:
			undoEraseMode();
			break;
			
		case PAINT_UNDO:
			undoPaintMode();
			break;
			
		default:
			break;
		}
		
		mUndoModes.remove(mUndoModes.size()-1);
		
	}
	
	public void updateProcessedMatWithLargestContour(){
		//Find largest contour in the foreground from accumulated mask mat
		Mat tempBinaryMat = new Mat();
		tempBinaryMat.create(mAccumulatedMaskMat.size(), mAccumulatedMaskMat.type());
		mAccumulatedMaskMat.copyTo(tempBinaryMat);
		Mat hierarchyMat = new Mat();
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        
		//First find largest bounding rect and then apply flood fill to find 
		Imgproc.findContours(tempBinaryMat, contours, hierarchyMat, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
		double maxArea = 0;
        Iterator<MatOfPoint> each = contours.iterator();
         maxBoundingRect = new Rect(0, 0, 0, 0);
        while (each.hasNext()) {
            MatOfPoint wrapper = each.next();
            double area = Imgproc.contourArea(wrapper);
            if (area > maxArea)
            {
                maxArea = area;
                maxBoundingRect = Imgproc.boundingRect(wrapper);
            }
        }
        
        tempBinaryMat.setTo(new Scalar(255), mAccumulatedMaskMat);
        //Now we have max bounding rect... grow this using floodfill
        Mat aggregatedMaskMat = new Mat();
        aggregatedMaskMat = Mat.zeros(mAccumulatedMaskMat.rows()+2, mAccumulatedMaskMat.cols()+2, CvType.CV_8UC1);	
		Range rowRange = new Range(1, aggregatedMaskMat.rows()-1);
		Range colRange = new Range(1, aggregatedMaskMat.cols()-1);
        
		Point centerPoint = new Point(maxBoundingRect.x+maxBoundingRect.width/2, maxBoundingRect.y+maxBoundingRect.height/2);
		int newMaskVal = 255;
		int connectivity = 8;
		int flags = connectivity + (newMaskVal << 8 ) + Imgproc.FLOODFILL_FIXED_RANGE + Imgproc.FLOODFILL_MASK_ONLY;		
		Imgproc.floodFill(tempBinaryMat, aggregatedMaskMat, centerPoint, new Scalar(255),new Rect(),new Scalar(20),new Scalar(20),flags);
		
		Mat subMat = aggregatedMaskMat.submat(rowRange, colRange);
		subMat.copyTo(mAccumulatedMaskMat);
		populateColorMaskMat();
	
		tempBinaryMat.release();
		aggregatedMaskMat.release();
		
	}

   public Rect getBoundingRect() {
       if (maxBoundingRect.width == 0 || maxBoundingRect.height == 0)
           updateProcessedMatWithLargestContour();
       return maxBoundingRect;
   }
	
}
