package com.logic.photo.algorithms;

import android.graphics.Bitmap;
import android.graphics.PointF;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;



public class GrabCut {
	
	//Declare a public interafce to notify on grab cut completion with extracted bitmap
	public interface GrabCutCompleteListener{
		public void OnGrabCutComplete(Bitmap bmp);
	}
	
	public enum GrabCutMode {OPEN_MASK_MODE,RECT_MODE,CLOSED_MASK_MODE};
	//Variables for grabcut separation
	private Mat mImageMat;	
	//Set mat without transparency
	private Mat mRGBImageMat;
	private Mat mRGBMaskImageMat;
	//Mask mats
	private Mat mBinaryMask;
	private Mat mImageMask;
	
	private Mat mCompareMat;
	//Final masked image mat
	private Bitmap mForegroundBitmap;
	
	private Mat mBgdModel;
	private Mat mFgdModel;
	private ArrayList<PointF> mFgdPixels, mBgdPixels,mPrFgdPixels,mPrBgdPixels;
	private int mIterCount;
	private Rect maskRect;
	private int mRadius;
	private GrabCutCompleteListener mGrabCutCompleted;
	private GrabCutMode mGrabCutMode;
	
	//Variables for operation on resized mode
	private int mScaleFactor = 2;
	private float mScaleMultiplier;
	private Mat mForegroundMat;
	private Mat mDownsampledMat;
	private Mat mUpSampleMaskMat;
	private Mat mTransparentForegroundMat;
	
	private int mPassNumber;
	public GrabCut(){
		//Initialize all matrices
		mImageMat = new Mat();
		mRGBImageMat = new Mat();
		//Image masking
		mImageMask = new Mat();
		mBinaryMask = new Mat();
		//mCompareMat = new Mat();
		
		mFgdModel = new Mat(); 
		mBgdModel = new Mat() ;
		mFgdPixels = new ArrayList<PointF>();
		mBgdPixels = new ArrayList<PointF>();
		mPrFgdPixels = new ArrayList<PointF>();
		mPrBgdPixels = new ArrayList<PointF>();
		mIterCount = 1;
		//Set the value of all mats
		mFgdModel.setTo(new Scalar(255));
		mBgdModel.setTo(new Scalar(255));
		//This is circle radius
		mRadius = 2;
		mGrabCutCompleted = null;
		mForegroundMat = new Mat();
		maskRect = new Rect(0, 0, 0, 0);
		//mForegroundBitmap = new Bitmap();
		//0 means free drawing mode
		mGrabCutMode = GrabCutMode.OPEN_MASK_MODE;
		mScaleMultiplier = 1.0f/mScaleFactor;
		mDownsampledMat = new Mat();
		mUpSampleMaskMat = new Mat();
		mPassNumber = 1;
		mTransparentForegroundMat = new Mat();
		mRGBMaskImageMat = new Mat();
	}
	
	public void setOnGrabCutCompleteListener(GrabCutCompleteListener grabCutCompleted){
		this.mGrabCutCompleted = grabCutCompleted;
	}
	
	//Mode 0 means free grab mode and mode 1 means rectangle mode
	public void setGrabCutMode(GrabCutMode mode){
		mGrabCutMode = mode;
	}
	
	//Create binary mask based on imagemask 
	private void createBinaryMask(){
		if(mBinaryMask.empty()){
			mBinaryMask.create(mImageMask.size(), CvType.CV_8UC1);
		}	
		mBinaryMask.setTo(new Scalar(1.0));
		Core.bitwise_and(mImageMask, mBinaryMask,mBinaryMask);
			
		
	}
	
	private void resetData(){
		//BY default set whole image mask as background
	    if(!mImageMask.empty()) 
	    {
	    	if(mGrabCutMode == GrabCutMode.OPEN_MASK_MODE)
	    		mImageMask.setTo(new Scalar(Imgproc.GC_PR_BGD));
	    	else if(mGrabCutMode == GrabCutMode.CLOSED_MASK_MODE)
	    	{
	    		mImageMask.setTo(new Scalar(Imgproc.GC_BGD));
	    		
	    	}
	    	
	    }
	    mBgdPixels.clear(); mFgdPixels.clear();
	    mPrBgdPixels.clear(); mPrFgdPixels.clear();
	   
	   
	}
	
	public void setImageMatFromBitmap(Bitmap bitmap) {
		Utils.bitmapToMat(bitmap, mImageMat);
		//Also make a copy of the bitmap
		mForegroundBitmap = Bitmap.createBitmap(bitmap);
		if(mTransparentForegroundMat.empty())
		{
			mTransparentForegroundMat.create(mImageMat.size(), CvType.CV_8UC4);
			mTransparentForegroundMat.setTo(new Scalar(new double[]{255,0,0,255}));
		}
		if(mImageMask.empty())
			mImageMask.create(new Size(mImageMat.width()/mScaleFactor, mImageMat.height()/mScaleFactor), CvType.CV_8UC1);
		//reset the mask data and all 
		resetData();
	}
	
	public void setMaskImage(Bitmap bitmap){
		//Update mImageMask and copy 
		Utils.bitmapToMat(bitmap, mRGBMaskImageMat);
	}
	
	private void drawLabels(){
		//mask value 0
		for(PointF point:mBgdPixels)
			Imgproc.circle(mImageMask, new org.opencv.core.Point(point.x*mScaleMultiplier, point.y*mScaleMultiplier), mRadius, new Scalar(Imgproc.GC_BGD), -1);
		//mask value 1
		for(PointF point:mFgdPixels)
			Imgproc.circle(mImageMask, new org.opencv.core.Point(point.x*mScaleMultiplier, point.y*mScaleMultiplier), mRadius, new Scalar(Imgproc.GC_FGD), -1);
		//mask value 2
		for(PointF point:mPrBgdPixels)
			Imgproc.circle(mImageMask, new org.opencv.core.Point(point.x*mScaleMultiplier, point.y*mScaleMultiplier), mRadius, new Scalar(Imgproc.GC_PR_BGD), -1);
		//mask value 3
		for(PointF point:mPrFgdPixels)
			Imgproc.circle(mImageMask, new org.opencv.core.Point(point.x*mScaleMultiplier, point.y*mScaleMultiplier), mRadius, new Scalar(Imgproc.GC_PR_FGD), -1);
		
	}
	
	private void configureImageMaskForCloseCurve(){
		Mat rChannelMat = new Mat();
		Core.extractChannel(mRGBMaskImageMat, rChannelMat, 1);
		//create scaled mat 
		Mat rScaledMat = new Mat();
		if(mScaleFactor == 1)
			rChannelMat.copyTo(rScaledMat);
		else
			Imgproc.pyrDown(rChannelMat, rScaledMat, new Size(mRGBImageMat.cols()/mScaleFactor, mRGBImageMat.rows()/mScaleFactor));
		//First set mImageMask to PR_FGD
		mImageMask.setTo(new Scalar(Imgproc.GC_PR_FGD));
		
		//Then mask it with provided image mask
		Core.bitwise_and(rScaledMat, mImageMask, mImageMask);
	}
	private void setUpFirstPass(){
		if(mGrabCutMode == GrabCutMode.OPEN_MASK_MODE)
		{
			if(mBgdPixels.size() == 0)
				mImageMask.setTo(new Scalar(Imgproc.GC_PR_BGD));
			if(mFgdPixels.size() == 0)
				mImageMask.setTo(new Scalar(Imgproc.GC_PR_FGD));
		}
		else if(mGrabCutMode == GrabCutMode.CLOSED_MASK_MODE){
			configureImageMaskForCloseCurve();
		}
		//Do image conversions on first pass
		Imgproc.cvtColor(mImageMat, mRGBImageMat, Imgproc.COLOR_RGBA2RGB);
		if(mScaleFactor == 1){
			mRGBImageMat.copyTo(mDownsampledMat);
		}
		else
			Imgproc.pyrDown(mRGBImageMat, mDownsampledMat, new Size(mRGBImageMat.cols()/mScaleFactor, mRGBImageMat.rows()/mScaleFactor));
		
	}
	
	private void resetOnPassComplete(){
		//All foreground and background pixels can be cleared
		mFgdPixels.clear(); mBgdPixels.clear();mPrFgdPixels.clear();mPrBgdPixels.clear();
		mForegroundMat.release();
		mForegroundMat = new Mat();
	}
	
	public void runGrabCut(){
		
		//If this is first pass then set default foreground/background mat based on selection made
		if(mPassNumber == 1){
			setUpFirstPass();
			if(mGrabCutMode==GrabCutMode.OPEN_MASK_MODE && mBgdPixels.size() == 0 && mFgdPixels.size()==0)
				return;
			mPassNumber +=1;
		}
		//Now set mask image points 
		drawLabels();
		
		if(mGrabCutMode == GrabCutMode.OPEN_MASK_MODE || mGrabCutMode == GrabCutMode.CLOSED_MASK_MODE)
			Imgproc.grabCut(mDownsampledMat, mImageMask, maskRect, mBgdModel, mFgdModel, mIterCount,Imgproc.GC_INIT_WITH_MASK);
		else
			Imgproc.grabCut(mDownsampledMat, mImageMask, maskRect, mBgdModel, mFgdModel, mIterCount,Imgproc.GC_INIT_WITH_RECT);
		//For time being just scale the mask Image and output this
		//Now create binary mask based on Image Mask
		createBinaryMask();
		//Now can merge this with ImageMat and 
		if(mScaleFactor == 1)
			mBinaryMask.copyTo(mUpSampleMaskMat);
		else
			Imgproc.resize(mBinaryMask, mUpSampleMaskMat,mImageMat.size());
			//Imgproc.pyrUp(mBinaryMask, mUpSampleMaskMat, new Size(mImageMask.cols()*mScaleFactor,mImageMask.rows()*mScaleFactor));
		
		mTransparentForegroundMat.copyTo(mForegroundMat,mUpSampleMaskMat);
		//Copy 
		//Merge mForegroundMat and mImageMat(Original Mat) to generate transparent blending
		Core.addWeighted(mImageMat,1.0f, mForegroundMat, 0.5f, 0.0f, mForegroundMat);
		Utils.matToBitmap(mForegroundMat, mForegroundBitmap);
		if(this.mGrabCutCompleted != null)
			mGrabCutCompleted.OnGrabCutComplete(mForegroundBitmap);
		
		resetOnPassComplete();
		
	}
	
	public void addForegroundPixels(ArrayList<PointF> points){
		for(PointF point:points)
			mFgdPixels.add(point);
	}
	
	public void addBackgroundPixels(ArrayList<PointF> points){
		for(PointF point:points)
			mBgdPixels.add(point);
	}

	public void addProbableForegroundPixels(ArrayList<PointF> points){
		for(PointF point:points)
			mPrFgdPixels.add(point);
	}
	
	public void addProbableBackgroundPixels(ArrayList<PointF> points){
		for(PointF point:points)
			mPrBgdPixels.add(point);
	}
	
	public void setGrabCutRect(android.graphics.Rect selectedRect){
		maskRect.x = selectedRect.left;
		maskRect.y = selectedRect.top;
		maskRect.width = selectedRect.right - selectedRect.left;
		maskRect.height = selectedRect.bottom - selectedRect.top;
	}
}
