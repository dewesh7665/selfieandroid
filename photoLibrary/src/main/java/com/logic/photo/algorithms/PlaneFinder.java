package com.logic.photo.algorithms;

import android.graphics.Bitmap;

import com.logic.photo.models.Line;
import com.logic.photo.models.Quadrilateral;
import com.logic.photo.utils.PhotoUtils;
import com.logic.photo.utils.QuadrilateralFinder;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;


public class PlaneFinder {
	private final int MAX_IMAGE_WIDTH = 400;
	private final int MIN_SOBEL_THRESH = 10;
	private final int MAX_SOBEL_THRESH = 20;
	private final int GAUSSIAN_KERNEL_DIMENSION = 3;
	private final int MAX_LINE_GAP = 6;
	private final int MIN_THRESH_VALUE = 10;
	private final int MAX_VERTICAL_THRESH_VALUE = 40;
	private final int MAX_HORIZONTAL_THRESH_VALUE = 40;
	private final int MIN_HORIZONTAL_THRESH_VALUE = 20;
	private final int MIN_VERTICAL_THRESH_VALUE = 20;
	private final int MAX_PROMINENT_LINES_TO_BE_FOUND = 20;
	private int mThreshold;
	private int minLineSize;
	private Mat mRGBAMat;
	//Final copy of bitmap
	private Bitmap mForegroundBitmap;
	private Mat mGrayMat;
	private Mat mProcessGrayMat;
	private Mat mProcessedRGBMat;
	//This will be binary horizontal sobel mat
	private Mat mHorizontalEdgeMap;
	private Mat mVerticalEdgemap;
	private ArrayList<Line> mHorizontalLines;
	private ArrayList<Line> mVerticalLines;
	private Mat mHorizontalLinesMat;
	private Mat mVerticalLinesMat;
	private int mScaleFactor = 1;
	private int mResizedWidth;
	private int mResizedHeight;
	private ArrayList<Quadrilateral> mPlaneQuads;
	private ArrayList<Quadrilateral> mVerticalQuads;
	private Bitmap mResultBitmap = null;
	
	private enum LineDirection{HORIZONTAL, VERTICAL};
	public interface OnPlainFoundListener{
		public void onPlainFound(Bitmap bmp);
	}
	private OnPlainFoundListener mPlainFoundListener;
	public PlaneFinder(){
		mRGBAMat = new Mat();
		mGrayMat = new Mat();
		mProcessGrayMat = new Mat();
		mHorizontalEdgeMap = new Mat();
		mVerticalEdgemap = new Mat();
		mHorizontalLinesMat = new Mat();
		mVerticalLinesMat = new Mat();
		mProcessedRGBMat = new Mat();
		mPlaneQuads = new ArrayList<Quadrilateral>();
		mHorizontalLines = new ArrayList<Line>();
		mVerticalLines = new ArrayList<Line>();
		mPlaneQuads = new ArrayList<Quadrilateral>();
		mVerticalQuads = new ArrayList<Quadrilateral>();
	}
	
	public void setPlainFoundListener(OnPlainFoundListener plainfoundlistener){
		mPlainFoundListener = plainfoundlistener;
	}
	public void setOriginalImage(Bitmap bitmap){
		Utils.bitmapToMat(bitmap, mRGBAMat);
		//Also make a copy of the bitmap
		mForegroundBitmap = Bitmap.createBitmap(bitmap);
		Imgproc.cvtColor(mRGBAMat, mGrayMat, Imgproc.COLOR_RGBA2GRAY);
		//Now first resize to sizes where we can operate easily
		mScaleFactor = PhotoUtils.getScaleFatorToBeApplied(mGrayMat.cols(), mGrayMat.rows(), MAX_IMAGE_WIDTH);
		mResizedWidth =(int) mGrayMat.cols()/mScaleFactor;
		mResizedHeight = (int)mGrayMat.rows()/mScaleFactor;
		//Initialize other matrices and variables
		if(mScaleFactor == 1)
			mGrayMat.copyTo(mProcessGrayMat);
		else{
			//No need to do gaussian blurring as pyramid down will automatically 
			PhotoUtils.downSampleOriginalMat(mGrayMat, mProcessGrayMat, mScaleFactor);
		}
		Imgproc.GaussianBlur(mProcessGrayMat, mProcessGrayMat, new Size(3, 3), 0.0);
		mResultBitmap = Bitmap.createBitmap(bitmap, 0, 0, mProcessGrayMat.width(), mProcessGrayMat.height(), null, false);
		//Now set hough parameter values
		int minimumDimension = mProcessGrayMat.cols() < mProcessGrayMat.rows() ? mProcessGrayMat.cols() : mProcessGrayMat.rows();
	    mThreshold = minimumDimension/4;
	    minLineSize = minimumDimension/4;
	    //Set horizontal and vertical mats
	    mHorizontalEdgeMap.create(mProcessGrayMat.size(), CvType.CV_8UC1);
	    mVerticalEdgemap.create(mProcessGrayMat.size(), CvType.CV_8UC1);
	    //Create processed bitmap for representation purposes 
	    mProcessedRGBMat.create(mProcessGrayMat.size(), CvType.CV_8UC3);
	}
	
	private void configureThresholdMats(){
		
	}
	
	private void findLines(Mat lineMat, LineDirection direction){
		int countIndex = 0;
		for(int x=0; x<lineMat.cols(); x++){
			if(countIndex > MAX_PROMINENT_LINES_TO_BE_FOUND)
				break;
			double[] vec = lineMat.get(0, x);
	          double x1 = vec[0], y1 = vec[1], x2 = vec[2],
	                 y2 = vec[3];
	          Point start = new Point(vec[0], vec[1]);
	          Point end = new Point(vec[2], vec[3]);
	          if(direction == LineDirection.HORIZONTAL && (Math.abs(x2-x1) > Math.abs(y2-y1)))
	          {
	        	  mHorizontalLines.add(new Line(start, end));
	        	  countIndex++;
	          }
	          else if(direction == LineDirection.VERTICAL && (Math.abs(y2-y1) > Math.abs(x2-x1))){
	        	  mVerticalLines.add(new Line(start, end));
	        	  countIndex++;
	          }      
		}
	}
	
	public Quadrilateral findFrameQuad(){
		//Now operate on processed gray mat and get sobel edges
		Mat absHorizontalMat = new Mat();
		absHorizontalMat.create(mProcessGrayMat.size(), CvType.CV_16S);
		Mat absVerticalMat = new Mat();
		absVerticalMat.create(mProcessGrayMat.size(), CvType.CV_16S);
		
		//Apply sobel matrices on each of them
		Imgproc.Sobel(mProcessGrayMat, absHorizontalMat, CvType.CV_16S,0, 1);
		Core.convertScaleAbs(absHorizontalMat, mHorizontalEdgeMap);
		
		Imgproc.Sobel(mProcessGrayMat, absVerticalMat, CvType.CV_16S, 1, 0);
		Core.convertScaleAbs(absVerticalMat, mVerticalEdgemap);
		
		//Now threshold horizontal and vertical mats
		Imgproc.threshold(mHorizontalEdgeMap, mHorizontalEdgeMap, MAX_HORIZONTAL_THRESH_VALUE , 255, Imgproc.THRESH_BINARY);
		Imgproc.threshold(mVerticalEdgemap, mVerticalEdgemap, MAX_VERTICAL_THRESH_VALUE, 255, Imgproc.THRESH_BINARY);
		
		//Find prominent horizontal lines in horizontal edge map
		Imgproc.HoughLinesP(mHorizontalEdgeMap, mHorizontalLinesMat, 1, Math.PI/60, mThreshold, minLineSize, MAX_LINE_GAP);
		findLines(mHorizontalLinesMat, LineDirection.HORIZONTAL);
		
		//Find prominent vertical lines in vertical edge map
		Imgproc.HoughLinesP(mVerticalEdgemap, mVerticalLinesMat, 1, Math.PI/60, mThreshold, minLineSize, MAX_LINE_GAP);	
		findLines(mVerticalLinesMat, LineDirection.VERTICAL);
		
		/*if(mPlainFoundListener != null){
			Imgproc.cvtColor(mHorizontalEdgeMap, mProcessedRGBMat, Imgproc.COLOR_GRAY2RGB);
			//Draw vertical lines on image
			for(Line horizontalLine: mHorizontalLines)
				Core.line(mProcessedRGBMat, horizontalLine.getStartPoint(), horizontalLine.getEndPoint(), new Scalar(0, 255, 0), 2);
			for(Line verticalLine: mVerticalLines)
				Core.line(mProcessedRGBMat, verticalLine.getStartPoint(), verticalLine.getEndPoint(), new Scalar(255, 0, 0), 2);
			Utils.matToBitmap(mProcessedRGBMat, mResultBitmap);
			mPlainFoundListener.onPlainFound(mResultBitmap);
			return new Quadrilateral();
		}*/
		
		//Now find prominent corners well separated in space.
		//First find array of good corner postulates.. At least 4-5 of them
		mPlaneQuads.clear();
		mPlaneQuads = QuadrilateralFinder.findQuadrilaterals(mHorizontalLines, mVerticalLines,true);
		mVerticalQuads.clear();
		mVerticalQuads = QuadrilateralFinder.findQuadrilaterals(mVerticalLines, mHorizontalLines,false);
		//Constraint the search region..First find the best quad and based on that find out next quad
		Quadrilateral firstQuad = mPlaneQuads.size()>0 ? mPlaneQuads.get(0) : null;
		Quadrilateral secondQuad = mVerticalQuads.size()>0 ? mVerticalQuads.get(0) : null;
		Quadrilateral minOverlapQuad = null;
		
		ArrayList<Line> baseLines = new ArrayList<Line>();
		if(firstQuad !=null && secondQuad != null){
			if((firstQuad.getLeftFirstVertOverlapPercentage()+ firstQuad.getRightFirstVertOverlapPercentage()) < (secondQuad.getLeftFirstVertOverlapPercentage()+ secondQuad.getRightFirstVertOverlapPercentage()))
			{
				minOverlapQuad = firstQuad;
				mVerticalQuads.clear();
				baseLines.add(minOverlapQuad.getVertLine1());
				baseLines.add(minOverlapQuad.getVertLine2());
				mVerticalQuads = QuadrilateralFinder.findQuadrilaterals(baseLines, mHorizontalLines, false);
			}
			else
			{
				minOverlapQuad = secondQuad;
				mPlaneQuads.clear();
				baseLines.add(minOverlapQuad.getVertLine1());
				baseLines.add(minOverlapQuad.getVertLine2());
				mPlaneQuads = QuadrilateralFinder.findQuadrilaterals(baseLines, mVerticalLines, true);
			}
			
		}
		Imgproc.cvtColor(mVerticalEdgemap, mProcessedRGBMat, Imgproc.COLOR_GRAY2RGB);
		/*if(mPlainFoundListener != null){
			//Imgproc.cvtColor(mHorizontalEdgeMap, mProcessedRGBMat, Imgproc.COLOR_GRAY2RGB);
			//Draw vertical lines on image
			for(Quadrilateral quad:mPlaneQuads){
				Line firstHorLine = quad.getHorLine1();
				Line secondHorLine = quad.getHorLine2();
				Line firstVertLine = quad.getVertLine1();
				Line secondVertLine = quad.getVertLine2();
				if(firstHorLine != null)
					Core.line(mProcessedRGBMat, firstHorLine.getStartPoint(), firstHorLine.getEndPoint(), new Scalar(0, 255, 0), 2);
				if(secondHorLine != null)
					Core.line(mProcessedRGBMat, secondHorLine.getStartPoint(), secondHorLine.getEndPoint(), new Scalar(0, 255, 0), 2);
				if(firstVertLine != null)
					Core.line(mProcessedRGBMat, firstVertLine.getStartPoint(), firstVertLine.getEndPoint(), new Scalar(0, 255, 0), 2);
				if(secondVertLine != null)
					Core.line(mProcessedRGBMat, secondVertLine.getStartPoint(), secondVertLine.getEndPoint(), new Scalar(0, 255, 0), 2);
			}
			//Draw vertical lines on image
			for(Quadrilateral quad:mVerticalQuads){
				Line firstHorLine = quad.getHorLine1();
				Line secondHorLine = quad.getHorLine2();
				Line firstVertLine = quad.getVertLine1();
				Line secondVertLine = quad.getVertLine2();
				if(firstHorLine != null)
					Core.line(mProcessedRGBMat, firstHorLine.getStartPoint(), firstHorLine.getEndPoint(), new Scalar(0, 0, 255), 2);
				if(secondHorLine != null)
					Core.line(mProcessedRGBMat, secondHorLine.getStartPoint(), secondHorLine.getEndPoint(), new Scalar(0, 0, 255), 2);
				if(firstVertLine != null)
					Core.line(mProcessedRGBMat, firstVertLine.getStartPoint(), firstVertLine.getEndPoint(), new Scalar(0, 0, 255), 2);
				if(secondVertLine != null)
					Core.line(mProcessedRGBMat, secondVertLine.getStartPoint(), secondVertLine.getEndPoint(), new Scalar(0, 0, 255), 2);
			}
			//Utils.matToBitmap(mProcessedRGBMat, mResultBitmap);
			//mPlainFoundListener.onPlainFound(mResultBitmap);
		}*/
		firstQuad = mPlaneQuads.size()>0 ? mPlaneQuads.get(0) : null;
		secondQuad = mVerticalQuads.size()>0 ? mVerticalQuads.get(0) : null;
		Quadrilateral bestQuad = QuadrilateralFinder.findMergedQuadrilateral(firstQuad, secondQuad);
		if(mPlainFoundListener != null){
			//Imgproc.cvtColor(mHorizontalEdgeMap, mProcessedRGBMat, Imgproc.COLOR_GRAY2RGB);
			if(bestQuad != null)
			{
				Point firstPoint = bestQuad.getPt1();
				Point secondPoint = bestQuad.getPt2();
				Point thirdPoint = bestQuad.getPt3();
				Point forthPoint  = bestQuad.getPt4();
				Imgproc.line(mRGBAMat, new Point(firstPoint.x*mScaleFactor, firstPoint.y*mScaleFactor), new Point(secondPoint.x*mScaleFactor, secondPoint.y*mScaleFactor), new Scalar(0, 255, 0), 4);
				Imgproc.line(mRGBAMat, new Point(secondPoint.x*mScaleFactor, secondPoint.y*mScaleFactor), new Point(thirdPoint.x*mScaleFactor, thirdPoint.y*mScaleFactor), new Scalar(0, 255 ,0), 4);
				Imgproc.line(mRGBAMat, new Point(thirdPoint.x*mScaleFactor, thirdPoint.y*mScaleFactor), new Point(forthPoint.x*mScaleFactor, forthPoint.y*mScaleFactor), new Scalar(0, 255, 0), 4);
				Imgproc.line(mRGBAMat, new Point(forthPoint.x*mScaleFactor, forthPoint.y*mScaleFactor), new Point(firstPoint.x*mScaleFactor, firstPoint.y*mScaleFactor), new Scalar(0, 255 , 0), 4);
			}
			Utils.matToBitmap(mRGBAMat, mForegroundBitmap);
			mPlainFoundListener.onPlainFound(mForegroundBitmap);
			
		}
		
		//Draw plane quads on 
		return bestQuad;
	}
}
