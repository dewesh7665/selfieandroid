package com.logic.photo.algorithms;

import android.graphics.Bitmap;
import android.graphics.PointF;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.Point;
import org.opencv.core.Range;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Arrays;
//Create 2D histogram using selected regions. Then backproject that region to image to segment similar regions
public class HistogramSegmentation {
	public enum ColorSpace {HSV_COLORSPACE,YCRCB_COLORSPACE};
	private int mColorCode;
	private Mat mImageMat;	
	//Set mat without transparency
	private Mat mRGBImageMat;
	private Mat mColorSpaceMat;
	//Masking related variables
	private int mCircleRadius = 2;
	private Mat mSingleChannelMaskMat;
	//Bins for HSV color space
	private int hbins = 30, sbins = 32;
	private int[] hbinSizes = new int[]{hbins,sbins};
	//Bins for YCrCb color space 
	private int crbins = 32, cbbins = 32;
	private int[] crcbbinsizes = new int[]{crbins,cbbins};
	//Bins for selected color space
	private MatOfInt mSelectedBinSizes;
	private MatOfInt mSelectedChannels;
	private MatOfFloat mRangesMat;
	//Histogram calculation variables
	private Mat mHistogramMat;
	private Mat mBackProjMat;
	//Matrices for flood fill segmentation
	private Mat mAggregatedMaskMat;
	private Mat mSubMaskMat;
	private Range mRowRange;
	private Range mColRange;
	private int mLowVal = 20;
	private int mHighVal = 20;
	
	public interface SegmentationCompleteListener{
			public void OnSegmentationComplete(Bitmap bmp);
	}
	private SegmentationCompleteListener mSegmentationCompleteListener;
	private ArrayList<Point> mFgdPixels;
	private ColorSpace mSelectedColorSpace;
	//Foreground outputs
	private Bitmap mForegroundBitmap;
	private Mat mForegroundMat;
	private Mat mTransparentForegroundMat;
	private Boolean isFloodFillProcessingEnabled;
	public void setColorSpace(ColorSpace selectedColorSpace){
		mSelectedColorSpace = selectedColorSpace;
		if(mSelectedColorSpace == ColorSpace.HSV_COLORSPACE)
		{
			mColorCode = Imgproc.COLOR_RGB2HSV;
			mSelectedBinSizes = new MatOfInt(hbins,sbins);
			mSelectedChannels = new MatOfInt(0,1);
			mRangesMat = new MatOfFloat(0,179,0,255);
		}
		else{
			mColorCode = Imgproc.COLOR_RGB2YCrCb;
			mSelectedBinSizes = new MatOfInt(crbins,cbbins);
			mSelectedChannels = new MatOfInt(1,2);
			mRangesMat = new MatOfFloat(0,255,0,255);
		}
	}
	
	public void setChannels(int[] channels,int[] binSizes,float[] dataRanges){
		mSelectedChannels = new MatOfInt(channels);
		mSelectedBinSizes = new MatOfInt(binSizes);
		mRangesMat = new MatOfFloat(dataRanges);
		
	}
	
	public HistogramSegmentation(){
		mImageMat = new Mat();
		mRGBImageMat = new Mat();
		mColorSpaceMat = new Mat();
		mFgdPixels = new ArrayList<Point>();
		mColorCode = Imgproc.COLOR_RGB2HSV;
		
		mSingleChannelMaskMat = new Mat();
		mSingleChannelMaskMat = new Mat();
		//Set default color space to HSV
		setColorSpace(ColorSpace.HSV_COLORSPACE);
		mHistogramMat = new Mat();
		mBackProjMat = new Mat();
		mForegroundMat = new Mat();
		isFloodFillProcessingEnabled = false;
		mAggregatedMaskMat = new Mat();
		mSubMaskMat = new Mat();
		mTransparentForegroundMat = new Mat();
	
	}
	
	/*public void setMaskImage(Bitmap bitmap){
		//Update mImageMask and copy 
		Utils.bitmapToMat(bitmap, mRGBMaskImageMat);
		Core.extractChannel(mRGBMaskImageMat, mSingleChannelMaskMat, 0);
	}*/
	//Mask can be on basis of flood fill 
	//Other option can be just to 
	public void addForegroundPixels(ArrayList<PointF> points){
		for(PointF point:points)
		{
			mFgdPixels.add(new Point(point.x, point.y));
			//We can add points on mask image by drawing circles of radius 2 around the point.
			Imgproc.circle(mSingleChannelMaskMat, new org.opencv.core.Point(point.x, point.y), mCircleRadius, new Scalar(255), -1);
		}
	}
	
	
	public void setImageMatFromBitmap(Bitmap bitmap){
		Utils.bitmapToMat(bitmap, mImageMat);
		//Can do hsv segmentation here as well
		Imgproc.cvtColor(mImageMat, mRGBImageMat, Imgproc.COLOR_RGBA2RGB);
		//Foreground outputs.. which will eventually go
		mForegroundBitmap = Bitmap.createBitmap(bitmap);
		mForegroundMat.create(new Size(mImageMat.width(), mImageMat.height()), CvType.CV_8UC3);
		Imgproc.cvtColor(mRGBImageMat, mColorSpaceMat, mColorCode);
		if(mSingleChannelMaskMat.empty())
			mSingleChannelMaskMat.create(new Size(mImageMat.width(), mImageMat.height()), CvType.CV_8UC1);
		//Also set foregroundMat with transparent red color
		if(mTransparentForegroundMat.empty())
		{
			mTransparentForegroundMat.create(mImageMat.size(), CvType.CV_8UC4);
			mTransparentForegroundMat.setTo(new Scalar(new double[]{255,0,0,255}));
		}
		mAggregatedMaskMat = Mat.zeros(mImageMat.rows()+2, mImageMat.cols()+2, CvType.CV_8UC1);
		mRowRange = new Range(1, mAggregatedMaskMat.rows()-1);
		mColRange = new Range(1, mAggregatedMaskMat.cols()-1);
	}
	
	public void setHistSegmentationCompleteListner(SegmentationCompleteListener listener){
		this.mSegmentationCompleteListener = listener;
	}
	
	public void enableFloodFillPostProcessing(int lowerThreshVal,int upperThreshVal){
		isFloodFillProcessingEnabled = true;
		//
		mLowVal = lowerThreshVal;
		mHighVal = upperThreshVal;
	}
	
	private void perfromFloodFillSegmentation(){
		Scalar threshScalar = Core.mean(mBackProjMat, mSingleChannelMaskMat);
		Mat threshMat = new Mat();
		Imgproc.threshold(mBackProjMat, threshMat, threshScalar.val[0]*3/4, 255, Imgproc.THRESH_BINARY);
		//Copy mBackProjMat into mAggregated Mask mat in mRowRange to mColRange
		//Mat tempMat = mAggregatedMaskMat.rowRange(mRowRange).colRange(mColRange);
		//threshMat.copyTo(tempMat);
		//	mBackProjMat.copyTo(mAggregatedMaskMat.submat(mRowRange,mColRange));
		for(Point point:mFgdPixels){
			int newMaskVal = 255;
			int connectivity = 8;
			int flags = connectivity + (newMaskVal << 8 ) + Imgproc.FLOODFILL_FIXED_RANGE + Imgproc.FLOODFILL_MASK_ONLY;		
			
			Rect rect = null;
			Imgproc.floodFill(mBackProjMat, mAggregatedMaskMat, point, new Scalar(255),rect,new Scalar(mLowVal),new Scalar(mHighVal),flags);
		}
		mSubMaskMat = mAggregatedMaskMat.submat(mRowRange,mColRange);
		Core.bitwise_and(mSubMaskMat, threshMat , mSubMaskMat);
		threshMat.release();
		
	}
	
	public void segmentImage(){
		//Now get histogram from mask image
		Imgproc.calcHist(Arrays.asList(mColorSpaceMat), mSelectedChannels, mSingleChannelMaskMat, mHistogramMat, mSelectedBinSizes, mRangesMat);
		Core.normalize(mHistogramMat,mHistogramMat,0,255,Core.NORM_MINMAX,-1,new Mat());
		//And then do the backprojection
		Imgproc.calcBackProject(Arrays.asList(mColorSpaceMat), mSelectedChannels, mHistogramMat, mBackProjMat, mRangesMat, 1);
		if(isFloodFillProcessingEnabled){
			perfromFloodFillSegmentation();
			mTransparentForegroundMat.copyTo(mForegroundMat,mSubMaskMat);
			//Core.add(mForegroundMat, new Scalar(20,20,20), mForegroundMat);
			Core.addWeighted(mImageMat,1.0f, mForegroundMat, 0.5f, 0.0f, mForegroundMat);
			Utils.matToBitmap(mForegroundMat, mForegroundBitmap);
		}
		else{
			//else just threshold with average of mask image and threshold on the basis of that
			Scalar threshScalar = Core.mean(mBackProjMat, mSingleChannelMaskMat);
			Imgproc.threshold(mBackProjMat, mBackProjMat, threshScalar.val[0], 255, Imgproc.THRESH_BINARY);
			Imgproc.cvtColor(mBackProjMat, mForegroundMat, Imgproc.COLOR_GRAY2RGB);
			Utils.matToBitmap(mForegroundMat, mForegroundBitmap);
		}
		if(this.mSegmentationCompleteListener != null)
			mSegmentationCompleteListener.OnSegmentationComplete(mForegroundBitmap);
	}
}
