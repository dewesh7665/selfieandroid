package com.logic.photo.algorithms;

import android.graphics.PointF;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

public class ImagePaintFilter extends ImageBasicFilter {

	//Erase variables
	private ArrayList<Point> mErasePoints;
	private ArrayList<ArrayList<Point>> mArrayOfErasePoints;
	private int mEraseRadius;
	
	//Paint variables
	private ArrayList<Point> mPaintPoints;
	private ArrayList<ArrayList<Point>> mArrayOfPaintPoints;
	private int mPaintRadius;
	
	protected Mat mAccumulatedPaintMaskMat;
	
	//Erase and paint masks
	private ArrayList<Point> mTempErasePoints;
	private ArrayList<Point> mTempPaintPoints;
	private int mTempPointThresh = 2;
	
	public ImagePaintFilter() {
		// TODO Auto-generated constructor stub
		mArrayOfErasePoints = new ArrayList<ArrayList<Point>>();
		mErasePoints = new ArrayList<Point>();
		mEraseRadius = 15;
		
		mPaintPoints = new ArrayList<Point>();
		mArrayOfPaintPoints = new ArrayList<ArrayList<Point>>();
		
		mAccumulatedPaintMaskMat = new Mat();
		
		//Temp variables
		mTempErasePoints = new ArrayList<Point>();
		mTempPaintPoints = new ArrayList<Point>();
	}
	
	
	
	public int getEraseRadius(){
		return mEraseRadius;
	}
	
	public int getPaintRadius(){
		return mPaintRadius;
	}
	
	@Override
	public void undoFilter() {
		// TODO Auto-generated method stub
		
	}
	
	protected ArrayList<ArrayList<Point>> getAllErasedPoints(){
		return mArrayOfErasePoints;
	}
	
	protected ArrayList<ArrayList<Point>> getAllPaintedPoints(){
		return mArrayOfPaintPoints;
	}
	
	protected ArrayList<ArrayList<Point>> getUndoEraseContour(){
		if(mArrayOfErasePoints.size() == 0)
			return null;
		/*ArrayList<Point> currentContourPoints = new ArrayList<Point>();
		for(int i=0;i< mArrayOfErasePoints.size()-1;i++){
			for(Point point:mArrayOfErasePoints.get(i)){
				currentContourPoints.add(point);
			}
		}*/
		int lastIndex = mArrayOfErasePoints.size()-1;
		mArrayOfErasePoints.remove(lastIndex);
		return mArrayOfErasePoints;
	}
	
	protected ArrayList<ArrayList<Point>> getUndoPaintContour(){
		if(mArrayOfPaintPoints.size() == 0)
			return null;
		/*ArrayList<Point> currentContourPoints = new ArrayList<Point>();
		for(int i=0;i< mArrayOfPaintPoints.size()-1;i++){
			for(Point point:mArrayOfPaintPoints.get(i)){
				currentContourPoints.add(point);
			}
		}*/
		int lastIndex = mArrayOfPaintPoints.size()-1;
		mArrayOfPaintPoints.remove(lastIndex);
		return mArrayOfPaintPoints;
	}
	
	//This will be used for undo mask. This has to be called whenever finger is lifted up
	private void setErasePoints(ArrayList<PointF> erasePoints, int radius){
		mErasePoints.clear();
		
		ArrayList<Point> allPoints = new ArrayList<Point>();
		for(PointF point:erasePoints)
		{
			mErasePoints.add(new Point(point.x, point.y));
			allPoints.add(new Point(point.x, point.y));
		}
		mArrayOfErasePoints.add(allPoints);
		mEraseRadius = radius;
		mUndoModes.add(UndoMode.ERASE_UNDO);
	}
	
	
	protected void undoEraseMode(){
		int eraseRadius = getEraseRadius();
		int paintRadius = getPaintRadius();
		Point startPt = new Point(0, 0), endPt = new Point(0, 0);
		mAccumulatedMaskMat.copyTo(mAccumulatedPaintMaskMat);
		ArrayList<ArrayList<Point>> erasePointsArray = getUndoEraseContour();
		for(ArrayList<Point> erasePoints:erasePointsArray){
			for(int i=0;i< erasePoints.size();i++){
				if(i==0)
				{
					startPt = erasePoints.get(0);
					continue;
				}
				endPt = new Point(erasePoints.get(i).x,erasePoints.get(i).y);
				//Core.line(mImageMask, startPt, endPt, new Scalar(Imgproc.GC_FGD), (int) (eraseRadius*mScaleMultiplier));
				//Core.line(mProcessedMat, startPt, endPt, new Scalar(255), eraseRadius);
				Imgproc.line(mAccumulatedPaintMaskMat, startPt, endPt, new Scalar(0), eraseRadius);
				startPt = endPt;
				
			}
		}
		//Now draw all paint points
		ArrayList<ArrayList<Point>> allPaintPoints = getAllPaintedPoints();
		for(ArrayList<Point> paintPoints:allPaintPoints){
			for(int i=0;i< paintPoints.size();i++){
				if(i==0)
				{
					startPt = paintPoints.get(0);
					continue;
				}
				endPt = new Point(paintPoints.get(i).x,paintPoints.get(i).y);
				//Core.line(mImageMask, startPt, endPt, new Scalar(Imgproc.GC_PR_FGD), (int) (paintRadius*mScaleMultiplier));
				//Core.line(mProcessedMat, startPt, endPt, new Scalar(255), paintRadius);
				Imgproc.line(mAccumulatedPaintMaskMat, startPt, endPt, new Scalar(255), paintRadius);
				startPt = endPt;
				
			}
		}
		
		paintOriginalMat(mAccumulatedPaintMaskMat);
	}
	
	protected void undoPaintMode(){
		int eraseRadius = getEraseRadius();
		int paintRadius = getPaintRadius();
		Point startPt = new Point(0, 0), endPt = new Point(0, 0);
		mAccumulatedMaskMat.copyTo(mAccumulatedPaintMaskMat);
		ArrayList<ArrayList<Point>> paintPointsArray = getUndoPaintContour();
		
		for(ArrayList<Point> paintPoints: paintPointsArray){
			for(int i=0;i< paintPoints.size();i++){
				if(i==0)
				{
					startPt = paintPoints.get(0);
					continue;
				}
				endPt = new Point(paintPoints.get(i).x,paintPoints.get(i).y);
				//Core.line(mImageMask, startPt, endPt, new Scalar(Imgproc.GC_PR_FGD), (int) (paintRadius*mScaleMultiplier));
				//Core.line(mProcessedMat, startPt, endPt, new Scalar(255), paintRadius);
				Imgproc.line(mAccumulatedPaintMaskMat, startPt, endPt, new Scalar(255), paintRadius);
				startPt = endPt;
				
			}
		}
		//Now  draw all erased points
		ArrayList<ArrayList<Point>> allErasedPoints = getAllErasedPoints();
		for(ArrayList<Point> erasePoints:allErasedPoints){
			for(int i=0;i< erasePoints.size();i++){
				if(i==0)
				{
					startPt = erasePoints.get(0);
					continue;
				}
				endPt = new Point(erasePoints.get(i).x,erasePoints.get(i).y);
				//Core.line(mImageMask, startPt, endPt, new Scalar(Imgproc.GC_FGD), (int) (eraseRadius*mScaleMultiplier));
				//Core.line(mProcessedMat, startPt, endPt, new Scalar(255), eraseRadius);
				Imgproc.line(mAccumulatedPaintMaskMat, startPt, endPt, new Scalar(0), eraseRadius);
				startPt = endPt;
				
			}
		}
		paintOriginalMat(mAccumulatedPaintMaskMat);
	}
	
	private void setPaintPoints(ArrayList<PointF> paintPoints,int radius){
		mPaintPoints.clear();
		
		ArrayList<Point> allPoints = new ArrayList<Point>();
		for(PointF point:paintPoints)
		{
			mPaintPoints.add(new Point(point.x, point.y));
			allPoints.add(new Point(point.x, point.y));
		}
		mArrayOfPaintPoints.add(allPoints);
		mPaintRadius = radius;
		mUndoModes.add(UndoMode.PAINT_UNDO);
	}
	
	
	public void setErasePointContour(ArrayList<PointF> erasePoints, int radius){
		setErasePoints(erasePoints, radius);
		//Draw the remaining lines
		if(mTempErasePoints.size()>0){
			eraseTempPoints(radius);
			mTempErasePoints.clear();
		}
	}
	
	public void setPaintPointContour(ArrayList<PointF> paintPoints,int radius){
		setPaintPoints(paintPoints, radius);
		//Draw the remaining lines
		if(mTempPaintPoints.size()>0){
			drawTempPaintPoints(radius);
			mTempPaintPoints.clear();
		}
	}
	
	
	private void eraseTempPoints(int radius){
		Point startPt = new Point(0, 0), endPt = new Point(0, 0);
		for(int i=0;i< mTempErasePoints.size();i++){
			if(i==0)
			{
				startPt = mTempErasePoints.get(0);
				continue;
			}
			endPt = new Point(mTempErasePoints.get(i).x,mTempErasePoints.get(i).y);
			//Core.line(mImageMask, startPt, endPt, new Scalar(Imgproc.GC_PR_BGD), (int) (radius*mScaleMultiplier));
			//Core.line(mProcessedMat, startPt, endPt, new Scalar(0), radius);
			Imgproc.line(mAccumulatedPaintMaskMat, startPt, endPt, new Scalar(0),2*radius);
			startPt = endPt;
			
		}
		mTempErasePoints.clear();
		mTempErasePoints.add(endPt);
		//Now update the image bitmap
		paintOriginalMat(mAccumulatedPaintMaskMat);
	}
	
	//This is temporal call and will update bitmap on addition of 10 points or so
	public void erasePointWithRadius(PointF erasePoint, int radius){
		//Update mask mat, processed mat and foreground mat
		mTempErasePoints.add(new Point(erasePoint.x, erasePoint.y));
		if(mTempErasePoints.size() == mTempPointThresh){
			eraseTempPoints(radius);
		}
	}
	
	
	
	private void drawTempPaintPoints(int radius){
		Point startPt = new Point(0, 0), endPt = new Point(0, 0);
		for(int i=0;i< mTempPaintPoints.size();i++){
			if(i==0)
			{
				startPt = mTempPaintPoints.get(0);
				continue;
			}
			endPt = new Point(mTempPaintPoints.get(i).x,mTempPaintPoints.get(i).y);
			//Core.line(mImageMask, startPt, endPt, new Scalar(Imgproc.GC_PR_FGD), (int) (radius*mScaleMultiplier));
			//Core.line(mProcessedMat, startPt, endPt, new Scalar(255), radius);
			Imgproc.line(mAccumulatedPaintMaskMat, startPt, endPt, new Scalar(255), 2*radius);
			//Core.circle(mAccumulatedPaintMaskMat,new Point(startPt,endPt),new Scalar(255),radius);
			startPt = endPt;
			
		}
		mTempPaintPoints.clear();
		mTempPaintPoints.add(endPt);
		//Now update the image bitmap
		paintOriginalMat(mAccumulatedPaintMaskMat);
	}
	
	public void paintPointWithColor(PointF paintPoint,int radius,int rColor,int gColor,int bColor){
		mTempPaintPoints.add(new Point(paintPoint.x, paintPoint.y));
		if(mTempPaintPoints.size() == mTempPointThresh){
			drawTempPaintPoints(radius);
		}
	}
	
	

}
