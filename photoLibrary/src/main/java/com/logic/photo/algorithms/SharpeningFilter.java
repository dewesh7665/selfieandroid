package com.logic.photo.algorithms;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;


public class SharpeningFilter extends ImageContourMaskFilter{
	
	private Mat mSmoothMat;
	//private Mat mLaplaceMat;
	//private Mat mGrayMat;
	private int mSharpenRadius;
	
	public SharpeningFilter(){
		super();
		//mSharpMat = new Mat();
		mSmoothMat = new Mat();
		//mLaplaceMat = new Mat();
		//mGrayMat = new Mat();
		mSharpenRadius = 11;
	}
	
	/*@Override
	public void getContourMaskResult(java.util.ArrayList<android.graphics.PointF> contourPoints, int radius) {
		//check if foreground mat is empty then first run prior edge filter
		if(mForegroundMat.empty())
			runFilterPrioriOnImage(false);
		//Now invoke masking method in super class
		super.getContourMaskResult(contourPoints, radius);
		Utils.matToBitmap(mForegroundMat, mForegroundBitmap);
		if(mImageFilterCompleteListener != null)
			mImageFilterCompleteListener.OnImageFilterComplete(mForegroundBitmap);
	};*/
	
	/*public void setImageMatFromBitmap(Bitmap bitmap){
		Utils.bitmapToMat(bitmap, mImageMat);
		//Can do hsv segmentation here as well
		Imgproc.cvtColor(mImageMat, mRGBImageMat, Imgproc.COLOR_RGBA2RGB);
		//Foreground outputs.. which will eventually go
		mForegroundBitmap = Bitmap.createBitmap(bitmap);
		//mForegroundMat.create(new Size(mImageMat.width(), mImageMat.height()), CvType.CV_8UC3);
		mLaplaceMat.create(mImageMat.size(), CvType.CV_16S);
	}*/
	
	@Override
	public void applyFilter(){
		//One approach is to use gaussian blur and this seem to be better than laplacian approach
		//processFilter();
		//Another approach is to use laplacian detection
		/*
		Imgproc.GaussianBlur(mRGBImageMat, mSmoothMat, new Size(7, 7), 0);
		Imgproc.Laplacian(mSmoothMat, mLaplaceMat, CvType.CV_16S, 3, 1, 0);
		Core.convertScaleAbs(mLaplaceMat, mSharpMat);
		Core.addWeighted(mRGBImageMat, 1.0, mSharpMat, 1.0, 0, mSharpMat);*/
		getCurrentContourMaskResult();
		Utils.matToBitmap(mForegroundMat, mForegroundBitmap);
		if(mImageFilterCompleteListener != null)
			mImageFilterCompleteListener.OnImageFilterComplete(mForegroundBitmap);
	}

	@Override
	protected void processFilter() {
		// TODO Auto-generated method stub
		Imgproc.GaussianBlur(mRGBImageMat, mSmoothMat, new Size(mSharpenRadius, mSharpenRadius), 0);
		
		Core.subtract(mRGBImageMat, mSmoothMat, mProcessedMat);
		Core.addWeighted(mRGBImageMat, 1.0, mProcessedMat, 1.5, 0, mProcessedMat);
	}

	@Override
	public void undoFilter() {
		// TODO Auto-generated method stub
		getContourUndoMaskResult();
		Utils.matToBitmap(mForegroundMat, mForegroundBitmap);
		if(mImageFilterCompleteListener != null)
			mImageFilterCompleteListener.OnImageFilterComplete(mForegroundBitmap);
	}
}
