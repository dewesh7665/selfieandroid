package com.logic.photo.models;

import org.opencv.core.Point;

public class Quadrilateral {
	private Point pt1;
	private Point pt2;
	private Point pt3;
	private Point pt4;
	//Other representation can be set of 4 lines 
	private Line horLine1=null;
	private Line horLine2 = null;
	private Line vertLine1 = null;
	private Line vertLine2 = null;
	//Keep overlap percentage to maximum when initialized
	private float leftFirstVertOverlapPercentage = 5.0f;
	private float rightFirstVertOverlapPercentage = 5.0f;
	private float secondHoriOverlapPercentage = 5.0f;
	
	private float firstHorLineLength;
	private float leftVertLineLength;
	private float rightVertLineLength;
	
	private boolean allLinesFound = false;
	public float getFirstHorLineLength() {
		return firstHorLineLength;
	}
	public void setFirstHorLineLength(float firstHorLineLength) {
		this.firstHorLineLength = firstHorLineLength;
	}
	
	
	public float getLeftVertLineLength() {
		return leftVertLineLength;
	}
	public void setLeftVertLineLength(float leftVertLineLength) {
		this.leftVertLineLength = leftVertLineLength;
	}
	
	public float getRightVertLineLength() {
		return rightVertLineLength;
	}
	public void setRightVertLineLength(float rightVertLineLength) {
		this.rightVertLineLength = rightVertLineLength;
	}
	
	public float getLeftFirstVertOverlapPercentage() {
		return leftFirstVertOverlapPercentage;
	}
	public void setLeftFirstVertOverlapPercentage(
			float leftFirstVertOverlapPercentage) {
		this.leftFirstVertOverlapPercentage = leftFirstVertOverlapPercentage;
	}
	public float getRightFirstVertOverlapPercentage() {
		return rightFirstVertOverlapPercentage;
	}
	public void setRightFirstVertOverlapPercentage(
			float rightFirstVertOverlapPercentage) {
		this.rightFirstVertOverlapPercentage = rightFirstVertOverlapPercentage;
	}

	public Line getHorLine1() {
		return horLine1;
	}
	public void setHorLine1(Line horLine1) {
		this.horLine1 = new Line(horLine1.getStartPoint(), horLine1.getEndPoint());
	}
	public Line getHorLine2() {
		return horLine2;
	}
	public void setHorLine2(Line horLine2) {
		this.horLine2 = new Line(horLine2.getStartPoint(), horLine2.getEndPoint());
	}
	public Line getVertLine1() {
		return vertLine1;
	}
	public void setVertLine1(Line vertLine1) {
		this.vertLine1 = new Line(vertLine1.getStartPoint(), vertLine1.getEndPoint());
	}
	public Line getVertLine2() {
		return vertLine2;
	}
	public void setVertLine2(Line vertLine2) {
		this.vertLine2 = new Line(vertLine2.getStartPoint(), vertLine2.getEndPoint());
	}
	
	
	public Point getPt1() {
		return pt1;
	}
	public void setPt1(Point pt1) {
		this.pt1 = pt1;
	}
	public Point getPt2() {
		return pt2;
	}
	public void setPt2(Point pt2) {
		this.pt2 = pt2;
	}
	public Point getPt3() {
		return pt3;
	}
	public void setPt3(Point pt3) {
		this.pt3 = pt3;
	}
	public Point getPt4() {
		return pt4;
	}
	public void setPt4(Point pt4) {
		this.pt4 = pt4;
	}
	public float getSecondHoriOverlapPercentage() {
		return secondHoriOverlapPercentage;
	}
	public void setSecondHoriOverlapPercentage(float secondHoriOverlapPercentage) {
		this.secondHoriOverlapPercentage = secondHoriOverlapPercentage;
	}
	public boolean isAllLinesFound() {
		return allLinesFound;
	}
	public void setAllLinesFound(boolean allLinesFound) {
		this.allLinesFound = allLinesFound;
	}
	
	
}
