package com.logic.photo.models;

import org.opencv.core.Point;

public class Line {
	private Point pt1;
	private Point pt2;
	public Point getStartPoint(){
		return pt1;
	}
	public Point getEndPoint(){
		return pt2;
	}
	public Line(Point startpt,Point endpt){
		pt1 = startpt;
		pt2 = endpt;
	}
}
