package com.logic.photo.models;

import android.graphics.Bitmap;

import com.logic.photo.filters.MultipleObjectFilter;

import org.opencv.core.Mat;

/**
 * @author Dewesh Kumar
 * Wrapper around Bitmap and filter.
 */
public class InputFilterInfo {
    private MultipleObjectFilter multipleObjectFilter = null;
    private String originalBMPPath;
    // private String intermediateBMPPath;
    private Mat intermediateMat;

    //TODO: Try not to use below bitmaps as could leak memory
    private Bitmap originalBitmap;
    //private Bitmap intermediateBitmap;

    public InputFilterInfo(String originalBMPPath) {
        this.originalBMPPath = originalBMPPath;
        multipleObjectFilter = new MultipleObjectFilter();
    }


    public MultipleObjectFilter getMultipleObjectFilter() {
        return multipleObjectFilter;
    }

    public void setMultipleObjectFilter(MultipleObjectFilter multipleObjectFilter) {
        this.multipleObjectFilter = multipleObjectFilter;
    }

    public String getOriginalBMPPath() {
        return originalBMPPath;
    }

    public void setOriginalBMPPath(String originalBMPPath) {
        this.originalBMPPath = originalBMPPath;
    }

    /*public String getIntermediateBMPPath() {
        return intermediateBMPPath;
    }

    public void setIntermediateBMPPath(String intermediateBMPPath) {
        this.intermediateBMPPath = intermediateBMPPath;
    }*/

    public Bitmap getOriginalBitmap() {
        return originalBitmap;
    }

    public void setOriginalBitmap(Bitmap originalBitmap) {
        this.originalBitmap = originalBitmap;
    }

    /*
    public Bitmap getIntermediateBitmap() {
        return intermediateBitmap;
    }

    public void setIntermediateBitmap(Bitmap intermediateBitmap) {
        this.intermediateBitmap = intermediateBitmap;
    }
    */
    public void resetObjectFilter(){
        /*if(multipleObjectFilter != null){
            multipleObjectFilter.resetGrabCut();
            multipleObjectFilter = null;
        }*/
    }

    public Mat getIntermediateMat() {
        return intermediateMat;
    }

    public void setIntermediateMat(Mat intermediateMat) {
        this.intermediateMat = intermediateMat;
    }
}
