package com.logic.photo.models;

import org.opencv.core.Point;


public class LineMatch {
	private Line firstLine;
	private Line secondLine;
	private double perpendicualrMatchPercentage;
	private double parallelMatchPercentage;
	private Point intersectionPoint;
	public Point getIntersectionPoint() {
		return intersectionPoint;
	}
	public void setIntersectionPoint(Point intersectionPoint) {
		this.intersectionPoint = intersectionPoint;
	}
	public double getPerpendicualrMatchPercentage() {
		return perpendicualrMatchPercentage;
	}
	public void setPerpendicualrMatchPercentage(double perpendicualrMatchPercentage) {
		this.perpendicualrMatchPercentage = perpendicualrMatchPercentage;
	}
	public double getParallelMatchPercentage() {
		return parallelMatchPercentage;
	}
	public void setParallelMatchPercentage(float parallelMatchPercentage) {
		this.parallelMatchPercentage = parallelMatchPercentage;
	}
	public Line getFirstLine() {
		return firstLine;
	}
	public void setFirstLine(Line firstLine) {
		this.firstLine = firstLine;
	}
	public Line getSecondLine() {
		return secondLine;
	}
	public void setSecondLine(Line secondLine) {
		this.secondLine = secondLine;
	}
	
}

