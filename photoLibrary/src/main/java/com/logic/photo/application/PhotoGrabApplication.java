package com.logic.photo.application;

import com.logic.photo.models.InputFilterInfo;

import java.util.ArrayList;
import java.util.List;

public class PhotoGrabApplication extends PhotoApplication{
    private static PhotoGrabApplication mInstance;
    /**
     * This is current/recent selection.
     * Life will be from selection of image to just before going to preview activity.
     * Will go in list just before going to preview activity.
      */
    private InputFilterInfo currentInputFilterInfo;
    /**
     * List of all input filters to be render on preview screen
     */
    private List<InputFilterInfo> inputFilterInfoList = new ArrayList<InputFilterInfo>();
    private String intermediateBMPPath;
    public PhotoGrabApplication() {
        mInstance = this;
    }
    public static PhotoGrabApplication getInstance() {
        return mInstance;
    }

    public List<InputFilterInfo> getInputFilterInfoList() {
        return inputFilterInfoList;
    }

    public InputFilterInfo getCurrentInputFilterInfo() {
        return currentInputFilterInfo;
    }

    public void setCurrentInputFilterInfo(InputFilterInfo inputFilterInfo) {
        this.currentInputFilterInfo = inputFilterInfo;
    }


    public String getIntermediateBMPPath() {
        return intermediateBMPPath;
    }

    public void setIntermediateBMPPath(String intermediateBMPPath) {
        this.intermediateBMPPath = intermediateBMPPath;
    }

    /************************************** Background Selection *****************************/
    private String selectedBGPath;
    private int selectedBGColor = -1;

    public int getSelectedBGColor() {
        return selectedBGColor;
    }

    public void setSelectedBGColor(int selectedBGColor) {
        this.selectedBGColor = selectedBGColor;
    }

    public String getSelectedBGPath() {
        return selectedBGPath;
    }

    public void setSelectedBGPath(String selectedBGPath) {
        this.selectedBGPath = selectedBGPath;
    }

    public void resetAppState() {
        selectedBGColor = -1;
        selectedBGPath = null;
        intermediateBMPPath = null;
        inputFilterInfoList.clear();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }
}

