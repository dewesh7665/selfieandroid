package com.logic.photo.utils;

import com.logic.photo.models.Line;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;

public class PhotoUtils {
	public static int getScaleFatorToBeApplied(int imageWidth,int imageHeight,int maxAllowedWidth){
		int processedframeWidth = imageWidth;
		int scaleFactor =1;
		while(processedframeWidth > maxAllowedWidth)
		{
			processedframeWidth = maxAllowedWidth/2;
			scaleFactor *=2;
		}
		return scaleFactor;
	}
	
	public static Point getPointOfIntersection(Line line1, Line line2){
		int x1 = (int)line1.getStartPoint().x,x2 = (int)line1.getEndPoint().x,y1=(int)line1.getStartPoint().y,y2=(int)line1.getEndPoint().y;
		int x3 = (int)line2.getStartPoint().x,x4 = (int)line2.getEndPoint().x,y3=(int)line2.getStartPoint().y,y4=(int)line2.getEndPoint().y;
		int d = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4);
		if (d == 0) return null;
		float xi = ((x3-x4)*(x1*y2-y1*x2)-(x1-x2)*(x3*y4-y3*x4))/d;
		float yi = ((y3-y4)*(x1*y2-y1*x2)-(y1-y2)*(x3*y4-y3*x4))/d;
		return new Point((int)xi,(int)yi);
	}
	
	public static void downSampleOriginalMat(Mat originalMat, Mat downsampledMat, int scaleFactor){
		if(scaleFactor == 1)
			originalMat.copyTo(downsampledMat);
		else{
			Mat tempResizedMat = new Mat();
			originalMat.copyTo(tempResizedMat);
			while(scaleFactor > 1){
				Mat tempDownMat = new Mat();
				Imgproc.pyrDown(tempResizedMat,tempDownMat);
				tempResizedMat.release();
				tempResizedMat = new Mat();
				tempDownMat.copyTo(tempResizedMat);
				scaleFactor = scaleFactor/2;
				tempDownMat.release();
			}
			//Finally copy tempDownMat to downsampledMat
			tempResizedMat.copyTo(downsampledMat);
			
		}
	}
	
	public static void upSampleOriginalMat(Mat downsampledMat, Mat restoredMat,int scaleFactor){
		//Here we can do simple resizing
	}
}
