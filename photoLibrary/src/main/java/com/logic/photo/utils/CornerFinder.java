package com.logic.photo.utils;

import com.logic.photo.models.Line;
import com.logic.photo.models.LineMatch;

import org.opencv.core.Point;
import org.opencv.core.Rect;

import java.util.ArrayList;



public class CornerFinder {
	private ArrayList<Line> m_horizontalLines;
	private ArrayList<Line> m_verticalLines;
	private ArrayList<Rect> m_boundingRects;
	private ArrayList<Rect> m_overlappingRects;
	private ArrayList<LineMatch> m_MatchingLines;
	private int frameWidth;
	private int frameHeight;
	private LineMatch m_ProminentLineMatch = null;
	private Line m_ReferenceHorizontalLine = null;
	private Line m_ReferenceVerticalLine = null;
	
	public CornerFinder(ArrayList<Line> horizontalLines,ArrayList<Line> verticalLines,ArrayList<Rect> boundingRects){
		//frameWidth = referenceMat.cols();
		//frameHeight = referenceMat.rows();
		m_horizontalLines = horizontalLines;
		m_verticalLines = verticalLines;
		m_boundingRects = boundingRects;
		m_MatchingLines = new ArrayList<LineMatch>();
	}
	
	private Point getPointOfIntersection(Line line1, Line line2){
		int x1 = (int)line1.getStartPoint().x,x2 = (int)line1.getEndPoint().x,y1=(int)line1.getStartPoint().y,y2=(int)line1.getEndPoint().y;
		int x3 = (int)line2.getStartPoint().x,x4 = (int)line2.getEndPoint().x,y3=(int)line2.getStartPoint().y,y4=(int)line2.getEndPoint().y;
		int d = (x1-x2)*(y3-y4) - (y1-y2)*(x3-x4);
		if (d == 0) return null;
		float xi = ((x3-x4)*(x1*y2-y1*x2)-(x1-x2)*(x3*y4-y3*x4))/d;
		float yi = ((y3-y4)*(x1*y2-y1*x2)-(y1-y2)*(x3*y4-y3*x4))/d;
		return new Point((int)xi,(int)yi);
	}
	
	public void setReferenceLines(Line horizontalRefLine,Line verticalRefLine){
		m_ReferenceHorizontalLine = horizontalRefLine;
		m_ReferenceVerticalLine = verticalRefLine;
	}
	
	
	
	public LineMatch getCornerLines(){
		//For each horizontal line find intersecting vertical line and if point of interesection is within some percentage range for 
		
		for(int i=0;i<m_horizontalLines.size();i++){
			//Calculate the minimumoverlap for each perpendicualr lines..
			//First find point of intersection 
			Line horizontalLine = m_horizontalLines.get(i);
			int horx1 = (int)horizontalLine.getStartPoint().x,hory1 = (int)horizontalLine.getStartPoint().y;
			int horx2 = (int)horizontalLine.getEndPoint().x, hory2 = (int)horizontalLine.getEndPoint().y; 
			float horizontalLineLength = (float)Math.sqrt((hory2-hory1)*(hory2- hory1) +(horx2-horx1)*(horx2-horx1));
			//First check if it is too close to reference line then continue
			if(m_ReferenceHorizontalLine != null){
				int refHorX1 = (int)m_ReferenceHorizontalLine.getStartPoint().x, refHorX2 = (int)m_ReferenceHorizontalLine.getEndPoint().x;
				int minY1Dist = (refHorX1 - horx1) < (refHorX1 - horx2) ? (refHorX1 - horx1):(refHorX1 - horx2);
				int minY2Dist = (refHorX2 - horx1) < (refHorX2 - horx2) ? (refHorX2 - horx1):(refHorX2 - horx2);
				int distFilter = (int)horizontalLineLength/5;
				if(Math.abs(minY1Dist) < distFilter || Math.abs(minY2Dist) < distFilter){
					float minYFilter = minY1Dist < minY2Dist ? minY1Dist : minY2Dist;
					continue;
					
				}
			}
			double minOverlapPercentage = 2.0;
			int minOverlapIndex = -1;
			Point minPtIntersection = null;
			for(int j=0;j<m_verticalLines.size();j++){
				Line verticalLine = m_verticalLines.get(j);
				// If the point of interection lies on line segments then calculate minimum distance from end of line segments and convert them to percentage of 
				//line segment lengths and sum them for both lines
				Point ptIntersection = getPointOfIntersection(horizontalLine, verticalLine);
				if(ptIntersection == null )
					continue;
				
				//Get distance from first point on horizontal line
				int firstXDistance =(int) ((ptIntersection.x-horx1)*((int)ptIntersection.x-horx1)+(ptIntersection.y -hory1)*(ptIntersection.y - hory1));
				//Get distance from second point on horizontal line
				int secondXDistance = (int) ((ptIntersection.x-horx2)*(ptIntersection.x-horx2)+(ptIntersection.y -hory2)*(ptIntersection.y - hory2));
				int minHorDistance = firstXDistance <secondXDistance ? firstXDistance : secondXDistance;
				
				//Get distance from vertical line
				int vertX1 = (int)verticalLine.getStartPoint().x,vertY1 = (int)verticalLine.getStartPoint().y;
				int vertX2 = (int)verticalLine.getEndPoint().x,vertY2 = (int)verticalLine.getEndPoint().y;
				double verticalLineLength = (float)Math.sqrt((vertY2 - vertY1)*(vertY2 - vertY1) + (vertX2 - vertX1)*(vertX2 - vertX1));
				
				if(m_ReferenceVerticalLine != null){
					int refHorY1 = (int)m_ReferenceVerticalLine.getStartPoint().y, refHorY2 = (int)m_ReferenceVerticalLine.getEndPoint().y;
					int minY1Dist = (refHorY1 - vertY1) < (refHorY1 - vertY2) ? (refHorY1 - vertY1):(refHorY1 - vertY2);
					int minY2Dist = (refHorY2 - vertY1) < (refHorY2 - vertY2) ? (refHorY2 - vertY1):(refHorY2 - vertY2);
					int distFilter = (int)verticalLineLength/5;
					if(Math.abs(minY1Dist) < distFilter || Math.abs(minY2Dist) < distFilter){
						float minYFilter = minY1Dist < minY2Dist ? minY1Dist : minY2Dist;
						continue;
						
					}
				}
				//Check the ratio of horizontal and vertical line length .. if any of
				int firstYDistance = ((int)ptIntersection.x - vertX1)*((int)ptIntersection.x - vertX1) + ((int)ptIntersection.y - vertY1)*((int)ptIntersection.y - vertY1);
				int secondYDistance = (int) ((ptIntersection.x - vertX2)*(ptIntersection.x - vertX2) + (ptIntersection.y - vertY2)*(ptIntersection.y - vertY2));
				int minVertDistance = firstYDistance < secondYDistance ? firstYDistance : secondYDistance;
				
				// If point of intersection doesn't lie on line segment.. find the minimum distance from line segments and convert them to percentage of line segments 
				// and sum for both lines
				//First check if it is not close to reference vertical line
				double overlapPercentage = (Math.sqrt(minHorDistance)/horizontalLineLength) + (Math.sqrt(minVertDistance)/verticalLineLength) ;
				if(overlapPercentage < minOverlapPercentage){
					minOverlapPercentage = overlapPercentage;
					minOverlapIndex = j;
					minPtIntersection = ptIntersection;
				}
			}
			if(minOverlapIndex >=0){
				LineMatch matchingLine = new LineMatch();
				matchingLine.setFirstLine(horizontalLine);matchingLine.setSecondLine(m_verticalLines.get(minOverlapIndex));
				matchingLine.setPerpendicualrMatchPercentage(minOverlapPercentage);
				matchingLine.setIntersectionPoint(minPtIntersection);
				m_MatchingLines.add(matchingLine);
			}
			
		}
		//Find the most prominent line match
		double minOverlapPercentage = 2.0;
		for(int i=0;i<m_MatchingLines.size();i++){
			LineMatch matchingLine = m_MatchingLines.get(i);
			if(matchingLine.getPerpendicualrMatchPercentage() < minOverlapPercentage){
				minOverlapPercentage = matchingLine.getPerpendicualrMatchPercentage();
				m_ProminentLineMatch = matchingLine;
			}
		}
		return m_ProminentLineMatch;
	}
	
	
	public void getOverlappingRects(){
		//For each rectangle find percentage of overlap
		
		//If it overlaps with both horizontal and vertical lines it is a strong contender along with lines
		
		//If it just overlaps with one line it is a weak contender specially if line is along the diagonal
		
	}
}
