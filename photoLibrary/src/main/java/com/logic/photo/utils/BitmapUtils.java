package com.logic.photo.utils;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Environment;
import android.view.View;
import android.widget.ImageView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;

public class BitmapUtils {
	public static Bitmap convertToBitmap(byte[] data) {
		return BitmapFactory.decodeByteArray(data, 0, data.length).copy(
				Bitmap.Config.ARGB_8888, true);
	}

	/**
	 * Converts a immutable bitmap to a mutable bitmap. This operation doesn't
	 * allocates more memory that there is already allocated.
	 * 
	 * @param imgIn
	 *            - Source image. It will be released, and should not be used
	 *            more
	 * @return a copy of imgIn, but muttable.
	 */
	public static Bitmap convertToMutable(Bitmap imgIn) {
		try {
			// this is the file going to use temporally to save the bytes.
			// This file will not be a image, it will store the raw image data.
			File file = new File(Environment.getExternalStorageDirectory()
					+ File.separator + "temp.tmp");

			// Open an RandomAccessFile
			// Make sure you have added uses-permission
			// android:name="android.permission.WRITE_EXTERNAL_STORAGE"
			// into AndroidManifest.xml file
			RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");

			// get the width and height of the source bitmap.
			int width = imgIn.getWidth();
			int height = imgIn.getHeight();
			Config type = imgIn.getConfig();

			// Copy the byte to the file
			// Assume source bitmap loaded using options.inPreferredConfig =
			// Config.ARGB_8888;
			FileChannel channel = randomAccessFile.getChannel();
			MappedByteBuffer map = channel.map(MapMode.READ_WRITE, 0,
					imgIn.getRowBytes() * height);
			imgIn.copyPixelsToBuffer(map);
			// recycle the source bitmap, this will be no longer used.
			imgIn.recycle();
			System.gc();// try to force the bytes from the imgIn to be released

			// Create a new bitmap to load the bitmap again. Probably the memory
			// will be available.
			imgIn = Bitmap.createBitmap(width, height, type);
			map.position(0);
			// load it back from temporary
			imgIn.copyPixelsFromBuffer(map);
			// close the temporary file and channel , then delete that also
			channel.close();
			randomAccessFile.close();

			// delete the temp file
			file.delete();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return imgIn;
	}

	public static Bitmap getResizedBitmap(Bitmap bitmapIn, int width, int height) {
		return Bitmap.createScaledBitmap(bitmapIn, width, height, true);
	}

	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and
			// keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	public static Bitmap decodeSampledBitmapFromResource(Resources res,
			int resId, int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth,
				reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;
		return BitmapFactory.decodeResource(res, resId, options);
	}

	public static Bitmap getBlurredBitmap(Bitmap bm, int inSampleSize) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = inSampleSize;
		if (bm.hasAlpha()) {
			bm.compress(CompressFormat.PNG, 0, baos);
		} else {
			bm.compress(CompressFormat.JPEG, 100, baos);
		}
		return BitmapFactory.decodeStream(
				new ByteArrayInputStream(baos.toByteArray()), null, options);
	}
	
	public static Bitmap createBitmap(View target) {
		Bitmap b = Bitmap.createBitmap(target.getWidth(), target.getHeight(), Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(b);
		target.draw(c);
		return b;
	}
	
	public static void bindBitmapToImageView(String imageUri, ImageView imageView) {
		/* There isn't enough memory to open up more than a couple camera photos */
		/* So pre-scale the target bitmap into which the file is decoded */

		/* Get the size of the ImageView */
		int targetW = imageView.getWidth();
		int targetH = imageView.getHeight();

		/* Get the size of the image */
		BitmapFactory.Options bmOptions = new BitmapFactory.Options();
		bmOptions.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(imageUri, bmOptions);
		int photoW = bmOptions.outWidth;
		int photoH = bmOptions.outHeight;

		/* Figure out which way needs to be reduced less */
		int scaleFactor = 1;
		if ((targetW > 0) || (targetH > 0)) {
			scaleFactor = Math.min(photoW / targetW, photoH / targetH);
		}

		/* Set bitmap options to scale the image decode target */
		bmOptions.inJustDecodeBounds = false;
		bmOptions.inSampleSize = scaleFactor;
		bmOptions.inPurgeable = true;

		/* Decode the JPEG file into a Bitmap */
		Bitmap bitmap = BitmapFactory.decodeFile(imageUri, bmOptions);

		/* Associate the Bitmap to the ImageView */
		imageView.setImageBitmap(bitmap);
		imageView.setVisibility(View.VISIBLE);
	}

	public static Bitmap getMergedBitmap(Bitmap bm1, Bitmap bm2) {
		Bitmap newBitmap = null;

		int w, h;

		/*
		 * if(bm1.getWidth() >= bm2.getWidth()){ w = bm1.getWidth(); }else{ w =
		 * bm2.getWidth(); }
		 * 
		 * if(bm1.getHeight() >= bm2.getHeight()){ h = bm1.getHeight(); }else{ h
		 * = bm2.getHeight(); }
		 */
		w = bm1.getWidth();
		h = bm1.getHeight();

		Config config = bm1.getConfig();
		if (config == null) {
			config = Bitmap.Config.ARGB_8888;
		}

		newBitmap = Bitmap.createBitmap(w, h, config);
		Canvas newCanvas = new Canvas(newBitmap);
		newCanvas.drawBitmap(bm1, 0, 0, null);
		Bitmap modifiedBMP = Bitmap.createBitmap(bm2, 0, 0, w, h);
		Paint paint = new Paint();
		paint.setAlpha(128);
		newCanvas.drawBitmap(modifiedBMP, 0, 0, paint);
		return newBitmap;
	}

}
