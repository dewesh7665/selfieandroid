package com.logic.photo.utils;

import com.logic.photo.models.Line;
import com.logic.photo.models.Quadrilateral;

import org.opencv.core.Point;

import java.util.ArrayList;

public class QuadrilateralFinder {
	
	static private float findOverlapPercentage(Line horizontalLine,Line verticalLine,Point ptIntersection,float horizontalLineLength,float verticalLineLength,boolean applyCheck){
		int horx1 = (int)horizontalLine.getStartPoint().x,hory1 = (int)horizontalLine.getStartPoint().y;
		int horx2 = (int)horizontalLine.getEndPoint().x, hory2 = (int)horizontalLine.getEndPoint().y;
		//First find point of intersection
		//ptIntersection = PhotoUtils.getPointOfIntersection(horizontalLine, verticalLine);
		
		//Get distance from first point on horizontal line
		int firstXDistance =(int) ((ptIntersection.x-horx1)*((int)ptIntersection.x-horx1)+(ptIntersection.y -hory1)*(ptIntersection.y - hory1));
		//Get distance from second point on horizontal line
		int secondXDistance = (int) ((ptIntersection.x-horx2)*(ptIntersection.x-horx2)+(ptIntersection.y -hory2)*(ptIntersection.y - hory2));
		int minHorDistance = firstXDistance <secondXDistance ? firstXDistance : secondXDistance;
		if(applyCheck && minHorDistance > horizontalLineLength/2)
			return 0.0f;
		
		//Get vertical distance from point of intersection
		int vertX1 = (int)verticalLine.getStartPoint().x,vertY1 = (int)verticalLine.getStartPoint().y;
		int vertX2 = (int)verticalLine.getEndPoint().x,vertY2 = (int)verticalLine.getEndPoint().y;
		int firstYDistance = ((int)ptIntersection.x - vertX1)*((int)ptIntersection.x - vertX1) + ((int)ptIntersection.y - vertY1)*((int)ptIntersection.y - vertY1);
		int secondYDistance = (int) ((ptIntersection.x - vertX2)*(ptIntersection.x - vertX2) + (ptIntersection.y - vertY2)*(ptIntersection.y - vertY2));
		int minVertDistance = firstYDistance < secondYDistance ? firstYDistance : secondYDistance;
		if(applyCheck && minVertDistance > verticalLineLength/2)
			return 0.0f;
		//Find overlap percentage and get 
		float overlapPercentage = (float)((Math.sqrt(minHorDistance)/horizontalLineLength) + (Math.sqrt(minVertDistance)/verticalLineLength)) ;
		return overlapPercentage;
	}
	
	static public Quadrilateral findMergedQuadrilateral(Quadrilateral horizontalQuad, Quadrilateral verticalQuad){
		//First figure out the best quad out of that...
		if(horizontalQuad == null && verticalQuad == null){
			return null;
		}
		//Handle null cases
		if(horizontalQuad == null || verticalQuad == null){
			Quadrilateral baseQuad = new Quadrilateral();
			if(horizontalQuad == null)
				baseQuad = verticalQuad;
			else
				baseQuad = horizontalQuad;
			Point firstPoint = PhotoUtils.getPointOfIntersection(baseQuad.getHorLine1(), baseQuad.getVertLine1());
			Point secondPoint = PhotoUtils.getPointOfIntersection(baseQuad.getHorLine1(), baseQuad.getVertLine2());
			//Third and forth point will be points on vertical line which are away from horizontal line
			//Find the point farthest from second point on vertical line 2
			float firstVertDist =(float) ((secondPoint.x - baseQuad.getVertLine2().getStartPoint().x)* (secondPoint.x - baseQuad.getVertLine2().getStartPoint().x) + (secondPoint.y - baseQuad.getVertLine2().getStartPoint().y)* (secondPoint.y - baseQuad.getVertLine2().getStartPoint().y));
			float secondVertDist =(float) ((secondPoint.x - baseQuad.getVertLine2().getEndPoint().x)* (secondPoint.x - baseQuad.getVertLine2().getEndPoint().x) + (secondPoint.y - baseQuad.getVertLine2().getEndPoint().y)* (secondPoint.y - baseQuad.getVertLine2().getEndPoint().y));
			Point thirdPoint = new Point();
			if(firstVertDist > secondVertDist)
				thirdPoint = baseQuad.getVertLine2().getStartPoint();
			else
				thirdPoint = baseQuad.getVertLine2().getEndPoint();
			
			firstVertDist =(float) ((firstPoint.x - baseQuad.getVertLine1().getStartPoint().x)* (firstPoint.x - baseQuad.getVertLine1().getStartPoint().x) + (firstPoint.y - baseQuad.getVertLine1().getStartPoint().y)* (firstPoint.y - baseQuad.getVertLine1().getStartPoint().y));
			secondVertDist =(float) ((firstPoint.x - baseQuad.getVertLine1().getEndPoint().x)* (firstPoint.x - baseQuad.getVertLine1().getEndPoint().x) + (firstPoint.y - baseQuad.getVertLine1().getEndPoint().y)* (firstPoint.y - baseQuad.getVertLine1().getEndPoint().y));
			Point forthPoint = new Point();
			if(firstVertDist > secondVertDist)
				forthPoint = baseQuad.getVertLine1().getStartPoint();
			else
				forthPoint = baseQuad.getVertLine1().getEndPoint();
			
			baseQuad.setPt1(firstPoint);baseQuad.setPt2(secondPoint);baseQuad.setPt3(thirdPoint);baseQuad.setPt4(forthPoint);
			return baseQuad;
		}
		
		
		float horizontalOverlapSum = horizontalQuad.getLeftFirstVertOverlapPercentage()+ horizontalQuad.getRightFirstVertOverlapPercentage();
		float verticalOverlapSum = verticalQuad.getLeftFirstVertOverlapPercentage() + verticalQuad.getRightFirstVertOverlapPercentage();
		//keep one quad as a base quad -- one with minimum overlap
		Quadrilateral baseQuad = null;//horizontalOverlapSum < verticalOverlapSum ? horizontalQuad : verticalQuad;
		if(horizontalOverlapSum < verticalOverlapSum){
			baseQuad = horizontalQuad;
			//Now set the fourth side of the quad as farthest horizontal line in second quad
			Line firstHorizontalLine = verticalQuad.getVertLine1();
			Line secondHorizontalLine = verticalQuad.getVertLine2();
			Line baseHorizontalLine = baseQuad.getHorLine1();
			Line baseSecondHorizontalLine = null;
			//Check which one is closest line
			float baseCenterY = (float)(baseHorizontalLine.getStartPoint().y + baseHorizontalLine.getEndPoint().y)/2;
			float firstCenterY = (float)(firstHorizontalLine.getStartPoint().y + firstHorizontalLine.getEndPoint().y)/2;
			float secondCenterY = (float)(secondHorizontalLine.getStartPoint().y + secondHorizontalLine.getEndPoint().y)/2;
			if(Math.abs(firstCenterY - baseCenterY) > Math.abs(secondCenterY - baseCenterY))
				baseSecondHorizontalLine = firstHorizontalLine;
			else
				baseSecondHorizontalLine = secondHorizontalLine;
			baseQuad.setHorLine2(baseSecondHorizontalLine);
		}
		else{
			baseQuad = verticalQuad;
			//Now set the fourth side of the quad as farthest horizontal line in second quad
			Line firstVerticalLine = horizontalQuad.getVertLine1();
			Line secondVerticalLine = horizontalQuad.getVertLine2();
			Line baseVerticalLine = baseQuad.getHorLine1();
			Line baseSecondHorizontalLine = null;
			//Check which one is closest line
			float baseCenterX = (float)(baseVerticalLine.getStartPoint().x + baseVerticalLine.getEndPoint().x)/2;
			float firstCenterX = (float)(firstVerticalLine.getStartPoint().x + firstVerticalLine.getEndPoint().x)/2;
			float secondCenterX = (float)(secondVerticalLine.getStartPoint().x + secondVerticalLine.getEndPoint().x)/2;
			if(Math.abs(firstCenterX - baseCenterX) > Math.abs(secondCenterX - baseCenterX))
				baseSecondHorizontalLine = firstVerticalLine;
			else
				baseSecondHorizontalLine = secondVerticalLine;
			baseQuad.setHorLine2(baseSecondHorizontalLine);
		}
		//Now find points of intersections and create a quadrilateral from that
		if(baseQuad != null)
		{
			Point firstPoint = PhotoUtils.getPointOfIntersection(baseQuad.getHorLine1(), baseQuad.getVertLine1());
			Point secondPoint = PhotoUtils.getPointOfIntersection(baseQuad.getHorLine1(), baseQuad.getVertLine2());
			Point thirdPoint = PhotoUtils.getPointOfIntersection(baseQuad.getVertLine2(), baseQuad.getHorLine2());
			Point forthPoint = PhotoUtils.getPointOfIntersection(baseQuad.getHorLine2(), baseQuad.getVertLine1());
			baseQuad.setPt1(firstPoint);baseQuad.setPt2(secondPoint);baseQuad.setPt3(thirdPoint);baseQuad.setPt4(forthPoint);
		}
		return baseQuad;
	}
	
	static public ArrayList<Quadrilateral> findQuadrilaterals(ArrayList<Line> baseLines, ArrayList<Line>comparisonLines,Boolean isHorizontalMode){
		//First itreate through horizontal lines and find the lines with vertical intersection on both sides of center
	
		ArrayList<Quadrilateral> probableQuadrilaterals = new ArrayList<Quadrilateral>();
		for(Line horizontalLine: baseLines){
			int horx1 = (int)horizontalLine.getStartPoint().x,hory1 = (int)horizontalLine.getStartPoint().y;
			int horx2 = (int)horizontalLine.getEndPoint().x, hory2 = (int)horizontalLine.getEndPoint().y;
			double centerX = (horizontalLine.getStartPoint().x + horizontalLine.getEndPoint().x)/2.0f;
			double centerY =  (horizontalLine.getStartPoint().y + horizontalLine.getEndPoint().y)/2.0f;
			float horizontalLineLength = (float)Math.sqrt((hory2-hory1)*(hory2- hory1) +(horx2-horx1)*(horx2-horx1));
			
			Line leftVerticalLine = null;
			Line rightVerticalLine = null;
			float leftVerticalDistancePercentage = 2.0f;
			float rightVerticalDistancePercentage = 2.0f;
			float leftVerticalLineLength = 0.0f;
			float rightVerticalLineLength = 0.0f;
			for(Line verticalLine:comparisonLines){
				//First find point of intersection
				Point ptIntersection = PhotoUtils.getPointOfIntersection(horizontalLine, verticalLine);
				if(ptIntersection == null )
					continue;
				int vertX1 = (int)verticalLine.getStartPoint().x,vertY1 = (int)verticalLine.getStartPoint().y;
				int vertX2 = (int)verticalLine.getEndPoint().x,vertY2 = (int)verticalLine.getEndPoint().y;
				float verticalLineLength = (float)Math.sqrt((vertY2 - vertY1)*(vertY2 - vertY1) + (vertX2 - vertX1)*(vertX2 - vertX1));
				
				//Find overlap percentage and get 
				float overlapPercentage =findOverlapPercentage(horizontalLine,verticalLine,ptIntersection,horizontalLineLength,verticalLineLength,false);
				if(overlapPercentage == 0.0f)
					continue;
				//Now check whether this lies on left or on right of horizontal line
				Boolean doesthislieOnLeft = false;
				if(isHorizontalMode){
					if(ptIntersection.x < centerX)
						doesthislieOnLeft = true;
				}
				else{
					if(ptIntersection.y < centerY)
						doesthislieOnLeft= true;
				}
				if(doesthislieOnLeft){
					//This lies on left
					if(overlapPercentage < leftVerticalDistancePercentage){
						leftVerticalDistancePercentage = overlapPercentage;
						leftVerticalLine = new Line(verticalLine.getStartPoint(), verticalLine.getEndPoint());
						leftVerticalLineLength = verticalLineLength; 
					}
				}
				else{
					//This lies on right
					if(overlapPercentage < rightVerticalDistancePercentage){
						rightVerticalDistancePercentage = overlapPercentage;
						rightVerticalLine = new Line(verticalLine.getStartPoint(), verticalLine.getEndPoint());
						rightVerticalLineLength = verticalLineLength;
					}
				}
			}
			if(leftVerticalLine != null && rightVerticalLine!= null){
				//First check if both are on same side of horizontal line only then consider them
				Boolean isValidCombination = true;
				if(isHorizontalMode){
					//Lines center y difference multiplication should be positive
					float leftVerticalCenterY = (float)(leftVerticalLine.getStartPoint().y + leftVerticalLine.getEndPoint().y)/2;
					float rightVerticalCenterY = (float)(rightVerticalLine.getStartPoint().y + rightVerticalLine.getEndPoint().y)/2;
					if((leftVerticalCenterY - centerY)*(rightVerticalCenterY - centerY) < 0.0f)
						isValidCombination = false;
				}
				else{
					//Lines center y difference multiplication should be positive
					float leftVerticalCenterX = (float)(leftVerticalLine.getStartPoint().x + leftVerticalLine.getEndPoint().x)/2;
					float rightVerticalCenterX = (float)(rightVerticalLine.getStartPoint().x + rightVerticalLine.getEndPoint().x)/2;
					if((leftVerticalCenterX - centerX)*(rightVerticalCenterX - centerX) < 0.0f)
						isValidCombination = false;
				}
				//This is a probable quad
				if(isValidCombination)
				{
					Quadrilateral quad = new Quadrilateral();
					quad.setHorLine1(horizontalLine);
					quad.setVertLine1(leftVerticalLine);
					quad.setVertLine2(rightVerticalLine);
					quad.setLeftFirstVertOverlapPercentage(leftVerticalDistancePercentage);
					quad.setRightFirstVertOverlapPercentage(rightVerticalDistancePercentage);
					quad.setLeftVertLineLength(leftVerticalLineLength);
					quad.setRightVertLineLength(rightVerticalLineLength);
					quad.setFirstHorLineLength(horizontalLineLength);
					probableQuadrilaterals.add(quad);
				}
			}
		}
		//Now we have all probable Quadrilaterals. Now we need to close this quadrilateral
		//Out of all probable quadrialterals find the one with best overlap
		/*for(Quadrilateral quad: probableQuadrilaterals){
			Line firstHorizontalLine = quad.getHorLine1();
			Line firstVerticaLine = quad.getVertLine1();
			Line secondVerticalLine = quad.getVertLine2();
			float minVerticaLineLLength = quad.getLeftVertLineLength() < quad.getRightVertLineLength() ? quad.getLeftVertLineLength() : quad.getRightVertLineLength() ;
			Line bestFitHorizontalLine = null;
			float bestFitOverlap = 5.0f;
			for(Line horizontalLine : horizontalLines){
				if(firstHorizontalLine.getStartPoint() == horizontalLine.getStartPoint())
					continue;
				//Consider next horizontal line only if centres are at least separated by half the length of min vertical line length 
				float basecenterX = (float) ((firstHorizontalLine.getStartPoint().x + firstHorizontalLine.getEndPoint().x)/2.0f);
				float basecenterY = (float) ((firstHorizontalLine.getStartPoint().x + firstHorizontalLine.getEndPoint().x)/2.0f);
				
				float firstcenterX = (float) ((horizontalLine.getStartPoint().x + horizontalLine.getEndPoint().x)/2.0f);
				float firstcenterY = (float) ((horizontalLine.getStartPoint().x + horizontalLine.getEndPoint().x)/2.0f);
				
				float verticalDistance = (float)Math.sqrt((firstcenterY-basecenterY)*(firstcenterY- basecenterY) +(firstcenterX-basecenterX)*(firstcenterX-basecenterX));
				if(verticalDistance < (minVerticaLineLLength*2)/3)
					continue;
				//Else find points of intersection with vertical lines
				Point ptFirstIntersection = PhotoUtils.getPointOfIntersection(horizontalLine, firstVerticaLine);
				Point ptSecondIntersection = PhotoUtils.getPointOfIntersection(horizontalLine, secondVerticalLine);
				int horx1 = (int)horizontalLine.getStartPoint().x,hory1 = (int)horizontalLine.getStartPoint().y;
				int horx2 = (int)horizontalLine.getEndPoint().x, hory2 = (int)horizontalLine.getEndPoint().y;
				//Now find distance ratio from first and second point of intersection
				float horizontalLineLength = (float)Math.sqrt((hory2-hory1)*(hory2- hory1) +(horx2-horx1)*(horx2-horx1));
				float leftOverlapPercentage = findOverlapPercentage(horizontalLine, firstVerticaLine, ptFirstIntersection, horizontalLineLength, quad.getLeftVertLineLength(),false);
				float rightOverlapPercentage = findOverlapPercentage(horizontalLine, secondVerticalLine, ptSecondIntersection, horizontalLineLength, quad.getRightVertLineLength(),false);
				if(leftOverlapPercentage == 0.0f && rightOverlapPercentage == 0.0f)
					continue;
				if(leftOverlapPercentage == 0.0f)
					leftOverlapPercentage = 2.0f;
				if(rightOverlapPercentage == 0.0f)
					rightOverlapPercentage = 2.0f;
				if(leftOverlapPercentage + rightOverlapPercentage < bestFitOverlap){
					bestFitOverlap = leftOverlapPercentage + rightOverlapPercentage;
					bestFitHorizontalLine = new Line(horizontalLine.getStartPoint(), horizontalLine.getEndPoint());
				}
			}
			if(bestFitHorizontalLine != null){
				quad.setHorLine2(bestFitHorizontalLine);
				quad.setSecondHoriOverlapPercentage(bestFitOverlap);
				quad.setAllLinesFound(true);
			}
		}*/	
		//Now find sum of all overlaps and find the one with minimum overlap sum
		ArrayList<Quadrilateral> bestQuads = new ArrayList<Quadrilateral>();
		float sumOverlap = 20.0f;
		Quadrilateral bestQuad = null;
		for(Quadrilateral quad: probableQuadrilaterals){
			float sum = quad.getLeftFirstVertOverlapPercentage() + quad.getRightFirstVertOverlapPercentage() + quad.getSecondHoriOverlapPercentage();
			if(sum < sumOverlap)
			{
				sumOverlap = sum;
				bestQuad = quad;
			}
		}
		if(bestQuad != null)
			bestQuads.add(bestQuad);
		return bestQuads;
	}
}
