package com.logic.photo.filters;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class PaintFilter extends BaseObjectFilter{
	int resizeFactor =2;
	int repitions = 7;
	int filterSize = 9;
	
	
	public PaintFilter() {
		// TODO Auto-generated constructor stub
		super();
		mBlurIntervalArray = new int[]{10,20,30,40,50,60,70,80};
		mBlurRadius = 40;
		mEdgeRadius = 50;
	}
	
	private void applyBaseFilter(){
		Mat resizedMat = new Mat();
		Imgproc.resize(mOriginalMat, resizedMat, new Size(mOriginalMat.width()/resizeFactor, mOriginalMat.height()/resizeFactor));
		Mat tempMat = new Mat();
		tempMat.create(resizedMat.size(), resizedMat.type());
		for(int i=0;i<repitions;i++){
			Imgproc.bilateralFilter(resizedMat, tempMat, filterSize, mBlurRadius, mBlurRadius);
			Imgproc.bilateralFilter(tempMat, resizedMat, filterSize, mBlurRadius, mBlurRadius);
		}
		Imgproc.resize(resizedMat, mProcessedMat, mProcessedMat.size());
		resizedMat.release();
		tempMat.release();
	}
	
	public Mat getPaintedMat(){
		applyBaseFilter();
		return mProcessedMat;
	}
	
	@Override
	public void applyFilter(Boolean isReverseMode) {
		// TODO Auto-generated method stub
		super.applyFilter(isReverseMode);
		applyBaseFilter();
		if(isReverseMode){
			Mat reverseMaskMat = new Mat();
			reverseMaskMat.create(mMaskMat.size(), CvType.CV_8UC1);
			Core.bitwise_not(mMaskMat, reverseMaskMat);
			mOriginalMat.copyTo(mProcessedMat, reverseMaskMat);
			reverseMaskMat.release();
		}
		else{
			if(!mMaskMat.empty()){
				mOriginalMat.copyTo(mProcessedMat, mMaskMat);
			}
		}
		
		
	}
	
	@Override
	public void applyFilterOnForeground() {
		// TODO Auto-generated method stub
		applyBaseFilter();
		Mat tempMat = new Mat();
		tempMat.create(mOriginalMat.size(), mMaskMat.type());
		Core.bitwise_not(mMaskMat, tempMat);
		mProcessedMat.setTo(new Scalar(0, 0, 0), tempMat);
		tempMat.release();
	}
}
