package com.logic.photo.filters;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;

public class ColorChangeFilter extends BaseObjectFilter{
	double mRedMean=0, mGreenMean=0, mBlueMean=0;
	private Mat mTempColorMat;
	private Mat mTempBgColorMat;
	private Mat mInvertMaskMat;
	
	public ColorChangeFilter() {
		// TODO Auto-generated constructor stub
		mTempColorMat = new Mat();
		mInvertMaskMat = new Mat();
		mTempBgColorMat = new Mat();
	}
	public void setMeanColor(int r, int g, int b){
		mRedMean = r;
		mGreenMean = g;
		mBlueMean = b;
	}
	
	public void applyColorOnBackground(int r, int g, int b){
		if(mInvertMaskMat.empty())
		{
			mInvertMaskMat.create(mMaskMat.size(), mMaskMat.type());
			Core.bitwise_not(mMaskMat, mInvertMaskMat);
		}
		if(mRedMean==0 && mGreenMean==0){
			//First calculate the mean color for background image
			Scalar currentMean = Core.mean(mOriginalMat, mInvertMaskMat);
			mRedMean = currentMean.val[0];
			mGreenMean = currentMean.val[1];
			mBlueMean = currentMean.val[2];
		}
		double rFactor = 1.0, gFactor = 1.0, bFactor = 1.0;
		if(mRedMean> 0)
			rFactor = (double)r/mRedMean;
		if(mGreenMean > 0)
			gFactor = (double)g/mGreenMean;
		if(mBlueMean > 0)
			bFactor = (double)b/mBlueMean;
		
		if(mTempBgColorMat.empty()){
			mTempBgColorMat.create(mOriginalMat.size(), mOriginalMat.type());
		}
		mTempBgColorMat.setTo(new Scalar(0, 0, 0));
		mOriginalMat.copyTo(mTempBgColorMat, mInvertMaskMat);
		//Core.multip
		Core.multiply(mTempBgColorMat,new Scalar(rFactor, gFactor, bFactor,1.0), mTempBgColorMat);
		mTempBgColorMat.copyTo(mProcessedMat);
		mOriginalMat.copyTo(mProcessedMat, mMaskMat);
	}
	
	public void applyColorOnForeground(int r, int g, int b){
		double rFactor = 1.0, gFactor = 1.0, bFactor = 1.0;
		if(mRedMean> 0)
			rFactor = r/mRedMean;
		if(mGreenMean > 0)
			gFactor = g/mGreenMean;
		if(mBlueMean > 0)
			bFactor = b/mBlueMean;
		
		if(mTempColorMat.empty()){
			mTempColorMat.create(mOriginalMat.size(), mOriginalMat.type());
		}
		mOriginalMat.copyTo(mTempColorMat, mMaskMat);
		//Core.multip
		Core.multiply(mTempColorMat,new Scalar(rFactor, gFactor, bFactor,1.0), mTempColorMat);
		mTempColorMat.copyTo(mProcessedMat);
	}
	
}
