package com.logic.photo.filters;

import android.graphics.PointF;

import com.logic.photo.algorithms.GrabCutFilter;
import com.logic.photo.utils.PhotoUtils;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;

import java.util.ArrayList;

public class MultipleGrabFilter extends BaseObjectFilter{
	
	protected ArrayList<GrabCutFilter> mGrabCutObjects;
	protected GrabCutFilter mCurrentGrabCutObject;
	protected int mSelectedObjectIndex = 0;
	private Mat mDownSampledMat;
	//This is just added to lower the memory footprint
	protected Mat mTempProcessedMat;
	private int mScaleFactor = 4;
	private int mDefaultRed = 150;
	private int mDefaultGreen = 60;
	private Scalar mDefaultColor = new Scalar(mDefaultRed, mDefaultGreen, 0);
	private ArrayList<Scalar> mAppliedColors;
	private ArrayList<Scalar> mMeanColors;
	
	public MultipleGrabFilter() {
		// TODO Auto-generated constructor stub
		mGrabCutObjects = new ArrayList<GrabCutFilter>();
		mDownSampledMat = new Mat();
		mTempProcessedMat = new Mat();
		mAppliedColors = new ArrayList<Scalar>();
		mMeanColors = new ArrayList<Scalar>();
	}
	
	private void addNewObject(){
		GrabCutFilter grabCut = new GrabCutFilter();
		mGrabCutObjects.add(grabCut);
		mSelectedObjectIndex = mGrabCutObjects.size()-1;
		mCurrentGrabCutObject = grabCut;
		//Set mats in basic filter
		mCurrentGrabCutObject.setOriginalMat(mOriginalMat);
		mCurrentGrabCutObject.setColorSpaceMat(mOriginalMat);
		mCurrentGrabCutObject.setRGBImageMat(mOriginalMat);
		//Set downsampled mat to be operated
		mCurrentGrabCutObject.setMaskMatsAndDownSampledMat(mOriginalMat, mDownSampledMat);
		mAppliedColors.add(mDefaultColor);
		mMeanColors.add(new Scalar(0, 0, 0));
	}
	
	public void setCurrentObjectIndex(int currentObjectIndex){
		//Here create a new object if object at this index doesn't exist
		if(currentObjectIndex > mGrabCutObjects.size()-1){
			addNewObject();
		}
		else
		{
			mSelectedObjectIndex = currentObjectIndex;
			mCurrentGrabCutObject = mGrabCutObjects.get(mSelectedObjectIndex);
		}
	}
	
	@Override
	public void setOriginalMat(Mat mat) {
		// TODO Auto-generated method stub
		super.setOriginalMat(mat);
		//Also set downsampled mat if is empty
		if(mDownSampledMat.empty())
		{
			PhotoUtils.downSampleOriginalMat(mOriginalMat, mDownSampledMat, mScaleFactor);
		}
		
	}
	
	public void resetGrabCut(){
		mGrabCutObjects.clear();
		mDownSampledMat.release();
	}
	
	//This is for setting undo mode
	public void setErasePointContour(ArrayList<PointF> erasePoints, int radius){
		mCurrentGrabCutObject.setErasePointContour(erasePoints, radius);
	}
	
	public void setPaintPointContour(ArrayList<PointF> paintPoints,int radius){
		mCurrentGrabCutObject.setPaintPointContour(paintPoints, radius);
	}
	
	public Point getCenterPoint(){
		return mCurrentGrabCutObject.getCenterPoint();
	}
	
	private void applyRGBColor(int red,int green,int blue){
		double rFactor = 1.0, gFactor = 1.0, bFactor = 1.0;
		Scalar meanColor =mMeanColors.get(mSelectedObjectIndex);
		if(meanColor.val[0]> 0)
			rFactor = red/meanColor.val[0];
		if(meanColor.val[1] > 0)
			gFactor = green/meanColor.val[1];
		if(meanColor.val[2]> 0)
			bFactor = blue/meanColor.val[2];
		
		Mat maskMat = mCurrentGrabCutObject.getProcessedMaskMat();
		mOriginalMat.copyTo(mTempProcessedMat,maskMat);
		Core.multiply(mTempProcessedMat,new Scalar(rFactor, gFactor, bFactor,1.0), mTempProcessedMat);
		mOriginalMat.copyTo(mProcessedMat);
		mTempProcessedMat.copyTo(mProcessedMat, maskMat);
		
		//reset applied colors to new color
		mAppliedColors.set(mSelectedObjectIndex, new Scalar(red, green, blue));
	}
	
	public void applyRGBFilter(int red,int green,int blue){
		//applyRGBColor(red, green, blue);
		mAppliedColors.set(mSelectedObjectIndex, new Scalar(red, green, blue));
		updateMaskMat();
	}
	
	//Have to retain last applied color along with whether it was applied or not
	private void updateMaskMat(){
		//First initialize temp processed mat
		if(mTempProcessedMat.empty())
			mTempProcessedMat.create(mOriginalMat.size(), mOriginalMat.type());
		Mat maskMat = mCurrentGrabCutObject.getProcessedMaskMat();
		//Calculate and update mean color
		Scalar currentMean = Core.mean(mOriginalMat, maskMat);
		mMeanColors.set(mSelectedObjectIndex, currentMean);
		
		Scalar mSelectedColor = mAppliedColors.get(mSelectedObjectIndex);
		//If color has not been applied yet then use weighted mat as processed mat
		mTempProcessedMat.setTo(new Scalar(0, 0, 0));
		mProcessedMat.setTo(new Scalar(0, 0, 0));
		if(mSelectedColor.val[0]==mDefaultRed && mSelectedColor.val[1]==mDefaultGreen){
			mTempProcessedMat.setTo(mSelectedColor,maskMat);
			//mOriginalMat.copyTo(mProcessedMat, maskMat);
			Core.addWeighted(mOriginalMat, 0.4, mTempProcessedMat, 0.6, 0, mTempProcessedMat);
			mOriginalMat.copyTo(mProcessedMat);
			mTempProcessedMat.copyTo(mProcessedMat, maskMat);
		}
		else{
			//Apply RGB color and update mask mat
			applyRGBColor((int)mSelectedColor.val[0], (int)mSelectedColor.val[1], (int)mSelectedColor.val[2]);
		}
		//Now invoke the complete listener
		if(mImageFilterCompleteListener != null)
			mImageFilterCompleteListener.OnImageFilterComplete();
			
	}
	
	private void removeObjectsAtIndex(int lastUndoIndex){
		mGrabCutObjects.remove(lastUndoIndex);
	}
	
	public void undoFilter(){
		mCurrentGrabCutObject.undoFilter();
		updateMaskMat();
	}
	
	public void updateSelectedMaskMatWithLargestContour(){
		mCurrentGrabCutObject.updateProcessedMatWithLargestContour();
		updateMaskMat();
	}
	
	public void paintPointWithColor(PointF paintPoint,int radius,int rColor,int gColor,int bColor){
		mCurrentGrabCutObject.paintPointWithColor(paintPoint, radius, rColor, gColor, bColor);
		updateMaskMat();
	}
	
	public void erasePointWithRadius(PointF erasePoint, int radius){
		mCurrentGrabCutObject.erasePointWithRadius(erasePoint, radius);
		updateMaskMat();
	}
	
	public void setBackgroundPoints(ArrayList<PointF> contourPoints){
		mCurrentGrabCutObject.setBackgroundPoints(contourPoints);
		mCurrentGrabCutObject.applyFilter();
		//Now get mask mats and update them
		updateMaskMat();
	}
	
	public void setForegroundPoints(ArrayList<PointF> contourPoints){
		mCurrentGrabCutObject.setForegroundPoints(contourPoints);
		mCurrentGrabCutObject.applyFilter();
		updateMaskMat();
	}
}
