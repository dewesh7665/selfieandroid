package com.logic.photo.filters;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class BaseObjectFilter {
	protected Mat mOriginalMat;
	protected Mat mProcessedMat;
	protected Mat mMaskMat;
	protected Mat mForegroundMaskMat;
	protected int mEdgeRadius;
	protected int mBlurRadius;
	
	protected int[] mEdgeIntervalArray;
	protected int[] mBlurIntervalArray;
	
	public interface ImageFilterCompleteListener{
		public void OnImageFilterComplete();
	}
	protected ImageFilterCompleteListener mImageFilterCompleteListener;
	
	public void setImageFilterListener(ImageFilterCompleteListener listener){
		this.mImageFilterCompleteListener = listener;
	}
	
	public BaseObjectFilter(){
		mOriginalMat = new Mat();
		mProcessedMat = new Mat();
		mMaskMat = new Mat();
		mForegroundMaskMat = new Mat();
	}
	public void setOriginalMat(Mat mat){
		mOriginalMat = mat;
		if(mProcessedMat.empty())
			mProcessedMat.create(mOriginalMat.size(), CvType.CV_8UC4);
	}
	
	//This will be used purely for updating original mat for post processing the results
	public void resetOriginalMat(Mat mat){
		mOriginalMat = mat;
	}
	
	public void setMaskMat(Mat mat){
		mMaskMat = mat;
	}
	
	public Mat getProcessedMat(){
		return mProcessedMat;
	}
	
	public void setForegroundMaskMat(Mat mat){
		mForegroundMaskMat = mat;
	}
	
	public void setEdgeRadius(int edgeRadius){
		mEdgeRadius = edgeRadius;
	}
	
	public void setBlurRadius(int blurRadius){
		mBlurRadius = blurRadius;
	}
	
	public int getEdgeRadius(){
		return mEdgeRadius;
	}
	
	public int getBlurRadius(){
		return mBlurRadius;
	}
	
	
	public void applyFilter(Boolean isReverseMode){
		//Do nothing
	}
	
	public int[] getEdgeIntervalArray(){
		return mEdgeIntervalArray;
	}
	
	public int[] getBlurIntervalArray(){
		return mBlurIntervalArray;
	}
	
	public void applyFilterOnForeground(){		
		
	}
	
}
