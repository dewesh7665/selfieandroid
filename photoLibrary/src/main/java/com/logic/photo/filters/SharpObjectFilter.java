package com.logic.photo.filters;

import android.graphics.PointF;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

public class SharpObjectFilter extends BaseContourObjectFilter{
	private Boolean isSharpeningDone;
	
	public SharpObjectFilter() {
		// TODO Auto-generated constructor stub
		isSharpeningDone = false;
		mBlurIntervalArray = new int[]{5,10,15,20,25,30};
		mEdgeIntervalArray = new int[]{7,9,11,13,15,17};
		mBlurRadius = 15;
		mEdgeRadius = 11;
	}
	
	public void runPriorFilter(){
		isSharpeningDone = true;
		Mat smoothMat = new Mat();
		smoothMat.create(mOriginalMat.size(), mOriginalMat.type());
		Imgproc.GaussianBlur(mOriginalMat, smoothMat, new Size(mEdgeRadius, mEdgeRadius), 0);
		
		Core.subtract(mOriginalMat, smoothMat, mProcessedMat);
		Core.addWeighted(mOriginalMat, 1.0, mProcessedMat, 1.5, 0, mProcessedMat);		
	}
	
	private void applyMaskMat(){
		if(!isSharpeningDone){
			runPriorFilter();
		}
		//Update processed mat based on mask mat
		mOriginalMat.copyTo(mProcessedMat, mMaskMat);
		if(mImageFilterCompleteListener != null)
			mImageFilterCompleteListener.OnImageFilterComplete();
		
	}
	
	public void applyFilter(PointF selectedPoint){
		addContourPoint(selectedPoint);
		//Now apply mask
		applyMaskMat();
	}
	
	public void applyFilter(ArrayList<PointF> ptsArray){
		addContourPoints(ptsArray);
		applyMaskMat();
		//Also set Undo Points in this case
		setUndoPoints(ptsArray);
	}
	
	public void setUndoPoints(ArrayList<PointF> points){
		setUndoPointsArray(points);
	}
	
	public void applyUndoFilter(){
		if(getUndoCount() ==0)
			return;
		else{
			undoPointsArray();
			applyMaskMat();
		}
	}
	
}
