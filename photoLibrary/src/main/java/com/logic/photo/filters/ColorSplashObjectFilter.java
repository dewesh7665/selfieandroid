package com.logic.photo.filters;

import android.graphics.Bitmap;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

public class ColorSplashObjectFilter extends MultipleObjectFilter{
	
	private Mat mSplashMat; 
	private Mat mCombinedFilterMat;
	private Mat mCombinedOriginalMat;
	private Bitmap mSplashBitmap;
	private PaintFilter mPaintFilter;
	private int mBlurRadius;
	private ArrayList<Integer> mSplashUndoCount;
	
	private int[] mBlurIntervalArray = new int[]{10,20,30,40,50,60,70,80};;
	
	
	public ColorSplashObjectFilter() {
		// TODO Auto-generated constructor stub
		super();
		mSplashMat = new Mat();
		mCombinedFilterMat = new Mat();
		mCombinedOriginalMat = new Mat();
		mPaintFilter = new PaintFilter();
		mBlurRadius = 40;
		mSplashUndoCount = new ArrayList<Integer>();
		//Add 10 items and set each to 0
		int maxCount = 20;
		for(int i=0;i< maxCount;i++)
			mSplashUndoCount.add(0);
	}
	
	//set interface for mask listener
	public interface ColorMaskFilterCompleteListener{
		public void OnColorMaskFilterComplete(Bitmap bmp);
	}
	
	protected ColorMaskFilterCompleteListener mColorMaskFilterCompleteListener;
		
	public void setImageColorMaskFilterListener(ColorMaskFilterCompleteListener listener){
		this.mColorMaskFilterCompleteListener = listener;
	}
	
	public void setSplashFilter(){
		if(mSplashMat.empty()){
			mSplashMat.create(mOriginalMat.size(), CvType.CV_8UC4);
		}
		if(mCombinedFilterMat.empty())
		{
			mCombinedFilterMat.create(mOriginalMat.size(), CvType.CV_8UC1);
			//updateCombinedMaskMat();
		}
		if(mCombinedOriginalMat.empty()){
			mCombinedOriginalMat.create(mOriginalMat.size(), CvType.CV_8UC4);
		}
		mSplashMat.setTo(new Scalar(0, 0, 0, 0));
		mCombinedFilterMat.setTo(new Scalar(0));
		//mCombinedFilterMat.setTo(new Scalar(255),mCombinedMaskMat);
		//set splash bitmap
		if(mSplashBitmap == null)
			mSplashBitmap = Bitmap.createBitmap(mProcessedMaskBitmap);
		mPaintFilter.setOriginalMat(mOriginalMat);
		mPaintFilter.setBlurRadius(mBlurRadius);
		//Create combined original mat
		//Core.addWeighted(mImageMat, 0.4, mCombinedForegroundMat, 0.6, 0, mCombinedOriginalMat);
	}
	
	private void combineFilterMat(Mat processedMat){
		Mat selectedMaskMat = mProcessedMaskMats.get(mSelectedObjectIndex);
		mCombinedFilterMat.setTo(new Scalar(255), selectedMaskMat);
		
		//mCombinedOriginalMat.copyTo(mSplashMat, mCombinedFilterMat);
		mCombinedOriginalMat.setTo(new Scalar(0,0,0,0));	
		mCombinedForegroundMat.copyTo(mCombinedOriginalMat);		
		processedMat.copyTo(mSplashMat, selectedMaskMat);
		mSplashMat.copyTo(mCombinedOriginalMat,mCombinedFilterMat);
		//Now update the image bitmap to mask the original bitmap
		Utils.matToBitmap(mCombinedOriginalMat, mSplashBitmap);
		if(mColorMaskFilterCompleteListener != null){
			mColorMaskFilterCompleteListener.OnColorMaskFilterComplete(mSplashBitmap);
		}
	}
	
	@Override
	public void updateMaskMat() {
		// TODO Auto-generated method stub
		//First reset the splash mat
		if(!mSplashMat.empty())
		{
			Mat previousMaskMat = mProcessedMaskMats.get(mSelectedObjectIndex);
			Mat currentMaskMat = mCurrentGrabCutObject.getProcessedMaskMat();
			Mat tempSplashMaskMat = new Mat();
			tempSplashMaskMat.create(mSplashMat.size(), mSplashMat.type());
			mSplashMat.copyTo(tempSplashMaskMat, currentMaskMat);
			mSplashMat.setTo(new Scalar(0, 0, 0, 0), previousMaskMat);
			tempSplashMaskMat.copyTo(mSplashMat, currentMaskMat);
			
			tempSplashMaskMat.release();
		}
		//Mat diffMat = new Mat();
		//diffMat.create(currentMaskMat.size(), currentMaskMat.type());
		//Core.subtract(currentMaskMat, src2, dst)
		super.updateMaskMat();
	}
	
	public Bitmap getMixedBitmap(){
		mCombinedOriginalMat.setTo(new Scalar(0,0,0,0));	
		mCombinedForegroundMat.copyTo(mCombinedOriginalMat);
		//Core.add(mCombinedForegroundMat, mSplashMat, mCombinedOriginalMat);
		
		mSplashMat.copyTo(mCombinedOriginalMat,mCombinedFilterMat);
		//Now update the image bitmap to mask the original bitmap
		Utils.matToBitmap(mCombinedOriginalMat, mSplashBitmap);
		return mSplashBitmap;
	}
	
	public void applyPaintFilter(){
		mSplashUndoCount.set(mSelectedObjectIndex, 1);
		Mat paintMat = mPaintFilter.getPaintedMat();
		Mat rgbaPaintMat = new Mat();
		rgbaPaintMat.create(mImageMat.size(), CvType.CV_8UC4);
		Imgproc.cvtColor(paintMat, rgbaPaintMat, Imgproc.COLOR_RGB2RGBA);
		combineFilterMat(rgbaPaintMat);
		rgbaPaintMat.release();
	}
	
	public void applyColorFilter(int r,int g, int b){
		mSplashUndoCount.set(mSelectedObjectIndex, 1);
		mAppliedColors.set(mSelectedObjectIndex, new Scalar(r, g, b));
		double rFactor = 1.0, gFactor = 1.0, bFactor = 1.0;
		Scalar meanColor = mMeanColors.get(mSelectedObjectIndex);
		if(meanColor.val[0]> 0)
			rFactor = r/meanColor.val[0];
		if(meanColor.val[1] > 0)
			gFactor = g/meanColor.val[1];
		if(meanColor.val[2]> 0)
			bFactor = b/meanColor.val[2];
		if(!mTempMaskMat.empty()){
			mTempMaskMat.setTo(new Scalar(0));
		}
		mImageMat.copyTo(mTempMaskMat, mProcessedMaskMats.get(mSelectedObjectIndex));
		//Core.multip
		Core.multiply(mTempMaskMat,new Scalar(rFactor, gFactor, bFactor,1.0), mTempMaskMat);
		combineFilterMat(mTempMaskMat);
		
	}
	
	@Override
	public void undoFilter() {
		// TODO Auto-generated method stub
		if(mSplashUndoCount.get(mSelectedObjectIndex) == 1)
		{
			mSplashUndoCount.set(mSelectedObjectIndex, 0);
			
			Mat maskMat = mProcessedMaskMats.get(mSelectedObjectIndex);
			mCombinedOriginalMat.setTo(new Scalar(0,0,0,0));	
			mSplashMat.setTo(new Scalar(0, 0, 0, 0), maskMat);
			mSplashMat.copyTo(mCombinedOriginalMat,mCombinedFilterMat);
			mCombinedForegroundMat.copyTo(mCombinedOriginalMat,maskMat);
			Utils.matToBitmap(mCombinedOriginalMat, mSplashBitmap);
			if(mColorMaskFilterCompleteListener != null){
				mColorMaskFilterCompleteListener.OnColorMaskFilterComplete(mSplashBitmap);
			}
			maskMat.release();
			//Now update the image bitmap to mask the original bitmap
			//Utils.matToBitmap(mCombinedOriginalMat, mSplashBitmap);
			//return mSplashBitmap;
		}
		else
			super.undoFilter();
	}
	
	/*public void applyUndoFilter(){
		//Undo the whole filter
		Mat maskMat = mProcessedMaskMats.get(mSelectedObjectIndex);
		mSplashMat.setTo(new Scalar(0, 0, 0, 0), maskMat);
		combineFilterMat(mImageMat);
	}*/
	
	public void applyOriginalFilter(){
		//Apply original filter in selected mask mat
		combineFilterMat(mImageMat);
	}
	
	public void applyBorderOnSelectedObject(){
		Mat tempMat = new Mat();
		Mat dilatedMat = new Mat();
		tempMat.create(mOriginalMat.size(), CvType.CV_8UC1);
		dilatedMat.create(mOriginalMat.size(), CvType.CV_8UC1);
		Mat maskMat = mProcessedMaskMats.get(mSelectedObjectIndex);
		tempMat.setTo(new Scalar(255), maskMat);
		Imgproc.dilate(tempMat, dilatedMat, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(15, 15)));
		Core.subtract(dilatedMat, tempMat, dilatedMat);
		
		/*mCombinedOriginalMat.setTo(new Scalar(0,0,0,0));	
		mCombinedForegroundMat.copyTo(mCombinedOriginalMat);
		if(!mSplashMat.empty())
			mSplashMat.copyTo(mCombinedOriginalMat,mCombinedFilterMat);
		*/
		Mat borderResultMat = new Mat();
		borderResultMat.create(mImageMat.size(), mImageMat.type());		
		mCombinedForegroundMat.copyTo(borderResultMat);
		if(!mSplashMat.empty())
			mSplashMat.copyTo(borderResultMat,mCombinedFilterMat);
		
		borderResultMat.setTo(new Scalar(255, 255, 255, 255), dilatedMat);
		//Now update the image bitmap to mask the original bitmap
		
		Utils.matToBitmap(borderResultMat, mSplashBitmap);
		if(mColorMaskFilterCompleteListener != null){
			mColorMaskFilterCompleteListener.OnColorMaskFilterComplete(mSplashBitmap);
		}
		tempMat.release();
		dilatedMat.release();
		borderResultMat.release();
	}
	
	public void applyGrayFilter(){
		//apply gray filter on selected mask
		//Set this for undo effect
		mSplashUndoCount.set(mSelectedObjectIndex, 1);
		Mat grayMat = new Mat();
		grayMat.create(mOriginalMat.size(), CvType.CV_8UC1);
		Imgproc.cvtColor(mOriginalMat, grayMat, Imgproc.COLOR_RGB2GRAY);
	
		
		Mat colorGrayMat = new Mat();
		colorGrayMat.create(grayMat.size(), CvType.CV_8UC4);
		Imgproc.cvtColor(grayMat, colorGrayMat, Imgproc.COLOR_GRAY2RGBA);
		
		combineFilterMat(colorGrayMat);
		
		grayMat.release();
		colorGrayMat.release();
	}
	
	
	
	
	public void setEdgeRadius(int edgeRadius){
		//mEdgeRadius = edgeRadius;
		mPaintFilter.setEdgeRadius(edgeRadius);
	}
	
	public void setBlurRadius(int blurRadius){
		mPaintFilter.setBlurRadius(blurRadius);
	}
	
	public int getEdgeRadius(){
		return mPaintFilter.getEdgeRadius();
	}
	
	public int getBlurRadius(){
		return mPaintFilter.getBlurRadius();
	}
	
	
	public int[] getEdgeIntervalArray(){
		return mPaintFilter.getEdgeIntervalArray();
	}
	
	public int[] getBlurIntervalArray(){
		return mPaintFilter.getBlurIntervalArray();
	}
	
	
}
