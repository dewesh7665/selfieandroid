package com.logic.photo.filters;

import android.graphics.PointF;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

public class BaseContourObjectFilter extends BaseObjectFilter{
	private ArrayList<ArrayList<Point>> mArrayOfContourPoints;
	private ArrayList<Point> mContourPoints;
	private int mUndoCount;
	//private Mat mContourMaskMat;
	
	public BaseContourObjectFilter() {
		// TODO Auto-generated constructor stub
		mContourPoints = new ArrayList<Point>();
		mArrayOfContourPoints = new ArrayList<ArrayList<Point>>();
		mMaskMat = new Mat();
		mUndoCount = 0;
	}
	
	protected void addContourPoints(ArrayList<PointF> ptArray){
		initializeMaskMat();
		for(PointF point:ptArray)
		{
			Point pt = new Point(point.x, point.y);
			mContourPoints.add(pt);
			Imgproc.circle(mMaskMat, pt, mBlurRadius, new Scalar(0),-1);
		}
	}
	
	private void initializeMaskMat(){
		if(mMaskMat.empty()){
			mMaskMat.create(mOriginalMat.size(), CvType.CV_8UC1);
			mMaskMat.setTo(new Scalar(255));
		}
	}
	
	protected void addContourPoint(PointF pt){
		Point contourPt = new Point(pt.x, pt.y);
		mContourPoints.add(contourPt);
		initializeMaskMat();
		//Draw a circle of mBlurRadius for
		Imgproc.circle(mMaskMat, contourPt, mBlurRadius, new Scalar(0), -1);
	}
	
	protected void setUndoPointsArray(ArrayList<PointF> points){
		mUndoCount +=1;
		ArrayList<Point> currentPoints = new ArrayList<Point>();
		for(PointF point:points)
			currentPoints.add(new Point(point.x, point.y));
		mArrayOfContourPoints.add(currentPoints);
	}
	
	private void setMaskWithContourPoints(){
		for(Point point:mContourPoints)
			Imgproc.circle(mMaskMat, point, mBlurRadius, new Scalar(0), -1);
	}
	
	protected void reduceUndoCount(){
		
	}
	
	public int getUndoCount(){
		return mUndoCount;
	}
	
	protected void undoPointsArray(){
		//Update contour mask mat
		mContourPoints.clear();
		//Reset this to max value
		mMaskMat.setTo(new Scalar(255));
		
		if(mArrayOfContourPoints.size() == 0)
			return;
		
		for(int i=0;i< mArrayOfContourPoints.size()-1;i++){
			for(Point point:mArrayOfContourPoints.get(i)){
				mContourPoints.add(point);
			}
		}
		int lastIndex = mArrayOfContourPoints.size()-1;
		mArrayOfContourPoints.remove(lastIndex);
		//update contour mask mat
		setMaskWithContourPoints();
	}
}
