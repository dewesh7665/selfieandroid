package com.logic.photo.filters;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class ToonFilter extends BaseObjectFilter{
	int resizeFactor =2;
	int repitions = 7;
	int filterSize = 9;
	
	public ToonFilter() {
		// TODO Auto-generated constructor stub
		super();
		mBlurIntervalArray = new int[]{10,20,30,40,50,60,70};
		mEdgeIntervalArray = new int[]{30,40,50,60,70,80,90,100};
		mBlurRadius = 40;
		mEdgeRadius = 60;
	}
	
	
	/*@Override
	public void applyFilter(Boolean isReverseMode) {
		// TODO Auto-generated method stub
		//super.applyFilter();
		Mat absHorizontalMat = new Mat();
		absHorizontalMat.create(mOriginalMat.size(), CvType.CV_16S);
		
		Mat absVerticalMat = new Mat();
		absVerticalMat.create(mOriginalMat.size(), CvType.CV_16S);
		
		//Mat resultMat = new Mat();
		//resultMat.create(mOriginalMat.size(), CvType.CV_8UC3);
		
		Mat resultMatVert = new Mat();
		resultMatVert.create(mOriginalMat.size(), CvType.CV_8UC3);
		
		Mat smoothMat = new Mat();
		smoothMat.create(mOriginalMat.size(), mOriginalMat.type());
		
		Imgproc.GaussianBlur(mOriginalMat, smoothMat, new Size(5, 5), 0);
		
		Mat resultHorMat = new Mat();
		resultHorMat.create(mOriginalMat.size(), mOriginalMat.type());
		
		Imgproc.Sobel(smoothMat, absHorizontalMat, CvType.CV_16S,0, 1);
		Core.convertScaleAbs(absHorizontalMat, resultHorMat);
		
		Imgproc.Sobel(smoothMat, absVerticalMat, CvType.CV_16S,1, 0);
		Core.convertScaleAbs(absVerticalMat, resultMatVert);
		
		Core.addWeighted(resultHorMat, 0.5, resultMatVert, 0.5, 0, mProcessedMat);
		
		Core.bitwise_not(mProcessedMat, mProcessedMat);
		if(!mMaskMat.empty()){
			mOriginalMat.copyTo(mProcessedMat, mMaskMat);
		}
		
		absHorizontalMat.release();
		absVerticalMat.release();
		//resultMat.release();
		resultMatVert.release();
		smoothMat.release();
		resultHorMat.release();
		
	}*/
	
	private void applyBaseFilter(){
		Mat grayMat = new Mat();
		Mat resizedMat = new Mat();
		Mat tempMat = new Mat();
		Mat edgeMat = new Mat();
		Mat paintResizedMat = new Mat();
		 
		//First find edges
		grayMat.create(mOriginalMat.size(), CvType.CV_8UC1);
		edgeMat.create(mOriginalMat.size(), CvType.CV_8UC1);
		Imgproc.cvtColor(mOriginalMat, grayMat, Imgproc.COLOR_RGB2GRAY);
		Imgproc.medianBlur(grayMat, grayMat,9);
		Imgproc.Laplacian(grayMat, edgeMat, CvType.CV_8U,5,1,0);
		Imgproc.threshold(edgeMat, edgeMat, mEdgeRadius, 255, Imgproc.THRESH_BINARY_INV);
		Imgproc.erode(edgeMat, edgeMat, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3))); 
		 
		Imgproc.resize(mOriginalMat, resizedMat, new Size(mOriginalMat.width()/resizeFactor, mOriginalMat.height()/resizeFactor));
		tempMat.create(resizedMat.size(), resizedMat.type());
		for(int i=0;i<repitions;i++){
			Imgproc.bilateralFilter(resizedMat, tempMat, filterSize, mBlurRadius, mBlurRadius);
			Imgproc.bilateralFilter(tempMat, resizedMat, filterSize, mBlurRadius, mBlurRadius);
		}
		
		
		Imgproc.resize(resizedMat, paintResizedMat, mOriginalMat.size());
		
		paintResizedMat.copyTo(mProcessedMat, edgeMat);
		
		grayMat.release();
		edgeMat.release();
		resizedMat.release();
		paintResizedMat.release();
		tempMat.release();
	}
	@Override
	public void applyFilter(Boolean isReverseMode)  {
		// TODO Auto-generated method stub
		super.applyFilter(false);
		
		applyBaseFilter();
		if(isReverseMode){
			Mat reverseMaskMat = new Mat();
			reverseMaskMat.create(mMaskMat.size(), CvType.CV_8UC1);
			Core.bitwise_not(mMaskMat, reverseMaskMat);
			mOriginalMat.copyTo(mProcessedMat, reverseMaskMat);
			reverseMaskMat.release();
		}
		else{
			if(!mMaskMat.empty()){
				mOriginalMat.copyTo(mProcessedMat, mMaskMat);
			}
		}
		
	}
	
	@Override
	public void applyFilterOnForeground() {
		// TODO Auto-generated method stub
		applyBaseFilter();
		Mat tempMat = new Mat();
		tempMat.create(mOriginalMat.size(), mMaskMat.type());
		Core.bitwise_not(mMaskMat, tempMat);
		mProcessedMat.setTo(new Scalar(0, 0, 0), tempMat);
		tempMat.release();
	}
	
}
