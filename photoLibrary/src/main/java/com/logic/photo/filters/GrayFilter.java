package com.logic.photo.filters;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

public class GrayFilter extends BaseObjectFilter{
	
	/*public void applyFilter(){
		Mat grayMat = new Mat();
		grayMat.create(mOriginalMat.size(), CvType.CV_8UC1);
		Imgproc.cvtColor(mOriginalMat,grayMat ,Imgproc.COLOR_RGB2GRAY);
		Imgproc.cvtColor(grayMat, mProcessedMat, Imgproc.COLOR_GRAY2RGB);
		//Apply mask where the filter is not to be applied
		if(!mMaskMat.empty()){
			mOriginalMat.copyTo(mProcessedMat, mMaskMat);
		}
		grayMat.release();
	}*/
	
	void applyBaseFilter(){
		Mat grayMat = new Mat();
		grayMat.create(mOriginalMat.size(), CvType.CV_8UC1);
		Imgproc.cvtColor(mOriginalMat,grayMat ,Imgproc.COLOR_RGB2GRAY);
		Imgproc.cvtColor(grayMat, mProcessedMat, Imgproc.COLOR_GRAY2RGB);
		grayMat.release();
	}
	
	@Override
	public void applyFilter(Boolean isReverseMode) {
		// TODO Auto-generated method stub
		super.applyFilter(isReverseMode);
		applyBaseFilter();
		//Apply mask where the filter is not to be applied
		if(!mMaskMat.empty()){
			mOriginalMat.copyTo(mProcessedMat, mMaskMat);
		}
		
	}
	
	@Override
	public void applyFilterOnForeground() {
		// TODO Auto-generated method stub
		applyBaseFilter();
		Mat tempMat = new Mat();
		tempMat.create(mOriginalMat.size(), mMaskMat.type());
		Core.bitwise_not(mMaskMat, tempMat);
		mProcessedMat.setTo(new Scalar(0, 0, 0), tempMat);
		tempMat.release();
	}
}

