package com.logic.photo.filters;

import android.graphics.Bitmap;

import com.logic.photo.algorithms.HistogramFilter;

import org.opencv.android.Utils;

public class LipFilter extends HistogramFilter{
	private int mColorLevel;
	
	public LipFilter() {
		// TODO Auto-generated constructor stub
		super();
		setRangeValueForFilling(20, 20);
		mColorLevel = 50;
		super.setChannels(new int[]{0}, new int[]{45}, new float[]{0,179});
	}
	@Override
	public void setImageMatFromBitmap(Bitmap bitmap, ColorSpace colorSpace) {
		// TODO Auto-generated method stub
		//set color space etc here if required
		super.setImageMatFromBitmap(bitmap, ColorSpace.HSV_COLORSPACE);
	}
	@Override
	public void applyFilter() {
		// TODO Auto-generated method stub
		super.applyFilter();
		//Now increase whiteness etc..
		mRGBImageMat.copyTo(mForegroundMat);
		//Core.add(mForegroundMat, new Scalar(0, 0, mColorLevel), mForegroundMat, mProcessedMat);
		Utils.matToBitmap(mForegroundMat, mForegroundBitmap);
		if(mImageFilterCompleteListener != null)
			mImageFilterCompleteListener.OnImageFilterComplete(mForegroundBitmap);
		
	}
}
