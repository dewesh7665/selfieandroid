package com.logic.photo.filters;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;

public class EdgeFilter extends BaseObjectFilter{
	public void applyFilter(){
		Mat absHorizontalMat = new Mat();
		absHorizontalMat.create(mOriginalMat.size(), CvType.CV_16SC3);
		
		Mat resultMat = new Mat();
		resultMat.create(mOriginalMat.size(), CvType.CV_8UC3);
		
		//Apply sobel matrices on each of them
		Imgproc.Sobel(mOriginalMat, absHorizontalMat, CvType.CV_16S,1, 1);
		Core.convertScaleAbs(absHorizontalMat, resultMat);
		resultMat.copyTo(mProcessedMat);
		
	}
}
