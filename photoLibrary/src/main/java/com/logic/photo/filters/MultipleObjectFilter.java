package com.logic.photo.filters;

import android.graphics.Bitmap;
import android.graphics.PointF;

import com.logic.photo.algorithms.GrabCutFilter;
import com.logic.photo.utils.PhotoUtils;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

public class MultipleObjectFilter {
	//This will instantiate multiple grab cuts and will minimize memory footprint
	protected ArrayList<GrabCutFilter> mGrabCutObjects;
	protected GrabCutFilter mCurrentGrabCutObject;
	protected int mSelectedObjectIndex = 0;
	
	//These mats will be same across all instances
	//This is RGBA mat
	protected Mat mImageMat;
	protected Mat mOriginalMat;
	private Mat mDownSampledMat;
	//This is just added to lower the memory footprint
	protected Mat mTempMaskMat;
	private int mScaleFactor = 4;
	private int mBaseIntensityValue = 50;
	private int mDiscreteColorStep = 10;
	private int mBaseRedValue = 150;
	private int mBaseGreenValue = 60;
	//This is array of processed mask mats
	protected ArrayList<Mat> mProcessedMaskMats;
	protected Mat mForegroundContourMat;
	
	//This is combined foreground mask mat
	protected Mat mCombinedForegroundMat;
	protected Mat mCombinedMaskMat;
	
	private Mat mResultMat;
	protected Bitmap mProcessedMaskBitmap;
	protected ArrayList<Scalar> mMeanColors;
	protected ArrayList<Scalar> mAppliedColors;
    private Mat mFGMat;
	
	//private int mOverlapTransparency = 150;
	//private int mSelectionTransparecny = 150;
	
	public MultipleObjectFilter() {
		// TODO Auto-generated constructor stub
		mGrabCutObjects = new ArrayList<GrabCutFilter>();
		mOriginalMat = new Mat();
		mImageMat = new Mat();
		mDownSampledMat = new Mat();
		mCombinedForegroundMat = new Mat();
		mTempMaskMat = new Mat();
		mProcessedMaskMats = new ArrayList<Mat>();
		mMeanColors = new ArrayList<Scalar>();
		mAppliedColors = new ArrayList<Scalar>();
		mResultMat = new Mat();
		mCombinedMaskMat = new Mat();
		mForegroundContourMat = new Mat();
        mFGMat = new Mat();
	}
	
	//set interface 
	public interface MaskFilterCompleteListener{
		public void OnMaskFilterComplete(Bitmap bmp);
	}
	protected MaskFilterCompleteListener mMaskFilterCompleteListener;
	
	public void setImageMaskFilterListener(MaskFilterCompleteListener listener){
		this.mMaskFilterCompleteListener = listener;
	}
	
	public void setCurrentObjectIndex(int currentObjectIndex){
		mSelectedObjectIndex = currentObjectIndex;
		mCurrentGrabCutObject = mGrabCutObjects.get(mSelectedObjectIndex);
	}
	
	
	
	public void setImageMatFromBitmap(Bitmap bitmap){
		//This will be called once and then same will be used for all grabcuts
		//mImageMat is RGBA mat
		mProcessedMaskBitmap = Bitmap.createBitmap(bitmap);
		Utils.bitmapToMat(bitmap, mImageMat);
		Imgproc.cvtColor(mImageMat, mOriginalMat, Imgproc.COLOR_RGBA2RGB);
		//mImageMat.copyTo(mResultMat);
		//We dont need to set 
		PhotoUtils.downSampleOriginalMat(mOriginalMat, mDownSampledMat, mScaleFactor);
		mCombinedForegroundMat = new Mat();
		mCombinedForegroundMat.create(mImageMat.size(), CvType.CV_8UC4);
		mCombinedForegroundMat.setTo(new Scalar(new double[]{0,0,0,0}));
	}
	
	public void resetGrabCut(){
		mGrabCutObjects.clear();
		mOriginalMat.release();
		mImageMat.release();
		mDownSampledMat.release();
		mCombinedForegroundMat.release();
		mTempMaskMat.release();
		for(Mat mat:mProcessedMaskMats)
			mat.release();
		mProcessedMaskMats.clear();
		mResultMat.release();
		mCombinedMaskMat.release();
		mProcessedMaskBitmap.recycle();
		mProcessedMaskBitmap = null;
	}
	
	public void addNewObject(){
		GrabCutFilter grabCut = new GrabCutFilter();
		mGrabCutObjects.add(grabCut);
		mSelectedObjectIndex = mGrabCutObjects.size()-1;
		mCurrentGrabCutObject = grabCut;
		//Here set all the mats for added grabcut object
		//Set mats in basic filter
		mCurrentGrabCutObject.setOriginalMat(mOriginalMat);
		mCurrentGrabCutObject.setColorSpaceMat(mOriginalMat);
		mCurrentGrabCutObject.setRGBImageMat(mOriginalMat);
		
		//Create new processed mask mat and add that to list
		Mat maskMat = new Mat();
		maskMat.create(mOriginalMat.size(), CvType.CV_8UC1);
		maskMat.setTo(new Scalar(0));
		mProcessedMaskMats.add(maskMat);
		//Now set grab cut mats
		mCurrentGrabCutObject.setMaskMatsAndDownSampledMat(mOriginalMat, mDownSampledMat);
		
		//Now add and initialize mean colors
		mMeanColors.add(new Scalar(0, 0, 0));
		mAppliedColors.add(new Scalar(0, 0, 0));
		//Now update image mask mat
		/*for(int i=0;i< mGrabCutObjects.size()-1;i++){
			
		}*/
		//updateImageWithContour();
		Utils.matToBitmap(mCombinedForegroundMat, mProcessedMaskBitmap);
		if(mMaskFilterCompleteListener != null){
			mMaskFilterCompleteListener.OnMaskFilterComplete(mProcessedMaskBitmap);
		}
	}
	
	
	public void setErasePointContour(ArrayList<PointF> erasePoints, int radius){
		mCurrentGrabCutObject.setErasePointContour(erasePoints, radius);
	}
	
	public void setPaintPointContour(ArrayList<PointF> paintPoints,int radius){
		mCurrentGrabCutObject.setPaintPointContour(paintPoints, radius);
	}
	
	
	
	public Mat getOriginalMat(){
		return mOriginalMat;
	}
	
	//Update combined foreground mask mat 
	private void addBorderMask(){
		mCombinedForegroundMat.setTo(new Scalar(255, 255, 255,255), mForegroundContourMat);
	}
	
	private void setForegroundContourMat(){
		if(mForegroundContourMat.empty())
			mForegroundContourMat.create(mOriginalMat.size(), CvType.CV_8UC1);
		mForegroundContourMat.setTo(new Scalar(0));
		Mat tempMat = new Mat();
		Mat dilatedMat = new Mat();
		tempMat.create(mOriginalMat.size(), CvType.CV_8UC1);
		dilatedMat.create(mOriginalMat.size(), CvType.CV_8UC1);
		for(int i=0;i<mProcessedMaskMats.size();i++){
			if(i != mSelectedObjectIndex){
				tempMat.setTo(new Scalar(0));
				dilatedMat.setTo(new Scalar(0));
				Mat maskMat = mProcessedMaskMats.get(i);
				tempMat.setTo(new Scalar(255), maskMat);
				Imgproc.dilate(tempMat, dilatedMat, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(15, 15)));
				Core.subtract(dilatedMat, tempMat, dilatedMat);
				Core.add(dilatedMat, mForegroundContourMat, mForegroundContourMat);
			}
		}
		tempMat.release();
		dilatedMat.release();
	}
	
	
	
	private void updateImageWithContour(){
		//setForegroundContourMat();
		//addBorderMask();
		//Now update the image bitmap to mask the original bitmap
		Utils.matToBitmap(mCombinedForegroundMat, mProcessedMaskBitmap);
		if(mMaskFilterCompleteListener != null){
			mMaskFilterCompleteListener.OnMaskFilterComplete(mProcessedMaskBitmap);
		}
	}
	
	
	public void setCurrentObjectForPoint(int x,int y){
		//get color of the point and based on that put it into a certain bucket and accordingly set the selected index
		double[]pixelValues =  mCombinedForegroundMat.get(y, x);
		if(pixelValues == null)
			return;
		int r = (int)pixelValues[0], g= (int)pixelValues[1],b=(int)pixelValues[2];
		//If all are equal then it is one of the selected objects
		if(r==0 && g==0 && b==0)
			return;
		if((r-mBaseRedValue)%10==0 && (g-mBaseGreenValue)%10==0){
			int selectedObjectIndex = (r- mBaseRedValue)/mDiscreteColorStep;
			if(selectedObjectIndex < 0 || selectedObjectIndex > mGrabCutObjects.size()-1)
				return;
			if(selectedObjectIndex == mSelectedObjectIndex)
				return;
			setCurrentObjectIndex(selectedObjectIndex);
			
			//Now set rest of them to zero
			/*setForegroundContourMat();
			addBorderMask();
			
			//First set the default color for selected object border
			Mat tempMat = new Mat();
			Mat dilatedMat = new Mat();
			tempMat.create(mOriginalMat.size(), CvType.CV_8UC1);
			dilatedMat.create(mOriginalMat.size(), CvType.CV_8UC1);
			Mat maskMat = mProcessedMaskMats.get(mSelectedObjectIndex);
			tempMat.setTo(new Scalar(255), maskMat);
			Imgproc.dilate(tempMat, dilatedMat, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(15, 15)));
			Core.subtract(dilatedMat, tempMat, dilatedMat);
			mImageMat.copyTo(mCombinedForegroundMat, dilatedMat);
			//mCombinedForegroundMat.setTo(new Scalar(0, 0, 0, 255), dilatedMat);
			tempMat.release();
			dilatedMat.release();*/
			
			
			//Now update the image bitmap to mask the original bitmap
			/*Utils.matToBitmap(mCombinedForegroundMat, mProcessedMaskBitmap);
			if(mMaskFilterCompleteListener != null){
				mMaskFilterCompleteListener.OnMaskFilterComplete(mProcessedMaskBitmap);
			}*/
		}
	}
	
	
	public void updateMaskMat(){
		//Update processed mask mats
		Mat selectedMaskMat = mProcessedMaskMats.get(mSelectedObjectIndex);
		Mat maskMat = mCurrentGrabCutObject.getProcessedMaskMat();
		//Now populate foreground mat and set that to mask value/color value
		//First set selectedMaskMat which is last mask mat to all zeros to override last mask related drawings
		mCombinedForegroundMat.setTo(new Scalar(new double[]{0,0,0,0}),selectedMaskMat);
		int colorValue = mBaseIntensityValue+mSelectedObjectIndex*mDiscreteColorStep;
		int redColorValue = mBaseRedValue + mSelectedObjectIndex*mDiscreteColorStep;
		int greenColorValue = mBaseGreenValue + mSelectedObjectIndex*mDiscreteColorStep;
		mCombinedForegroundMat.setTo(new Scalar(new double[]{redColorValue,greenColorValue,0,155}),maskMat);
		//Now copy mask mat to selected mask mat
		maskMat.copyTo(selectedMaskMat);
		
		//Now add border mask
		//addBorderMask();
		
		//Now update the image bitmap to mask the original bitmap
		Utils.matToBitmap(mCombinedForegroundMat, mProcessedMaskBitmap);
		if(mMaskFilterCompleteListener != null){
			mMaskFilterCompleteListener.OnMaskFilterComplete(mProcessedMaskBitmap);
		}
		//Calculate mean color
		Scalar currentMean = Core.mean(mOriginalMat, maskMat);
		
		//currentMean = Core.mean(mOriginalMat, maskMat);
		mMeanColors.set(mSelectedObjectIndex, currentMean);
		Scalar selectedColor = mAppliedColors.get(mSelectedObjectIndex);
		mAppliedColors.set(mSelectedObjectIndex, selectedColor);
		/*if(selectedColor.val[0]> 0 && selectedColor.val[1]> 0 && selectedColor.val[2]>0)
			applyRGBColor((int)selectedColor.val[0], (int)selectedColor.val[1], (int)selectedColor.val[2]);*/
	}
	
	private void setForegroundAllObjectContourMat(){
		/*if(mForegroundContourMat.empty())
			mForegroundContourMat.create(mOriginalMat.size(), CvType.CV_8UC1);
		mForegroundContourMat.setTo(new Scalar(0));
		Mat tempMat = new Mat();
		Mat dilatedMat = new Mat();
		tempMat.create(mOriginalMat.size(), CvType.CV_8UC1);
		dilatedMat.create(mOriginalMat.size(), CvType.CV_8UC1);
		for(int i=0;i<mProcessedMaskMats.size();i++){
			tempMat.setTo(new Scalar(0));
			dilatedMat.setTo(new Scalar(0));
			Mat maskMat = mProcessedMaskMats.get(i);
			tempMat.setTo(new Scalar(255), maskMat);
			Imgproc.dilate(tempMat, dilatedMat, Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(15, 15)));
			Core.subtract(dilatedMat, tempMat, dilatedMat);
			Core.add(dilatedMat, mForegroundContourMat, mForegroundContourMat);
		}
		tempMat.release();
		dilatedMat.release();*/
	}
	
	protected void updateCombinedMaskMat(){
		if(mCombinedMaskMat.empty())
			mCombinedMaskMat.create(mOriginalMat.size(), CvType.CV_8UC1);
		mCombinedMaskMat.setTo(new Scalar(0));
		for(Mat mat:mProcessedMaskMats){
			mCombinedMaskMat.setTo(new Scalar(255), mat);
		}
	}
	
	public Mat getMaskMat(){
		if(mCombinedMaskMat.empty())
			updateCombinedMaskMat();
		return mCombinedMaskMat;
	}
	
	public Bitmap getResultBitmap(){
		updateCombinedMaskMat();
		mResultMat.setTo(new Scalar(0));
		//mImageMat.copyTo(mResultMat);
		Core.addWeighted(mImageMat, 0.5, mCombinedForegroundMat, 0.5, 0, mResultMat);
		Core.bitwise_not(mCombinedMaskMat, mCombinedMaskMat);
		mImageMat.copyTo(mResultMat,mCombinedMaskMat);
		Core.bitwise_not(mCombinedMaskMat, mCombinedMaskMat);
		//Core.subtract(mResultMat, new Scalar(50, 50, 50, 150), mResultMat, mCombinedMaskMat);
		setForegroundAllObjectContourMat();
		mResultMat.setTo(new Scalar(255, 255, 255,255), mForegroundContourMat);
		Utils.matToBitmap(mResultMat, mProcessedMaskBitmap);
		return mProcessedMaskBitmap;
	}
	
	public Bitmap getProcessedBitmap(){
		return mProcessedMaskBitmap;
	}
	
	public Scalar getCurrentMeanColor(){
		return mMeanColors.get(mSelectedObjectIndex);
	}
	
	public void applyRGBColor(int red,int green,int blue){
		mAppliedColors.set(mSelectedObjectIndex, new Scalar(red, green, blue));
		double rFactor = 1.0, gFactor = 1.0, bFactor = 1.0;
		Scalar meanColor = mMeanColors.get(mSelectedObjectIndex);
		if(meanColor.val[0]> 0)
			rFactor = red/meanColor.val[0];
		if(meanColor.val[1] > 0)
			gFactor = green/meanColor.val[1];
		if(meanColor.val[2]> 0)
			bFactor = blue/meanColor.val[2];
		if(!mTempMaskMat.empty()){
			mTempMaskMat.setTo(new Scalar(0));
		}
		mImageMat.copyTo(mTempMaskMat, mProcessedMaskMats.get(mSelectedObjectIndex));
		//Core.multip
		Core.multiply(mTempMaskMat,new Scalar(rFactor, gFactor, bFactor,1.0), mTempMaskMat);
		mTempMaskMat.copyTo(mCombinedForegroundMat, mProcessedMaskMats.get(mSelectedObjectIndex));
		
		//Now update the image bitmap to mask the original bitmap
		Utils.matToBitmap(mCombinedForegroundMat, mProcessedMaskBitmap);
		if(mMaskFilterCompleteListener != null){
			mMaskFilterCompleteListener.OnMaskFilterComplete(mProcessedMaskBitmap);
		}
	}
	
	private void removeObjectsAtIndex(int lastUndoIndex){
		mProcessedMaskMats.remove(lastUndoIndex);
		mGrabCutObjects.remove(lastUndoIndex);
		mMeanColors.remove(lastUndoIndex);
		mAppliedColors.remove(lastUndoIndex);
	}
	
	public void undoFilter(){
		mCurrentGrabCutObject.undoFilter();
		updateMaskMat();
		if(!mCurrentGrabCutObject.isUndoModeApplicable()){
			if(mSelectedObjectIndex > 0){
				//Remove all the items for last index. First item will remain selected
				if(mSelectedObjectIndex>0)
					removeObjectsAtIndex(mSelectedObjectIndex);
				mSelectedObjectIndex -=1;
				mCurrentGrabCutObject = mGrabCutObjects.get(mSelectedObjectIndex);
				
			}
				
		}
	}
	
	public Point getCenterPoint(){
		return mCurrentGrabCutObject.getCenterPoint();
	}
	
	public void updateSelectedMaskMatWithLargestContour(){
		mCurrentGrabCutObject.updateProcessedMatWithLargestContour();
		updateMaskMat();
	}
	
	public void paintPointWithColor(PointF paintPoint,int radius,int rColor,int gColor,int bColor){
		mCurrentGrabCutObject.paintPointWithColor(paintPoint, radius, rColor, gColor, bColor);
		updateMaskMat();
	}
	
	public void erasePointWithRadius(PointF erasePoint, int radius){
		mCurrentGrabCutObject.erasePointWithRadius(erasePoint, radius);
		updateMaskMat();
	}
	
	public void setBackgroundPoints(ArrayList<PointF> contourPoints){
		mCurrentGrabCutObject.setBackgroundPoints(contourPoints);
		mCurrentGrabCutObject.applyFilter();
		//Now get mask mats and update them
		updateMaskMat();
	}
	
	public void setForegroundPoints(ArrayList<PointF> contourPoints){
		mCurrentGrabCutObject.setForegroundPoints(contourPoints);
		mCurrentGrabCutObject.applyFilter();
		updateMaskMat();
	}

    public Mat getProcessedFGMat() {
        if (mFGMat.empty()) {
            mFGMat.create(mImageMat.rows(), mImageMat.cols(), mImageMat.type());
        }
        mFGMat.setTo(new Scalar(0,0,0,0));
        mImageMat.copyTo(mFGMat, mProcessedMaskMats.get(mSelectedObjectIndex));
        Rect rect = mCurrentGrabCutObject.getBoundingRect();

        Mat subMat = mFGMat.submat(rect);
        return subMat;
        //return mFGMat;
    }

    public Mat getTrimmedFGMat() {
        getProcessedFGMat();
        Rect rect = mCurrentGrabCutObject.getBoundingRect();
        Mat subMat = mFGMat.submat(rect);
        return subMat;
    }

    public Rect getBoundingRect() {
        return mCurrentGrabCutObject.getBoundingRect();
    }

    /**
     * Will return 4 channel original mat
     * @return
     */
	public Mat getImageMat() {
        return mImageMat;
    }
}
