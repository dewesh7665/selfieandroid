package com.logic.photo.filters;

import android.graphics.PointF;

import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;

public class SmoothFilter extends BaseContourObjectFilter{
	private Boolean isSmoothingDone;
	public SmoothFilter() {
		// TODO Auto-generated constructor stub
		//Set blur interval array and edge interval array
		mBlurIntervalArray = new int[]{9,11,13,15,17,19};
		mEdgeIntervalArray = new int[]{20,30,40,50,60,70,80};
		mBlurRadius = 15;
		mEdgeRadius = 50;
		isSmoothingDone = false;
	}
	
	public void runPriorFilter(){
		isSmoothingDone = true;
		Imgproc.bilateralFilter(mOriginalMat, mProcessedMat, mBlurRadius, mEdgeRadius, mEdgeRadius);
	}
	
	private void applyMaskMat(){
		if(!isSmoothingDone){
			runPriorFilter();
		}
		//Update processed mat based on mask mat
		mOriginalMat.copyTo(mProcessedMat, mMaskMat);
		if(mImageFilterCompleteListener != null)
			mImageFilterCompleteListener.OnImageFilterComplete();
		
	}
	
	public void applyFilter(PointF selectedPoint){
		addContourPoint(selectedPoint);
		//Now apply mask
		applyMaskMat();
	}
	
	public void applyFilter(ArrayList<PointF> ptsArray){
		addContourPoints(ptsArray);
		applyMaskMat();
		//Also set Undo Points in this case
		setUndoPoints(ptsArray);
	}
	
	public void setUndoPoints(ArrayList<PointF> points){
		setUndoPointsArray(points);
	}
	
	public void applyUndoFilter(){
		if(getUndoCount() ==0)
			return;
		else{
			undoPointsArray();
			applyMaskMat();
		}
	}
}
