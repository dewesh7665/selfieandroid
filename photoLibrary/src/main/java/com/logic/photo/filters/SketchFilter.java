package com.logic.photo.filters;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class SketchFilter extends BaseObjectFilter{
	
	/*public void applyFilter(){
		Mat absHorizontalMat = new Mat();
		absHorizontalMat.create(mOriginalMat.size(), CvType.CV_16S);
		
		Mat absVerticalMat = new Mat();
		absVerticalMat.create(mOriginalMat.size(), CvType.CV_16S);
		
		//Mat resultMat = new Mat();
		//resultMat.create(mOriginalMat.size(), CvType.CV_8UC3);
		
		Mat resultMatVert = new Mat();
		resultMatVert.create(mOriginalMat.size(), CvType.CV_8UC3);
		
		Mat smoothMat = new Mat();
		smoothMat.create(mOriginalMat.size(), mOriginalMat.type());
		
		Imgproc.GaussianBlur(mOriginalMat, smoothMat, new Size(5, 5), 0);
		Imgproc.Sobel(smoothMat, absHorizontalMat, CvType.CV_16S,0, 1);
		Core.convertScaleAbs(absHorizontalMat, mProcessedMat);
		
		Imgproc.Sobel(smoothMat, absVerticalMat, CvType.CV_16S,1, 0);
		Core.convertScaleAbs(absVerticalMat, resultMatVert);
		
		Core.addWeighted(mProcessedMat, 0.5, resultMatVert, 0.5, 0, mProcessedMat);
		
		//resultMat.copyTo(mProcessedMat);
		if(!mMaskMat.empty()){
			mOriginalMat.copyTo(mProcessedMat, mMaskMat);
		}
		
		absHorizontalMat.release();
		absVerticalMat.release();
		//resultMat.release();
		resultMatVert.release();
		smoothMat.release();
	}*/
	
	/*public void applyFilter(){
		Mat resizedMat = new Mat();
		Imgproc.resize(mOriginalMat, resizedMat, new Size(mOriginalMat.width()/2, mOriginalMat.height()/2));
		Mat tempMat = new Mat();
		tempMat.create(resizedMat.size(), resizedMat.type());
		for(int i=0;i<7;i++){
			Imgproc.bilateralFilter(resizedMat, tempMat, 9, 50, 50);
			Imgproc.bilateralFilter(tempMat, resizedMat, 9, 50, 50);
		}
		
		Imgproc.resize(resizedMat, mProcessedMat, mProcessedMat.size());
	}*/
	
	public void applyFilter(){
		 Mat grayMat = new Mat();
		 grayMat.create(mOriginalMat.size(), CvType.CV_8UC1);
		 
		 Mat edgeMat = new Mat();
		 edgeMat.create(mOriginalMat.size(), CvType.CV_8UC1);
		 
		 Imgproc.cvtColor(mOriginalMat, grayMat, Imgproc.COLOR_RGB2GRAY);
		 
		 Imgproc.medianBlur(grayMat, grayMat,9);
		 
		 Imgproc.Laplacian(grayMat, edgeMat, CvType.CV_8U,5,1,0);
		 
		 //Imgproc.threshold(edgeMat, edgeMat, 80, 255, Imgproc.THRESH_BINARY);
		 
		 Imgproc.cvtColor(edgeMat, mProcessedMat, Imgproc.COLOR_GRAY2RGB);
		 
	}
	
   public void applyFilterPencil(){
	   //Do sketch operation
	   Mat grayMat = new Mat();
	   grayMat.create(mOriginalMat.size(), CvType.CV_8UC1);
	   Imgproc.cvtColor(mOriginalMat, grayMat, Imgproc.COLOR_RGB2GRAY);
	   Mat smoothMat = new Mat();
	   smoothMat.create(mOriginalMat.size(), CvType.CV_8UC1);
	   Imgproc.GaussianBlur(grayMat, smoothMat, new Size(25, 25), 0);
	   Mat invertMat = new Mat();
	   invertMat.create(mOriginalMat.size(), CvType.CV_8UC1);
	   Core.bitwise_not(grayMat,invertMat);
	   Core.addWeighted(smoothMat, 0.5, invertMat, 0.5, 0, grayMat);
	   Imgproc.threshold(grayMat, grayMat, 120, 255, Imgproc.THRESH_BINARY);
	   //Imgproc.equalizeHist(grayMat, grayMat);
	   Imgproc.cvtColor(grayMat, mProcessedMat, Imgproc.COLOR_GRAY2RGB);
	   /*if(!mMaskMat.empty()){
			mOriginalMat.copyTo(mProcessedMat, mMaskMat);
		}*/
	   /*Mat smoothMat = new Mat();
	   smoothMat.create(mOriginalMat.size(), CvType.CV_8UC1);
	   Mat grayMat = new Mat();
	   grayMat.create(mOriginalMat.size(), CvType.CV_8UC1);
	   Mat invertMat = new Mat();
	   invertMat.create(mOriginalMat.size(), CvType.CV_8UC1);
	   
	   Imgproc.cvtColor(mOriginalMat, grayMat, Imgproc.COLOR_RGB2GRAY);
	   Imgproc.GaussianBlur(grayMat, smoothMat, new Size(0, 0), 5);
	   Core.bitwise_not(smoothMat, invertMat);
	   Core.addWeighted(grayMat, 0.5, invertMat, 0.5, 0, grayMat);
	   //gray dodge
	   for(int x=0;x< grayMat.cols();x++){
		   for(int y=0;y< grayMat.rows();y++){
			   grayMat.
		   }
	   }*/
	   /*Mat colorMat = new Mat();
	   Mat smoothMat = new Mat();
	   smoothMat.create(mOriginalMat.size(), mOriginalMat.type());
	   
	   Mat invertMat = new Mat();
	   invertMat.create(mOriginalMat.size(), mOriginalMat.type());
	   
	   Imgproc.GaussianBlur(mOriginalMat, smoothMat, new Size(0, 0), 5);
	   
	   Core.bitwise_not(mOriginalMat, invertMat);
	   
	   //Core.add(invertMat, smoothMat, mProcessedMat);
	   //mOriginalMat.copyTo(colorMat);
	   
	   Core.addWeighted(invertMat, 0.5, smoothMat, 0.5, 0, mProcessedMat);
	   
	 //color dodge
	  /*double buff[] = new double[(int)smoothMat.total()*smoothMat.channels()];
	  smoothMat.get(0, 0, buff);
	  //byte invertbuff[] = new byte[(int)invertMat.total()*invertMat.channels()];
	  //invertMat.get(0, 0, invertbuff);
	   for(int i=0;i<buff.length;i+=3){
		   double val0 = buff[i]/2;//(int)0.5*buff[i]+ (int)0.5*invertbuff[i];//Math.min(255,buff[i] + invertbuff[i]);
		   double val1 = buff[i+1]/2;//(int)0.5*buff[i+1]+ (int)0.5*invertbuff[i+1];;//Math.min(255,buff[i+1] + invertbuff[i+1]);
		   double val2 = buff[i+2]/2;//(int)0.5*buff[i+2]+ (int)0.5*invertbuff[i+2];// Math.min(255,buff[i+2] + invertbuff[i+2]);
		   buff[i] = val0;
		   buff[i+1] = val1;
		   buff[i+2] = val2;
		   int valO = 	(buff[i] == 255 ? 255: Math.min(255, buff[i]*255/(255- buff[i])));
		   int val1  = 	(int)(buff[i+1] == 255 ? 255: Math.min(255, buff[i+1]*255/(255- buff[i+1])));
		   int val2 = 	(int)(buff[i+2] == 255 ? 255: Math.min(255, buff[i+2]*255/(255- buff[i+2])));
		   buff[i] =(byte) valO;
		   buff[i+1] = (byte) val1;
		   buff[i+2] = (byte) val2;
		   
	   }
	   mProcessedMat.put(0, 0, buff);*/
	   
	   /*for(int x=0;x< colorMat.rows();x++){
		   for(int y=0;y< colorMat.cols();y++){
			   double[] values = invertMat.get(x, y);
			   //int[] outValues = new int[3];
			   //outValues[0] = 	(int)(values[0] == 255 ? 255: Math.min(255, values[0]*255/(255- values[0])));
			   //outValues[1] = 	(int)(values[1] == 255 ? 255: Math.min(255, values[1]*255/(255- values[1])));
			   //outValues[2] = 	(int)(values[2] == 255 ? 255: Math.min(255, values[2]*255/(255- values[2])));
			   //mProcessedMat.put(x, y, values);
			   //colorMat..
		   }
	   }*/
	   //invertMat.copyTo(mProcessedMat);
	   //smoothMat.copyTo(mProcessedMat);
	   //Imgproc.GaussianBlur(mProcessedMat, mProcessedMat, new Size(7, 7), 0);
	   
   }
}
