package com.logic.photo.filters;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class ForegroundMaskFilter extends BaseObjectFilter{
	private Mat mDilatedMat;
	private Mat mDilatedColorMat;
	//private Mat mTempDilatedMat;
	public ForegroundMaskFilter() {
		// TODO Auto-generated constructor stub
		mBlurIntervalArray = new int[]{1,3,5,7,9,11,13,15,17,19,21,23,25};
		mBlurRadius = 11;
		mDilatedMat = new Mat();
		mDilatedColorMat = new Mat();
	}
	
	//Will be always selected mode
	@Override
	public void applyFilter(Boolean isReverseMode) {
		// TODO Auto-generated method stub
		super.applyFilter(isReverseMode);
		if(mDilatedMat.empty())
			mDilatedMat.create(mOriginalMat.size(), CvType.CV_8UC1);
		if(mDilatedColorMat.empty())
			mDilatedColorMat.create(mOriginalMat.size(), mOriginalMat.type());
		//if(mTempDilatedMat.empty())
			//mTempDilatedMat.create(mOriginalMat.size(), CvType.CV_8UC1);
		mDilatedMat.setTo(new Scalar(0));
		mDilatedMat.setTo(new Scalar(255), mMaskMat);
		Imgproc.dilate(mDilatedMat, mDilatedMat, Imgproc.getStructuringElement(Imgproc.MORPH_RECT,new Size(mBlurRadius, mBlurRadius)));
		Core.subtract(mDilatedMat, mMaskMat, mDilatedMat);
		Imgproc.cvtColor(mDilatedMat, mDilatedColorMat, Imgproc.COLOR_GRAY2RGB);
		//Now set processed Mat
		mProcessedMat.setTo(new Scalar(0,0,0));
		mOriginalMat.copyTo(mProcessedMat, mMaskMat);
		Core.add(mProcessedMat, mDilatedColorMat, mProcessedMat);
		
	}
	
	@Override
	public void applyFilterOnForeground() {
		// TODO Auto-generated method stub
		applyFilter(false);
	}
}
