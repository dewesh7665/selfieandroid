package com.logic.photo.filters;

import com.logic.photo.algorithms.FloodFillFilter;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Scalar;

public class HairFilter extends FloodFillFilter{
	private int mBrightnessLevel;
	public HairFilter() {
		// TODO Auto-generated constructor stub
		super();
		setRangeValue(20, 20);
		//Set default brightness level to 20
		mBrightnessLevel = 70;
	}
	
	public void addBrightnessLevel(int sumValue){
		mBrightnessLevel = sumValue;
	}
	
	@Override
	public void applyFilter() {
		// TODO Auto-generated method stub
		super.applyFilter();
		//Now increase whiteness etc..
		mRGBImageMat.copyTo(mForegroundMat);
		
		Core.add(mForegroundMat, new Scalar(mBrightnessLevel*2, mBrightnessLevel/2, mBrightnessLevel/2), mForegroundMat, mProcessedMat);
		Utils.matToBitmap(mForegroundMat, mForegroundBitmap);
		if(mImageFilterCompleteListener != null)
			mImageFilterCompleteListener.OnImageFilterComplete(mForegroundBitmap);
		
	}
}
