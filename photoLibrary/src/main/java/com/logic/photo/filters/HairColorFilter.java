package com.logic.photo.filters;

import android.graphics.PointF;

import com.logic.photo.algorithms.GrabCutFilter;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

public class HairColorFilter extends GrabCutFilter{
	private Mat mHSVMat;
	private Mat hChannel;
	private Mat sChannel;
	//Blue color hue 148, 138, 0
	private Mat maskColorMat;
	private Mat mTempForegroundMat;
	private Scalar meanColor;
	public HairColorFilter() {
		// TODO Auto-generated constructor stub
		super();
		mHSVMat = new Mat();
		maskColorMat = new Mat();
	}
	
	@Override
	public void setImageMatFromBitmap(android.graphics.Bitmap bitmap, ColorSpace colorSpace){
		super.setImageMatFromBitmap(bitmap, colorSpace);
		if(mColorSpace != ColorSpace.HSV_COLORSPACE){
			if(mHSVMat.empty())
				Imgproc.cvtColor(mColorSpaceMat, mHSVMat, Imgproc.COLOR_RGB2HSV);
		}
		else
		{
			if(mHSVMat.empty())
				mColorSpaceMat.copyTo(mHSVMat);
			
		}
	}
	
	private void applyHSVFilter(){
		/*Mat hmat = new Mat();
		Core.extractChannel(mHSVMat, hmat, 0);
		hmat.setTo(new Scalar(75));
		Mat smat = new Mat();
		Core.extractChannel(mHSVMat, smat, 1);
		smat.setTo(new Scalar(138));
		Mat vmat = new Mat();
		Core.extractChannel(mHSVMat, vmat, 2);*/
		mHSVMat.reshape(1, mHSVMat.rows()*mHSVMat.cols()).col(0).setTo(new Scalar(8));
		mHSVMat.reshape(1, mHSVMat.rows()*mHSVMat.cols()).col(1).setTo(new Scalar(70));
	
	}
	
	/*public void applyRGBColor(int red,int green,int blue){
		//It is assumed that we
		double rFactor = 1.0, gFactor = 1.0, bFactor = 1.0;
		if(meanColor.val[0]> 0)
			rFactor = red/meanColor.val[0];
		if(meanColor.val[1] > 0)
			gFactor = green/meanColor.val[1];
		if(meanColor.val[2]> 0)
			bFactor = blue/meanColor.val[2];
		Mat tempMaskMat = new Mat();
		Core.multiply(maskColorMat,new Scalar(rFactor, gFactor, bFactor), tempMaskMat);
		Core.subtract(mRGBImageMat, maskColorMat, mForegroundMat);
		Core.add(tempMaskMat, mForegroundMat, mForegroundMat);
		tempMaskMat.release();
		Utils.matToBitmap(mForegroundMat, mForegroundBitmap);
		if(mImageFilterCompleteListener != null)
			mImageFilterCompleteListener.OnImageFilterComplete(mForegroundBitmap);
	}*/
	
	public void applyHSVColor(float hue,float sat,float intensity){
		
	}
	
	private void updateImageBitmap(){
		Utils.matToBitmap(mForegroundMat, mForegroundBitmap);
		if(mImageFilterCompleteListener != null)
			mImageFilterCompleteListener.OnImageFilterComplete(mForegroundBitmap);
	}
	
	@Override
	public void applyFilter() {
		// TODO Auto-generated method stub
		super.applyFilter();
		//applyHSVFilter();
		
		/*mRGBImageMat.copyTo(maskColorMat,mProcessedMat);
		//Get mean value of accumulated mask
		meanColor = Core.mean(mRGBImageMat, mAccumulatedMaskMat);
		//
		//Keep a temp mask mat for intermediate effects
		Mat tempMaskMat = new Mat();
		mRGBImageMat.copyTo(tempMaskMat, mProcessedMat);
		
		Core.subtract(mRGBImageMat, tempMaskMat, mForegroundMat);
		//Scalar meanColor = Core.mean(mRGBImageMat, mProcessedMat);
		Core.multiply(tempMaskMat, new Scalar(1.0,0.2, 0.4), tempMaskMat);
		
		//Imgproc.cvtColor(mHSVMat, mForegroundMat, Imgproc.COLOR_HSV2RGB);
		
		//Core.add(maskColorMat, new Scalar(-100, 10, -30), maskColorMat);
		
		//maskColorMat.setTo(new Scalar(0, 0,0));
		//We need to do bitwise not for accumulated mask mat
		//Mat aggregatedMaskMat = new Mat();
		//aggregatedMaskMat.create(mAccumulatedMaskMat.size(), CvType.CV_8UC1);
		//Core.bitwise_not(mAccumulatedMaskMat, aggregatedMaskMat);
		//Core.subtract(mAccumulatedMaskMat, new Scalar(255), mAccumulatedMaskMat);
		//mForegroundMat.setTo(new Scalar(0,0,0));
		//Imgproc.threshold(mAccumulatedMaskMat, mAccumulatedMaskMat, 10, 255, Imgproc.THRESH_BINARY);
		//Core.bitwise_not(mAccumulatedMaskMat, aggregatedMaskMat);
		//mRGBImageMat.copyTo(mForegroundMat, mAccumulatedMaskMat);
		//Imgproc.cvtColor(mProcessedMat, mForegroundMat, Imgproc.COLOR_GRAY2RGB);
		
		Core.add(tempMaskMat, mForegroundMat, mForegroundMat);
		
		tempMaskMat.release();
		//Core.add
		//tempMat.release();
		//maskColorMat.release();
		//Core.multiply(mForegroundMat, new Scalar(1.4, 1.5, 1.0), mForegroundMat);
		//Core.add(mForegroundMat, new Scalar(0, 50, 50), mForegroundMat, mProcessedMat);
		*/
		updateImageBitmap();
		
	}
	
	@Override
	public void erasePointWithRadius(PointF erasePoint, int radius) {
		// TODO Auto-generated method stub
		super.erasePointWithRadius(erasePoint, radius);
		updateImageBitmap();
	}
	
	@Override
	public void paintPointWithColor(PointF paintPoint, int radius, int rColor,
			int gColor, int bColor) {
		// TODO Auto-generated method stub
		super.paintPointWithColor(paintPoint, radius, rColor, gColor, bColor);
		updateImageBitmap();
	}
	
	@Override
	public void undoFilter() {
		// TODO Auto-generated method stub
		super.undoFilter();
		
		updateImageBitmap();
		
	}
}
