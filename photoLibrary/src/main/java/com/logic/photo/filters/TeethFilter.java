package com.logic.photo.filters;

import android.graphics.Bitmap;

import com.logic.photo.algorithms.FloodFillFilter;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Scalar;

public class TeethFilter extends FloodFillFilter{
	private int mBrightnessLevel;
	//private Mat mTransparentForegroundMat = new Mat();
	public TeethFilter() {
		// TODO Auto-generated constructor stub
		super();
		setRangeValue(20, 20);
		//Set default brightness level to 20
		mBrightnessLevel = 70;
		/*if(mTransparentForegroundMat.empty())
		{
			mTransparentForegroundMat.create(mImageMat.size(), CvType.CV_8UC4);
			mTransparentForegroundMat.setTo(new Scalar(new double[]{255,0,0,255}));
		}*/
	}
	public void addBrightnessLevel(int sumValue){
		mBrightnessLevel = sumValue;
	}
	
	@Override
	public void setImageMatFromBitmap(Bitmap bitmap, ColorSpace colorSpace) {
		// TODO Auto-generated method stub
		//set color space etc here if required
		super.setImageMatFromBitmap(bitmap, colorSpace);
	}
	
	private void updateImageBitmap(Boolean isUndoMode){
		if(isUndoMode)
			mOriginalMat.copyTo(mForegroundMat);
		else
			mRGBImageMat.copyTo(mForegroundMat);
		
		Core.add(mForegroundMat, new Scalar(mBrightnessLevel, mBrightnessLevel, mBrightnessLevel), mForegroundMat, mProcessedMat);
		Utils.matToBitmap(mForegroundMat, mForegroundBitmap);
		if(mImageFilterCompleteListener != null)
			mImageFilterCompleteListener.OnImageFilterComplete(mForegroundBitmap);
	}
	
	@Override
	public void applyFilter() {
		// TODO Auto-generated method stub
		super.applyFilter();
		//Now increase whiteness etc..
		updateImageBitmap(false);
		
	}
	
	@Override
	public void undoFilter() {
		// TODO Auto-generated method stub
		super.undoFilter();
		
		updateImageBitmap(true);
		
	}
}
