package com.logic.photo.interfaces;

/**
 * @author Dewesh Kumar
 */
public class FilterInterfaces {
    abstract static interface BaseFilterInterface {
        //public void reset();
        //public int undoFilter();
    }

    public static interface OutlineFilterListener extends BaseFilterInterface{
        public void applyOutlineFilter(int value);
    }

    public static interface PaintFilterListener extends BaseFilterInterface{
        public void applyPaintFilter(int value);
    }

    public static interface ColorFilterListener extends BaseFilterInterface{
        public void applyColorFilter(int value);
    }
}
