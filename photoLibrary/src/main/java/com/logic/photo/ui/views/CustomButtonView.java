package com.logic.photo.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logic.photolibrary.R;
/**
 * Add xmlns:auto="http://schemas.android.com/apk/res-auto" at top of root view below xmlns:android.
 * use auto:img_resource, auto:txt
 * @author Dewesh_MAC
 *
 */
public class CustomButtonView extends LinearLayout{
	private int mTextColor = R.color.secondary_text_default_material_dark;
	private int mActiveTextColor = R.color.primary_text_default_material_dark;
	//private int mLayoutWrapperId = -1;//To be used in case of progrmatically inserting custom button. Setting style programatically for
	//custom views is not supported fully
	public CustomButtonView(Context context) {
		super(context);
		init(context, null, 0, R.layout.view_custom_btn_fixed_style);
	}

	public CustomButtonView(Context context, AttributeSet attrs) {
		this(context,attrs,0);
	}

	public CustomButtonView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs, defStyle, R.layout.view_custom_btn);
	}

	private void init(Context context, AttributeSet attrs, int defStyle, int layoutResId) {
		inflate(context,layoutResId, this);
		this.setFocusable(true);
		this.setClickable(true);

		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomButton);
		int imgResource = a.getResourceId(R.styleable.CustomButton_img_resource, 0);
		String btnText = a.getString(R.styleable.CustomButton_text);
		if (imgResource == 0) {
			((ImageButton)this.findViewById(R.id.customBtnBtn)).setVisibility(GONE);
		}
		else {
			setImgResource(imgResource);
		}
		if(btnText == null)
			((TextView)this.findViewById(R.id.customBtnText)).setVisibility(View.GONE);
		else
			setText(btnText);
		updateSelection(a.getBoolean(R.styleable.CustomButton_isSelected, false));
		a.recycle();
	}

	public void setText(String text) {
		TextView textView = ((TextView)this.findViewById(R.id.customBtnText));
		textView.setVisibility(VISIBLE);
		textView.setText(text);
	}

	public void setImgResource(int imgResourceId) {
		((ImageButton)this.findViewById(R.id.customBtnBtn)).setImageResource(imgResourceId);
	}

	public void updateSelection(Boolean isSelected) {
		int visiblity = isSelected ? View.VISIBLE : View.GONE;
		int textColor = isSelected ? mActiveTextColor : mTextColor;
		this.findViewById(R.id.customBtnSelectedBorder).setVisibility(visiblity);
		((TextView)this.findViewById(R.id.customBtnText)).setTextColor(getResources().getColor(textColor));
	}

}

