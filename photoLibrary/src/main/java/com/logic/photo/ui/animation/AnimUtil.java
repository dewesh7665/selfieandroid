package com.logic.photo.ui.animation;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.view.View;

import com.logic.photolibrary.R;

public class AnimUtil {
	private static final int GLOW_ANIM_DURATION = 1000;
	
    public static void viewGlowAnimation(View targetView) {
        final ObjectAnimator objAnim =
                ObjectAnimator.ofObject(targetView,
                                        "backgroundColor", // we want to modify the backgroundColor
                                        new ArgbEvaluator(), // this can be used to interpolate between two color values
                                        targetView.getContext().getResources().getColor(R.color.selector_color), // start color defined in resources as #ff333333
                                        targetView.getContext().getResources().getColor(R.color.app_bg) // end color defined in resources as #ff3355dd
                );
        objAnim.setDuration(GLOW_ANIM_DURATION);
        objAnim.setRepeatMode(ValueAnimator.RESTART); // start reverse animation after the "growing" phase
        objAnim.setRepeatCount(0);
        objAnim.start();
    }
}
