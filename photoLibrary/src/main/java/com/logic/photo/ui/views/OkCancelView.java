package com.logic.photo.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import com.logic.photolibrary.R;

public class OkCancelView extends LinearLayout{
	public static interface OnActionListner{
		public void onOk();
		public void onCancel();
	}
	private final int[] arrActionableIds = {R.id.viewOkCancelCancel,R.id.viewOkCancelOk};
	private OnActionListner onActionListner;
	public OkCancelView(Context context) {
		this(context,null);
	}

	public OkCancelView(Context context, AttributeSet attrs) {
		this(context,attrs,0);
	}

	public OkCancelView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		inflate(context,R.layout.view_ok_cancel, this);
		for(int i =0; i < arrActionableIds.length;i++)
			((LinearLayout)this.findViewById(arrActionableIds[i])).setOnClickListener(onClickListner);
	}

	public void setOnActionListner(OnActionListner onActionListner){
		this.onActionListner = onActionListner;
	}

	private OnClickListener onClickListner = new OnClickListener() {

		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(OkCancelView.this.onActionListner == null)
				return;
			if (v.getId() == R.id.viewOkCancelCancel) {
				onActionListner.onCancel();
			}
			else if (v.getId() == R.id.viewOkCancelOk) {
				onActionListner.onOk();
			}
		}
	};

}
