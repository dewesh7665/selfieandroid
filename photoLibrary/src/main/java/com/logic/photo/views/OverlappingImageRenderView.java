package com.logic.photo.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;

public class OverlappingImageRenderView extends ImageRenderView {

	private Bitmap mOverlappingBitmap = null;
	public OverlappingImageRenderView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
	}
	
	public void setOverlappingBitmap(Bitmap overlappingBitmap){
		mOverlappingBitmap = overlappingBitmap;
		invalidate();
	}
	@Override	
	public void resetResizedBitmap(Bitmap bitmap){
		
		super.resetResizedBitmap(bitmap);
		//Reset mPath and assign it to new Path
		
		invalidate();
	}
	@Override
	protected void onDraw(final Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		if(mOverlappingBitmap != null)
			canvas.drawBitmap(mOverlappingBitmap, 0, 0, null);
		
	}
}
