package com.logic.photo.views;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.graphics.PointF;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;

import java.util.ArrayList;

public class DrawableImageRenderView extends ImageRenderView {

	public enum DrawMode {FREE_CURVE, RECTANGLE,CLOSED_CURVE,CIRCULAR_CURVE,POINT_SELECTION};
	
	private Path mPath = new Path();
	private Rect mSelectedRect = new Rect();
	private PathEffect mPathEffect = null;
	private Canvas mCanvas = null;
	private Canvas mMaskCanvas = null;
	private Bitmap mMaskBitmap = null;
	
	private Paint mPaint;
	private Paint mFillPaint;
	private Paint mMaskPaint;
	private ArrayList<PointF> mDrawnPoints;
	private ArrayList<PointF> mCurrentContourPoints;
	//Circle mode
	private Paint mCirclePaint;
	private float mCenterX;
	private float mCenterY;
	private int mRadius = 15;
	
	private float mScaleFactor = 1.0f;
	public interface OnPointDrawn{
		public void pointDrawn(float x,float y);
	}
	
	public interface OnRectangleDrawn{
		public void rectDrawn(float startX,float startY,float endX,float endY);
	}
	
	public interface onDrawContourComplete{
		public void contourDrawn(ArrayList<PointF> contourPoints);
	}
	
	//public interface 
	private OnPointDrawn pointDrawnListener = null;
	private OnRectangleDrawn rectangleDrawnListener = null;
	private onDrawContourComplete mContourDrawnListener = null;
	
	private DrawMode mDrawMode;
	
	public void setContourDrawListener(onDrawContourComplete listener){
		mContourDrawnListener = listener;
	}

    public DrawableImageRenderView(Context context) {
        this(context, null);
    }
	
	public DrawableImageRenderView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		//Setup paints and effects
		initPaintAndCanvas();
	}
	
	public ArrayList<PointF> getDrawnPoints(){
		return mDrawnPoints;
	}
	private void initPaintAndCanvas(){
		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		// mPaint.setColor(getResources().getColor(R.color.green));
		//mPaint.setColor(Color.RED);
		mPaint.setARGB(255, 255, 255, 255);
		setDafultPaint();
		//By default set draw mode to free curve
		mDrawMode = DrawMode.FREE_CURVE;
		mDrawnPoints = new ArrayList<PointF>();
		mCurrentContourPoints = new ArrayList<PointF>();
		//Set fill paint as well
		mFillPaint = new Paint();
		mFillPaint.setAntiAlias(true);
		mFillPaint.setARGB(170, 255, 255, 255);
		mFillPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		//Set Mask paint as well
		mMaskPaint = new Paint();
		mMaskPaint.setAntiAlias(true);
		mMaskPaint.setARGB(255, 255, 255, 255);
		mMaskPaint.setStyle(Paint.Style.FILL_AND_STROKE);
		
		//Set circle paint mode and place this on a random location
		mCirclePaint = new Paint();
		mCirclePaint.setColor(Color.RED);
		mCirclePaint.setAntiAlias(true);
		mCirclePaint.setStyle(Paint.Style.STROKE);
		mCirclePaint.setStrokeWidth(5);
		mCenterX = 0;
		mCenterY = 0;
	}
	public void setOnDrawListener(OnPointDrawn pointDrawListener){
		this.pointDrawnListener = pointDrawListener;
	}
	
	public void setOnRectDrawListener(OnRectangleDrawn rectDrawListener){
		this.rectangleDrawnListener = rectDrawListener;
	}
	
	public Rect getCircularMaskRect(){
		return new Rect((int)mCenterX-mRadius, (int)mCenterY- mRadius, (int)mCenterX + mRadius, (int)mCenterY + mRadius);
	}
	
	public Bitmap getMaskImage(){
		return mMaskBitmap;
	}
	private void setDafultPaint(){
		mPaint.setStyle(Paint.Style.STROKE);
		mPaint.setStrokeWidth(8);
		mPathEffect = new DashPathEffect(new float[] { 10, 2,5,2 }, 5);
		mPaint.setPathEffect(mPathEffect);
	}
	
	private void configureFillMode(){
		mMaskBitmap = Bitmap.createBitmap(mResizedBitmap.getWidth(), mResizedBitmap.getHeight(), Config.ARGB_8888);
		mMaskBitmap.eraseColor(Color.argb(255, 0, 0, 0));
		mMaskCanvas = new Canvas(mMaskBitmap);
	}
	
	public void setDrawCurveType(DrawMode drawMode){
		this.mDrawMode = drawMode;
		switch (mDrawMode) {
		case CLOSED_CURVE:
			configureFillMode();
			break;
		case CIRCULAR_CURVE:
			if(mCenterX == 0){
			mCenterX = mResizedBitmap.getWidth()/2;
			mCenterY = mResizedBitmap.getHeight()/2;
			}
			invalidate();
			break;
		default:
			break;
		}
	}
	
	
	protected void setDrawingColor(int colorValue){
		mPaint.setColor(colorValue);
	}
	
	@Override
	protected void onDraw(final Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);
		if(mCanvas == null)
			mCanvas = new Canvas(mResizedBitmap);
		if(mDrawMode== DrawMode.POINT_SELECTION)
			return;

		if(mDrawMode == DrawMode.FREE_CURVE || mDrawMode == DrawMode.CLOSED_CURVE)
			canvas.drawPath(mPath, mPaint);
		else if(mDrawMode == DrawMode.CIRCULAR_CURVE)
			canvas.drawCircle(mCenterX, mCenterY, mRadius, mCirclePaint);
		else
			canvas.drawRect(mSelectedRect, mPaint);
		
	}
	
	
	
	public void resetResizedBitmap(Bitmap bitmap){
		
		super.resetResizedBitmap(bitmap);
		//Reset mPath and assign it to new Path
		mPath.reset();
		mPath = new Path();
		if(mCanvas != null)
			mCanvas = new Canvas(mResizedBitmap);
		
		if(mMaskBitmap!= null && mDrawMode == DrawMode.CLOSED_CURVE)
		{
			mMaskBitmap.eraseColor(Color.argb(0, 0, 0, 0));
			mMaskCanvas = new Canvas(mMaskBitmap);
		}
		invalidate();
	}
	
	public void resetPath(){
		//Just clear all the previous elements from path
		mPath.reset();
		mPath = new Path();
		mPaint.setARGB(255, 255, 255, 255);
	}
	
	public void setBackgroundColorMode(){
		mPaint.setARGB(255, 0, 0, 0);
		
	}
	
	public void setForegroundColorMode(){
		mPaint.setARGB(255, 255, 255, 255);
	}
	
	public void setEraseAndPaintMode(){
		mPaint.setARGB(0, 255, 255, 255);
	}
	
	public void resetMaskBitmap(Bitmap bitmap){
		mMaskBitmap = bitmap;
		mMaskCanvas = new Canvas(mMaskBitmap);
		//Path reset will be done here as well
		mPath.reset();
		mPath = new Path();
		//Dont reset resized bitmap
		
	}
	
	private float mX, mY;
    private static final float TOUCH_TOLERANCE = 4;

    private void touch_start(float x, float y) {
    	if(mDrawMode == DrawMode.FREE_CURVE || mDrawMode == DrawMode.CLOSED_CURVE){
	        //mPath.reset();
	        mPath.moveTo(x, y);
	        mX = x;
	        mY = y;
    	}
    	else if(mDrawMode == DrawMode.CIRCULAR_CURVE){
    		mX = x;
    		mY = y;
    	}
    	else{
    		mSelectedRect.left = (int)x;
    		mSelectedRect.top = (int)y;
    		mSelectedRect.right = (int)x;
    		mSelectedRect.bottom = (int)y;
    	}
    }
    private void touch_move(float x, float y) {
    	if(mDrawMode == DrawMode.FREE_CURVE || mDrawMode == DrawMode.CLOSED_CURVE || mDrawMode == DrawMode.CIRCULAR_CURVE){
	        float dx = Math.abs(x - mX);
	        float dy = Math.abs(y - mY);
	        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
	        	if(mDrawMode == DrawMode.CIRCULAR_CURVE){
	        		mCenterX += (x - mX);
	        		mCenterY += (y - mY);
	        		mX = x;
		            mY = y;
	        	}
	        	else{
	            mPath.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
	            mX = x;
	            mY = y;
	        	}
	        	//Now set the listener
	        	if(this.pointDrawnListener != null)
	            	pointDrawnListener.pointDrawn(x, y);
	        }
    	}
    	else{
    		mSelectedRect.right = (int)x;
    		mSelectedRect.bottom = (int)y;
    	}
    }
    private void touch_up(float x, float y) {
    	if(mDrawMode == DrawMode.FREE_CURVE || mDrawMode == DrawMode.CLOSED_CURVE){
	        mPath.lineTo(mX, mY);
	        if(mDrawMode == DrawMode.CLOSED_CURVE)
	        {
	        	mPath.close();
	        	//setFillPaintMode();
	        	//Draw maskcanvas as well
	        	mMaskCanvas.drawPath(mPath, mMaskPaint);
	        	mCanvas.drawPath(mPath, mFillPaint);
	        }
	        else
	        {
		        // commit the path to our offscreen
		        //mCanvas.drawPath(mPath, mPaint);
		        if(mContourDrawnListener != null)
		        {
		        	mContourDrawnListener.contourDrawn(mCurrentContourPoints);
		        	
		        }
		        mCurrentContourPoints.clear();
	        }
	       
	        // kill this so we don't double draw
	        //mPath.reset();
    	}
    	else if(mDrawMode == DrawMode.CIRCULAR_CURVE){
    		//Do nothing
    	}
    	else{
    		mSelectedRect.right = (int)x;
    		mSelectedRect.bottom = (int)y;
    		if(this.rectangleDrawnListener != null)
    			rectangleDrawnListener.rectDrawn(mSelectedRect.left, mSelectedRect.top, mSelectedRect.right, mSelectedRect.bottom);
    	}
    }
    
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        if(mDrawMode == DrawMode.POINT_SELECTION) {
        	//Now set the listener
        	if(this.pointDrawnListener != null)
            	pointDrawnListener.pointDrawn(x, y);
        	return true;
        }
        mDrawnPoints.add(new PointF(x, y));
        //Also add to current drawn points list
        mCurrentContourPoints.add(new PointF(x, y));
        
        
        
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                touch_start(x, y);
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
            		touch_move(x, y);
            		invalidate();
                break;
            case MotionEvent.ACTION_UP:
                touch_up(x,y);
                invalidate();
                break;
        }
        return true;
    }
}
