package com.logic.photo.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.view.View;

import com.logic.photo.application.PhotoApplication;

public class ImageRenderView extends View {

	private Context mContext = null;
	protected Bitmap mOriginalBitmap;
	protected Bitmap mResizedBitmap;
	private String mImageFilePath;
	private PhotoApplication mApplication = null;
	//private Mat mOriginalMat = new Mat();
	private int mResizedOriginX = 0;
	private int mResizedOriginY = 0;
	private int mResizedWidth = 0;
	private int mResizedHeight = 0;
	
	private Boolean isScaledBitmap = false;
	public interface onImageLoaded{
		public void imageLoaded(Bitmap bmp);
	}
	private onImageLoaded onImageLoadListener = null;

    public ImageRenderView(Context context) {
        this(context, null);
    }

    public ImageRenderView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mContext = context;
		mApplication = (PhotoApplication)mContext.getApplicationContext();
		//mImageFilePath = mApplication.getImagePath();
		//initUI();
	}
	private void initUI() {
		setFocusable(true);
		setFocusableInTouchMode(true);
		// this.setOnTouchListener(this);
		this.setDrawingCacheEnabled(true);
		//Now load bitmap from external directory
		loadBitmap();
		 

	}
	
	public void setOnImageLoadListener(onImageLoaded imageloadListener){
		this.onImageLoadListener = imageloadListener;
	}
	
	public void setBitmapFromPath(String filePath){
		mImageFilePath = filePath;
		initUI();
	}
	
	public void setBitmap(Bitmap bmp){
		mOriginalBitmap = bmp;
		setFocusable(true);
		setFocusableInTouchMode(true);
		this.setDrawingCacheEnabled(true);
	}
	
	public void setScaledBitmap(Bitmap bmp){
		isScaledBitmap = true;
		mOriginalBitmap = bmp;
		mResizedBitmap = bmp;
		setFocusable(true);
		setFocusableInTouchMode(true);
		this.setDrawingCacheEnabled(true);
	}
	
	private void loadBitmap(){
		isScaledBitmap = false;
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inPreferredConfig = Bitmap.Config.ARGB_8888;
		mOriginalBitmap = BitmapFactory.decodeFile(mImageFilePath, options);
		//Initialize the opencv matrix here as well
		//Utils.bitmapToMat(mOriginalBitmap, mOriginalMat);
		//Convert original image to gray 
		//mOriginalMat.
	}
	
	public void resetResizedBitmap(Bitmap bitmap){
		this.mResizedBitmap = bitmap;
	}
	public Bitmap getResizedBitmap(){
		return mResizedBitmap;
	}
	
	public Bitmap getOriginalBitmap(){
		return mOriginalBitmap;
	}
	
	@Override
	protected void onDraw(final Canvas canvas) {
		// TODO Auto-generated method stub
		super.onDraw(canvas);

		if(isScaledBitmap){
			//Draw directly
			canvas.drawBitmap(mResizedBitmap, mResizedOriginX, mResizedOriginY, null);
			if(this.onImageLoadListener != null)
	        	onImageLoadListener.imageLoaded(mResizedBitmap);
		}
		else{
			if (this.mOriginalBitmap != null) {
				drawScaledBitmapOnCanvas(canvas);
			}
		}
	}
	
	private void drawScaledBitmapOnCanvas(Canvas canvas){
		if(mResizedBitmap == null)
		{
		int width = mOriginalBitmap.getWidth();
        int height = mOriginalBitmap.getHeight();
        float scaleWidth = ((float) canvas.getWidth()) / width;
        float scaleHeight = ((float) canvas.getHeight()) / height;
        float fScale = Math.min(scaleHeight,  scaleWidth);
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BITMAP
        matrix.postScale(fScale, fScale);
     // RECREATE THE NEW BITMAP
        mResizedBitmap = Bitmap.createBitmap(mOriginalBitmap, 0, 0, width, height, matrix, false);
        if(this.onImageLoadListener != null)
        	onImageLoadListener.imageLoaded(mResizedBitmap);
        mResizedWidth = mResizedBitmap.getWidth();
        mResizedHeight = mResizedBitmap.getHeight();
        mResizedOriginX = 0;//  (canvas.getWidth() - mResizedBitmap.getWidth()) / 2;
        //Lets align the origin top y to zero of canvas
        mResizedOriginY = 0;//(canvas.getHeight() - mResizedBitmap.getHeight()) / 2;
        
		}

        canvas.drawBitmap(mResizedBitmap, mResizedOriginX, mResizedOriginY, null);
        
	}
}
