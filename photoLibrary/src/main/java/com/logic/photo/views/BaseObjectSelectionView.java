package com.logic.photo.views;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.logic.photo.application.PhotoGrabApplication;
import com.logic.photo.filters.MultipleObjectFilter;
import com.logic.photo.filters.MultipleObjectFilter.MaskFilterCompleteListener;
import com.logic.photo.gestures.DrawableMultiTouchListener.TouchMode;
import com.logic.photo.gestures.SingleTouchListener;
import com.logic.photo.gestures.SingleTouchPanListener;
import com.logic.photo.models.InputFilterInfo;
import com.logic.photo.ui.animation.AnimUtil;
import com.logic.photo.ui.views.CustomButtonView;
import com.logic.photo.ui.views.OkCancelView;
import com.logic.photo.ui.views.OkCancelView.OnActionListner;
import com.logic.photo.views.DrawableImageRenderView.DrawMode;
import com.logic.photo.views.DrawableImageRenderView.OnPointDrawn;
import com.logic.photo.views.DrawableImageRenderView.onDrawContourComplete;
import com.logic.photo.views.ImageRenderView.onImageLoaded;
import com.logic.photolibrary.R;

import java.util.ArrayList;

import afzkl.development.colorpickerview.view.ColorPickerView;

public class BaseObjectSelectionView extends FrameLayout{
	private Context mContext = null;
	private View mCurrentView = null;
	private MaskDrawableImageRenderView mRenderView = null;

	//Undo,erase buttons
	private CustomButtonView mBtnUndo = null;
	private CustomButtonView mBtnErase = null;
	private CustomButtonView mBtnPaint = null;

	private CustomButtonView mBtnBg = null;
	private CustomButtonView mBtnFg = null;
	private CustomButtonView mBtnNewObject = null;
	private CustomButtonView mBtnSelectObject = null;

	private CustomButtonView mBtnZoomTranslate = null;
	private CustomButtonView mBtnSave = null;

	private LinearLayout mLLFilterLayout = null;
	//private ObjectColorFilter mObjectFilter = null;
	protected MultipleObjectFilter mMultiObjectFilter = null;

	private enum FilterMaskMode{FOREGROUND_MODE,ERASE_MODE,PAINT_MODE,BACKGROUND_MODE,COLOR_MODE,SELECTION_MODE};
	private FilterMaskMode mFilterMaskMode;
	private int mPaintRadius = 20;
	private int mEraseRadius = 30;

	private PhotoGrabApplication mApplication;
	//Color picker
	private ColorPickerView mColorPickerView;

	//Progress dialog to show till the processing happens
	ProgressDialog mProgressDialog = null;

	private View mSelectorTouchView;
	private int mSelectionCircleRadius = -1; //Radius of circle preview for erase, paint.
	private int mSelectionDeltaYHandCircle = -1;// dy between circle center and hand

	//set interface for image preview
	public interface ImagePreviewListener{
		public void OnImagePreview();
	}

	private ImagePreviewListener mPreviewListener;

	public void setImagePreviewListener(ImagePreviewListener listener){
		this.mPreviewListener = listener;
	}

	//Set interface for foreground drawn
	public interface ForegroundDrawListener{
		public void OnForegroundDrawn();
	}

	private ForegroundDrawListener mForegroundDrawListener;

	public void setForegroundDrawListener(ForegroundDrawListener listener){
		this.mForegroundDrawListener = listener;
	}

	//Set interface for foreground drawn
	public interface ImageMaskDrawListener{
		public void OnImageMaskDraw();
	}

	private ImageMaskDrawListener mImageMaskDrawListener;

	public void setImageMaskDrawListener(ImageMaskDrawListener listener){
		this.mImageMaskDrawListener = listener;
	}

	//Set interface for object selection
	public interface ObjectSelectionListener{
		public void OnObjectSelected();
	}

	private ObjectSelectionListener mObjectSelectedListener;

	public void setObjectSelectionListener(ObjectSelectionListener listener){
		this.mObjectSelectedListener = listener;
	}

	//Set interface for undo listener
	public interface UndoListener{
		public void OnUndoEffect();
	}

	private UndoListener mUndoListner;

	public void setUndoListener(UndoListener listener){
		this.mUndoListner = listener;
	}

    private InputFilterInfo mInputFilterInfo;
	public BaseObjectSelectionView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		mContext = context;
		LayoutInflater layoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mCurrentView = layoutInflater.inflate(R.layout.view_object_selection,this);
		mRenderView =(MaskDrawableImageRenderView)findViewById(R.id.imageView);
		mBtnUndo = (CustomButtonView)findViewById(R.id.btnUndo);
		mBtnErase = (CustomButtonView)findViewById(R.id.btnErase);
		mLLFilterLayout = (LinearLayout)findViewById(R.id.llFilterLayout);
		mBtnPaint = (CustomButtonView)findViewById(R.id.btnPaint);
		mBtnBg = (CustomButtonView)findViewById(R.id.btnBg);
		mBtnFg = (CustomButtonView)findViewById(R.id.btnFg);
		mBtnNewObject = (CustomButtonView)findViewById(R.id.btnNewObject);
		mBtnSelectObject = (CustomButtonView)findViewById(R.id.btnSelectObject);
		mBtnZoomTranslate = (CustomButtonView)findViewById(R.id.btnZoom);
		mBtnSave = (CustomButtonView)findViewById(R.id.btnSave);

		mColorPickerView = (ColorPickerView) findViewById(R.id.color_picker_view);
		mSelectorTouchView = findViewById(R.id.imgGestureHand);

		mApplication = (PhotoGrabApplication)mContext.getApplicationContext();
        mInputFilterInfo = mApplication.getCurrentInputFilterInfo();
		//mRenderView.setBitmapFromPath(mApplication.getImagePath());
        mRenderView.setBitmapFromPath(mInputFilterInfo.getOriginalBMPPath());
        //First reset object filter.. Since it is in application state
		//mApplication.resetObjectFilter();
        mInputFilterInfo.resetObjectFilter();
		//mMultiObjectFilter = mApplication.getObjectFilter();
        mMultiObjectFilter = mInputFilterInfo.getMultipleObjectFilter();

		//Initialize progress dialog
		mProgressDialog = new ProgressDialog(mContext);
		mProgressDialog.setMessage("Loading..");
		mProgressDialog.setIndeterminate(true);
		mProgressDialog.setCancelable(false);

		//mColorPickerView.setAlpha(0.7f);
		setFilter();
		setClickListeners();
		setRenderViewListeners();
		//set initial mode to hair mode
		mRenderView.setOnImageLoadListener(new onImageLoaded() {

			@Override
			public void imageLoaded(Bitmap bmp) {
				// TODO Auto-generated method stub
				mMultiObjectFilter.setImageMatFromBitmap(mRenderView.getResizedBitmap());
				//mApplication.setOriginalBitmap(mRenderView.getResizedBitmap());
                mInputFilterInfo.setOriginalBitmap(mRenderView.getResizedBitmap());
				mMultiObjectFilter.addNewObject();
			}
		});
		mFilterMaskMode = FilterMaskMode.FOREGROUND_MODE;

		((OkCancelView)findViewById(R.id.viewOkCancel)).setOnActionListner(onZoomOkCancelActionListener);
		mBtnFg.performClick();
		mSelectorTouchView.setOnTouchListener(new SingleTouchPanListener(gestureHandTouchListener));

		mSelectionCircleRadius = getResources().getDimensionPixelSize(R.dimen.selection_circle_radius);
		mSelectionDeltaYHandCircle = getResources().getDimensionPixelSize(R.dimen.selection_dy_hand_circle);
	}

	public void hideAddObjectFilter(){
		mBtnNewObject.setVisibility(View.GONE);
	}

	public void showAddObjectFilter(){
		mBtnNewObject.setVisibility(View.VISIBLE);
	}

	public void hideObjectSelectionFilter(){
		mBtnSelectObject.setVisibility(View.GONE);
	}

	public void showObjectSelectionFilter(){
		mBtnSelectObject.setVisibility(View.VISIBLE);
	}

	public void showPreviewOption(){
		mBtnSave.setVisibility(View.VISIBLE);
	}

	public void hidePreviewOption(){
		mBtnSave.setVisibility(View.GONE);
	}

	public MaskDrawableImageRenderView getRenderView(){
		return mRenderView;
	}

	public void setFilterLayoutVisibility(int visibility){
		mLLFilterLayout.setVisibility(visibility);
	}

	private void setFilter(){

		mMultiObjectFilter.setImageMaskFilterListener(new MaskFilterCompleteListener() {

			@Override
			public void OnMaskFilterComplete(Bitmap bmp) {
				// TODO Auto-generated method stub
				if(mImageMaskDrawListener != null){
					mImageMaskDrawListener.OnImageMaskDraw();
				}
				else
					mRenderView.setForegroundMaskImage(bmp,255);
			}
		});
	}

	private void setClickListeners(){

		mBtnUndo.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// TODO Auto-generated method stub
				mSelectorTouchView.setVisibility(View.GONE);
				if(mUndoListner != null)
					mUndoListner.OnUndoEffect();
				//Also reset previously drawn contour path
				mRenderView.resetPath();
				mMultiObjectFilter.undoFilter();

			}
		});

		mBtnErase.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mSelectorTouchView.setVisibility(View.VISIBLE);
				bottomToolBarClickChangeUI(v);
				mFilterMaskMode = FilterMaskMode.ERASE_MODE;
				mRenderView.setTouchMode(TouchMode.DRAW_MODE);
				mRenderView.setEraseAndPaintMode();
			}
		});

		mBtnPaint.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mSelectorTouchView.setVisibility(View.VISIBLE);
				bottomToolBarClickChangeUI(v);
				mFilterMaskMode = FilterMaskMode.PAINT_MODE;
				mRenderView.setTouchMode(TouchMode.DRAW_MODE);
				mRenderView.setEraseAndPaintMode();
			}
		});


		mBtnBg.setOnClickListener(new OnClickListener() {		

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mSelectorTouchView.setVisibility(View.GONE);
				bottomToolBarClickChangeUI(v);
				mFilterMaskMode = FilterMaskMode.BACKGROUND_MODE;
				mRenderView.setTouchMode(TouchMode.DRAW_MODE);
				mRenderView.setBackgroundColorMode();
			}
		});

		mBtnFg.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mSelectorTouchView.setVisibility(View.GONE);
				bottomToolBarClickChangeUI(v);
				mFilterMaskMode = FilterMaskMode.FOREGROUND_MODE;
				mRenderView.setTouchMode(TouchMode.DRAW_MODE);
				mRenderView.setDrawCurveType(DrawMode.FREE_CURVE);
				mRenderView.setForegroundColorMode();
			}
		});

		mBtnNewObject.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFilterMaskMode = FilterMaskMode.FOREGROUND_MODE;
				mRenderView.setTouchMode(TouchMode.DRAW_MODE);
				mRenderView.setDrawCurveType(DrawMode.FREE_CURVE);
				//Also reset previously drawn contour path
				mRenderView.resetPath();

				mMultiObjectFilter.addNewObject();
			}
		});

		mBtnSelectObject.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mFilterMaskMode = FilterMaskMode.SELECTION_MODE;
				mRenderView.setTouchMode(TouchMode.DRAW_MODE);
				mRenderView.setDrawCurveType(DrawMode.POINT_SELECTION);
			}
		});


		mBtnZoomTranslate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				updateButtonSelection(v);
				updateUIOnZoomSelection(v, true);
				mRenderView.setTouchMode(TouchMode.ZOOM_TRANSLATE_MODE);
			}
		});

		mBtnSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//get both resized bitmap and foreground bitmap and merge them 
				//Intent intent = new Intent(ImageProcessingActivity.this,ImagePreviewActivity.class);
				//startActivity(intent);
				if(mPreviewListener != null){
					mPreviewListener.OnImagePreview();
				}
			}
		});
	}

	private void setRenderViewListeners(){
		mRenderView.setOnDrawListener(new OnPointDrawn() {

			@Override
			public void pointDrawn(float x, float y) {
				// TODO Auto-generated method stub
				switch (mFilterMaskMode) {
				case PAINT_MODE:
					//mMultiObjectFilter.paintPointWithColor(new PointF(x, y), mPaintRadius, 0, 0, 0);
					break;

				case ERASE_MODE:
					//mMultiObjectFilter.erasePointWithRadius(new PointF(x, y), mEraseRadius);
					break;

				case SELECTION_MODE:
					mMultiObjectFilter.setCurrentObjectForPoint((int)x, (int)y);
					//Call listener to show contour around selection
					if(mObjectSelectedListener != null)
						mObjectSelectedListener.OnObjectSelected();
					break;

				default:
					break;
				}

			}
		});

		mRenderView.setContourDrawListener(new onDrawContourComplete() {

			@Override
			public void contourDrawn(ArrayList<PointF> contourPoints) {
				// TODO Auto-generated method stub
				switch (mFilterMaskMode) {
				case FOREGROUND_MODE:

					//mObjectFilter.setImageMatFromBitmap(mRenderView.getResizedBitmap(), ColorSpace.RGB_COLORSPACE);
					mMultiObjectFilter.setForegroundPoints(contourPoints);
					if(mForegroundDrawListener != null)
						mForegroundDrawListener.OnForegroundDrawn();
					//mObjectFilter.applyFilter();
					break;

				case BACKGROUND_MODE:
					mMultiObjectFilter.setBackgroundPoints(contourPoints);
					//mObjectFilter.applyFilter();
					break;

				case PAINT_MODE:
					mMultiObjectFilter.setPaintPointContour(contourPoints, mPaintRadius);
					break;

				case ERASE_MODE:
					mMultiObjectFilter.setErasePointContour(contourPoints, mEraseRadius);
					break;

				default:
					break;
				}


			}
		});
	}

	private int mCurrentSelectionId = -1;
	private void updateButtonSelection(View view) {
		if (mCurrentSelectionId != -1)
			((CustomButtonView)findViewById(mCurrentSelectionId)).updateSelection(false);
		mCurrentSelectionId = view.getId();
		((CustomButtonView)findViewById(mCurrentSelectionId)).updateSelection(true);
	}

	/**
	 * Disable bottom bar when zoom/translate is selected
	 */
	private void updateUIOnZoomSelection(View view, Boolean isZoomSelected) {
		if (isZoomSelected) {
			findViewById(R.id.llTopBarStatic).setVisibility(VISIBLE);
			((TextView)findViewById(R.id.tvHeaderText)).setText("Pan and Zoom");
			((OkCancelView)findViewById(R.id.viewOkCancel)).setVisibility(VISIBLE);
			mLLFilterLayout.setVisibility(GONE);
			findViewById(R.id.llTopToolBar).setVisibility(View.GONE);
		}
		else {
			mLLFilterLayout.setVisibility(VISIBLE);
			findViewById(R.id.llTopToolBar).setVisibility(View.VISIBLE);
			findViewById(R.id.llTopBarStatic).setVisibility(GONE);
			((OkCancelView)findViewById(R.id.viewOkCancel)).setVisibility(GONE);
		}
	}

	private void bottomToolBarClickChangeUI(View view) {
		toggleToolBarVisiblity(true);
		updateButtonSelection(view);
		AnimUtil.viewGlowAnimation(view);
	}

	private void toggleToolBarVisiblity(Boolean isToBeShown) {
		if (isToBeShown) {
			findViewById(R.id.llTopDoneToolBar).setVisibility(View.GONE);
			findViewById(R.id.llTopToolBar).setVisibility(View.VISIBLE);
		}
		else {
			findViewById(R.id.llTopToolBar).setVisibility(View.GONE);
			findViewById(R.id.llTopDoneToolBar).setVisibility(View.VISIBLE);
		}
	}

	OnActionListner onZoomOkCancelActionListener = new OnActionListner() {

		@Override
		public void onOk() {
			// TODO Auto-generated method stub
			updateUIOnZoomSelection(null, false);
			mRenderView.setTouchMode(TouchMode.DRAW_MODE);
		}

		@Override
		public void onCancel() {
			// TODO Auto-generated method stub
			updateUIOnZoomSelection(null, false);
			mRenderView.setTouchMode(TouchMode.DRAW_MODE);
		}
	};

	SingleTouchListener.OnTouchChanged gestureHandTouchListener = new SingleTouchListener.OnTouchChanged() {
		@Override
		public void onTouchChanged(SingleTouchListener.TouchTransformInfo touchTransInfo) {
			switch (mFilterMaskMode) {
				case PAINT_MODE:
					mRenderView.drawSelectionCircle(touchTransInfo);
					mMultiObjectFilter.paintPointWithColor(new PointF(touchTransInfo.currentPoint.x, touchTransInfo.currentPoint.y - mSelectionDeltaYHandCircle), mSelectionCircleRadius, 0, 0, 0);
					break;

				case ERASE_MODE:
					mRenderView.drawSelectionCircle(touchTransInfo);
					//mMultiObjectFilter.erasePointWithRadius(new PointF(x, y), mEraseRadius);
					mMultiObjectFilter.erasePointWithRadius(new PointF(touchTransInfo.currentPoint.x, touchTransInfo.currentPoint.y - mSelectionDeltaYHandCircle), mSelectionCircleRadius);
					break;
			}
		}
	};
}
