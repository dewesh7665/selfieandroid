package com.logic.photo.views;

import android.content.Context;
import android.util.AttributeSet;

import com.logic.photo.gestures.MultiTouchListener;

public class ZoomPanRenderView extends ImageRenderView {

	
	private float mScaleFactor = 1.0f;
	private MultiTouchListener multiTouchListener = new MultiTouchListener();
	//private ScaleGestureDetector mScaleDetector;
	public ZoomPanRenderView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		//mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
		multiTouchListener.isRotateEnabled = false;
		this.setOnTouchListener(multiTouchListener);
	}
	
	
}
