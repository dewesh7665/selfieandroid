package com.logic.photo.views;

import android.content.Context;
import android.util.AttributeSet;

import com.logic.photo.gestures.DrawableMultiTouchListener;
import com.logic.photo.gestures.DrawableMultiTouchListener.TouchMode;

public class DrawableZoomPanRenderView extends DrawableImageRenderView{
	private DrawableMultiTouchListener drawableListener;
    public DrawableZoomPanRenderView(Context context) {
        this(context, null);
    }

	public DrawableZoomPanRenderView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		drawableListener = new DrawableMultiTouchListener();
		drawableListener.isRotateEnabled = false;
		drawableListener.setSelectedMode(TouchMode.ZOOM_TRANSLATE_MODE);
		this.setOnTouchListener(drawableListener);
	}
	
	public void setTouchMode(TouchMode touchMode){
		drawableListener.setSelectedMode(touchMode);
	}
}
