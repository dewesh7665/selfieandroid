package com.logic.photo.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;

import com.logic.photo.gestures.SingleTouchListener;
import com.logic.photolibrary.R;

public class MaskDrawableImageRenderView extends DrawableZoomPanRenderView {

    private Bitmap mForegroundBitmap = null;
    private Paint mAlphaChannelPaint = new Paint();
    private int mSelectionCircleX = -1;
    private int mSelectionCircleY = -1;
    private int mSelectionCircleRadius = -1; //Radius of circle preview for erase, paint.
    private int mSelectionDeltaYHandCircle = -1;// dy between circle center and hand
    protected Paint mCirclePaint;

    public MaskDrawableImageRenderView(Context context) {
        this(context, null);
    }

    public MaskDrawableImageRenderView(Context context, AttributeSet attrs) {

        super(context, attrs);
        mSelectionCircleRadius = getResources().getDimensionPixelSize(R.dimen.selection_circle_radius);
        mSelectionDeltaYHandCircle = getResources().getDimensionPixelSize(R.dimen.selection_dy_hand_circle);
        mCirclePaint = new Paint();
        mCirclePaint.setColor(getResources().getColor(R.color.black));
        mCirclePaint.setAntiAlias(true);
        mCirclePaint.setStyle(Paint.Style.STROKE);
        mCirclePaint.setStrokeWidth(1);

        // TODO Auto-generated constructor stub
        //mAlphaChannelPaint.setAlpha(100);
    }

    public void setForegroundMaskImage(Bitmap bmp,int transparency){
        mForegroundBitmap = bmp;
        mAlphaChannelPaint.setAlpha(transparency);
        mAlphaChannelPaint.setXfermode(new PorterDuffXfermode(
                PorterDuff.Mode.SRC_ATOP));
        invalidate();
    }

    @Override
    public void resetPath() {
        // TODO Auto-generated method stub
        super.resetPath();
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub
        super.onDraw(canvas);
        if(mForegroundBitmap != null){
            canvas.drawBitmap(mForegroundBitmap, 0, 0, mAlphaChannelPaint);
        }
        if (mSelectionCircleX != -1)
            canvas.drawCircle(mSelectionCircleX, mSelectionCircleY, mSelectionCircleRadius, mCirclePaint);
    }

    public void drawSelectionCircle(SingleTouchListener.TouchTransformInfo touchTransformInfo) {
        mSelectionCircleX = (int)touchTransformInfo.currentPoint.x;
        mSelectionCircleY = (int)touchTransformInfo.currentPoint.y - mSelectionDeltaYHandCircle;
        invalidate();
    }
}
